package com.beetle.goubuli.api;
import com.beetle.goubuli.api.body.Call;
import com.beetle.goubuli.api.body.PostAuthRefreshToken;
import com.beetle.goubuli.api.body.PostAuthToken;
import com.beetle.goubuli.api.body.PostQRCode;
import com.beetle.goubuli.api.types.*;
import com.google.gson.JsonObject;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;
import rx.Observable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tsung on 10/10/14.
 */
public interface IMHttp {

//    @GET("/verify_code")
//    Observable<Code> getVerifyCode(@Query("zone") String zone, @Query("number") String number);

//    @POST("/auth/token")
//    Observable<Token> postAuthToken(@Body PostAuthToken code);

//    @POST("/auth/refrrefresh_tokenesh_token")
//    Observable<Token> postAuthRefreshToken(@Body PostAuthRefreshToken refreshToken);

//    @POST("/member/login_organization")
//    Observable<Member> loginOrganization(@Body OrganizationID id);

//    @GET("/member/organizations")
//    Observable<JsonObject> getOrganizations();

//    @GET("/contact/sync")
//    Observable<JsonObject> syncContacts(@Query("sync_key") long key);

    @POST("/qrcode/scan")
    Observable<Object> postQRCode(@Body PostQRCode qrcode);

    @Multipart
    @POST("/avatars")
    Observable<JsonObject> postAvatars(@Part("file") TypedFile file);

    @POST("/calls")
    Observable<Object> postCall(@Body Call call);

    @POST("/calls/hangup")
    Observable<Object> hangupCall(@Body Call call);

    @POST("/conversations")
    Observable<Object> postConversation(@Body HashMap<String, Object> obj);

    @Multipart
    @POST("/crashes")
    Observable<Object> postCrash(@Part("file") TypedFile file);

    // 登陆
    @POST("/bpdm/rhtxUserLogin")
    Observable<Object> rhtxUserLogin(@Body UserLogin obj);

    // 联系人
    @POST("/bpdm/RhtxContacts")
    Observable<JsonObject> getContacts(@Body PostToken obj);

    // 会议列表
    @POST("/bpdm/RhtxMeetingRoom")
    Observable<JsonObject> getMeets(@Body PostModel obj);

    // 会议密码
    @POST("/bpdm/RhtxCheckMeetingRoom")
    Observable<JsonObject> checkMeet(@Body CheckMeet obj);

    // 头像上传
    @Multipart
    @POST("/bpdm/RhtxUploadImage")
    Observable<JsonObject> upImg(@QueryMap Map<String , String> json,
                                 @Part("file") MultipartBody.Part file);

    // 获取群组列表
    @POST("/bpdm/RtnGroup")
    Observable<Object> getRtnGroup(@Body RtnGroup rtnGroup);

    // 设置默认群组
    @POST("/bpdm/RhtxUserDefaultGroup")
    Observable<JsonObject> getRhtxUserDefaultGroup(@Body RtnDefaultGroup rtnDefaultGroup);


    // 获取版本更新
    @POST("/bpdm/RhtxAppVersion")
    Observable<Version> getLatestVersion(@Body RtnVersion rtnVersion);


}
