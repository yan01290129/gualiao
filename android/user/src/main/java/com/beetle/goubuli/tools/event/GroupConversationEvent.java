package com.beetle.goubuli.tools.event;

public class GroupConversationEvent {
    public long groupId;
    public String name;

    public GroupConversationEvent(long groupId, String name) {
        this.groupId = groupId;
        this.name = name;
    }
}
