package com.beetle.goubuli.api.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tsung on 10/10/14.
 */
public class OrganizationID {
    @SerializedName("org_id")
    public long orgID;

    @SerializedName("device_id")
    public String deviceID;

    @SerializedName("device_name")
    public String deviceName;

    public int platform;

}
