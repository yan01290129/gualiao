package com.beetle.goubuli.api.types;

import com.google.gson.annotations.SerializedName;

public class UserLogin {

    public String username;
    public String method;
    public String token;
    public String loginName;
    public String device_id;
    @SerializedName("device_name")
    public String device_name;
    public int platform;

}
