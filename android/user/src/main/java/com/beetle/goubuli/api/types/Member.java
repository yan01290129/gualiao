package com.beetle.goubuli.api.types;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tsung on 10/10/14.
 */
public class Member {
    @SerializedName("username")
    public String name;
    public String avatar;

    @SerializedName("origin_avatar")
    public String originAvatar;

    public long id;
    @SerializedName("gobelieve_token")
    public String gobelieveToken;

    public int timestamp;

}
