package com.beetle.goubuli.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import com.beetle.goubuli.util.Log;
import static com.beetle.goubuli.model.ContactManager.COLUMN_AVATAR;

/**
 * Created by houxh on 2017/7/22.
 */

public class DepartmentManager {

    private final static String TAG = "MoMoContactsManager";

    private final static String DEPARTMENT_TABLE_NAME = "department";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PARENT_ID = "parent_id";
    private static DepartmentManager instance = null;

    private DepartmentManager() {
    }

    public static DepartmentManager getInstance() {
        if (null == instance) {
            instance = new DepartmentManager();
        }
        return instance;
    }

    /**
     * 得到联系人的映射值
     *
     * @param dept
     * @return
     */
    private ContentValues convertContactToContentValues(Department dept) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, dept.id);
        values.put(COLUMN_NAME, dept.name);
        values.put(COLUMN_PARENT_ID, dept.parentID);
        return values;
    }

    // 批量增加部门
    public boolean addDepartments(List<Department> departments) {
        if (null == departments || departments.size() < 1)
            return false;
        boolean result = true;
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();
            for (Department dept : departments) {
                ContentValues values = convertContactToContentValues(dept);
                long row = mDB.insert(DEPARTMENT_TABLE_NAME, null, values);
                values.clear();
            }
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            mDB.endTransaction();
        }
        return result;
    }

    // id删除部门
    public boolean deleteDepartment(long id) {

        SQLiteDatabase mDB = null;
        try {
            mDB = ContactDatabaseHelper.getInstance();
            mDB.beginTransaction();

            mDB.delete(DEPARTMENT_TABLE_NAME, "id=?",
                    new String[] {
                            String.valueOf(id)
                    });

            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;
    }

    // 批量删除
    public boolean deleteDepartments(List<Long> departmentIDs) {
        if (null == departmentIDs || departmentIDs.size() < 1)
            return false;

        SQLiteDatabase mDB = null;
        try {
            mDB = ContactDatabaseHelper.getInstance();
            mDB.beginTransaction();
            for (Long id : departmentIDs) {
                mDB.delete(DEPARTMENT_TABLE_NAME, "id=?",
                        new String[] {
                                String.valueOf(id)
                        });
            }
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;
    }

    /**
     * 删除所有部门
     *
     * @return 删除結果
     */
    public boolean deleteAllDepartment() {
        SQLiteDatabase mDB = null;
        try {
            mDB = ContactDatabaseHelper.getInstance();
            mDB.beginTransaction();
            mDB.delete(DEPARTMENT_TABLE_NAME, null, null);
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;
    }

    // 批量修改
    public boolean updateDepartments(List<Department> departments) {
        if (null == departments || departments.size() == 0)
            return false;
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();

        boolean result = false;
        try {
            mDB.beginTransaction();
            for (Department dept : departments) {
                mDB.update(DEPARTMENT_TABLE_NAME,
                        convertContactToContentValues(dept), "id=?",
                        new String[] {
                                String.valueOf(dept.id)
                        });
            }
            mDB.setTransactionSuccessful();
            result = true;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return result;
    }

    // 查询所有部门
    public List<Department> getAllDepartments() {
        List<Department> deptList = new ArrayList<>();
        String sql = "select * from department; ";
        Cursor cursor = null;
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB = ContactDatabaseHelper.getInstance();
            cursor = mDB.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(COLUMN_ID));
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                String parentID = cursor.getString(cursor.getColumnIndex(COLUMN_PARENT_ID));
                Department department = new Department();
                department.id = id;
                department.name = name;
                department.parentID = parentID;
                deptList.add(department);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return deptList;
    }

    // id查询部门
    public Department getDepartment(String id) {

        SQLiteDatabase mDB = null;
        String sql = "select * from department where id ='" + id+"'";
        Cursor cursor = null;
        Department department = new Department();
        try {
            mDB = ContactDatabaseHelper.getInstance();
            mDB.beginTransaction();
            cursor = mDB.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                department.id = cursor.getString(cursor.getColumnIndex(COLUMN_ID));
                department.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                department.parentID = cursor.getString(cursor.getColumnIndex(COLUMN_PARENT_ID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return department;
    }

    // 指定 parentID 查询部门
    public List<Department> getDepartments(String parentID) {
        List<Department> deptList = new ArrayList<>();
        String sql = "select * from department where parent_id ='" + parentID+"'";
        Cursor cursor = null;
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB = ContactDatabaseHelper.getInstance();
            cursor = mDB.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                Department department = new Department();
                department.id = cursor.getString(cursor.getColumnIndex(COLUMN_ID));
                department.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                department.parentID = cursor.getString(cursor.getColumnIndex(COLUMN_PARENT_ID));
                deptList.add(department);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return deptList;
    }
}
