
package com.beetle.goubuli.model;

public class SQLCreator {
    public final static String CONTACT = "CREATE TABLE IF NOT EXISTS \"contact\" "
            + "(\"id\" VARCHAR(255) PRIMARY KEY NOT NULL , "
            + "\"name\" VARCHAR(32), "
            + "\"avatar\" VARCHAR(255), "
            + "\"origin_avatar\" VARCHAR(255), "
            + "\"mobile\" VARCHAR(255), "
            + "\"email\" VARCHAR(255), "
            + "\"title\" VARCHAR(255), "
            + "\"gender\" INTEGER DEFAULT 0, "
            + "\"dept_id\" VARCHAR(255), "
            + "\"user_no\" VARCHAR(255) ) ;";


    //public final static String CONTACT_ID_IDX = "CREATE INDEX [contact_id_idx] On [contact] ( [contact_id] );";

    public final static String DEPARTMENT = "CREATE TABLE IF NOT EXISTS \"department\" "
            + "(\"id\" VARCHAR(255) PRIMARY KEY NOT NULL , "
            + "\"name\" VARCHAR(32), "
            + "\"parent_id\" VARCHAR(255) ) ;";

    public final static String MEET = "CREATE TABLE IF NOT EXISTS \"meet\" "
            + "(\"fldtxnum\" VARCHAR(255) PRIMARY KEY NOT NULL , "
            + "\"fldroomname\" VARCHAR(255), "
            + "\"fldonlinenum\" VARCHAR(255), "
            + "\"flddepartid\" VARCHAR(255), "
            + "\"flddepart\" VARCHAR(255) ) ;";


    public final static String GROUP = "CREATE TABLE IF NOT EXISTS `group` "
            + "(\"id\" INTEGER PRIMARY KEY NOT NULL , "
            + "\"name\" VARCHAR(255), "
            + "\"master\" INTEGER DEFAULT 0, "
            + "\"notice\" VARCHAR(255), "
            + "\"do_not_disturb\" INTEGER DEFAULT 0, "
            + "\"leaved\" INTEGER DEFAULT 0, "
            + "\"disbanded\" INTEGER DEFAULT 0, "
            + "\"timestamp\" INTEGER DEFAULT 0, "
            + "\"f_Txnum\" INTEGER DEFAULT 0, "
            + "\"read_only\" INTEGER DEFAULT 0, "
            + "\"defaultgroupid\" INTEGER DEFAULT 0) ";


    public final static String GROUP_MEMBER = "CREATE TABLE IF NOT EXISTS \"group_member\" "
            + "(\"group_id\" INTEGER NOT NULL , "
            + "\"member_id\" INTEGER NOT NULL, "
            + "\"nickname\" VARCHAR(255) ) ";

    public final static String GROUP_ID_IDX = "CREATE UNIQUE INDEX [group_id_idx] On [group_member] ( [group_id], [member_id] );";

    //cid:peer_uid|group_id|store_id
    public final static String CONVERSATION = "CREATE TABLE IF NOT EXISTS \"conversation\" "
            + "(\"id\" INTEGER PRIMARY KEY NOT NULL , "
            + "\"cid\" INTEGER NOT NULL, "
            + "\"type\" INTEGER NOT NULL, "
            + "\"name\" VARCHAR(255), "
            + "\"state\" INTEGER DEFAULT 0, "
            + "\"unread\" INTEGER DEFAULT 0) ";


    public final static String CONVERSATION_IDX = "CREATE UNIQUE INDEX [cid_type_idx] On [conversation] ( [cid], [type] );";


    public final static String PEER_MESSAGE = "CREATE TABLE \"peer_message\" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `peer` INTEGER NOT NULL, `secret` INTEGER DEFAULT 0, `sender` INTEGER NOT NULL, `receiver` INTEGER NOT NULL, `timestamp` INTEGER NOT NULL, `flags` INTEGER NOT NULL, `content` TEXT, `uuid` TEXT );";

    public final static String GROUP_MESSAGE = "CREATE TABLE \"group_message\" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `sender` INTEGER NOT NULL, `group_id` INTEGER NOT NULL, `timestamp` INTEGER NOT NULL, `flags` INTEGER NOT NULL, `content` TEXT, `uuid` TEXT );";

    public final static String CUSTOMER_MESSAGE = "CREATE TABLE \"customer_message\" ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `customer_id` INTEGER NOT NULL, `customer_appid` INTEGER NOT NULL, `store_id` INTEGER NOT NULL, `seller_id` INTEGER NOT NULL, `timestamp` INTEGER NOT NULL, `flags` INTEGER NOT NULL, `is_support` INTEGER NOT NULL, `content` TEXT, `uuid` TEXT );";

    public final static String PEER_MESSAGE_FTS = "CREATE VIRTUAL TABLE peer_message_fts USING fts4(content TEXT);";

    public final static String GROUP_MESSAGE_FTS = "CREATE VIRTUAL TABLE group_message_fts USING fts4(content TEXT);";

    public final static String CUSTOMER_MESSAGE_FTS = "CREATE VIRTUAL TABLE customer_message_fts USING fts4(content TEXT);";

    public final static String PEER_MESSAGE_IDX = "CREATE INDEX `peer_index` ON `peer_message` (`peer`, `secret`, `id`);";
    public final static String PEER_MESSAGE_UUID_IDX = "CREATE INDEX `peer_uuid_index` ON `peer_message` (`uuid`)";
    public final static String GROUP_MESSAGE_UUID_IDX = "CREATE INDEX `group_uuid_index` ON `group_message` (`uuid`)";
    public final static String CUSTOMER_MESSAGE_UUID_IDX = "CREATE INDEX `customer_uuid_index` ON `customer_message` (`uuid`)";

    //"delete from peer_message where id not in (SELECT min(id) from peer_message WHERE uuid is not null group by uuid) and uuid is not null"
}
