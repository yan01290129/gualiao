
package com.beetle.goubuli.model;

import java.util.ArrayList;
import java.util.List;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.beetle.goubuli.util.Log;
import com.beetle.goubuli.util.PinyinHelper;


public class ContactManager {
    private final static String TAG = "ContactManager";

    private final static String CONTACT_TABLE_NAME = "contact";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_AVATAR = "avatar";
    public static final String COLUMN_ORIGIN_AVATAR = "origin_avatar";
    public static final String COLUMN_MOBILE = "mobile";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DEPARTMENT_ID = "dept_id";
    public static final String COLUMN_USERNO = "user_no";


    private SQLiteDatabase mDB;

    private static ContactManager instance = null;

    private ContactManager() {
    }

    public static ContactManager getInstance() {
        if (null == instance) {
            instance = new ContactManager();
        }
        return instance;
    }

    // 指定 deptID 查询联系人
    public List<Contact> getUser(String deptID) {
        List<Contact> contactList = new ArrayList<>();
        String sql = "select * from contact where dept_id ='" + deptID+"'";
        Cursor cursor = null;
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB = ContactDatabaseHelper.getInstance();
            cursor = mDB.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                long contactId = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                String avatar = cursor.getString(cursor.getColumnIndex(COLUMN_AVATAR));
                String mobile = cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE));
                Contact contact = new Contact();
                contact.setContactId(contactId);
                contact.setName(name);
                contact.setAvatar(avatar);
                contact.setMobile(mobile);
                contact.setPinYin(PinyinHelper.convertChineseToPinyin(name));
                contact.setNamePinyin(PinyinHelper.convertChineseToPinyinArray(name));
                contactList.add(contact);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return contactList;
    }

    // 查询所有联系人
    public List<Contact> getAllContact() {
        return getAllDisplayContactsList();
    }
    public List<Contact> getAllDisplayContactsList() {
        List<Contact> contactList = new ArrayList<>();
        String sql = "select * from contact; ";
        Cursor cursor = null;
        try {
            mDB = ContactDatabaseHelper.getInstance();
            cursor = mDB.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                long contactId = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
                String avatar = cursor.getString(cursor.getColumnIndex(COLUMN_AVATAR));
                String mobile = cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE));
                String userno = cursor.getString(cursor.getColumnIndex(COLUMN_USERNO));
                String deptId = cursor.getString(cursor.getColumnIndex(COLUMN_DEPARTMENT_ID));

                Contact contact = new Contact();
                contact.setContactId(contactId);
                contact.setName(name);
                contact.setAvatar(avatar);
                contact.setMobile(mobile);
                contact.setPinYin(PinyinHelper.convertChineseToPinyin(name));
                contact.setNamePinyin(PinyinHelper.convertChineseToPinyinArray(name));
                contact.setUserno(userno);
                contact.setDeptID(deptId);
                contactList.add(contact);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return contactList;
    }



    /**
     * 根据id更新联系人图像
     *
     * @param contact
     * @return
     */
    public boolean upContact(Contact contact) {
        if (null == contact)
            return false;
        mDB = ContactDatabaseHelper.getInstance();
        mDB.beginTransaction();
        boolean result = false;
        long contactId = contact.getContactId();
        if (contactId < 1)
            return false;
        try {
            ContentValues values=new ContentValues();
            values.put(COLUMN_AVATAR, contact.getAvatar());
            int a = mDB.update(CONTACT_TABLE_NAME,values,"id=?",new String[] {
                    String.valueOf(contactId)
            });
            mDB.setTransactionSuccessful();
            result = true;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return result;
    }




    /**
     * @return 限定条件之后的联系人集合
     */
    public List<Contact> getAllContactsIdList() {
        String sql = "select id from contact ; ";
        Cursor cursor = null;
        List<Contact> contactList = new ArrayList<Contact>();
        try {
            mDB = ContactDatabaseHelper.getInstance();
            cursor = mDB.rawQuery(sql, null);
            if (null == cursor)
                return contactList;
            while (cursor.moveToNext()) {
                long contactId = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_ID));

                Contact contact = new Contact(contactId);
                contactList.add(contact);
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return contactList;
    }

    /**
     * @param userno
     * @retur 对应的联系人
     */
    public Contact getContactByIdUserno(String userno) {
        if (userno == null)
            return null;
        String sql = "select * from contact "
                + " where contact.user_no = " + userno + " ; ";
        return getContactBySql(sql);
    }



    /**
     * @param contactId
     * @retur 对应的联系人
     */
    public Contact getContactById(long contactId) {
        if (contactId < 0)
            return null;
        String sql = "select * from contact "
                + " where contact.id = " + contactId + " ; ";
        return getContactBySql(sql);
    }


    /**
     * get specified contact with sql clause
     * 
     * @param sql
     * @return
     */
    private Contact getContactBySql(String sql) {
        Contact contact = null;
        Cursor cursor = null;
        try {
            mDB = ContactDatabaseHelper.getInstance();
            cursor = mDB.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                contact = retrieveContactFromCursor(cursor);
                break;
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return contact;
    }
    



    /**
     * @param cursor
     * @return 从游标中取出联系人
     */
    private Contact retrieveContactFromCursor(Cursor cursor) {
        if (null == cursor)
            return null;
        long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));

        String name = cursor.getString(cursor
                .getColumnIndex(COLUMN_NAME));

        String avatar = cursor.getString(cursor
                .getColumnIndex(COLUMN_AVATAR));

        String originAvatar = cursor.getString(cursor
                .getColumnIndex(COLUMN_ORIGIN_AVATAR));

        String title = cursor.getString(cursor
                .getColumnIndex(COLUMN_TITLE));

        String mobile = cursor.getString(cursor
                .getColumnIndex(COLUMN_MOBILE));

        String email = cursor.getString(cursor
                .getColumnIndex(COLUMN_EMAIL));

        String userno = cursor.getString(cursor
                .getColumnIndex(COLUMN_USERNO));

        String deptID = cursor.getString(cursor.getColumnIndex(COLUMN_DEPARTMENT_ID));

        Contact contact = new Contact(id);
        contact.setName(name);
        contact.setAvatar(avatar);
        contact.setOriginAvatar(originAvatar);
        contact.setMobile(mobile);
        contact.setEmail(email);
        contact.setDeptID(deptID);
        contact.setPinYin(PinyinHelper.convertChineseToPinyin(name));
        contact.setNamePinyin(PinyinHelper.convertChineseToPinyinArray(name));
        contact.setUserno(userno);
        return contact;
    }





    /**
     * 根据联系人id删除该联系人
     * 
     * @param momoContactId
     * @return
     */
    public boolean delContact(long momoContactId) {
        if (momoContactId < 1)
            return false;
        try {
            mDB = ContactDatabaseHelper.getInstance();
            mDB.beginTransaction();
            mDB.delete(CONTACT_TABLE_NAME, "id=?",
                    new String[] {
                        String.valueOf(momoContactId)
                    });
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;
    }

    /**
     * 删除所有联系人
     * 
     * @return 删除結果
     */
    public boolean deleteAllContacts() {
        try {
            mDB = ContactDatabaseHelper.getInstance();
            mDB.beginTransaction();
            mDB.delete(CONTACT_TABLE_NAME, null, null);
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;

    }

    /**
     * 批量删除联系人
     * 
     * @param contactsList
     * @return 删除結果
     */
    public boolean batchDeleteContact(List<Contact> contactsList) {
        if (null == contactsList || contactsList.size() < 1)
            return false;
        try {
            mDB = ContactDatabaseHelper.getInstance();
            mDB.beginTransaction();
            for (Contact contact : contactsList) {
                long momoContactId = contact.getContactId();
                mDB.delete(CONTACT_TABLE_NAME, "id=?",
                        new String[] {
                            String.valueOf(momoContactId)
                        });
            }
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;
    }

    /**
     * 批量删除联系人
     * 
     * @param contactIdList
     * @return 删除結果
     */
    public boolean batchDeleteContactByIdList(List<Long> contactIdList) {
        if (null == contactIdList || contactIdList.size() < 1)
            return false;
        try {
            mDB = ContactDatabaseHelper.getInstance();
            mDB.beginTransaction();
            for (Long momoContactId : contactIdList) {
                mDB.delete(CONTACT_TABLE_NAME, "id=?",
                        new String[] {
                            String.valueOf(momoContactId)
                        });
            }
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;
    }

    /**
     * 批量增加联系人
     * 
     * @param contactList
     */
    public boolean batchAddContacts(List<Contact> contactList) {
        if (null == contactList || contactList.size() < 1)
            return false;
        boolean result = true;
        mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();
            for (Contact contact : contactList) {
                String name = contact.getName();
                if (TextUtils.isEmpty(name))
                    continue;
                ContentValues values = convertContactToContentValues(contact);
                Long row = mDB.insert(CONTACT_TABLE_NAME, null, values);
                values.clear();
            }
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            mDB.endTransaction();
        }
        return result;
    }
    



    /**
     * 批量更新联系人
     * 
     * @param contactList
     * @return
     */
    public boolean updateContacts(List<Contact> contactList) {
        if (null == contactList)
            return false;
        mDB = ContactDatabaseHelper.getInstance();
        mDB.beginTransaction();
        boolean result = false;
        try {
            for (Contact contact : contactList) {
                long contactId = contact.getContactId();
                if (contactId < 1)
                    continue;
                mDB.update(CONTACT_TABLE_NAME,
                        convertContactToContentValues(contact), "id=?",
                        new String[] {
                            String.valueOf(contactId)
                        });
            }
            mDB.setTransactionSuccessful();
            result = true;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return result;
    }



    /**
     * 得到联系人的映射值
     * 
     * @param contact
     * @return
     */
    private ContentValues convertContactToContentValues(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, contact.getContactId());
        values.put(COLUMN_NAME, contact.getName());
        values.put(COLUMN_AVATAR, contact.getAvatar());
        values.put(COLUMN_ORIGIN_AVATAR, contact.getOriginAvatar());
        values.put(COLUMN_MOBILE, contact.getMobile());
        values.put(COLUMN_TITLE, contact.getTitle());
        values.put(COLUMN_DEPARTMENT_ID, contact.getDeptID());
        values.put(COLUMN_USERNO, contact.getUserno());

        return values;
    }




}
