package com.beetle.goubuli.tools.event;

public class ApplicationEvent {
    public boolean isBackground;
    public ApplicationEvent(boolean isBackground) {
        this.isBackground = isBackground;
    }
}
