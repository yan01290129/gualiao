package com.beetle.goubuli.api.types;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tsung on 10/10/14.
 */
public class Token {

    @SerializedName("access_token")
    public String accessToken;
    @SerializedName("refresh_token")
    public String refreshToken;
    @SerializedName("expires_in")
    public int expireTimestamp;

    @Expose
    public long uid;

    @Expose
    public ArrayList<Organization> organizations;
}
