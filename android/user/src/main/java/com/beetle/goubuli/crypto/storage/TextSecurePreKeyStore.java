package com.beetle.goubuli.crypto.storage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.whispersystems.libsignal.InvalidKeyIdException;
import org.whispersystems.libsignal.InvalidMessageException;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.libsignal.state.SignedPreKeyStore;
import org.whispersystems.libsignal.state.PreKeyRecord;
import org.whispersystems.libsignal.state.PreKeyStore;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.LinkedList;
import java.util.List;

public class TextSecurePreKeyStore implements PreKeyStore, SignedPreKeyStore {


    public  static final String PREKEY_DIRECTORY        = "prekeys";
    public  static final String SIGNED_PREKEY_DIRECTORY = "signed_prekeys";


    private static final int    PLAINTEXT_VERSION      = 2;
    private static final int    CURRENT_VERSION_MARKER = 2;
    private static final Object FILE_LOCK              = new Object();
    private static final String TAG                    = TextSecurePreKeyStore.class.getSimpleName();

    @NonNull  private final Context      context;


    public TextSecurePreKeyStore(@NonNull Context context) {
        this.context      = context;

    }

    @Override
    public PreKeyRecord loadPreKey(int preKeyId) throws InvalidKeyIdException {
        synchronized (FILE_LOCK) {
            try {
                return new PreKeyRecord(loadSerializedRecord(getPreKeyFile(preKeyId)));
            } catch (IOException | InvalidMessageException e) {
                Log.w(TAG, e);
                throw new InvalidKeyIdException(e);
            }
        }
    }

    @Override
    public SignedPreKeyRecord loadSignedPreKey(int signedPreKeyId) throws InvalidKeyIdException {
        synchronized (FILE_LOCK) {
            try {
                return new SignedPreKeyRecord(loadSerializedRecord(getSignedPreKeyFile(signedPreKeyId)));
            } catch (IOException | InvalidMessageException e) {
                Log.w(TAG, e);
                throw new InvalidKeyIdException(e);
            }
        }
    }

    @Override
    public List<SignedPreKeyRecord> loadSignedPreKeys() {
        synchronized (FILE_LOCK) {
            File                     directory = getSignedPreKeyDirectory();
            List<SignedPreKeyRecord> results   = new LinkedList<>();

            for (File signedPreKeyFile : directory.listFiles()) {
                try {
                    if (!"index.dat".equals(signedPreKeyFile.getName())) {
                        results.add(new SignedPreKeyRecord(loadSerializedRecord(signedPreKeyFile)));
                    }
                } catch (IOException | InvalidMessageException e) {
                    Log.w(TAG, signedPreKeyFile.getAbsolutePath(), e);
                }
            }

            return results;
        }
    }

    @Override
    public void storePreKey(int preKeyId, PreKeyRecord record) {
        synchronized (FILE_LOCK) {
            try {
                storeSerializedRecord(getPreKeyFile(preKeyId), record.serialize());
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }

    @Override
    public void storeSignedPreKey(int signedPreKeyId, SignedPreKeyRecord record) {
        synchronized (FILE_LOCK) {
            try {
                storeSerializedRecord(getSignedPreKeyFile(signedPreKeyId), record.serialize());
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }

    @Override
    public boolean containsPreKey(int preKeyId) {
        File record = getPreKeyFile(preKeyId);
        return record.exists();
    }

    @Override
    public boolean containsSignedPreKey(int signedPreKeyId) {
        File record = getSignedPreKeyFile(signedPreKeyId);
        return record.exists();
    }


    @Override
    public void removePreKey(int preKeyId) {
        File record = getPreKeyFile(preKeyId);
        record.delete();
    }

    @Override
    public void removeSignedPreKey(int signedPreKeyId) {
        File record = getSignedPreKeyFile(signedPreKeyId);
        record.delete();
    }


    public void migrateRecords() {
        synchronized (FILE_LOCK) {
            File preKeyRecords = getPreKeyDirectory();

            for (File preKeyRecord : preKeyRecords.listFiles()) {
                try {
                    int          preKeyId = Integer.parseInt(preKeyRecord.getName());
                    PreKeyRecord record   = loadPreKey(preKeyId);

                    storePreKey(preKeyId, record);
                } catch (InvalidKeyIdException | NumberFormatException e) {
                    Log.w(TAG, e);
                }
            }

            File signedPreKeyRecords = getSignedPreKeyDirectory();

            for (File signedPreKeyRecord : signedPreKeyRecords.listFiles()) {
                try {
                    int                signedPreKeyId = Integer.parseInt(signedPreKeyRecord.getName());
                    SignedPreKeyRecord record         = loadSignedPreKey(signedPreKeyId);

                    storeSignedPreKey(signedPreKeyId, record);
                } catch (InvalidKeyIdException | NumberFormatException e) {
                    Log.w(TAG, e);
                }
            }
        }
    }

    private byte[] loadSerializedRecord(File recordFile)
            throws IOException, InvalidMessageException
    {
        FileInputStream fin           = new FileInputStream(recordFile);
        int             recordVersion = readInteger(fin);

        if (recordVersion > CURRENT_VERSION_MARKER) {
            throw new AssertionError("Invalid version: " + recordVersion);
        }

        byte[] serializedRecord = readBlob(fin);


        fin.close();
        return serializedRecord;
    }

    private void storeSerializedRecord(File file, byte[] serialized) throws IOException {
        RandomAccessFile recordFile = new RandomAccessFile(file, "rw");
        FileChannel      out        = recordFile.getChannel();

        out.position(0);
        writeInteger(CURRENT_VERSION_MARKER, out);
        writeBlob(serialized, out);
        out.truncate(out.position());
        recordFile.close();
    }

    private File getPreKeyFile(int preKeyId) {
        return new File(getPreKeyDirectory(), String.valueOf(preKeyId));
    }

    private File getSignedPreKeyFile(int signedPreKeyId) {
        return new File(getSignedPreKeyDirectory(), String.valueOf(signedPreKeyId));
    }

    private File getPreKeyDirectory() {
        return getRecordsDirectory(PREKEY_DIRECTORY);
    }


    private File getSignedPreKeyDirectory() {
        return getRecordsDirectory(SIGNED_PREKEY_DIRECTORY);
    }

    private File getRecordsDirectory(String directoryName) {
        File directory = new File(context.getFilesDir(), directoryName);

        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                Log.w(TAG, "PreKey directory creation failed!");
            }
        }

        return directory;
    }

    private byte[] readBlob(FileInputStream in) throws IOException {
        int length       = readInteger(in);
        byte[] blobBytes = new byte[length];

        in.read(blobBytes, 0, blobBytes.length);
        return blobBytes;
    }

    private void writeBlob(byte[] blobBytes, FileChannel out) throws IOException {
        writeInteger(blobBytes.length, out);
        out.write(ByteBuffer.wrap(blobBytes));
    }

    private int readInteger(FileInputStream in) throws IOException {
        byte[] integer = new byte[4];
        in.read(integer, 0, integer.length);
        return byteArrayToInt(integer);
    }

    private void writeInteger(int value, FileChannel out) throws IOException {
        byte[] valueBytes = intToByteArray(value);
        out.write(ByteBuffer.wrap(valueBytes));
    }


    public static int byteArrayToInt(byte[] bytes) {
        return byteArrayToInt(bytes, 0);
    }

    public static int byteArrayToInt(byte[] bytes, int offset)  {
        return
                (bytes[offset]     & 0xff) << 24 |
                        (bytes[offset + 1] & 0xff) << 16 |
                        (bytes[offset + 2] & 0xff) << 8  |
                        (bytes[offset + 3] & 0xff);
    }

    public static byte[] intToByteArray(int value) {
        byte[] bytes = new byte[4];
        intToByteArray(bytes, 0, value);
        return bytes;
    }

    public static int intToByteArray(byte[] bytes, int offset, int value) {
        bytes[offset + 3] = (byte)value;
        bytes[offset + 2] = (byte)(value >> 8);
        bytes[offset + 1] = (byte)(value >> 16);
        bytes[offset]     = (byte)(value >> 24);
        return 4;
    }

}
