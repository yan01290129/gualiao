package com.beetle.goubuli.api.types;

import com.google.gson.annotations.SerializedName;

/**
 * Created by houxh on 2017/7/22.
 */

public class Organization {
    public Long id;
    @SerializedName("user_id")
    public Long userID;

    public String name;
}
