package com.beetle.goubuli.model;

/**
 * Created by houxh on 2016/11/5.
 */


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by houxh on 2016/10/28.
 */

public class Profile {
    private static Profile instance;
    public static Profile getInstance() {
        if (instance == null) {
            instance = new Profile();
        }
        return instance;
    }

    public long uid;
    public String name;
    public String avatar;
    public String originAvatar;
    public long contactSyncKey;
    public int loginTimestamp;
    public String organizationName;
    public long organizationID;
    public boolean keepalive;//后台运行
    public String deptName;

    public void save(Context context) {
        SharedPreferences pref = context.getSharedPreferences("profile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putLong("uid", this.uid);
        editor.putString("name", (this.name != null ? this.name : ""));
        editor.putString("avatar", this.avatar != null ? this.avatar : "");
        editor.putString("origin_avatar", this.originAvatar != null ? this.originAvatar : "");
        editor.putLong("contact_sync_key", this.contactSyncKey);
        editor.putInt("login_timestamp", this.loginTimestamp);

        editor.putString("user_organization_name", organizationName != null ? organizationName : "");
        editor.putLong("user_organization_id", organizationID);
        editor.putInt("keepalive", this.keepalive ? 1 : 0);

        editor.commit();
    }

    public void load(Context context) {
        SharedPreferences customer = context.getSharedPreferences("profile", Context.MODE_PRIVATE);

        this.uid = customer.getLong("uid", 0);
        this.name = customer.getString("name", "");
        this.avatar = customer.getString("avatar", "");
        this.originAvatar = customer.getString("origin_avatar", "");
        this.contactSyncKey = customer.getLong("contact_sync_key", 0);
        this.loginTimestamp = customer.getInt("login_timestamp", 0);

        this.organizationID = customer.getLong("user_organization_id", 0);
        this.organizationName = customer.getString("user_organization_name", "");
        this.keepalive = customer.getInt("keepalive", 0) == 1;
    }

    public void clear(Context context) {
        this.uid = 0;
        this.name = null;
        this.avatar = null;
        this.originAvatar = null;
        this.contactSyncKey = 0;
        this.loginTimestamp = 0;
        this.organizationID = 0;
        this.organizationName = null;

        SharedPreferences pref = context.getSharedPreferences("profile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }
}

