package com.beetle.goubuli;

import com.beetle.goubuli.model.Contact;

import java.util.List;

public interface UserApplication {
    void setContacts(List<Contact> contacts);
    List<Contact> getContacts();
}
