package com.beetle.goubuli;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import com.reactnativenavigation.controllers.NavigationCommandsHandler;

/**
 * Created by tsung on 12/10/14.
 */
public class BaseActivity extends AppCompatActivity {
    protected ActionBar actionBar;

    private static long id = 0;

    protected String navigatorID;
    protected String screenInstanceID;
    protected String navigatorEventID;


    public static long generateID() {
        return ++id;
    }

    public static String generateNavigatorID() {
        return "_navigatorID" + generateID();
    }

    public static String generateScreenInstanceID() {
        return "_screenInstanceID" + generateID();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        navigatorID = intent.getStringExtra("navigatorID");
        screenInstanceID = intent.getStringExtra("screenInstanceID");
        navigatorEventID = intent.getStringExtra("navigatorEventID");

        if (!TextUtils.isEmpty(navigatorID)) {
            NavigationCommandsHandler.registerNavigationActivity(this, navigatorID);
        }

        if (!TextUtils.isEmpty(screenInstanceID)) {
            NavigationCommandsHandler.registerActivity(this, screenInstanceID);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (!TextUtils.isEmpty(navigatorID)) {
            NavigationCommandsHandler.unregisterNavigationActivity(this, navigatorID);
        }

        if (!TextUtils.isEmpty(screenInstanceID)) {
            NavigationCommandsHandler.unregisterActivity(screenInstanceID);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (canBack()) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            } else {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            actionBar.show();
        }
    }

    public boolean canBack() {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (canBack()) {
                    onBackPressed();
                    return true;
                }
        }
        return false;
    }
}
