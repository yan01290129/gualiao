
package com.beetle.goubuli.model;


public class Contact {
    public final static String TAG = "ContactTAG";

    private long id;
    private String name;
    private String avatar;
    private String originAvatar;
    private String mobile;
    private String email;
    private String title;
    private String deptID;
    private String pinYin;//name's pin yin
    private String userno;
    private String fphone;

    private String[][] namePinYin;

    private int weight;

    public Contact() {

    }

    public Contact(long contactId) {
        this.id = contactId;
    }

    public Contact(long contactId,
            String name, String mobile, String email,
            String title, String deptID) {

        super();

        this.id = contactId;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.title = title;
        this.deptID = deptID;
    }

    public void setUserno(String userno){
        this.userno = userno;
    }

    public String getUserno(){
        return userno;
    }


    public long getContactId() {
        return id;
    }

    public void setContactId(long contactId) {
        this.id = contactId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {return this.avatar;}

    public void setOriginAvatar(String url) {
        originAvatar = url;
    }

    public String getOriginAvatar() {
        return this.originAvatar;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }
    public String getDeptID() {
        return this.deptID;
    }

    public void setPinYin(String pinYin) {
        this.pinYin = pinYin;
    }

    public String getPinYin() {
        return this.pinYin;
    }

    public void setNamePinyin(String[][] namePinYin) {
        this.namePinYin = namePinYin;
    }
    public String[][] getNamePinyin() {
        return this.namePinYin;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    public int getWeight() {
        return this.weight;
    }
}
