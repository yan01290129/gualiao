package com.beetle.goubuli.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by houxh on 15/3/21.
 */
public class GroupDB {
    private final static String TAG = "goubuli";

    private final static String GROUP_TABLE_NAME = "`group`";
    private final static String GROUP_MEMBER_TABLE_NAME = "group_member";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_MASTER = "master";
    public static final String COLUMN_TIMESTAMP = "timestamp";
    public static final String COLUMN_DISBANDED = "disbanded";
    public static final String COLUMN_LEAVED = "leaved";
    public static final String COLUMN_NOTICE = "notice";
    public static final String COLUMN_DONOTDISTURB = "do_not_disturb";
    public static final String COLUMN_FTXNUM = "f_Txnum";
    public static final String COLUMN_READONLY = "read_only";


    public static final String COLUMN_GROUP_ID = "group_id";
    public static final String COLUMN_MEMBER_ID = "member_id";
    public static final String COLUMN_MEMBER_NICKNAME = "nickname";
    public static final String COLUMN_DEFAULTGROUPID = "defaultgroupid";


    private static GroupDB instance = new GroupDB();
    public static GroupDB getInstance() {
        return instance;
    }

    public List<Group> loadGroups() {
        ArrayList<Group> groups = new ArrayList<>();

        String sql = "select id, name, master, timestamp, disbanded, leaved, notice, do_not_disturb, f_Txnum, read_only, defaultgroupid from `group` ";

        Cursor cursor = null;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            String[] args = new String[]{};
            cursor = mDB.rawQuery(sql, args);
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                String name = cursor.getString(cursor
                        .getColumnIndex(COLUMN_NAME));
                long master = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_MASTER));
                long ts = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_TIMESTAMP));
                String notice = cursor.getString(
                        cursor.getColumnIndex(COLUMN_NOTICE));
                long doNotDisturb = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_DONOTDISTURB));
                String fTxnum = cursor.getString(cursor
                        .getColumnIndex(COLUMN_FTXNUM));
                long readonly = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_READONLY));
                long defaultgroupid = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_DEFAULTGROUPID));

                Group group = new Group();
                group.groupID = id;
                group.topic = name;
                group.master = master;
                group.timestamp = (int)ts;
                group.notice = notice;
                group.doNotDisturb = doNotDisturb == 1;
                group.fTxnum = fTxnum;
                group.readonly = readonly == 1;
                group.defaultgroupid = defaultgroupid;

                groups.add(group);
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return groups;
    }

    public String getGroupfTxnum(long fTxnum) {
        String sql = "select name from `group` "
                + " where f_Txnum = ?;";

        String name = "";
        Cursor cursor = null;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            String[] args = new String[]{ String.valueOf(fTxnum) };
            cursor = mDB.rawQuery(sql, args);
            if (cursor.moveToNext()) {
                name = cursor.getString(cursor
                        .getColumnIndex(COLUMN_NAME));
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return name;
    }


    public boolean addGroupMember(long groupID, long uid)  {

        boolean result = true;
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(COLUMN_GROUP_ID, groupID);
            values.put(COLUMN_MEMBER_ID, uid);
            mDB.insert(GROUP_MEMBER_TABLE_NAME, null, values);
            values.clear();
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            mDB.endTransaction();
        }
        return result;

    }

    public boolean removeGroupMember(long groupID, long uid) {

        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();

            mDB.delete(GROUP_MEMBER_TABLE_NAME, "group_id=? AND member_id=?",
                    new String[] {
                            String.valueOf(groupID),
                            String.valueOf(uid)
                    });
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }

        return true;
    }

    public boolean updateMemberNickname(long groupID, long memberID, String nickname) {
        boolean result;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            ContentValues values = new ContentValues();
            values.put(COLUMN_MEMBER_NICKNAME, nickname);

            int r = mDB.update(GROUP_MEMBER_TABLE_NAME,
                    values, "group_id=? AND member_id=?",
                    new String[] {
                            String.valueOf(groupID),
                            String.valueOf(memberID)
                    });

            result = r > 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        }
        return result;
    }

    // 群组入库
    public boolean addGroup(Group group) {
        boolean result = true;
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();

            ContentValues values = new ContentValues();
            values.put(COLUMN_ID, group.groupID);
            values.put(COLUMN_NAME, group.topic);
            values.put(COLUMN_MASTER, group.master);
            values.put(COLUMN_TIMESTAMP, group.timestamp);
            values.put(COLUMN_NOTICE, group.notice);
            values.put(COLUMN_DONOTDISTURB, group.doNotDisturb ? 1 : 0);
            values.put(COLUMN_FTXNUM, group.fTxnum);
            values.put(COLUMN_READONLY, group.readonly ? 1 : 0);
            values.put(COLUMN_DEFAULTGROUPID, group.defaultgroupid);
            mDB.insert(GROUP_TABLE_NAME, null, values);
            values.clear();

            for (GroupMember member : group.getGroupMembers()) {
                values = new ContentValues();
                values.put(COLUMN_GROUP_ID, group.groupID);
                values.put(COLUMN_MEMBER_ID, member.uid);
                values.put(COLUMN_MEMBER_NICKNAME, member.nickname);
                mDB.insert(GROUP_MEMBER_TABLE_NAME, null, values);
                values.clear();
            }

            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            mDB.endTransaction();
        }
        return result;
    }

    public boolean removeGroup(long groupID) {
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();
            mDB.delete(GROUP_TABLE_NAME, "id=?",
                    new String[] {
                            String.valueOf(groupID)
                    });
            mDB.delete(GROUP_MEMBER_TABLE_NAME, "group_id=?",
                    new String[] {
                            String.valueOf(groupID)
                    });
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }

        return true;
    }

    public ArrayList<GroupMember> loadGroupMember(long groupID) {

        String sql = "select group_id, member_id, nickname from `group_member` "
                + " where group_id = ?;";

        ArrayList<GroupMember> ids = new ArrayList<>();
        Cursor cursor = null;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            String[] args = new String[]{ String.valueOf(groupID) };
            cursor = mDB.rawQuery(sql, args);
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(COLUMN_MEMBER_ID));
                String nickname = cursor.getString(cursor.getColumnIndex(COLUMN_MEMBER_NICKNAME));
                GroupMember member = new GroupMember();
                member.uid = id;
                member.nickname = nickname;
                ids.add(member);
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return ids;

    }
    public Group loadGroup(long groupID) {
        String sql = "select * from `group` "
                + " where id = ?;";

        Group group = null;
        Cursor cursor = null;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            String[] args = new String[]{ String.valueOf(groupID) };
            cursor = mDB.rawQuery(sql, args);
            if (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                String name = cursor.getString(cursor
                        .getColumnIndex(COLUMN_NAME));
                long master = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_MASTER));
                long ts = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_TIMESTAMP));
                String notice = cursor.getString(
                        cursor.getColumnIndex(COLUMN_NOTICE));
                long doNotDisturb = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_DONOTDISTURB));
                String fTxnum = cursor.getString(cursor
                        .getColumnIndex(COLUMN_FTXNUM));
                long readonly = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_READONLY));


                group = new Group();
                group.groupID = id;
                group.topic = name;
                group.master = master;
                group.timestamp = (int)ts;
                group.notice = notice;
                group.doNotDisturb = doNotDisturb == 1;
                group.fTxnum = fTxnum;
                group.readonly = readonly == 1;
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return group;
    }

    //退出群的标志
    public boolean leaveGroup(long groupID) {
        boolean result;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            ContentValues values = new ContentValues();
            values.put(COLUMN_LEAVED, 1);

            int r = mDB.update(GROUP_TABLE_NAME,
                    values, "id=?",
                    new String[] {
                            String.valueOf(groupID)
                    });

            result = r > 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        }
        return result;
    }

    //清空退出群的标志
    public boolean joinGroup(long groupID) {
        boolean result;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            ContentValues values = new ContentValues();
            values.put(COLUMN_LEAVED, 0);

            int r = mDB.update(GROUP_TABLE_NAME,
                    values, "id=?",
                    new String[] {
                            String.valueOf(groupID)
                    });

            result = r > 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        }
        return result;
    }

    public boolean isLeaved(long groupID) {
        String sql = "select leaved from `group` "
                + " where id = ?;";

        boolean leaved = false;
        Cursor cursor = null;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            String[] args = new String[]{ String.valueOf(groupID) };
            cursor = mDB.rawQuery(sql, args);
            if (cursor.moveToNext()) {
                long l = cursor.getLong(cursor
                        .getColumnIndex(COLUMN_LEAVED));
                leaved = (l == 1);
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return leaved;
    }

    public boolean disbandGroup(long groupID) {
        boolean result;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            ContentValues values = new ContentValues();
            values.put(COLUMN_DISBANDED, 1);

            int r = mDB.update(GROUP_TABLE_NAME,
                    values, "id=?",
                    new String[] {
                            String.valueOf(groupID)
                    });

            result = r > 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        }
        return result;

    }

    public String getGroupTopic(long groupID) {
        String sql = "select name from `group` "
                + " where id = ?;";

        String name = "";
        Cursor cursor = null;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            String[] args = new String[]{ String.valueOf(groupID) };
            cursor = mDB.rawQuery(sql, args);
            if (cursor.moveToNext()) {
                name = cursor.getString(cursor
                        .getColumnIndex(COLUMN_NAME));
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return name;
    }

    public boolean setGroupTopic(long groupID, String name) {
        boolean result;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME, name);

            int r = mDB.update(GROUP_TABLE_NAME,
                    values, "id=?",
                    new String[] {
                            String.valueOf(groupID)
                    });

            result = r > 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        }
        return result;
    }


    public boolean setGroupNotice(long groupID, String notice) {
        boolean result;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            ContentValues values = new ContentValues();
            values.put(COLUMN_NOTICE, notice);

            int r = mDB.update(GROUP_TABLE_NAME,
                    values, "id=?",
                    new String[] {
                            String.valueOf(groupID)
                    });

            result = r > 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        }
        return result;
    }

    public boolean setGroupDoNotDisturb(long groupID, boolean doNotDisturb) {
        boolean result;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            ContentValues values = new ContentValues();
            values.put(COLUMN_DONOTDISTURB, doNotDisturb ? 1 : 0);

            int r = mDB.update(GROUP_TABLE_NAME,
                    values, "id=?",
                    new String[] {
                            String.valueOf(groupID)
                    });

            result = r > 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = false;
        }
        return result;
    }


    public boolean clear() {
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();
            mDB.delete(GROUP_TABLE_NAME, null, null);
            mDB.delete(GROUP_MEMBER_TABLE_NAME, null, null);
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;
    }

}
