package com.beetle.goubuli.tools.event;

import com.beetle.goubuli.model.Contact;

import java.util.List;

/**
 * Created by houxh on 16/1/22.
 */
public class SyncCompleteEvent {
    public List<Contact> contacts;
    public SyncCompleteEvent(List<Contact> contacts) {
        this.contacts = contacts;
    }

}
