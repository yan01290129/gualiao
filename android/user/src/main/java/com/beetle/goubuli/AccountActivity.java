package com.beetle.goubuli;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.LoginSuccessEvent;
import com.squareup.otto.Subscribe;

/**
 * Created by tsung on 12/2/14.
 */
public class AccountActivity  extends AppCompatActivity{
    final Object messageHandler = new Object() {
        @Subscribe
        public void onLoginSuccess(LoginSuccessEvent event) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(messageHandler);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(messageHandler);
    }
}
