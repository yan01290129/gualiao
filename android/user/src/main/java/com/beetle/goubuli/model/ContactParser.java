
package com.beetle.goubuli.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactParser {
    private static final String KEY_ID = "user_id";
    private static final String KEY_NAME = "name";
    private static final String KEY_AVATAR = "avatar";
    private static final String KEY_ORIGIN_AVATAR = "origin_avatar";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE = "mobile";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DEPARTMENT_ID = "dept_id";

    public Contact parseContact(JSONObject json) throws JSONException {
        Contact contact = new Contact();
        contact.setContactId(json.getLong(KEY_ID));
        contact.setName(json.getString(KEY_NAME));
        contact.setAvatar(json.getString(KEY_AVATAR));
        contact.setOriginAvatar(json.getString(KEY_ORIGIN_AVATAR));
        contact.setMobile(json.getString(KEY_MOBILE));
        contact.setEmail(json.getString(KEY_EMAIL));
        contact.setTitle("");
        contact.setDeptID(json.getString(KEY_DEPARTMENT_ID));
        return contact;
    }



}
