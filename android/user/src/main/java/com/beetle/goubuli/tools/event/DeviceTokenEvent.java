package com.beetle.goubuli.tools.event;

/**
 * Created by houxh on 2017/11/13.
 */

public class DeviceTokenEvent {
    public String deviceToken;
    public String hwDeviceToken;
    public DeviceTokenEvent(String deviceToken, String hwDeviceToken) {
        this.deviceToken = deviceToken;
        this.hwDeviceToken = hwDeviceToken;
    }

    public DeviceTokenEvent(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
