package com.beetle.goubuli;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.types.UserLogin;
import com.beetle.goubuli.model.Profile;
import com.beetle.goubuli.model.Token;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.LoginSuccessEvent;
import com.beetle.goubuli.util.ParseMD5;
import com.beetle.user.R;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by houxh on 14-8-11.
 */
public class LoginActivity extends AccountActivity implements TextView.OnEditorActionListener {
    private final String TAG = "beetle";
    protected ActionBar actionBar;

    EditText phoneText;
    EditText passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // 透明状态栏（任务栏）--5.0及以上系统才支持
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            View decorView = getWindow().getDecorView();
            //两个Flag必须要结合在一起使用，表示会让应用的主体内容占用系统状态栏的空间
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            //将状态栏设置成透明色
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        phoneText = (EditText)findViewById(R.id.user_phone);
        passwordText = (EditText)findViewById(R.id.user_password);
        //---------------------------设置引入图片的尺寸---------------------------------------------
        Drawable username_drawable = getResources().getDrawable(R.drawable.img_phone);
        Drawable password_drawable = getResources().getDrawable(R.drawable.img_password);
        //四个参数分别是设置图片的左、上、右、下的尺寸
        username_drawable.setBounds(10,0,60,60);
        password_drawable.setBounds(10,0,60,60);
        //这个是选择将图片绘制在EditText的位置，参数对应的是：左、上、右、下
        phoneText.setCompoundDrawables(username_drawable,null,null,null);
        passwordText.setCompoundDrawables(password_drawable,null,null,null);

    }

    public UserLogin gettakenjosn(String phone) {
        String apiStr = "rhtxUserLogin";
        String username = "user101";
        String password = "123";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String time = sdf.format(date);
        String token = ParseMD5.parseStrToMd5L32(username + password + "rhtxUserLogin" + sdf.format(date));
        UserLogin p = new UserLogin();
        p.username = username;
        p.method = apiStr;
        p.token = token;
        p.loginName = phone;
        int PLATFORM_ANDROID = 2;
        String androidID = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        String deviceName = String.format("%s-%s", android.os.Build.BRAND, android.os.Build.MODEL);
        p.device_id = androidID;
        p.device_name = deviceName;
        p.platform = PLATFORM_ANDROID;
        return p;
    }

    public void Login(View sender) {
        Log.i(TAG, "get getLoginsy");
        final String phone = phoneText.getText().toString();
        final String password = passwordText.getText().toString();
        if (password.length() < 6) {
            Toast.makeText(getApplicationContext(), "密码长度不正确", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!password.equals("123456")) {
            Toast.makeText(getApplicationContext(), "密码不正确", Toast.LENGTH_SHORT).show();
            return;
        }
        final ProgressDialog dialog = ProgressDialog.show(this, null, "正在登陆...");
        IMHttp imHttp = IMHttpFactory.Singleton();
        imHttp.rhtxUserLogin(this.gettakenjosn(phone))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object obj) {
                        dialog.dismiss();
                        Gson g = new Gson();
                        JsonObject jobj = g.toJsonTree(obj).getAsJsonObject();
                        JsonObject data = jobj.getAsJsonObject("data");
                        Log.i(TAG, "登陆成功");
                        // 设置
                        Profile profile = Profile.getInstance();
                        profile.uid = data.get("uid").getAsLong();
                        profile.keepalive = true;
                        profile.save(LoginActivity.this);
                        // 用户
                        Token token = Token.getInstance();
                        token.gobelieveToken = data.get("token").getAsString();
                        token.uid = data.get("uid").getAsLong();
                        token.save(LoginActivity.this);
                        BusProvider.getInstance().post(new LoginSuccessEvent());

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.i(TAG, "request code fail");
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "登陆失败", Toast.LENGTH_SHORT).show();
                    }
                });
    }

//    private void selectCompany(ArrayList<Organization> organizations) {
//        if (organizations.size() == 0) {
//            Toast.makeText(getApplicationContext(), "您的号码无法登陆,请联系系统管理员", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//
//        final List<Organization> companyArray = new ArrayList<>();
//        final List<String> companies = new ArrayList<>();
//
//        for (Organization org : organizations) {
//            companies.add(org.name);
//            companyArray.add(org);
//        }
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        DialogInterface.OnClickListener l = new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                Organization org = companyArray.get(which);
//                Log.i(TAG, "company:" + org.name + " " + org.id + " " + org.userID);
//                login(org.id, org.name);
//            }
//        };
//
//        DialogInterface.OnClickListener l2 = new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                Log.i(TAG, "click:" + which);
//            }
//        };
//
//
//        builder.setTitle(" 公司名称")
//                .setSingleChoiceItems(companies.toArray(new String[0]), 0, l)
//                .setNegativeButton("取消", l2).show();
//    }

//    private void login(final long orgID, final String orgName) {
//        final ProgressDialog dialog = ProgressDialog.show(this, null, "登录中...");
//
//        int PLATFORM_ANDROID = 2;
//        String androidID = Settings.Secure.getString(this.getContentResolver(),
//                Settings.Secure.ANDROID_ID);
//        String deviceName = String.format("%s-%s", android.os.Build.BRAND, android.os.Build.MODEL);
//        OrganizationID id = new OrganizationID();
//        id.orgID = orgID;
//        id.deviceID = androidID;
//        id.deviceName = deviceName;
//        id.platform = PLATFORM_ANDROID;
//
//
//        IMHttp imHttp = IMHttpFactory.Singleton();
//        imHttp.loginOrganization(id)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<com.beetle.goubuli.api.types.Member>() {
//                    @Override
//                    public void call(com.beetle.goubuli.api.types.Member member) {
//                        dialog.dismiss();
//                        Log.i(TAG, "login success");
//                        Token t = Token.getInstance();
//                        t.uid = member.id;
//                        t.gobelieveToken = member.gobelieveToken;
//                        t.save(LoginActivity.this);
//
//                        Profile p = Profile.getInstance();
//                        p.name = member.name;
//                        p.uid = member.id;
//                        p.avatar = member.avatar;
//                        p.originAvatar = member.originAvatar;
//                        p.organizationID = orgID;
//                        p.organizationName = orgName;
//                        p.loginTimestamp = member.timestamp;
//
//                        if (RomUtils.isMiuiRom()) {
//                            //xiaomi支持系统级推送
//                            p.keepalive = false;
//                        } else {
//                            p.keepalive = true;
//                        }
//                        p.save(LoginActivity.this);
//
//                        BusProvider.getInstance().post(new LoginSuccessEvent());
//                    }
//                }, new Action1<Throwable>() {
//                    @Override
//                    public void call(Throwable throwable) {
//                        Log.i(TAG, "login organization fail:" + throwable);
//                        dialog.dismiss();
//                        Toast.makeText(getApplicationContext(), "登录失败", Toast.LENGTH_SHORT).show();
//                    }
//                });
//    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//        if (i == EditorInfo.IME_ACTION_NEXT) {
////            getVerifyCode(null);
//            getLoginsy(null);
//        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (canBack()) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            } else {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            actionBar.show();
        }
    }

    public boolean canBack() {
        return false;
    }
}
