package com.beetle.goubuli.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by houxh on 14-8-11.
 */
public class Token {
    private static Token instance;
    public static Token getInstance() {
        if (instance == null) {
            instance = new Token();
        }
        return instance;
    }

    public String accessToken;
    public String refreshToken;
    public int expireTimestamp;

    public String gobelieveToken;

    public long uid;


    public void save(Context context) {
        SharedPreferences pref = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();


        editor.putLong("user_id", uid);
        editor.putString("user_access_token", accessToken);
        editor.putString("user_refresh_token", refreshToken);
        editor.putLong("user_token_expire", expireTimestamp);
        editor.putString("user_gobelieve_token", gobelieveToken);

        editor.commit();
    }


    public void load(Context context) {
        SharedPreferences customer = context.getSharedPreferences("token", Context.MODE_PRIVATE);

        this.uid = customer.getLong("user_id", 0);
        this.accessToken = customer.getString("user_access_token", "");
        this.refreshToken = customer.getString("user_refresh_token", "");
        this.expireTimestamp = (int)customer.getLong("user_token_expire", 0);
        this.gobelieveToken = customer.getString("user_gobelieve_token", "");
    }


    public void clear(Context context) {
        this.uid = 0;
        this.accessToken = null;
        this.refreshToken = null;
        this.expireTimestamp = 0;

        SharedPreferences pref = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }


}
