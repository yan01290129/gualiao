package com.beetle.goubuli.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.beetle.goubuli.api.types.Meet;

import java.util.ArrayList;
import java.util.List;

public class MeetDB {

    private final static String TAG = "goubuli";

    private final static String MEET_TABLE_NAME = "`meet`";

    public static final String COLUMN_FLDEOOM = "fldroomname";
    public static final String COLUMN_FLDTX = "fldtxnum";
    public static final String COLUMN_FLDONLINE = "fldonlinenum";
    public static final String COLUMN_FLDDEPTID = "flddepartid";
    public static final String COLUMN_FLDDEPT = "flddepart";

    private static MeetDB instance = new MeetDB();
    public static MeetDB getInstance() {
        return instance;
    }

    // 查询所有会议
    public List<Meet> loadGroups() {
        ArrayList<Meet> meets = new ArrayList<>();

        String sql = "select * from `meet` ";

        Cursor cursor = null;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            String[] args = new String[]{};
            cursor = mDB.rawQuery(sql, args);
            while (cursor.moveToNext()) {
                String fldroomname = cursor.getString(cursor
                        .getColumnIndex(COLUMN_FLDEOOM));
                String fldtxnum = cursor.getString(cursor
                        .getColumnIndex(COLUMN_FLDTX));
                String fldonlinenum = cursor.getString(cursor
                        .getColumnIndex(COLUMN_FLDONLINE));
                String flddepartid = cursor.getString(cursor
                        .getColumnIndex(COLUMN_FLDDEPTID));
                String flddepart = cursor.getString(cursor
                        .getColumnIndex(COLUMN_FLDDEPT));
                Meet meet = new Meet();
                meet.fldroomname = fldroomname;
                meet.fldtxnum = fldtxnum;
                meet.fldonlinenum = fldonlinenum;
                meet.flddepartid = flddepartid;
                meet.flddepart = flddepart;
                meets.add(meet);
            }
        } catch (Exception ex) {
            Log.e("", ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return meets;
    }

    //通过当前的会议室名称获取到对应的短号
    public String getMeetfldtxnum(String fldroomname){

        String sql = "select fldtxnum from `Meet` "
                + " where fldroomname = ?;";


        String fldtxnum = "";
        Cursor cursor = null;
        try {
            SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
            String[] args = new String[]{fldroomname};
            cursor = mDB.rawQuery(sql, args);
            if (cursor.moveToNext()) {
                fldtxnum = cursor.getString(cursor
                        .getColumnIndex(COLUMN_FLDTX));
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return fldtxnum;
    }

    // 添加会议
    public boolean addMeet(Meet meet){
        boolean result = true;
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(COLUMN_FLDEOOM, meet.fldroomname);
            values.put(COLUMN_FLDTX, meet.fldtxnum);
            values.put(COLUMN_FLDONLINE, meet.fldonlinenum);
            values.put(COLUMN_FLDDEPTID, meet.flddepartid);
            values.put(COLUMN_FLDDEPT, meet.flddepart);
            long a = mDB.insert(MEET_TABLE_NAME, null, values);
            values.clear();
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            mDB.endTransaction();
        }
        return result;
    }

    // 清空会议
    public boolean clear() {
        SQLiteDatabase mDB = ContactDatabaseHelper.getInstance();
        try {
            mDB.beginTransaction();
            mDB.delete(MEET_TABLE_NAME, null, null);
            mDB.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
            return false;
        } finally {
            if (mDB.inTransaction()) {
                mDB.endTransaction();
            }
        }
        return true;
    }
}
