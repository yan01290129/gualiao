package com.beetle.goubuli.api;

import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.beetle.goubuli.Config;
import com.beetle.goubuli.model.Token;
import com.google.gson.Gson;

import java.util.Date;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by tsung on 10/10/14.
 */
class IMHttpRetrofit {
    final IMHttp service;

    IMHttpRetrofit() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Config.URL_API)
                .setConverter(new GsonConverter(new Gson()))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
//                        String accessToken = Token.getInstance().accessToken;
//                        String refreshToken = Token.getInstance().refreshToken;
//                        int exp = Token.getInstance().expireTimestamp;
//
//                        if (!TextUtils.isEmpty(accessToken)) {
//                            long now = getNow();
//                            if (now < exp - 60) {
//                                //access token 未过期
//                                request.addHeader("Authorization", "Bearer " + accessToken);
//                            } else if (!TextUtils.isEmpty(refreshToken)){
//                                request.addHeader("Refresh-Token", refreshToken);
//                            }
//                        }
                    }
                })
                .build();

        service = adapter.create(IMHttp.class);
    }

    public IMHttp getService() {
        return service;
    }

    private int getNow() {
        Date date = new Date();
        return (int)(date.getTime()/1000);
    }
}
