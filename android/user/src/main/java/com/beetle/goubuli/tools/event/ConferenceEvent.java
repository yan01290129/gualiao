package com.beetle.goubuli.tools.event;

/**
 * Created by houxh on 2017/11/14.
 */

public class ConferenceEvent {
    public ConferenceEvent(long initiator, long groupID, boolean finished) {
        this.initiator = initiator;
        this.groupID = groupID;
        this.finished = finished;
    }

    public long initiator;
    public long groupID;
    public boolean finished;
}
