package com.beetle.goubuli;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import com.beetle.goubuli.model.Profile;
import com.beetle.goubuli.model.Token;
import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.body.PostAuthToken;

import com.beetle.goubuli.api.types.Organization;
import com.beetle.goubuli.api.types.OrganizationID;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.LoginSuccessEvent;

import com.beetle.goubuli.util.RomUtils;
import com.beetle.user.R;


public class VerifyActivity extends AccountActivity implements TextView.OnEditorActionListener {
    static final String TAG = VerifyActivity.class.getSimpleName();
    static String EXTRA_PHONE = "im.phone";

    protected ActionBar actionBar;

    public static Intent newIntent(Context context, String phone) {
        Intent intent = new Intent();
        intent.setClass(context, VerifyActivity.class);
        intent.putExtra(EXTRA_PHONE, phone);
        return intent;
    }

    String phone;
    EditText verifyCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        verifyCode = (EditText) findViewById(R.id.verify_code);

        phone = getIntent().getStringExtra(EXTRA_PHONE);
        verifyCode.setOnEditorActionListener(this);
    }

    public static int getNow() {
        Date date = new Date();
        long t = date.getTime();
        return (int)(t/1000);
    }

    public void onLogin(View sender) {
        final String code = verifyCode.getText().toString();
        if (phone.length() == 0 || code.length() == 0) {
            return;
        }

        final ProgressDialog dialog = ProgressDialog.show(this, null, "正在校验...");

        PostAuthToken postAuthToken = new PostAuthToken();
        postAuthToken.code = code;
        postAuthToken.zone = "86";
        postAuthToken.number = phone;
        IMHttp imHttp = IMHttpFactory.Singleton();
//        imHttp.postAuthToken(postAuthToken)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<com.beetle.goubuli.api.types.Token>() {
//                    @Override
//                    public void call(com.beetle.goubuli.api.types.Token token) {
//                        dialog.dismiss();
//
//                        Token t = Token.getInstance();
//                        t.accessToken = token.accessToken;
//                        t.refreshToken = token.refreshToken;
//                        t.expireTimestamp = token.expireTimestamp + getNow();
//                        t.save(VerifyActivity.this);
//                        VerifyActivity.this.selectCompany(token.organizations);
//                    }
//                }, new Action1<Throwable>() {
//                    @Override
//                    public void call(Throwable throwable) {
//                        Log.i(TAG, "auth token fail:" + throwable);
//                        dialog.dismiss();
//                        Toast.makeText(getApplicationContext(), "登录失败", Toast.LENGTH_SHORT).show();
//                    }
//                });
        Log.i(TAG, "code:" + code);
    }


    private void selectCompany(ArrayList<Organization> organizations) {
        if (organizations.size() == 0) {
            Toast.makeText(getApplicationContext(), "您的号码无法登陆,请联系系统管理员", Toast.LENGTH_SHORT).show();
            return;
        }


        final List<Organization> companyArray = new ArrayList<>();
        final List<String> companies = new ArrayList<>();

        for (Organization org : organizations) {
            companies.add(org.name);
            companyArray.add(org);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        DialogInterface.OnClickListener l = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Organization org = companyArray.get(which);
                Log.i(TAG, "company:" + org.name + " " + org.id + " " + org.userID);
                login(org.id, org.name);
            }
        };

        DialogInterface.OnClickListener l2 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Log.i(TAG, "click:" + which);
            }
        };


        builder.setTitle(" 公司名称")
                .setSingleChoiceItems(companies.toArray(new String[0]), 0, l)
                .setNegativeButton("取消", l2).show();
    }

    private void login(final long orgID, final String orgName) {
        final ProgressDialog dialog = ProgressDialog.show(this, null, "登录中...");

        int PLATFORM_ANDROID = 2;
        String androidID = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        String deviceName = String.format("%s-%s", android.os.Build.BRAND, android.os.Build.MODEL);
        OrganizationID id = new OrganizationID();
        id.orgID = orgID;
        id.deviceID = androidID;
        id.deviceName = deviceName;
        id.platform = PLATFORM_ANDROID;


        IMHttp imHttp = IMHttpFactory.Singleton();
//        imHttp.loginOrganization(id)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<com.beetle.goubuli.api.types.Member>() {
//                    @Override
//                    public void call(com.beetle.goubuli.api.types.Member member) {
//                        dialog.dismiss();
//                        Log.i(TAG, "login success");
//                        Token t = Token.getInstance();
//                        t.uid = member.id;
//                        t.gobelieveToken = member.gobelieveToken;
//                        t.save(VerifyActivity.this);
//
//                        Profile p = Profile.getInstance();
//                        p.name = member.name;
//                        p.uid = member.id;
//                        p.avatar = member.avatar;
//                        p.originAvatar = member.originAvatar;
//                        p.organizationID = orgID;
//                        p.organizationName = orgName;
//                        p.loginTimestamp = member.timestamp;
//
//                        if (RomUtils.isMiuiRom()) {
//                            //xiaomi支持系统级推送
//                            p.keepalive = false;
//                        } else {
//                            p.keepalive = true;
//                        }
//                        p.save(VerifyActivity.this);
//
//                        BusProvider.getInstance().post(new LoginSuccessEvent());
//                    }
//                }, new Action1<Throwable>() {
//                    @Override
//                    public void call(Throwable throwable) {
//                        Log.i(TAG, "login organization fail:" + throwable);
//                        dialog.dismiss();
//                        Toast.makeText(getApplicationContext(), "登录失败", Toast.LENGTH_SHORT).show();
//                    }
//                });
    }


    public static SecureRandom getSecureRandom() {
        try {
            return SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_verify, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (canBack()) {
                    onBackPressed();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_GO) {
            onLogin(null);
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (canBack()) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            } else {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
            actionBar.show();
        }
    }



    public boolean canBack() {
        return true;
    }

}
