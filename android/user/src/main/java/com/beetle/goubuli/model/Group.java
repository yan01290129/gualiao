package com.beetle.goubuli.model;

import java.util.ArrayList;

/**
 * Created by houxh on 15/3/21.
 */
public class Group {
    public long groupID;
    public long master;//管理员or创建者
    public String topic;
    public String notice;//群公告
    public boolean doNotDisturb;//免打扰
    public String nickname;//群内昵称
    public boolean disbanded;//是否解散
    public int timestamp;//创建时间
    public String avatarURL;
    public String fTxnum; // 寻呼号
    public boolean readonly; //是否任许增减人员

    //name为nil时，界面显示identifier字段
    public String identifier;
    public long defaultgroupid;




    private ArrayList<GroupMember> members = new ArrayList<GroupMember>();
    public void addMember(long uid) {
        this.addMember(uid, "");
    }

    public void addMember(long uid, String nickname) {
        int index = -1;
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).uid == uid) {
                index = i;
            }
        }

        if (index == -1) {
            GroupMember m = new GroupMember();
            m.uid = uid;
            m.nickname = nickname;
            members.add(m);
        }
    }

    public void removeMember(long uid) {
        int index = -1;
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).uid == uid) {
                index = i;
            }
        }
        if (index != -1) {
            members.remove(index);
        }
    }

    public ArrayList<Long> getMembers() {
        ArrayList<Long> uids = new ArrayList<>();
        for (int i = 0; i < members.size(); i++) {
            uids.add(members.get(i).uid);
        }
        return uids;
    }

    public ArrayList<GroupMember> getGroupMembers() {
        return this.members;
    }

    public void setGroupMembers(ArrayList<GroupMember> members) {
        this.members = members;
    }

    public void setMembers(ArrayList<Long> members) {
        ArrayList<GroupMember> array = new ArrayList<>();
        int index = -1;
        for (int i = 0; i < members.size(); i++) {
            GroupMember m = new GroupMember();
            m.uid = members.get(i);
            array.add(m);
        }
        this.members = array;
    }
}
