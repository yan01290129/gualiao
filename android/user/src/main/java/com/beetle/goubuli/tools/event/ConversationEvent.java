package com.beetle.goubuli.tools.event;

public class ConversationEvent {
    public long peer;
    public boolean secret;
    public boolean sessionConnected;
    public String name;

    public ConversationEvent(long peer, String name) {
        this.peer = peer;
        this.name = name;
        this.secret = false;
        this.sessionConnected = false;
    }

    public ConversationEvent(long peer, String name, boolean secret, boolean sessionConnected) {
        this.peer = peer;
        this.name = name;
        this.secret = secret;
        this.sessionConnected = sessionConnected;
    }
}
