package com.beetle.goubuli.crypto.database;

import android.support.annotation.NonNull;
import org.whispersystems.libsignal.SignalProtocolAddress;

public class Address extends SignalProtocolAddress {
    public Address(String name, int deviceId) {
        super(name, deviceId);
    }

    public String serialize() {
        return toString();
    }

    public static @NonNull
    Address fromSerialized(@NonNull String serialized) {
        String[] arr = serialized.split(":", 2);
        return new Address(arr[0], Integer.parseInt(arr[1]));
    }
}
