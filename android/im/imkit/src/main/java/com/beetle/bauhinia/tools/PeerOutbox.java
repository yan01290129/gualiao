package com.beetle.bauhinia.tools;

import android.util.Log;
import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.PeerMessageDB;
import com.beetle.bauhinia.db.message.*;
import com.beetle.im.IMMessage;
import com.beetle.im.IMService;
import org.whispersystems.libsignal.SessionCipher;
import org.whispersystems.libsignal.SignalProtocolAddress;
import org.whispersystems.libsignal.UntrustedIdentityException;
import org.whispersystems.libsignal.protocol.CiphertextMessage;
import org.whispersystems.libsignal.state.SignalProtocolStore;

import java.io.*;
import java.io.File;


/**
 * Created by houxh on 14-12-3.
 */
public class PeerOutbox extends Outbox {
    private static final String TAG = "goubuli";

    private static PeerOutbox instance = new PeerOutbox();
    public static PeerOutbox getInstance() {
        return instance;
    }


    private SignalProtocolStore store;
    public void setStore(SignalProtocolStore store) {
        this.store = store;
    }

    protected CiphertextMessage encrypt(String text, long peerUID) {
        try {
            return encrypt(text.getBytes("UTF-8"), peerUID);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }



    protected CiphertextMessage encrypt(byte[] data, long peerUID) {
        try {
            SignalProtocolAddress BOB_ADDRESS = new SignalProtocolAddress("" + peerUID, 1);
            if (store != null && store.containsSession(BOB_ADDRESS)) {
                SessionCipher aliceSessionCipher = new SessionCipher(store, BOB_ADDRESS);
                CiphertextMessage outgoingMessage = aliceSessionCipher.encrypt(data);
                return outgoingMessage;
            } else {
                return null;
            }

        } catch (UntrustedIdentityException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected boolean encrypt(IMMessage im, String uuid) {
        CiphertextMessage outgoingMessage = encrypt(im.content, im.receiver);

        if (outgoingMessage == null) {
            return false;
        }
        byte[] base64 = android.util.Base64.encode(outgoingMessage.serialize(), android.util.Base64.DEFAULT);
        Log.i(TAG, "encrypt len:" + outgoingMessage.serialize().length + " base64 length:" + base64.length);
        String ciphertext = new String(base64);
        Log.i(TAG, "ciphertext:" + ciphertext);
        int type = 0;
        switch (outgoingMessage.getType()) {
            case CiphertextMessage.PREKEY_TYPE:
                type = 1;
                break;
            case CiphertextMessage.WHISPER_TYPE:
                type = 2;
                break;
        }
        im.content = Secret.newSecret(ciphertext, type, uuid).getRaw();
        im.secret = true;

        return true;
    }

    @Override
    protected String encryptFile(String path, long peerUID) {
        try {
            File f = new File(path);
            long len = f.length();
            byte[] buff = new byte[(int)len];
            FileInputStream s = new FileInputStream(f);
            int r = s.read(buff);
            assert(len == r);

            s.close();

            CiphertextMessage cipherMsg = encrypt(buff, peerUID);
            if (cipherMsg == null) {
                return "";
            }

            byte[] base64 = android.util.Base64.encode(cipherMsg.serialize(), android.util.Base64.DEFAULT);
            Log.i("goubuli", "encrypt len:" + cipherMsg.serialize().length + " base64 length:" + base64.length);
            String type = "0";
            switch (cipherMsg.getType()) {
                case CiphertextMessage.PREKEY_TYPE:
                    type = "1";
                    break;
                case CiphertextMessage.WHISPER_TYPE:
                    type = "2";
                    break;
            }

            File outputFile = File.createTempFile("secret_", null);
            FileOutputStream outStream = new FileOutputStream(outputFile);
            outStream.write(type.getBytes());
            outStream.write(base64);
            outStream.close();


            return outputFile.getAbsolutePath();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void markMessageFailure(IMessage msg) {
        PeerMessageDB.getInstance().markMessageFailure(msg.msgLocalID, msg.receiver);
    }

    @Override
    protected void saveMessageAttachment(IMessage msg, String url) {
        if (PeerMessageDB.SQL_ENGINE_DB) {
            String content = "";
            if (msg.content.getType() == MessageContent.MessageType.MESSAGE_AUDIO) {
                Audio audio = (Audio)msg.content;
                content = Audio.newAudio(url, audio.duration, audio.getUUID()).getRaw();
            } else if (msg.content.getType() == MessageContent.MessageType.MESSAGE_IMAGE) {
                Image image = (Image) msg.content;
                content = Image.newImage(url, image.width, image.height, image.getUUID()).getRaw();
            } else {
                return;
            }

            PeerMessageDB.getInstance().updateContent(msg.msgLocalID, content);
        } else {
            IMessage attachment = new IMessage();
            attachment.content = Attachment.newURLAttachment(msg.msgLocalID, url);
            attachment.sender = msg.sender;
            attachment.receiver = msg.receiver;
            saveMessage(attachment);
        }
    }

    void saveMessage(IMessage imsg) {
        PeerMessageDB.getInstance().insertMessage(imsg, imsg.receiver);
    }

    @Override
    protected void sendImageMessage(IMessage imsg, String url) {
        IMMessage msg = new IMMessage();
        msg.sender = imsg.sender;
        msg.receiver = imsg.receiver;

        Image image = (Image)imsg.content;
        msg.content = Image.newImage(url, image.width, image.height, image.getUUID()).getRaw();
        msg.msgLocalID = imsg.msgLocalID;

        boolean r = true;
        if (imsg.secret) {
            r = encrypt(msg, imsg.getUUID());
        }
        if (r) {
            IMService im = IMService.getInstance();
            im.sendPeerMessage(msg);
        }

    }

    @Override
    protected void sendAudioMessage(IMessage imsg, String url) {
        Audio audio = (Audio)imsg.content;

        IMMessage msg = new IMMessage();
        msg.sender = imsg.sender;
        msg.receiver = imsg.receiver;
        msg.msgLocalID = imsg.msgLocalID;
        msg.content = Audio.newAudio(url, audio.duration, audio.getUUID()).getRaw();


        boolean r = true;
        if (imsg.secret) {
            r = encrypt(msg, imsg.getUUID());

        }
        if (r) {
            IMService im = IMService.getInstance();
            im.sendPeerMessage(msg);
        }

    }

    @Override
    protected void sendVideoMessage(IMessage imsg, String url, String thumbURL) {
        Video video = (Video)imsg.content;

        IMMessage msg = new IMMessage();
        msg.sender = imsg.sender;
        msg.receiver = imsg.receiver;
        msg.msgLocalID = imsg.msgLocalID;
        msg.content = Video.newVideo(url, thumbURL, video.width, video.height, video.duration, video.getUUID()).getRaw();


        boolean r = true;
        if (imsg.secret) {
            r = encrypt(msg, imsg.getUUID());
        }
        if (r) {
            IMService im = IMService.getInstance();
            im.sendPeerMessage(msg);
        }
    }



}
