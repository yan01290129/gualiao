package com.beetle.bauhinia.tools;
import android.os.AsyncTask;

import android.util.Base64;
import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.message.Audio;
import com.beetle.bauhinia.db.message.Image;
import com.beetle.bauhinia.db.message.MessageContent;
import com.beetle.bauhinia.db.message.Video;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import org.whispersystems.libsignal.DecryptionCallback;
import org.whispersystems.libsignal.SessionCipher;
import org.whispersystems.libsignal.SignalProtocolAddress;
import org.whispersystems.libsignal.protocol.PreKeySignalMessage;
import org.whispersystems.libsignal.protocol.SignalMessage;
import org.whispersystems.libsignal.state.SignalProtocolStore;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by houxh on 14-12-3.
 */
public class FileDownloader {
    public interface FileDownloaderObserver {
        public void onFileDownloadSuccess(IMessage msg);
        public void onFileDownloadFail(IMessage msg);
    }

    private static FileDownloader instance = new FileDownloader();
    public static FileDownloader getInstance() {
        return instance;
    }


    private SignalProtocolStore store;
    public void setStore(SignalProtocolStore store) {
        this.store = store;
    }


    ArrayList<FileDownloaderObserver> observers = new ArrayList<FileDownloaderObserver>();

    ArrayList<IMessage> messages = new ArrayList<IMessage>();

    public void addObserver(FileDownloaderObserver ob) {
        if (observers.contains(ob)) {
            return;
        }
        observers.add(ob);
    }

    public void removeObserver(FileDownloaderObserver ob) {
        observers.remove(ob);
    }

    public boolean isDownloading(IMessage msg) {
        for(IMessage m : messages) {
            if (m.sender == msg.sender &&
                m.receiver == msg.receiver &&
                m.msgLocalID == msg.msgLocalID) {
                return true;
            }
        }
        return false;
    }

    public byte[] decrypt(byte[] data, long sender) {
        try {
            String chiperText = new String(data, 1, data.length - 1, "UTF-8");
            int type = Integer.parseInt(new String(data, 0, 1));
            byte[] plainData = decrypt(chiperText, sender, type);
            return plainData;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] decrypt(String ciphertext, long sender, int type) {
        try {
            if  (type == 1) {
                PreKeySignalMessage incomingMessage = new PreKeySignalMessage(Base64.decode(ciphertext, Base64.DEFAULT));
                SignalProtocolAddress address = new SignalProtocolAddress("" + sender, 1);
                SessionCipher bobSessionCipher = new SessionCipher(store, address);
                byte[] plaintext = bobSessionCipher.decrypt(incomingMessage, new DecryptionCallback() {
                    @Override
                    public void handlePlaintext(byte[] plaintext) {

                    }
                });

                return plaintext;

            } else if (type == 2) {
                SignalMessage incomingMessage = new SignalMessage(Base64.decode(ciphertext, Base64.DEFAULT));
                SignalProtocolAddress address = new SignalProtocolAddress("" + sender, 1);
                SessionCipher bobSessionCipher = new SessionCipher(store, address);
                byte[] plaintext = bobSessionCipher.decrypt(incomingMessage, new DecryptionCallback() {
                    @Override
                    public void handlePlaintext(byte[] plaintext) {

                    }
                });

                return plaintext;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void download(final IMessage imsg) {
        if (isDownloading(imsg)) {
            return;
        }

        String msgURL = null;
        if (imsg.getType() == MessageContent.MessageType.MESSAGE_AUDIO) {
            Audio audio = (Audio) imsg.content;
            msgURL = audio.url;
        } else if (imsg.getType() == MessageContent.MessageType.MESSAGE_IMAGE) {
            Image image = (Image) imsg.content;
            msgURL = image.url;
        } else if (imsg.getType() == MessageContent.MessageType.MESSAGE_VIDEO) {
            Video video = (Video) imsg.content;
            msgURL = video.thumbnail;
        } else {
            return;
        }

        messages.add(imsg);
        final String url = msgURL;

        new AsyncTask<Void, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... urls) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder().url(url).build();
                    Response response = client.newCall(request).execute();
                    if (response.isSuccessful()) {
                        if (imsg.secret) {
                            InputStream s = response.body().byteStream();
                            byte[] data = readStream(s);
                            s.close();

                            if (data == null) {
                                return false;
                            }

                            byte[] plainData = decrypt(data, imsg.sender);
                            if (plainData == null) {
                                return false;
                            }

                            FileCache.getInstance().storeFile(url, plainData);
                        } else {
                            InputStream inputStream = response.body().byteStream();
                            FileCache.getInstance().storeFile(url, inputStream);
                            inputStream.close();
                        }
                        return true;
                    } else {
                        return false;
                    }
                } catch (IOException e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                messages.remove(imsg);
                if (result) {
                    FileDownloader.this.onDownloadSuccess(imsg);
                } else {
                    FileDownloader.this.onDownloadFail(imsg);
                }
            }
        }.execute();
    }

    public byte[] readStream(InputStream s) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            IOUtils.copy(s, outputStream);
            return outputStream.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void onDownloadSuccess(IMessage msg) {
        for (FileDownloaderObserver ob : observers) {
            ob.onFileDownloadSuccess(msg);
        }
    }

    private void onDownloadFail(IMessage msg) {
        for (FileDownloaderObserver ob : observers) {
            ob.onFileDownloadFail(msg);
        }
    }
}
