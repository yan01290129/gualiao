package com.beetle.bauhinia.db;

import android.text.TextUtils;
import android.util.Base64;
import com.beetle.bauhinia.db.EPeerMessageDB;
import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.MessageFlag;
import com.beetle.bauhinia.db.PeerMessageDB;
import com.beetle.bauhinia.db.message.MessageContent;
import com.beetle.bauhinia.db.message.Revoke;
import com.beetle.bauhinia.db.message.Secret;
import com.beetle.im.IMMessage;
import com.beetle.im.Message;

import org.json.JSONException;
import org.json.JSONObject;
import org.whispersystems.libsignal.DecryptionCallback;
import org.whispersystems.libsignal.SessionCipher;
import org.whispersystems.libsignal.SignalProtocolAddress;
import org.whispersystems.libsignal.protocol.PreKeySignalMessage;
import org.whispersystems.libsignal.protocol.SignalMessage;
import org.whispersystems.libsignal.state.SignalProtocolStore;

/**
 * Created by houxh on 14-7-22.
 */
public class PeerMessageHandler implements com.beetle.im.PeerMessageHandler {
    private static PeerMessageHandler instance = new PeerMessageHandler();

    public static PeerMessageHandler getInstance() {
        return instance;
    }

    //当前用户id
    private long uid;
    private SignalProtocolStore store;

    public void setStore(SignalProtocolStore store) {
        this.store = store;
    }

    public void setUID(long uid) {
        this.uid = uid;
    }

    private String decrypt(Secret secret, long sender) {
        try {
            String ciphertext = secret.ciphertext;
            int type = secret.type;
            if  (type == 1) {
                PreKeySignalMessage incomingMessage = new PreKeySignalMessage(Base64.decode(ciphertext, Base64.DEFAULT));
                SignalProtocolAddress address = new SignalProtocolAddress("" + sender, 1);
                SessionCipher bobSessionCipher = new SessionCipher(store, address);
                byte[] plaintext = bobSessionCipher.decrypt(incomingMessage, new DecryptionCallback() {
                    @Override
                    public void handlePlaintext(byte[] plaintext) {

                    }
                });

                String s = new String(plaintext, 0, plaintext.length, "UTF-8");
                return s;
            } else if (type == 2) {
                SignalMessage incomingMessage = new SignalMessage(Base64.decode(ciphertext, Base64.DEFAULT));
                SignalProtocolAddress address = new SignalProtocolAddress("" + sender, 1);
                SessionCipher bobSessionCipher = new SessionCipher(store, address);
                byte[] plaintext = bobSessionCipher.decrypt(incomingMessage, new DecryptionCallback() {
                    @Override
                    public void handlePlaintext(byte[] plaintext) {

                    }
                });

                String s = new String(plaintext, 0, plaintext.length, "UTF-8");
                return s;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    private String decrypt(String content, long sender) {
        try {
            JSONObject json = new JSONObject(content);
            if (json.has("secret")) {
                JSONObject secretJson = json.getJSONObject("secret");
                String ciphertext = secretJson.getString("ciphertext");
                int type = secretJson.getInt("type");
                if  (type == 1) {
                    PreKeySignalMessage incomingMessage = new PreKeySignalMessage(Base64.decode(ciphertext, Base64.DEFAULT));
                    SignalProtocolAddress address = new SignalProtocolAddress("" + sender, 1);
                    SessionCipher bobSessionCipher = new SessionCipher(store, address);
                    byte[] plaintext = bobSessionCipher.decrypt(incomingMessage, new DecryptionCallback() {
                        @Override
                        public void handlePlaintext(byte[] plaintext) {

                        }
                    });

                    String s = new String(plaintext, 0, plaintext.length, "UTF-8");
                    return s;
                } else if (type == 2) {
                    SignalMessage incomingMessage = new SignalMessage(Base64.decode(ciphertext, Base64.DEFAULT));
                    SignalProtocolAddress address = new SignalProtocolAddress("" + sender, 1);
                    SessionCipher bobSessionCipher = new SessionCipher(store, address);
                    byte[] plaintext = bobSessionCipher.decrypt(incomingMessage, new DecryptionCallback() {
                        @Override
                        public void handlePlaintext(byte[] plaintext) {

                        }
                    });

                    String s = new String(plaintext, 0, plaintext.length, "UTF-8");
                    return s;
                }
            }
        } catch (JSONException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void repaireFailureMessage(String uuid) {
        PeerMessageDB db = PeerMessageDB.getInstance();
        if (!TextUtils.isEmpty(uuid)) {
            IMessage m = db.getMessage(uuid);
            if (m == null) {
                return;
            }

            if ((m.flags & MessageFlag.MESSAGE_FLAG_FAILURE) != 0 || (m.flags & MessageFlag.MESSAGE_FLAG_ACK) == 0) {
                m.flags = m.flags & (~MessageFlag.MESSAGE_FLAG_FAILURE);
                m.flags = m.flags | MessageFlag.MESSAGE_FLAG_ACK;
                db.updateFlag(m.msgLocalID, m.flags);
            }
        }
    }

    public boolean handleMessage(IMMessage msg) {
        IMessage imsg = new IMessage();
        imsg.timestamp = msg.timestamp;
        imsg.sender = msg.sender;
        imsg.receiver = msg.receiver;
        imsg.setContent(msg.content);
        imsg.secret = false;

        msg.secret = false;
        if (imsg.getType() == MessageContent.MessageType.MESSAGE_SECRET) {
            Secret s = (Secret)imsg.content;
            String plainText = decrypt(s, msg.sender);
            if (plainText != null) {
                //解密成功
                imsg.setContent(plainText);
                msg.content = plainText;
            }
            imsg.secret = true;
            msg.secret = true;
        }

        if (this.uid == msg.sender) {
            imsg.flags = MessageFlag.MESSAGE_FLAG_ACK;
        }

        long uid = this.uid == msg.sender ? msg.receiver : msg.sender;

        if (msg.secret) {
            EPeerMessageDB db = EPeerMessageDB.getInstance();
            if (msg.isSelf) {
                assert (msg.sender == uid);
                //消息由本设备发出，则不需要重新入库，用于纠正消息标志位
                repaireFailureMessage(imsg.getUUID());
                return true;
            } else if (imsg.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                Revoke revoke = (Revoke)imsg.content;
                int msgLocalID = db.getMessageId(revoke.msgid);
                if (msgLocalID > 0) {
                    db.updateContent(msgLocalID, msg.content);
                    db.removeMessageIndex(msgLocalID, uid);
                }
                return true;
            } else {
                boolean r = db.insertMessage(imsg, uid);
                msg.msgLocalID = imsg.msgLocalID;
                return r;
            }
        } else {
            PeerMessageDB db = PeerMessageDB.getInstance();

            if (msg.isSelf) {
                assert (msg.sender == uid);
                //消息由本设备发出，则不需要重新入库，用于纠正消息标志位
                repaireFailureMessage(imsg.getUUID());
                return true;
            } else if (imsg.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                Revoke revoke = (Revoke) imsg.content;

                int msgLocalID = db.getMessageId(revoke.msgid);
                if (msgLocalID > 0) {
                    db.updateContent(msgLocalID, msg.content);
                    db.removeMessageIndex(msgLocalID, uid);
                }
                return true;
            } else {
                boolean r = db.insertMessage(imsg, uid);
                msg.msgLocalID = imsg.msgLocalID;
                return r;
            }
        }
    }

    public boolean handleMessageACK(IMMessage im) {
        long uid = im.receiver;
        int msgLocalID = im.msgLocalID;
        PeerMessageDB db = PeerMessageDB.getInstance();
        if (msgLocalID == 0) {
            MessageContent c = IMessage.fromRaw(im.plainContent);
            if (c.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                Revoke r = (Revoke)c;
                int revokedMsgId = db.getMessageId(r.msgid);
                if (revokedMsgId > 0) {
                    db.updateContent(revokedMsgId, im.plainContent);
                    db.removeMessageIndex(revokedMsgId, uid);
                }
            }
            return true;
        } else {
            return db.acknowledgeMessage(msgLocalID, uid);
        }
    }

    public boolean handleMessageFailure(IMMessage im) {
        long uid = im.receiver;
        int msgLocalID = im.msgLocalID;
        if (msgLocalID > 0) {
            PeerMessageDB db = PeerMessageDB.getInstance();
            return db.markMessageFailure(msgLocalID, uid);
        }
        return true;
    }
}
