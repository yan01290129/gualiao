package com.beetle.contact;

import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SearchContactList {

    List<Contact> filteredData = new ArrayList<>();

    public List<Contact> SearchToContact(String newText) {
        List<Contact> Allcontacts = ContactManager.getInstance().getAllDisplayContactsList();
        String text = newText.replace(" ", "");
        Iterator it = Allcontacts.iterator();
        while(it.hasNext()) {
            Contact c = (Contact) it.next();
            int a = c.getName().indexOf(text);
            int b = c.getUserno().indexOf(text);
            if(c.getName().indexOf(text) != -1 || c.getUserno().indexOf(text) != -1 ){
                filteredData.add(c);
            }
        }
        return filteredData;
    };

}
