package com.beetle.contact;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.Department;
import com.beetle.goubuli.model.DepartmentManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xudd on 2016/6/10.
 */
public class UserFragment extends ListFragment {

    private static final String KEY_USER = "dataUsers";
    private FragmentManager mFragmentManager;
    private List<Contact> contacts;
    private ListView mListContact = null; //联系人视图
    private BaseAdapter userAdapter;

    public static UserFragment getInstance(String deptID){
        UserFragment frag = new UserFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_USER, deptID);
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            String deptID = getArguments().getString(KEY_USER);
            contacts = ContactManager.getInstance().getUser(deptID);
        }
//        userAdapter = new ContactAdapter();
//        mListContact = (ListView)findViewById(R.id.list);
//        mListContact.setAdapter(userAdapter);
//        mListContact.setOnItemClickListener(this);
//        userAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /* 动态加载布局 */
        View view = inflater.inflate(R.layout.contact, null);
        userAdapter = new ContactAdapter();
        /**
         * 设置适配器。 注意：为什么不在onCreate方法中设置适配器，
         * 是因为我们使用了自定义的视图，
         * 也就是 说我们要加载完自定义的视图以后才可以设置适配器，
         * 如果在onCreate方法中就设置的话， 必然会出错
         */
        this.setListAdapter(userAdapter);
        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    }


    // 联系人适配器
    class ContactAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return contacts.size();
        }

        @Override
        public Object getItem(int position) {
            return contacts.get(position);
        };

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.contact, null);
            } else {
                view = convertView;
            }
            TextView tv = (TextView) view.findViewById(R.id.name);
            ImageView avatarView = (ImageView)view.findViewById(R.id.header);
            Contact c = contacts.get(position);
            tv.setText(c.getName());
            if (!TextUtils.isEmpty(c.getAvatar())) {
                Picasso.with(getActivity())
                        .load(c.getAvatar())
                        .placeholder(R.drawable.avatar_contact)
                        .into(avatarView);
            } else {
                avatarView.setImageResource(R.drawable.avatar_contact);
            }

            return view;
        }
    }
}
