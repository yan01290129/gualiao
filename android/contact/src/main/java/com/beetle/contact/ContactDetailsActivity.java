package com.beetle.contact;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.widget.Toast;

import com.beetle.bauhinia.MessageActivity;
import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.crypto.IdentityKeyUtil;
import com.beetle.goubuli.crypto.PreKeyUtil;
import com.beetle.goubuli.crypto.storage.SignalProtocolStoreImpl;
import com.beetle.goubuli.crypto.storage.TextSecureIdentityKeyStore;
import com.beetle.goubuli.crypto.storage.TextSecurePreKeyStore;
import com.beetle.goubuli.crypto.storage.TextSecureSessionStore;
import com.beetle.goubuli.model.*;
import com.beetle.goubuli.tools.event.BusProvider;

import com.beetle.goubuli.tools.event.ConversationEvent;
import com.beetle.goubuli.tools.event.TabEvent;

import com.beetle.goubuli.util.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.linphone.LinphoneActivity;
import org.linphone.LinphoneActivity2;
import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.InvalidKeyIdException;
import org.whispersystems.libsignal.SignalProtocolAddress;
import org.whispersystems.libsignal.state.PreKeyRecord;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.libsignal.util.KeyHelper;
import org.whispersystems.libsignal.util.Medium;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;


/**
 * 联系人信息Activity
 * 
 */
public class ContactDetailsActivity extends AppCompatActivity implements OnClickListener {
    private final String TAG = "ContactDetailsActivity";

    public static final int MSG_SAVE_CONTACT_SUCCESS = 11;

    public static final int MSG_SAVE_CONTACT_FAILED = 12;

    private LayoutInflater mInflater;

    private Contact mContact = null;

    private LinearLayout mLayoutContact = null;

    private ProgressDialog mProgress = null;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (mProgress != null && mProgress.isShowing()) {
                mProgress.dismiss();
            }
            switch (msg.what) {
                case MSG_SAVE_CONTACT_SUCCESS:
                    com.beetle.goubuli.util.Utils.displayToast("保存联系人成功", 0);
                    finish();
                    break;
                case MSG_SAVE_CONTACT_FAILED:
                    com.beetle.goubuli.util.Utils.displayToast("保存联系人失败...", 0);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.contact_details_activity);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        final Intent intent = getIntent();
        long contactID = intent.getLongExtra("contact_id", 0);
        if (contactID == 0) {
            com.beetle.goubuli.util.Utils.displayToast("联系人没有任何数据", 0);
            finish();
            return;
        }

        mContact = ContactManager.getInstance().getContactById(contactID);
        if (mContact == null) {
            com.beetle.goubuli.util.Utils.displayToast("联系人数据不合法", 0);
            finish();
            return;
        }
//        mLayoutContact = (LinearLayout)findViewById(R.id.layout_contact_details);

        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (contactID == Token.getInstance().uid) {
            findViewById(R.id.send_voice).setVisibility(View.GONE);
            findViewById(R.id.call_num).setVisibility(View.GONE);
            findViewById(R.id.send_message).setVisibility(View.GONE);
            findViewById(R.id.call_phone).setVisibility(View.GONE);
        }

        initContactView();

        findViewById(R.id.send_voice).setOnClickListener(this);
        findViewById(R.id.call_num).setOnClickListener(this);
//        findViewById(R.id.send_message).setOnClickListener(this);
        findViewById(R.id.call_phone).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.contact_details_phone_item) {
            final String mobile = v.getTag().toString();
            String[] moreType = new String[] {
                    getResources().getString(
                            R.string.txt_user_card_phone_call),
                    getResources().getString(
                            R.string.txt_user_card_phone_sms)
            };
            new AlertDialog.Builder(ContactDetailsActivity.this)
                    .setTitle(
                            getResources().getString(
                                    R.string.txt_contact_info_cell))
                    .setItems(moreType,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(
                                        DialogInterface dialog, int whichButton) {
                                    try {
                                        if (whichButton == 0) {
                                            Utils.placeCall(mobile, ContactDetailsActivity.this);
                                        } else if (whichButton == 1) {
                                            ContactDetailsActivity.this.sendMsg(mobile);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).create().show();

        }

        if (v.getId() == R.id.send_voice) {
            String userno = mContact.getUserno();
            String name = mContact.getName();
            Bundle bundle = new Bundle();
            bundle.putString("userno", userno);
            bundle.putString("name", name);
            Intent intent = new Intent();
            intent.putExtras(bundle);
            intent.setClass(this,LinphoneActivity.class);
            ContactDetailsActivity.this.startActivity(intent);
        }else if(v.getId() == R.id.call_phone){
            String phone = mContact.getMobile();
            Uri uri=Uri.parse("tel:"+ phone);
            Intent intent=new Intent(Intent.ACTION_DIAL,uri);
            startActivity(intent);
        }else if (v.getId() == R.id.call_num){
            String userno = mContact.getMobile();
//            String userno = "8707";
            String name = mContact.getName();
            Bundle bundle = new Bundle();
            bundle.putString("userno", userno);
            bundle.putString("name", name);
            Intent intent = new Intent();
            intent.putExtras(bundle);
            if(userno == null || userno.equals("") || userno.length() > 4 ){
                Toast.makeText(this,"对方没有数字电话",Toast.LENGTH_SHORT).show();
            }else {
                intent.setClass(this,LinphoneActivity.class);
                ContactDetailsActivity.this.startActivity(intent);
            }
        }

    }

    private void initContactView() {
        ImageView avatarView = (ImageView)findViewById(R.id.img_contact_avatar);

        String avatar = mContact.getAvatar();
        if (!TextUtils.isEmpty(avatar)) {
            Picasso.with(this)
                    .load(avatar)
                    .placeholder(R.drawable.avatar_contact)
                    .into(avatarView);
        }

        TextView nameView = (TextView)findViewById(R.id.txt_contact_name);
        String contactName = mContact.getName();
        nameView.setText(contactName);

        TextView phoneView = (TextView)findViewById(R.id.phone);
        String phone = mContact.getMobile();
        phoneView.setText(phone);

        // 设置部门
        TextView deptView = (TextView)findViewById(R.id.dept);
        Department department = DepartmentManager.getInstance().getDepartment(mContact.getDeptID());
        deptView.setText(department.name);




//        String label = "";
//
//
//        String mobile = mContact.getMobile();
//        label = getString(R.string.txt_user_card_phone_label);
//        View view = dynamicAddContactItem(label, mobile);
//        view.setId(R.id.contact_details_phone_item);
//        view.setTag(mobile);
//        view.setOnClickListener(this);
//
//        // 邮箱列表
//        String email = mContact.getEmail();
//        if (!TextUtils.isEmpty(email)) {
//            label = getString(R.string.txt_user_card_email_label);
//            dynamicAddContactItem(label, email);
//        }
//
//
//        int childCount = mLayoutContact.getChildCount();
//        mLayoutContact.setVisibility(View.VISIBLE);
//        if (childCount > 1) {
//            for (int i = 0; i < childCount; i++) {
//                int itemResource = R.drawable.mm_frame_set_middle;
//                if (i == 0) {
//                    itemResource = R.drawable.mm_frame_set_up;
//                } else if (i == childCount - 1) {
//                    itemResource = R.drawable.mm_frame_set_down;
//                }
//                mLayoutContact.getChildAt(i).setBackgroundDrawable(getResources().getDrawable(
//                        itemResource));
//            }
//        } else if (childCount == 1) {
//            int itemResource = R.drawable.mm_frame_set_whole;
//            mLayoutContact.getChildAt(0).setBackgroundDrawable(getResources().getDrawable(
//                    itemResource));
//        } else {
//            mLayoutContact.setVisibility(View.GONE);
//        }

    }

    private View dynamicAddContactItem(String label, String value) {
        View itemView = mInflater.inflate(R.layout.user_card_item, null);
        TextView labelView = (TextView)itemView.findViewById(R.id.txt_label);
        TextView valueView = (TextView)itemView.findViewById(R.id.txt_value);
        labelView.setText(label);
        valueView.setText(value);
//        mLayoutContact.addView(itemView);
        return itemView;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }


    /**
     * 发送短信
     *
     * @param phoneNumber
     */
    public void sendMsg(String phoneNumber) {
        if (phoneNumber.length() > 0) {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:"
                    + phoneNumber));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
    
    public void onSendMessage(View v) {
        Log.i(TAG, "on send message");

        finish();

        BusProvider.getInstance().post(new ConversationEvent(mContact.getContactId(), mContact.getName()));
        BusProvider.getInstance().post(new TabEvent());
    }

    public void onSendP2PMessage(View v) {
        Log.i(TAG, "on send p2p message");

        TextSecureSessionStore sessionStore = new TextSecureSessionStore(this);
        SignalProtocolAddress address = new SignalProtocolAddress("" + mContact.getContactId(), 1);

        if (sessionStore.containsSession(address)) {
            finish();
            BusProvider.getInstance().post(new ConversationEvent(mContact.getContactId(),mContact.getName(), true, true));
            BusProvider.getInstance().post(new TabEvent());
            return;
        }


        TextSecureIdentityKeyStore identityKeyStore = new TextSecureIdentityKeyStore(this);
        TextSecurePreKeyStore preKeyStore = new TextSecurePreKeyStore(this);
        int signedPreKeyId = PreKeyUtil.getActiveSignedPreKeyId(this);
        if (signedPreKeyId == -1) {
            Log.e(TAG, "can't get signed prekey id");
            return;
        }

        try {
            int registrationId = identityKeyStore.getLocalRegistrationId();
            IdentityKeyPair identityKeyPair = identityKeyStore.getIdentityKeyPair();
            PreKeyRecord preKey = PreKeyUtil.generatePreKey(this);
            SignedPreKeyRecord signedPreKey = preKeyStore.loadSignedPreKey(signedPreKeyId);

            HashMap<String, Object> signedPreKeyJson = new HashMap<>();
            signedPreKeyJson.put("key_id", signedPreKey.getId());
            String publicKey = new String(Base64.encode(signedPreKey.getKeyPair().getPublicKey().serialize(), Base64.DEFAULT));
            signedPreKeyJson.put("public_key", publicKey);
            String signature = new String(Base64.encode(signedPreKey.getSignature(), Base64.DEFAULT));
            signedPreKeyJson.put("signature", signature);


            HashMap<String, Object> preKeyJson = new HashMap<>();
            preKeyJson.put("key_id", preKey.getId());
            publicKey = new String(Base64.encode(preKey.getKeyPair().getPublicKey().serialize(), Base64.DEFAULT));
            preKeyJson.put("public_key", publicKey);


            HashMap<String, Object> json = new HashMap<>();
            json.put("registration_id", registrationId);
            String identityKey = new String(Base64.encode(identityKeyPair.getPublicKey().serialize(), Base64.DEFAULT));
            json.put("identity_key", identityKey);
            json.put("pre_key", preKeyJson);
            json.put("signed_pre_key", signedPreKeyJson);

            json.put("channel_id", UUID.randomUUID().toString());
            json.put("peer_uid", mContact.getContactId());

            Log.i(TAG, "jjjj:" + json.toString());

            final ProgressDialog dialog = ProgressDialog.show(this, null, "创建中...");

            IMHttp imHttp = IMHttpFactory.Singleton();
            imHttp.postConversation(json)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object obj) {
                            dialog.dismiss();
                            Log.i(TAG, "post conversation success:" + obj);
                            finish();

                            BusProvider.getInstance().post(new ConversationEvent(mContact.getContactId(), mContact.getName(),true, false));
                            BusProvider.getInstance().post(new TabEvent());
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "post conversation fail:" + throwable);
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), "创建失败", Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (InvalidKeyIdException e) {
            Log.w(TAG, e);
        }
    }

}
