package com.beetle.contact;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import com.beetle.goubuli.BaseActivity;
import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.util.ContactFilter;

public class SearchContactActivity extends BaseActivity implements AdapterView.OnItemClickListener  {

    private static final String TAG = "goubuli";


    Toolbar toolbar;
    private SearchView searchView;
    private MenuItem searchItem;

    List<Contact> contacts = new ArrayList<>();
    private BaseAdapter adapter;
    private ListView mListContact = null;

    class ContactAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return contacts.size();
        }

        @Override
        public Object getItem(int position) {
            return contacts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.contact, null);
            } else {
                view = convertView;
            }
            TextView tv = (TextView) view.findViewById(R.id.name);
            Contact c = contacts.get(position);
            tv.setText(c.getName());
            return view;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contact);

        mListContact = (ListView)findViewById(R.id.list);
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        initActionBar();

        adapter = new ContactAdapter();
        mListContact.setAdapter(adapter);
        mListContact.setOnItemClickListener(this);
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            initSearchView(actionBar);
        }
    }

    private void initSearchView(ActionBar actionBar) {
        if (actionBar == null) {
            return;
        }

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = new SearchView(actionBar.getThemedContext());
        searchView.setQueryHint(getString(android.R.string.search_go));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setIconified(false);
        searchView.setMaxWidth(1000);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() < 1) {
                    contacts = new  ArrayList<Contact>();
                    adapter.notifyDataSetChanged();
                    return true;
                }

//                List<Contact> filteredData = ContactFilter.doFilterIgnoreEmptyPhoneNumber(newText);
                SearchContactList searchContactList = new SearchContactList();
                List<Contact> filteredData = searchContactList.SearchToContact(newText);
                contacts = filteredData;
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        initSearchItem(menu);
        return true;
    }


    private void initSearchItem(Menu menu) {
        searchItem = menu.add(android.R.string.search_go);

        searchItem.setIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case android.R.id.home:
                        finish();
                        break;
                }
                return false;
            }
        });

        MenuItemCompat.setActionView(searchItem, searchView);
        MenuItemCompat.setShowAsAction(searchItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                finish();
                return false;
            }
        });
        showSearch(true);
    }

    private void showSearch(boolean visible) {
        if (visible) {
            MenuItemCompat.expandActionView(searchItem);
        } else {
            MenuItemCompat.collapseActionView(searchItem);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        if (position < mListContact.getHeaderViewsCount()) {
            return;
        }

        position -= mListContact.getHeaderViewsCount();

        Contact c = contacts.get(position);
        Intent intent = new Intent(this, ContactDetailsActivity.class);
        intent.putExtra("contact_id", c.getContactId());
        startActivity(intent);
        Log.i(TAG, "item click");
    }
}
