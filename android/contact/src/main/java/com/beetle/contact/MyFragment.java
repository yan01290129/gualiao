package com.beetle.contact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.Department;
import com.beetle.goubuli.model.DepartmentManager;
import com.squareup.picasso.Picasso;

import java.util.List;
public class MyFragment extends ListFragment {

    private static final String ARG_PARAM = "param";
    private String mParam;
    private ListAdapter listAdapter;
    List<Department> departments;
    List<Contact> contacts;
    private ListView mListContact = null; //联系人视图

    public MyFragment() {
    }

    public static MyFragment newInstance(String param) {
        MyFragment fragment = new MyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM, param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam = getArguments().getString(ARG_PARAM);
        }
        departments = DepartmentManager.getInstance().getDepartments(mParam);
        contacts = ContactManager.getInstance().getUser(mParam);
        listAdapter = new MyAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, null);
        /**
         * 设置适配器。
         * 注意：为什么不在onCreate方法中设置适配器，是因为我们使用了自定义的视图，
         * 也就是 说我们要加载完自定义的视图以后才可以设置适配器，
         * 如果在onCreate方法中就设置的话， 必然会出错
         */
        this.setListAdapter(listAdapter);
        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // 更新布局
        if(departments.size() > position){
            Department department = departments.get(position);
            if(DepartmentManager.getInstance().getDepartments(department.id).size() == 0 && ContactManager.getInstance().getUser(department.id).size() == 0){
                Toast.makeText(getContext(), "该部门下没有人员", Toast.LENGTH_SHORT).show();
                return;
            }
            FragmentManager mFragmentManager = ((ContactListActivity) getContext()).getSupportFragmentManager();
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.replace(R.id.frag_container, MyFragment.newInstance(department.id));
            ft.setBreadCrumbTitle(department.name);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }else {
            Contact contact = contacts.get(position - departments.size());
            Intent intent = new Intent(getContext(), ContactDetailsActivity.class);
            intent.putExtra("contact_id", contact.getContactId());
            startActivity(intent);
        }
    }

    // 适配器
    class MyAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return departments.size() + contacts.size();
        }

        @Override
        public Object getItem(int position) {
            if(departments.size() > position){
                return departments.get(position);
            }else {
                return contacts.get(position - departments.size());
            }
        };

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if(departments.size() > position){
                view = getLayoutInflater().inflate(R.layout.dept, null);
                Department department = departments.get(position);
                TextView tv = (TextView) view.findViewById(R.id.deptname);
                tv.setText(department.name);
            }else {
                view = getLayoutInflater().inflate(R.layout.contact, null);
                Contact contact = contacts.get(position - departments.size());
                ImageView avatarView = (ImageView)view.findViewById(R.id.header);
                if (!TextUtils.isEmpty(contact.getAvatar())) {
                    Picasso.with(getContext())
                            .load(contact.getAvatar())
                            .placeholder(R.drawable.avatar_contact)
                            .into(avatarView);
                } else {
                    avatarView.setImageResource(R.drawable.avatar_contact);
                }
                TextView tv = (TextView) view.findViewById(R.id.name);
                tv.setText(contact.getName());
            }
            return view;
        }
    }

}
