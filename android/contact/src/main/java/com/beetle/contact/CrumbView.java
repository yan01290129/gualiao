package com.beetle.contact;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.beetle.goubuli.model.Department;
import com.beetle.goubuli.model.DepartmentManager;

import java.util.List;

/**
 * Created by xudd on 2016/6/10.
 */
public class CrumbView extends HorizontalScrollView {

    private int LIGHT_COLOR, DARK_COLOR;
    private Resources mRes;
    private LinearLayout mContainer;
    private FragmentManager mFragmentManager;

    public CrumbView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mRes = context.getResources();
        TypedArray typedArray = mRes.obtainAttributes(attrs, R.styleable.CrumbViewAttrs);
        try{
            LIGHT_COLOR = typedArray.getColor(R.styleable.CrumbViewAttrs_light_color, mRes.getColor(R.color.colorInputtext));
            DARK_COLOR = typedArray.getColor(R.styleable.CrumbViewAttrs_dark_color, mRes.getColor(R.color.colorPrimaryDark));
        }finally {
            typedArray.recycle();
        }
        initView(context);
    }

    private void initView(Context context) {
        mContainer = new LinearLayout(context);
        mContainer.setOrientation(LinearLayout.HORIZONTAL);
        mContainer.setPadding(mRes.getDimensionPixelOffset(R.dimen.crumb_view_padding), 0,
                mRes.getDimensionPixelOffset(R.dimen.crumb_view_padding), 0);
        mContainer.setGravity(Gravity.CENTER_VERTICAL);
        addView(mContainer);
    }

    // 更新第一个面包屑
    public void setActivity(String title){
        // 面包屑的数量
        int numCrumbs = mContainer.getChildCount();
        // 移除所有
        while(numCrumbs > 0){
            mContainer.removeViewAt(1);
            numCrumbs--;
        }
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.crumb_item_layout, null);
        TextView tv = (TextView) itemView.findViewById(R.id.crumb_name);
        tv.setText(title);
        itemView.setTag(title);
        itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //全部回退
                mFragmentManager = ((ContactListActivity) getContext()).getSupportFragmentManager();
                int count = mFragmentManager.getBackStackEntryCount();
                if (count > 0) {
                    FragmentManager.BackStackEntry bse = mFragmentManager.getBackStackEntryAt(0);
                    // 回退到指定标识栈为栈顶
                    mFragmentManager.popBackStack(bse.getId(), 0);
                    // 再后退一次
                    mFragmentManager.popBackStack();
                }
            }
        });
        mContainer.addView(itemView);
        //调整可见性和高亮
        highLightIndex(mContainer.getChildAt(0), true);
    }

    // 更新面包屑第一个之外的标题
    public void upCrumbs(){
        // 嵌套的fragment数量
        mFragmentManager = ((ContactListActivity) getContext()).getSupportFragmentManager();
        int numFrags = mFragmentManager.getBackStackEntryCount();
        // 面包屑的数量
        int numCrumbs = mContainer.getChildCount();
        // 移除第一个之外所有面包屑
        while(numCrumbs > 1){
            mContainer.removeViewAt(1);
            numCrumbs--;
        }
        // 添加第一个之外的
        for(int i = 0; i < numFrags; i++){
            FragmentManager.BackStackEntry backStackEntry = mFragmentManager.getBackStackEntryAt(i);
            View itemView = LayoutInflater.from(getContext()).inflate(R.layout.crumb_item_layout, null);
            itemView.setTag(backStackEntry);
            itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getTag() instanceof FragmentManager.BackStackEntry) {
                        FragmentManager.BackStackEntry bse = (FragmentManager.BackStackEntry) v.getTag();
                        mFragmentManager.popBackStack(bse.getId(), 0);
                    }
                }
            });
            TextView tv = (TextView) itemView.findViewById(R.id.crumb_name);
            tv.setText(backStackEntry.getBreadCrumbTitle());
            mContainer.addView(itemView);
        }
        //调整可见性
        numCrumbs = mContainer.getChildCount();
        for (int i = 0; i < numCrumbs; i++) {
            final View child = mContainer.getChildAt(i);
            // 高亮
            highLightIndex(child, !(i < numCrumbs - 1));
        }
        // 滑动到最后一个
        post(new Runnable() {
            @Override
            public void run() {
                fullScroll(ScrollView.FOCUS_RIGHT);
            }
        });
    }

    // UI
    public void highLightIndex(View view, boolean highLight) {
        TextView text = (TextView) view.findViewById(R.id.crumb_name);
        ImageView image = (ImageView) view.findViewById(R.id.crumb_icon);
        if (highLight) {
            text.setTextColor(LIGHT_COLOR);
            image.setVisibility(View.GONE);
        } else {
            text.setTextColor(DARK_COLOR);
            image.setVisibility(View.VISIBLE);
        }
    }
}
