package com.beetle.contact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import com.beetle.goubuli.BaseActivity;
import com.beetle.goubuli.UserApplication;
import com.beetle.goubuli.model.*;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.GroupConversationEvent;
import com.beetle.goubuli.tools.event.SyncCompleteEvent;
import java.util.Date;
import java.util.List;
import com.beetle.goubuli.tools.event.TabEvent;
import com.squareup.otto.Subscribe;

public class ContactListActivity extends BaseActivity{
    private static final String TAG = "goubuli";

    private static final int REQUEST_GROUP = 1;
    private LinearLayout mLayoutHeader = null; // 群主视图
    List<Contact> Allcontacts;
    private FragmentManager mFragmentManager = getSupportFragmentManager();
    CrumbView crumbView;
    private String gid = "root";

    // 页面适配联系人
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        mLayoutHeader = this.findViewById(R.id.group);
        mLayoutHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "click group");
                Intent intent = new Intent();
                intent.setClass(ContactListActivity.this, GroupContactActivity.class);
                startActivityForResult(intent, REQUEST_GROUP);
            }
        });

        // 注册监听
        BusProvider.getInstance().register(messageHandler);
    }
    private View statusBarView;
    private void initStatusBar() {
        if (statusBarView == null) {
            int identifier = getResources().getIdentifier("statusBarBackground", "id", "android");
            statusBarView = getWindow().findViewById(identifier);
        }
        if (statusBarView != null) {
            statusBarView.setBackgroundResource(R.drawable.shape_gradient);
        }
    }

    protected boolean isStatusBar() {
        return true;
    }

    // 接收广播动态更新联系人列表
    final Object messageHandler = new Object() {
        @Subscribe
        public void onSyncCompleteEvent(SyncCompleteEvent event) {
            // 更新全局联系人
            Allcontacts = ContactManager.getInstance().getAllDisplayContactsList();
            UserApplication app = (UserApplication)getApplication();
            app.setContacts(Allcontacts);
            // 頁面更新
            loadUI();
        }
    };

    // 默认更新UI
    public void loadUI(){
        if(gid.equals("root")){
            // 获取深业公司
            List<Department> departments = DepartmentManager.getInstance().getDepartments(gid);
            // 初始化面包屑标题
            crumbView = (CrumbView) findViewById(R.id.crumb_view);
            crumbView.setActivity(departments.get(0).name);
            // 初始化布局（不添加到回退栈）
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.replace(R.id.frag_container, MyFragment.newInstance(departments.get(0).id));
            ft.commitAllowingStateLoss();
            // 监听
            mFragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    crumbView.upCrumbs();
                }
            });
            gid = "";
        }
    }

    public boolean canBack() {
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(messageHandler);
    }

    public static int getNow() {
        Date date = new Date();
        long t = date.getTime();
        return (int)(t/1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.search) {
            Intent intent = new Intent();
            intent.setClass(this, SearchContactActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "result:" + resultCode + " request:" + requestCode);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_GROUP) {
            long groupId = data.getLongExtra("group_id", 0);
            if (groupId == 0) {
                return;
            }
            String name = data.getStringExtra("group_name");
            BusProvider.getInstance().post(new GroupConversationEvent(groupId, name != null ? name : ""));
            BusProvider.getInstance().post(new TabEvent());
        }
    }
}
