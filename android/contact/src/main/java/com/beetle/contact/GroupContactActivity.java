package com.beetle.contact;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.types.Meet;
import com.beetle.goubuli.api.types.RtnDefaultGroup;
import com.beetle.goubuli.api.types.RtnGroup;
import com.beetle.goubuli.model.Conversation;
import com.beetle.goubuli.model.Group;
import com.beetle.goubuli.model.GroupDB;
import com.beetle.goubuli.model.Token;
import com.beetle.goubuli.util.ParseMD5;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class GroupContactActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private List<Group> groups;
    private ListView lv;
    private BaseAdapter adapter;
    private boolean syncing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_contact);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("我的群组");
        }
        groups = GroupDB.getInstance().loadGroups();
        adapter = new ContactAdapter();
        lv = (ListView)findViewById(R.id.list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        // 设置上下文菜单
        registerForContextMenu(lv);

    }

    // 列表适配器
    class ContactAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return groups.size();
        }

        @Override
        public Object getItem(int position) {
            return groups.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.group_contact, null);
            } else {
                view = convertView;
            }
            TextView tv = (TextView) view.findViewById(R.id.name);
            Group c = groups.get(position);
            tv.setText(c.topic != null ? c.topic : "");
            if(c.groupID == c.defaultgroupid){
                tv.setText(tv.getText()+"(默认群组)");
            }
            return view;
        }
    }

    // 注册上下文菜单
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()== com.beetle.message.R.id.list) {
            menu.setHeaderTitle("选择操作");
            menu.add("设置为默认群组");
        }
    }

    // 列表点击显示上下文菜单
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Group g = groups.get(position);
        String topic = g.topic != null ? g.topic : "";
        Intent intent = new Intent();
        intent.putExtra("group_id", g.groupID);
        intent.putExtra("group_name", topic);
        setResult(RESULT_OK, intent);
        finish();
    }

    // 设置为群组菜单事件
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        Group group = groups.get(info.position);
        String groupID = String.valueOf(group.groupID);
        if (menuItemIndex == 0) {
            showWaitingDialog();
            if (syncing) {
                return true;
            }
            syncing = true;
            String username = "user101";
            String method = "RhtxUserDefaultGroup";
            String password = "123";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd",Locale.CHINA);
            String token = ParseMD5.parseStrToMd5L32(username+password+method+sdf.format(date));
            RtnDefaultGroup rtnDefaultGroup = new RtnDefaultGroup();
            rtnDefaultGroup.username  = username;
            rtnDefaultGroup.method  = method;
            rtnDefaultGroup.token = token;
            rtnDefaultGroup.f_imId = String.valueOf(Token.getInstance().uid);
            rtnDefaultGroup.f_imGroupId = groupID;
            IMHttp imHttp = IMHttpFactory.Singleton();
            imHttp.getRhtxUserDefaultGroup(rtnDefaultGroup)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<JsonObject>() {
                                   @Override
                                   public void call(JsonObject jsonObject) {
                                       syncing = false;
                                       disWaitingDialog();
                                       boolean flag = jsonObject.get("return_code").getAsBoolean();
                                       if(flag){
                                           // 重新拉取群主列表并且保存数据库
                                           getGroups();
                                       }else {
                                           Toast.makeText(GroupContactActivity.this, "设置群组失败", Toast.LENGTH_SHORT).show();
                                       }
                                   }
                               }, new Action1<Throwable>() {
                                   @Override
                                   public void call(Throwable throwable) {
                                       disWaitingDialog();
                                       Toast.makeText(GroupContactActivity.this, "设置群组失败", Toast.LENGTH_SHORT).show();
                                   }
                               }
                    );

        }
        return true;
    }

    // 获取群主
    private void getGroups() {
        String username = "user101";
        String method = "RtnGroup";
        String password = "123";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd",Locale.CHINA);
        String token = ParseMD5.parseStrToMd5L32(username+password+method+sdf.format(date));
        RtnGroup rtnGroup = new RtnGroup();
        rtnGroup.username  = username;
        rtnGroup.method  = method;
        rtnGroup.token = token;
        rtnGroup.userId =  String.valueOf(Token.getInstance().uid);
        IMHttp imHttp = IMHttpFactory.Singleton();
        imHttp.getRtnGroup(rtnGroup)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                               @Override
                               public void call(Object obj) {
                                   Gson g = new Gson();
                                   JsonArray data = g.toJsonTree(obj).getAsJsonArray();
//                                   Toast.makeText(MainActivity.this, "获取群主列表成功", Toast.LENGTH_SHORT).show();
                                   // 群主操作
                                   addGroups(data);
                               }
                           }, new Action1<Throwable>() {
                               @Override
                               public void call(Throwable throwable) {
                                   Log.i("", "get groups fail:", throwable);
                                   Toast.makeText(GroupContactActivity.this, "刷新群主列表失败", Toast.LENGTH_SHORT).show();
                               }
                           }
                );
    }

    // 群主入库
    void addGroups(JsonArray arr) {
        // 清空群主
        GroupDB.getInstance().clear();

        for (int i = 0;i<arr.size();i++){
            JsonObject data = arr.get(i).getAsJsonObject();
            long gid = data.get("id").getAsLong();
            String fTxnum = data.get("f_Txnum").getAsString();
            String name = data.get("name").getAsString();
            Long masterID = data.get("master").getAsLong();
            boolean doNotDisturb = data.get("do_not_disturb").getAsBoolean();
            Long defaultgroupid = data.get("Defaultgroupid").getAsLong();

            JsonArray members = data.get("members").getAsJsonArray();

            Group group = new Group();
            group.groupID = gid;
            group.fTxnum = fTxnum;
            group.topic = name;
            group.master = masterID;
            group.doNotDisturb = doNotDisturb;
            group.readonly = true;
            group.timestamp = now();
            group.defaultgroupid = defaultgroupid;

            for (int j = 0; j < members.size(); j++) {
                JsonObject m = members.get(j).getAsJsonObject();
                long uid = m.get("uid").getAsLong();
                group.addMember(uid);
            }
            // 群主入库
            GroupDB.getInstance().addGroup(group);
        }
        // 刷新
        groups.clear();
        groups.addAll(GroupDB.getInstance().loadGroups());
        adapter.notifyDataSetChanged();
        Toast.makeText(GroupContactActivity.this, "设置群组成功", Toast.LENGTH_SHORT).show();
    }

    // 显示加载中
    ProgressDialog waitingDialog;
    private void showWaitingDialog() {
        /* 等待Dialog具有屏蔽其他控件的交互能力
         * @setCancelable 为使屏幕不可点击，设置为不可取消(false)
         * 下载等事件完成后，主动调用函数关闭该Dialog
         */
        waitingDialog =
                new ProgressDialog(GroupContactActivity.this);
//        waitingDialog.setTitle("会议室检验");
        waitingDialog.setMessage("等待中...");
        waitingDialog.setIndeterminate(true);
        waitingDialog.setCancelable(true);
        waitingDialog.show();
    }
    // 销毁加载中
    private void disWaitingDialog() {
        waitingDialog.dismiss();
    }

    // 时间戳
    public static int now() {
        Date date = new Date();
        long t = date.getTime();
        return (int)(t/1000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish(); // back button
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
