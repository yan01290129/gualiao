package com.beetle.contact;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.Department;
import com.beetle.goubuli.model.DepartmentManager;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xudd on 2016/6/10.
 */
public class DeptFragment extends ListFragment {

    private static final String KEY_DEPT = "dataDepts";
    private FragmentManager mFragmentManager;
    private ArrayList<String> deptNamearrayList = new ArrayList<>();
    List<Department> dataDepts;

    public static DeptFragment getInstance(String parentID){
        DeptFragment frag = new DeptFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_DEPT, parentID);
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getActivity().getSupportFragmentManager();
        if(getArguments() != null){
            String parentID = getArguments().getString(KEY_DEPT);
            dataDepts = DepartmentManager.getInstance().getDepartments(parentID);
            for (int i=0;i<dataDepts.size();i++){
                deptNamearrayList.add(dataDepts.get(i).name);
            }
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, deptNamearrayList);
        setListAdapter(arrayAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String pid = dataDepts.get(position).id;
        List<Department> departments = DepartmentManager.getInstance().getDepartments(pid);
        List<Contact> contacts =  ContactManager.getInstance().getUser(pid);
        if(departments.size() == 0 && contacts.size() == 0){
            Toast.makeText(getActivity(),"该部门下没有人员",Toast.LENGTH_SHORT).show();
            return;
        }else {
            // 更新部门
            FragmentTransaction ft = mFragmentManager.beginTransaction();
            ft.setBreadCrumbTitle(deptNamearrayList.get(position));
//            ft.replace(R.id.deptlist, DeptFragment.getInstance(pid));
            ft.addToBackStack(pid);
            ft.commitAllowingStateLoss();
            // 更新人员（这里不更新，Fragment状态改变时面包屑统一捕捉到后调Activity更新人员，包括捕捉到返回按钮）
//            ContactListActivity contactListActivity = (ContactListActivity)getActivity();
//            contactListActivity.loadUser(pid);
        }
    }
}
