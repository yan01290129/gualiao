package com.beetle.contact;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.Department;
import com.beetle.goubuli.model.DepartmentManager;
import com.beetle.goubuli.model.Profile;
import com.beetle.goubuli.model.Token;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.SyncCompleteEvent;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.linphone.LinphoneLauncherActivity;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class LinphoneFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BusProvider.getInstance().register(this);
        return inflater.inflate(R.layout.fragment_linphone, container, false);
    }

    // 接受广播
    @Subscribe
    public void onSyncCompleteEvent(SyncCompleteEvent event) {
        Toast.makeText(getActivity(), "111111", Toast.LENGTH_SHORT).show();
        long uid =  Token.getInstance().uid;
        Contact contact = ContactManager.getInstance().getContactById(uid);
        //初始化linphone
        Intent intent = new Intent();
        Profile profile = Profile.getInstance();
        intent.putExtra("userno",contact.getUserno());
        intent.setClass(getActivity(),LinphoneLauncherActivity.class);
        this.startActivity(intent);
    }



}
