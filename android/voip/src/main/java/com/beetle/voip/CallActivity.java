package com.beetle.voip;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.body.Call;

/**
 * Created by houxh on 2016/12/27.
 */

public class CallActivity extends VOIPActivity {

    public static final int CALL_CANCLED = 1;
    public static final int CALL_REFUSED = 2;
    public static final int CALL_ACCEPTED = 3;
    public static final int CALL_UNRECEIVED = 4;

    private int result  = CALL_UNRECEIVED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
    }

    @Override
    protected void dismiss() {
        Intent intent = new Intent();
        intent.putExtra("result", this.result);
        intent.putExtra("duration", this.duration);
        intent.putExtra("video_enabled", this.videoEnabled);
        intent.putExtra("peer_uid", this.peerUID);
        setResult(RESULT_OK, intent);
        super.dismiss();
    }

    @Override
    public void dialVideo() {
        super.dialVideo();
        IMHttp imHttp = IMHttpFactory.Singleton();
        Call call = new Call();

        call.channelID = this.channelID;
        call.peerUID = this.peerUID;

        imHttp.postCall(call)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        Log.i(TAG, "post call success");
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.i(TAG, "post call fail");
                    }
                });
    }

    @Override
    public void dialVoice() {
        super.dialVoice();
        IMHttp imHttp = IMHttpFactory.Singleton();
        Call call = new Call();

        call.channelID = this.channelID;
        call.peerUID = this.peerUID;

        imHttp.postCall(call)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        Log.i(TAG, "post call success");
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.i(TAG, "post call fail");
                    }
                });
    }


    @Override
    public void hangup() {
        super.hangup();
        if (isCaller) {
            if (!isConnected) {
                result = CALL_CANCLED;
            }

            IMHttp imHttp = IMHttpFactory.Singleton();
            Call call = new Call();

            call.channelID = this.channelID;
            call.peerUID = this.peerUID;
            imHttp.hangupCall(call)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object o) {
                            Log.i(TAG, "hangup call success");
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "hangup call fail");
                        }
                    });
        }
    }

    @Override
    public void accept() {
        result = CALL_ACCEPTED;
        super.accept();
    }

    @Override
    public void refuse() {
        result = CALL_REFUSED;
        super.refuse();
    }

    //remove refuse
    @Override
    public void onRemoteRefuse() {
        super.onRemoteRefuse();
        result = CALL_REFUSED;
    }

    @Override
    public void onConnected() {
        super.onConnected();
        result = CALL_ACCEPTED;
    }

    @Override
    public void onAcceptAnother() {
        result = CALL_ACCEPTED;
        super.onAcceptAnother();
    }

    @Override
    public void onRefuseAnother() {
        result = CALL_REFUSED;
        super.onRefuseAnother();
    }

    @Override
    protected void startStream() {
        super.startStream();
    }

    @Override
    protected void stopStream() {
        super.stopStream();
    }
}
