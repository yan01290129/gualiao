package com.beetle.goubuli;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.TextView;
import android.widget.Toast;

import com.beetle.bauhinia.MessageActivity;
import com.beetle.bauhinia.db.GroupMessageDB;
import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.PeerMessageDB;
import com.beetle.bauhinia.db.message.GroupVOIP;
import com.beetle.bauhinia.tools.Notification;
import com.beetle.bauhinia.tools.NotificationCenter;
import com.beetle.goubuli.model.Conversation;
import com.beetle.goubuli.model.Group;
import com.beetle.goubuli.model.GroupDB;
import com.beetle.goubuli.model.GroupMember;
import com.beetle.goubuli.model.Profile;
import com.beetle.goubuli.model.Token;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.ConferenceEvent;
import com.beetle.im.IMMessage;
import com.beetle.im.IMService;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.reactnativenavigation.controllers.NavigationCommandsHandler;
import com.reactnativenavigation.react.NavigationApplication;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.squareup.otto.Subscribe;
import com.beetle.message.R;

import org.linphone.CallPhoneActivity3;
import org.linphone.LinphoneActivity;

/**
 * Created by houxh on 2017/7/24.
 */

public class GroupMessageActivity extends com.beetle.bauhinia.GroupMessageActivity{

    private final int AT_REQUEST = 1;
    private final int FORWARD_REQUEST = 2;

    private boolean leaved = false;
    private List<Contact> contacts;

    protected String navigatorID;
    protected String screenInstanceID;
    protected String navigatorEventID;

    public static GroupMessageActivity instance;
    public static GroupMessageActivity getInstance(){
        if(instance == null){
            instance = new GroupMessageActivity();
        }
        return instance;
    }

    public static void convertBundle(Bundle bundle, Intent intent) {
        Bundle props = bundle.getBundle("passProps");
        if (props != null) {
            String name = props.getString("groupName");
            if (!TextUtils.isEmpty(name)) {
                intent.putExtra("group_name", name);
            }

            int uid = props.getInt("currentUID", -1);
            if (uid != -1) {
                intent.putExtra("current_uid", (long) uid);
            }

             int gid = props.getInt("groupID", -1);
            if (gid != -1) {
                intent.putExtra("group_id", (long) gid);
            }
        }

        Bundle navigationParams = bundle.getBundle("navigationParams");
        if (navigationParams != null) {
            String navigatorID = navigationParams.getString("navigatorID", "");
            String navigatorEventID = navigationParams.getString("navigatorEventID", "");
            String screenInstanceID = navigationParams.getString("screenInstanceID", "");

            intent.putExtra("navigatorID", navigatorID);
            intent.putExtra("navigatorEventID", navigatorEventID);
            intent.putExtra("screenInstanceID", screenInstanceID);
        }
    }

    final Object messageHandler = new Object() {
        @Subscribe
        public void onConerenceEvent(ConferenceEvent event) {
            if (event.groupID == 0) {
                return;
            }
            IMessage msg = new IMessage();
            msg.sender = 0;
            msg.receiver = event.groupID;
            GroupVOIP voip = GroupVOIP.newGroupVOIP(event.initiator, event.finished);
            msg.setContent(voip);
            msg.timestamp = now();
            updateNotificationDesc(msg);
            GroupMessageActivity.this.insertMessage(msg);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance = this;
        contacts = ContactManager.getInstance().getAllContact();

        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        navigatorID = intent.getStringExtra("navigatorID");
        screenInstanceID = intent.getStringExtra("screenInstanceID");
        navigatorEventID = intent.getStringExtra("navigatorEventID");

        if (!TextUtils.isEmpty(navigatorID)) {
            NavigationCommandsHandler.registerNavigationActivity(this, navigatorID);
        }

        if (!TextUtils.isEmpty(screenInstanceID)) {
            NavigationCommandsHandler.registerActivity(this, screenInstanceID);
        }

        BusProvider.getInstance().register(messageHandler);

        leaved = GroupDB.getInstance().isLeaved(groupID);
        if (leaved) {
            inputMenu.setVisibility(View.GONE);
            findViewById(R.id.bottom_bar).setVisibility(View.VISIBLE);
            TextView tv = (TextView)findViewById(R.id.bottom_text);
            tv.setText("您已不是群成员");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (!TextUtils.isEmpty(navigatorID)) {
            NavigationCommandsHandler.unregisterNavigationActivity(this, navigatorID);
        }

        if (!TextUtils.isEmpty(screenInstanceID)) {
            NavigationCommandsHandler.unregisterActivity(screenInstanceID);
        }

        BusProvider.getInstance().unregister(messageHandler);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!leaved) {
            getMenuInflater().inflate(R.menu.menu_group_chat, menu);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_setting) {
            Group group = GroupDB.getInstance().loadGroup(groupID);
            if (group == null) {
                return true;
            }
            ArrayList<GroupMember> members = GroupDB.getInstance().loadGroupMember(groupID);
            group.setGroupMembers(members);

            List<Contact> contacts = ContactManager.getInstance().getAllContact();
            WritableArray contactUsers = this.getContacts(contacts);
            WritableMap g = this.getGroupMap(group, contacts);

            Profile p = Profile.getInstance();
            WritableMap profile = Arguments.createMap();
            profile.putDouble("uid", p.uid);
            profile.putString("name",  p.name != null ? p.name : "");
            profile.putString("avatar", p.avatar != null ? p.avatar : "");
//           profile.putString("accessToken", Token.getInstance().accessToken);
            profile.putString("gobelieveToken", Token.getInstance().gobelieveToken);



            WritableMap map = Arguments.createMap();
            map.putMap("group", g);
            map.putArray("contacts", contactUsers);
            map.putString("navigatorID", navigatorID);
            map.putMap("profile", profile);

            ReactContext reactContext = NavigationApplication.instance.getReactGateway().getReactContext();
            this.sendEvent(reactContext, "group_setting", map);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected User getUser(long uid) {
        for (Contact contact : contacts) {
            if (contact.getContactId() == uid) {
                User u = new User();
                u.uid = uid;
                u.name = contact.getName();
                u.avatarURL = contact.getAvatar();
                u.identifier = String.format("%d", uid);
                Log.i(TAG, "user name:" + u.name);
                return u;
            }
        }

        User u = new User();
        u.uid = uid;
        u.name = null;
        u.avatarURL = "";
        u.identifier = String.format("%d", uid);
        Log.i(TAG, "user name is null");
        return u;
    }


    private WritableMap getGroupMap(Group group, List<Contact> contacts) {
        WritableArray users = Arguments.createArray();
        ArrayList<GroupMember> members = group.getGroupMembers();
        String nickname = Profile.getInstance().name;
        for (int i = 0; i < members.size(); i++) {
            GroupMember m = members.get(i);
            if (m.uid == Profile.getInstance().uid) {
                if (!TextUtils.isEmpty(m.nickname)) {
                    nickname = m.nickname;
                }
            }
            String name = m.nickname;
            if (TextUtils.isEmpty(m.nickname)) {
                name = "" + m.uid;
                for (Contact c : contacts) {
                    if (c.getContactId() == m.uid) {
                        name = c.getName();
                        break;
                    }
                }
            }
            String avatar = "";
            for (Contact c : contacts) {
                if (c.getContactId() == m.uid) {
                    avatar = c.getAvatar();
                    break;
                }
            }

            WritableMap map = Arguments.createMap();
            map.putDouble("uid", m.uid);
            map.putDouble("id", m.uid);
            map.putString("name", name);
            map.putString("avatar", avatar != null ? avatar : "");
            users.pushMap(map);
        }

        WritableMap map = Arguments.createMap();
        map.putArray("members", users);
        map.putDouble("id", this.groupID);
        map.putString("name", groupName);
        map.putDouble("master", group.master);

        map.putString("notice", group.notice);
        map.putString("nickname", nickname);//群内昵称
        map.putBoolean("doNotDisturb", group.doNotDisturb);
        return map;
    }

    private WritableArray getContacts(List<Contact> contacts) {
        WritableArray users = Arguments.createArray();


        for (int i = 0; i < contacts.size(); i++) {
            Contact c = contacts.get(i);

            WritableMap map = Arguments.createMap();
            map.putString("name", c.getName());
            map.putString("avatar", c.getAvatar());
            map.putDouble("uid", c.getContactId());
            map.putDouble("id", c.getContactId());
            users.pushMap(map);

        }
        return users;
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }


    @Override
    protected void grpUserCall() {
        Group group = GroupDB.getInstance().loadGroup(groupID);
        if (group == null) {
            return;
        }
        // 发送文本消息
        sendTextMessage(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " " + "发起了语音对讲", null, null);

        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        String userno = group.fTxnum;
        String name = group.topic;
//        bundle.putString("userno",userno);
        intent.putExtra("userno", userno);
        intent.putExtra("name", name);
        intent.setClass(GroupMessageActivity.this, LinphoneActivity.class);
        GroupMessageActivity.this.startActivity(intent);
    }

    @Override
    protected void call() {
        Group group = GroupDB.getInstance().loadGroup(groupID);
        if (group == null) {
            return;
        }
        ArrayList<GroupMember> members = GroupDB.getInstance().loadGroupMember(groupID);
        group.setGroupMembers(members);

        List<Contact> contacts = ContactManager.getInstance().getAllContact();
        WritableArray users = Arguments.createArray();
        for (int i = 0; i < members.size(); i++) {
            GroupMember m = members.get(i);

            String name = m.nickname;
            if (TextUtils.isEmpty(m.nickname)) {
                name = "" + m.uid;
                for (Contact c : contacts) {
                    if (c.getContactId() == m.uid) {
                        name = c.getName();
                        break;
                    }
                }
            }

            String avatar = "";
            for (Contact c : contacts) {
                if (c.getContactId() == m.uid) {
                    avatar = c.getAvatar();
                    break;
                }
            }
            WritableMap map = Arguments.createMap();
            map.putDouble("uid", m.uid);
            map.putString("name", name);
            map.putString("avatar", avatar != null ? avatar : "");
            users.pushMap(map);
        }

        WritableMap map = Arguments.createMap();
        map.putArray("users", users);
        map.putDouble("groupID", groupID);
        map.putString("channelID", UUID.randomUUID().toString());
        map.putDouble("uid", Token.getInstance().uid);
        map.putString("token", Token.getInstance().gobelieveToken);
        map.putString("url", "");
        map.putString("navigatorID", navigatorID);
        ReactContext reactContext = NavigationApplication.instance.getReactGateway().getReactContext();
        this.sendEvent(reactContext, "create_group_call", map);
    }

    @Override
    protected void onAt() {
        Intent intent = new Intent(this, AtActivity.class);
        intent.putExtra("group_id", groupID);
        startActivityForResult(intent, AT_REQUEST);
    }

    @Override
    protected void forward(IMessage im) {
        if (TextUtils.isEmpty(im.getUUID())) {
            return;
        }
        Intent intent = new Intent(this, ForwardActivity.class);
        intent.putExtra("uuid", im.getUUID());
        startActivityForResult(intent, FORWARD_REQUEST);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            Log.i(TAG, "take or select picture fail:" + resultCode);
            return;
        }

        if (requestCode == AT_REQUEST) {
            long uid = data.getLongExtra("uid", -1);
            String name = data.getStringExtra("name");
            if (uid != -1 && !TextUtils.isEmpty(name)) {
                this.inputMenu.atUser(uid, name);
            }
            return;
        } else if (requestCode == FORWARD_REQUEST) {
            int type = data.getIntExtra("type", 0);
            long cid = data.getLongExtra("cid", 0);
            String uuid = data.getStringExtra("uuid");

            if (cid == 0) {
                return;
            }

            IMessage imsg = findMessage(uuid);
            if (imsg == null) {
                return;
            }

            imsg = (IMessage)imsg.clone();
            imsg.sender = currentUID;
            imsg.receiver = cid;
            imsg.content.generateRaw(UUID.randomUUID().toString());

            IMMessage msg = new IMMessage();
            msg.sender = imsg.sender;
            msg.receiver = imsg.receiver;
            msg.msgLocalID = imsg.msgLocalID;
            msg.content = imsg.content.getRaw();
            msg.plainContent = msg.content;

            if (type == Conversation.CONVERSATION_PEER) {
                PeerMessageDB db = PeerMessageDB.getInstance();
                db.insertMessage(imsg, cid);
                IMService.getInstance().sendPeerMessage(msg);
                NotificationCenter nc = NotificationCenter.defaultCenter();
                Notification notification = new Notification(imsg, com.beetle.bauhinia.PeerMessageActivity.SEND_MESSAGE_NAME);
                nc.postNotification(notification);
            } else if (type == Conversation.CONVERSATION_GROUP) {
                GroupMessageDB db = GroupMessageDB.getInstance();
                db.insertMessage(imsg, cid);
                IMService.getInstance().sendGroupMessage(msg);
                NotificationCenter nc = NotificationCenter.defaultCenter();
                Notification notification = new Notification(imsg, com.beetle.bauhinia.GroupMessageActivity.SEND_MESSAGE_NAME);
                nc.postNotification(notification);
            }

            if (type == Conversation.CONVERSATION_GROUP && cid == groupID) {
                insertMessage(imsg);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
