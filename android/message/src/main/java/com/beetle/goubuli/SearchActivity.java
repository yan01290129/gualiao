package com.beetle.goubuli;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.beetle.bauhinia.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import com.beetle.bauhinia.db.GroupMessageDB;
import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.PeerMessageDB;
import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.Conversation;
import com.beetle.goubuli.model.Group;
import com.beetle.goubuli.model.GroupDB;
import com.beetle.goubuli.model.Token;

import com.beetle.message.R;

public class SearchActivity extends BaseActivity implements AdapterView.OnItemClickListener  {
    private static final String TAG = "goubuli";
    Toolbar toolbar;
    private SearchView searchView;
    private MenuItem searchItem;

    List<Contact> contacts;
    List<Conversation> conversations = new ArrayList<>();
    String searchKey;

    private BaseAdapter adapter;
    private ListView mListContact = null;

    class ContactAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return conversations.size();
        }

        @Override
        public Object getItem(int position) {
            return conversations.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.conversation_message, null);
            } else {
                view = convertView;
            }
            Conversation conv = conversations.get(position);
            TextView tv = (TextView) view.findViewById(R.id.name);
            tv.setText(conv.getName());
            tv = (TextView)view.findViewById(R.id.content);
            tv.setText(conv.messages.size() + "相关聊天记录");
            return view;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Intent intent = getIntent();
        ParcelableObject obj = intent.getParcelableExtra("contacts");
        if (obj != null && obj.getObject() != null) {
            contacts = (List<Contact>) obj.getObject();
        }

        mListContact = (ListView)findViewById(R.id.list);
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        initActionBar();

        adapter = new ContactAdapter();
        mListContact.setAdapter(adapter);
        mListContact.setOnItemClickListener(this);
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            initSearchView(actionBar);
        }
    }

    private void initSearchView(ActionBar actionBar) {
        if (actionBar == null) {
            return;
        }

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = new SearchView(actionBar.getThemedContext());
        searchView.setQueryHint(getString(android.R.string.search_go));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setIconified(false);
        searchView.setMaxWidth(1000);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() < 1) {
                    conversations = new  ArrayList<>();
                    adapter.notifyDataSetChanged();
                    return true;
                }

                conversations = new ArrayList<Conversation>();

                ArrayList<Conversation> a1 = this.searchPeer(newText);
                ArrayList<Conversation> a2 = this.searchGroup(newText);

                conversations.addAll(a1);
                conversations.addAll(a2);
                searchKey = newText;

                adapter.notifyDataSetChanged();
                return true;
            }

            private ArrayList<Conversation> searchPeer(String newText) {
                long uid = Token.getInstance().uid;
                ArrayList<Conversation> conversations = new ArrayList<Conversation>();
                ArrayList<IMessage> messages = PeerMessageDB.getInstance().search(newText);
                for (int i = 0; i < messages.size(); i++) {
                    IMessage msg = messages.get(i);
                    long peer = msg.sender == uid ? msg.receiver : msg.sender;
                    int index = -1;
                    for (int j = 0; j < conversations.size(); j++) {
                        Conversation conv = conversations.get(j);
                        if (conv.cid == peer) {
                            index = j;
                            break;
                        }
                    }
                    Conversation conv = null;
                    if (index == -1) {
                        conv = new Conversation();
                        conv.cid = peer;
                        conv.type = Conversation.CONVERSATION_PEER;
                        conv.setName(getUserName(peer));
                        conversations.add(conv);
                    } else {
                        conv = conversations.get(index);
                    }
                    conv.messages.add(msg);
                }

                return conversations;
            }
            private ArrayList<Conversation> searchGroup(String newText) {
                ArrayList<Conversation> conversations = new ArrayList<Conversation>();
                ArrayList<IMessage> messages = GroupMessageDB.getInstance().search(newText);
                for (int i = 0; i < messages.size(); i++) {
                    IMessage msg = messages.get(i);
                    long groupID = msg.receiver;
                    int index = -1;
                    for (int j = 0; j < conversations.size(); j++) {
                        Conversation conv = conversations.get(j);
                        if (conv.cid == groupID) {
                            index = j;
                            break;
                        }
                    }
                    Conversation conv = null;
                    if (index == -1) {
                        conv = new Conversation();
                        conv.cid = groupID;
                        conv.type = Conversation.CONVERSATION_GROUP;
                        conv.setName(getGroupName(groupID));
                        conversations.add(conv);
                    } else {
                        conv = conversations.get(index);
                    }
                    conv.messages.add(msg);
                }

                return conversations;
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        initSearchItem(menu);
        return true;
    }


    private void initSearchItem(Menu menu) {
        searchItem = menu.add(android.R.string.search_go);

        searchItem.setIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case android.R.id.home:
                        finish();
                        break;
                }
                return false;
            }
        });

        MenuItemCompat.setActionView(searchItem, searchView);
        MenuItemCompat.setShowAsAction(searchItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                finish();
                return false;
            }
        });
        showSearch(true);
    }

    private void showSearch(boolean visible) {
        if (visible) {
            MenuItemCompat.expandActionView(searchItem);
        } else {
            MenuItemCompat.collapseActionView(searchItem);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        if (position < mListContact.getHeaderViewsCount()) {
            return;
        }

        position -= mListContact.getHeaderViewsCount();

        Conversation conv = conversations.get(position);

        Log.i(TAG, "item click:" + conv.getName());

        Intent intent = new Intent();
        intent.putExtra("conversation", new ParcelableObject(conv));
        intent.putExtra("search_key", searchKey);
        intent.setClass(this, SearchResultActivity.class);
        startActivity(intent);
    }

    protected String getUserName(long uid) {
        for (Contact contact : contacts) {
            if (contact.getContactId() == uid) {
                return contact.getName();
            }
        }
        return "";
    }

    protected String getGroupName(long gid) {
        Group g = GroupDB.getInstance().loadGroup(gid);
        if (g == null) {
            return "";
        }
        return g.topic;
    }
}
