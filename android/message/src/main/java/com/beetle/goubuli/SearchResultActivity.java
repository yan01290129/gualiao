package com.beetle.goubuli;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.message.MessageContent;
import com.beetle.bauhinia.db.message.Text;
import com.beetle.goubuli.model.Conversation;
import com.beetle.goubuli.model.Token;
import java.util.ArrayList;
import com.beetle.message.R;

public class SearchResultActivity extends BaseActivity implements AdapterView.OnItemClickListener  {

    private final String TAG = "goubuli";

    private BaseAdapter adapter;
    private ListView mListContact = null;

    private String searchKey;
    private Conversation conversation;
    private ArrayList<SpannableString> messages;
    class ContactAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return conversation.messages.size();
        }

        @Override
        public Object getItem(int position) {
            return conversation.messages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.conversation_message, null);
            } else {
                view = convertView;
            }
            TextView tv = (TextView) view.findViewById(R.id.name);
            tv.setText(conversation.getName());
            tv = (TextView)view.findViewById(R.id.content);
            tv.setText(messages.get(position));
            return view;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        Intent intent = getIntent();
        ParcelableObject obj = intent.getParcelableExtra("conversation");
        if (obj != null && obj.getObject() != null) {
            conversation = (Conversation) obj.getObject();
        }

        searchKey = intent.getStringExtra("search_key");
        initSpannableMessage();

        mListContact = (ListView)findViewById(R.id.list);
        adapter = new ContactAdapter();
        mListContact.setAdapter(adapter);
        mListContact.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        IMessage msg = conversation.messages.get(position);
        if (conversation.type == Conversation.CONVERSATION_PEER) {
            onPeerClick(conversation.cid, conversation.getName(), msg.msgLocalID);
        } else if (conversation.type == Conversation.CONVERSATION_GROUP){
            onGroupClick(conversation.cid, conversation.getName(), msg.msgLocalID);
        }
    }

    protected void onPeerClick(long peer, String name, int msgLocalID) {
        long currentUID = Token.getInstance().uid;
        Intent intent = new Intent(this, PeerMessageActivity.class);
        intent.putExtra("peer_uid", peer);
        intent.putExtra("peer_name", name);
        intent.putExtra("current_uid", currentUID);
        intent.putExtra("message_id", msgLocalID);

        String screenInstanceID = BaseActivity.generateScreenInstanceID();
        String navigatorEventID = screenInstanceID + "_events";

        intent.putExtra("navigatorID", navigatorID);
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);
        startActivity(intent);
    }

    protected void onGroupClick(long gid, String name, int msgLocalID) {
        long uid = Token.getInstance().uid;
        Intent intent = new Intent(this, GroupMessageActivity.class);
        intent.putExtra("group_id", gid);
        intent.putExtra("group_name", name);
        intent.putExtra("current_uid", uid);
        intent.putExtra("message_id", msgLocalID);

        String screenInstanceID = BaseActivity.generateScreenInstanceID();
        String navigatorEventID = screenInstanceID + "_events";

        intent.putExtra("navigatorID", navigatorID);
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);

        startActivity(intent);
    }

    private String tokenizer(String key) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < key.length(); i++) {
            char c = key.charAt(i);
            builder.append(c);
            if (c >= 0x4e00 && c <= 0x9fff) {
                builder.append(' ');
            }
        }
        return builder.toString();
    }

    private void initSpannableMessage() {
        String t = tokenizer(searchKey);
        String[] keys = t.split(" ");

        for (int j = 0; j < keys.length; j++) {
            keys[j] = keys[j].trim();
        }

        messages = new ArrayList<>();

        for (int i = 0; i < conversation.messages.size(); i++) {
            IMessage msg = conversation.messages.get(i);
            if (msg.content.getType() == MessageContent.MessageType.MESSAGE_TEXT) {
                String text = ((Text) msg.content).text;
                SpannableString ss = new SpannableString(text);
                for (int j = 0; j < keys.length; j++) {
                    int pos = text.indexOf(keys[j]);
                    if (pos == -1) {
                        continue;
                    }
                    ss.setSpan(new ForegroundColorSpan(Color.rgb(82, 125, 96)),
                            pos, pos + keys[j].length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                messages.add(ss);
            } else {
                messages.add(new SpannableString(""));
            }
        }
    }
}
