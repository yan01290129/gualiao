package com.beetle.goubuli;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.beetle.goubuli.util.Log;


public class Suspension extends BroadcastReceiver {

    public static final String TAG = "这里接收悬浮广播";

    private Callback callback;
    private Context mContext;

    public interface Callback {
        public void downEvent();
        public void upEvent();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("suspensionAction")) {
            //接收到广播，取出里面携带的数据
            String str = intent.getStringExtra("data");
            if(str.equals("downEvent")){
                callback.downEvent();
            }
            if(str.equals("upEvent")){
                callback.upEvent();
            }
        }
        Log.w("接受到广播");
    }


}

