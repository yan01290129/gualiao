package com.beetle.goubuli;

import com.beetle.goubuli.model.Conversation;

import java.util.List;

public interface MessageApplication {
    void setConversations(List<Conversation> conversations);
    List<Conversation> getConversations();
}
