package com.beetle.goubuli;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by houxh on 2017/10/15.
 */

public class ParcelableObject implements android.os.Parcelable {
    private static HashMap<String, Object> params = new HashMap<>();

    private Object object;
    private String uuid;

    public ParcelableObject(Object obj) {
        this.object = obj;
        this.uuid = UUID.randomUUID().toString();
    }

    public ParcelableObject(Object obj, String uuid) {
        this.object = obj;
        this.uuid = uuid;
    }

    public Object getObject() {
        return this.object;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        params.put(uuid, object);
        dest.writeString(uuid);
    }

    public static final Parcelable.Creator<ParcelableObject> CREATOR = new Creator<ParcelableObject>() {
        @Override
        public ParcelableObject createFromParcel(Parcel source) {
            String uuid = source.readString();
            Object obj = null;
            if (params.containsKey(uuid)) {
                obj = params.get(uuid);
                params.remove(uuid);
            }
            ParcelableObject p = new ParcelableObject(obj, uuid);
            return p;
        }

        @Override
        public ParcelableObject[] newArray(int size) {
            return new ParcelableObject[size];
        }
    };

}
