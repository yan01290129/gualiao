package com.beetle.goubuli;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.Nullable;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.beetle.goubuli.tools.event.BadgeEvent;
import com.beetle.goubuli.tools.event.GroupConversationEvent;
import com.beetle.im.CustomerMessage;
import com.beetle.im.CustomerMessageObserver;
import com.beetle.im.GroupMessageObserver;
import com.beetle.im.IMMessage;
import com.beetle.im.IMService;
import com.beetle.im.IMServiceObserver;
import com.beetle.im.PeerMessageObserver;
import com.beetle.im.RTMessage;
import com.beetle.im.RTMessageObserver;
import com.beetle.im.SystemMessageObserver;
import com.beetle.bauhinia.api.IMHttpAPI;
import com.beetle.bauhinia.db.*;
import com.beetle.bauhinia.db.message.*;
import com.beetle.bauhinia.tools.NotificationCenter;
import com.beetle.conference.ConferenceActivity;
import com.beetle.conference.ConferenceCommand;
import com.beetle.goubuli.crypto.storage.SignalProtocolStoreImpl;
import com.beetle.goubuli.crypto.storage.TextSecureIdentityKeyStore;
import com.beetle.goubuli.crypto.storage.TextSecureSessionStore;
import com.beetle.goubuli.model.*;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.ConferenceEvent;
import com.beetle.goubuli.tools.event.ConversationEvent;
import com.beetle.goubuli.tools.event.SyncCompleteEvent;
import com.beetle.goubuli.view.ConversationView;
import com.beetle.voip.CallActivity;
import com.beetle.voip.VOIPActivity;
import com.beetle.voip.VOIPCommand;
import com.beetle.voip.VOIPVideoActivity;
import com.beetle.voip.VOIPVoiceActivity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.reactnativenavigation.react.NavigationApplication;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import org.whispersystems.libsignal.*;
import org.whispersystems.libsignal.ecc.Curve;
import org.whispersystems.libsignal.ecc.ECPublicKey;
import org.whispersystems.libsignal.protocol.CiphertextMessage;
import org.whispersystems.libsignal.state.PreKeyBundle;
import retrofit.RetrofitError;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import com.beetle.message.R;


public class ConversationActivity extends BaseActivity implements IMServiceObserver,
        RTMessageObserver,
        PeerMessageObserver, GroupMessageObserver,
        SystemMessageObserver, CustomerMessageObserver,
        AdapterView.OnItemClickListener,
        NotificationCenter.NotificationCenterObserver {
    private static final String TAG = "beetle";
    public static final int CALL_VIDEO = 200;
    public static final int CALL_VOICE = 201;

    public static boolean isAppBackground = true;

    private List<Contact> contacts = new ArrayList<>();

    private List<Conversation> conversations;
    private ListView lv;
    protected long currentUID = 0;

    private MediaPlayer messageAlarmPlayer;
    private int receivedTimestamp = 0;

    private ArrayList<String> channelIDs = new ArrayList<>();

    private static ConversationActivity instance = null;
    public static ConversationActivity getInstance() {
        if(instance == null){
            instance = new ConversationActivity();
        }
        return  instance;
    }

    // 动态更新会话列表联系人信息
    final Object messageHandler = new Object() {
        @Subscribe
        public void onSyncCompleteEvent(SyncCompleteEvent event) {
            Log.i(TAG, "onSyncCompleteEvent");
            ConversationActivity.this.contacts = ContactManager.getInstance().getAllContact();

            for (int i = 0; i < ConversationActivity.this.conversations.size(); i++) {
                Conversation conv = ConversationActivity.this.conversations.get(i);
                if (conv.type == Conversation.CONVERSATION_PEER ||
                        conv.type == Conversation.CONVERSATION_PEER_SECRET) {
                    updatePeerConversationName(conv);
                } else if (conv.type == Conversation.CONVERSATION_GROUP) {
                    updateGroupConversationName(conv);
                }
            }
        }

        @Subscribe
        public void onConerenceEvent(ConferenceEvent event) {
            if (event.groupID == 0) {
                return;
            }
            IMessage msg = new IMessage();
            msg.sender = 0;
            msg.receiver = event.groupID;
            GroupVOIP voip = GroupVOIP.newGroupVOIP(event.initiator, event.finished);
            msg.setContent(voip);
            msg.timestamp = now();

            GroupMessageDB.getInstance().insertMessage(msg, event.groupID);
            ConversationActivity.this.onNewGroupMessage(msg);
        }

        @Subscribe
        public void onConversationEvent(ConversationEvent event) {
            if (event.secret) {
                startPeerSecretConversation(event.peer, event.sessionConnected);
            } else {
                startPeerConversation(event.peer, event.name);
            }
        }

        @Subscribe
        public void onGroupConversationEvent(GroupConversationEvent event) {
            startGroupConversation(event.groupId, event.name);
        }
    };


    private BaseAdapter adapter;
    class ConversationAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return conversations.size();
        }
        @Override
        public Object getItem(int position) {
            return conversations.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ConversationView view = null;
            if (convertView == null) {
                view = new ConversationView(ConversationActivity.this);
            } else {
                view = (ConversationView)convertView;
            }
            Conversation c = conversations.get(position);
            view.setConversation(c);;
            view.setTag(c);
            return view;
        }
    }

    private void initWidget() {
        lv = (ListView) findViewById(R.id.list);
        adapter = new ConversationAdapter();
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);

        registerForContextMenu(lv);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.list) {
            menu.setHeaderTitle("选择操作");
            menu.add("删除");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        final Conversation conv = conversations.get(info.position);
        if (menuItemIndex == 0) {
            Log.i(TAG, "delete conv:" + conv.cid);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("删除后，将清空该聊天的消息记录");
            builder.setPositiveButton("删除", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    deleteConversation(conv);
                }
            });
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_conversation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            Intent intent = new Intent();
            intent.putExtra("contacts", new ParcelableObject(contacts));
            intent.setClass(this, SearchActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.new_group) {
            this.onNewGroup();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        Log.i(TAG, "main activity create..." + ":" + ConversationActivity.this);

        setContentView(R.layout.activity_conversation);

        getSupportActionBar().setTitle(Profile.getInstance().organizationName);

        Intent intent = getIntent();

        currentUID = intent.getLongExtra("current_uid", 0);
        if (currentUID == 0) {
            Log.e(TAG, "current uid is 0");
            return;
        }

        try {
            Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);//用于获取手机默认铃声的Uri
            messageAlarmPlayer = new MediaPlayer();
            messageAlarmPlayer.setDataSource(this, alert);
            messageAlarmPlayer.setAudioStreamType(AudioManager.STREAM_RING);
            messageAlarmPlayer.setLooping(false);
            messageAlarmPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
            messageAlarmPlayer = null;
        }



        contacts = ContactManager.getInstance().getAllContact();

        IMService im =  IMService.getInstance();
        im.addObserver(this);
        im.addPeerObserver(this);
        im.addGroupObserver(this);
        im.addCustomerServiceObserver(this);
        im.addSystemObserver(this);
        im.addRTObserver(this);

        loadConversations();
        initWidget();

        NotificationCenter nc = NotificationCenter.defaultCenter();
        nc.addObserver(this, PeerMessageActivity.SEND_MESSAGE_NAME);
        nc.addObserver(this, PeerMessageActivity.CLEAR_MESSAGES);
        nc.addObserver(this, PeerMessageActivity.CLEAR_NEW_MESSAGES);

        nc.addObserver(this, PeerMessageActivity.SEND_SECRET_MESSAGE_NAME);
        nc.addObserver(this, PeerMessageActivity.CLEAR_SECRET_MESSAGES);
        nc.addObserver(this, PeerMessageActivity.CLEAR_SECRET_NEW_MESSAGES);

        nc.addObserver(this, GroupMessageActivity.SEND_MESSAGE_NAME);
        nc.addObserver(this, GroupMessageActivity.CLEAR_MESSAGES);
        nc.addObserver(this, GroupMessageActivity.CLEAR_NEW_MESSAGES);

        nc.addObserver(this, CustomerMessageActivity.SEND_MESSAGE_NAME);
        nc.addObserver(this, CustomerMessageActivity.CLEAR_MESSAGES);
        nc.addObserver(this, CustomerMessageActivity.CLEAR_NEW_MESSAGES);


        BusProvider.getInstance().register(messageHandler);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        IMService im =  IMService.getInstance();
        im.removeObserver(this);
        im.removePeerObserver(this);
        im.removeGroupObserver(this);
        im.removeCustomerServiceObserver(this);
        im.removeSystemObserver(this);
        im.removeRTObserver(this);
        NotificationCenter nc = NotificationCenter.defaultCenter();
        nc.removeObserver(this);

        BusProvider.getInstance().unregister(messageHandler);
        Log.i(TAG, "message list activity destroyed" + ":" + ConversationActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "MessageListActivity onResume"  + ":" + ConversationActivity.this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "MessageListActivity onStop"  + ":" + ConversationActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "MessageListActivity onPause"  + ":" + ConversationActivity.this);
    }

    private void onNewGroup() {
        WritableArray users = Arguments.createArray();
        for (int i = 0; i < contacts.size(); i++) {
            Contact c = contacts.get(i);
            WritableMap map = Arguments.createMap();
            map.putString("name", c.getName());
            map.putDouble("uid", c.getContactId());
            map.putDouble("id", c.getContactId());
            users.pushMap(map);
        }

        Profile p = Profile.getInstance();
        WritableMap profile = Arguments.createMap();
        profile.putDouble("uid", p.uid);
        profile.putString("name",  p.name != null ? p.name : "");
        profile.putString("avatar", p.avatar != null ? p.avatar : "");
//        profile.putString("accessToken", Token.getInstance().accessToken);
        profile.putString("gobelieveToken", Token.getInstance().gobelieveToken);

        WritableMap map = Arguments.createMap();
        map.putArray("users", users);
        map.putString("navigatorID", navigatorID);
        map.putMap("profile", profile);

        ReactContext reactContext = NavigationApplication.instance.getReactGateway().getReactContext();
        this.sendEvent(reactContext, "create_group", map);
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    public  String messageContentToString(MessageContent content) {
        if (content instanceof Text) {
            return ((Text) content).text;
        } else if (content instanceof Image) {
            return "一张图片";
        } else if (content instanceof Audio) {
            return "一段语音";
        } else if (content instanceof File) {
            return "一个文件";
        } else if (content instanceof Video) {
            return "一个视频";
        } else if (content instanceof Notification) {
            return ((Notification) content).description;
        } else if (content instanceof Location) {
            return "一个地理位置";
        } else if (content instanceof GroupVOIP) {
            return ((GroupVOIP) content).description;
        } else if (content instanceof VOIP) {
            VOIP voip = (VOIP) content;
            if (voip.videoEnabled) {
                return "视频聊天";
            } else {
                return "语音聊天";
            }
        } else if (content instanceof Secret) {
            return "消息未能解密";
        } else if (content instanceof P2PSession) {
            return "";
        } else {
            return "未知的消息类型";
        }
    }

    void updateConversationDetail(Conversation conv) {
        if (conv.message != null) {
            String detail = messageContentToString(conv.message.content);
            conv.setDetail(detail);
        }

        if (conv.type == Conversation.CONVERSATION_PEER_SECRET &&
                TextUtils.isEmpty(conv.getDetail())) {
            if (conv.state == Conversation.STATE_WAIT) {
                conv.setDetail("等待对方上线");
            } else if (conv.state == Conversation.STATE_CONNECTED) {
                conv.setDetail("加密会话已经创建成功");
            } else if (conv.state == Conversation.STATE_UNINITIALIZE) {
                conv.setDetail("加密会话未初始化");
            }
        }
    }

    void updatePeerConversationName(Conversation conv) {
        User u = getUser(conv.cid);
        if (TextUtils.isEmpty(u.name)) {
            conv.setName(u.identifier);
            final Conversation fconv = conv;
            asyncGetUser(conv.cid, new GetUserCallback() {
                @Override
                public void onUser(User u) {
                    fconv.setName(u.name);
                    fconv.setAvatar(u.avatarURL);
                }
            });
        } else {
            conv.setName(u.name);
        }
        conv.setAvatar(u.avatarURL);
    }

    void updateGroupConversationName(Conversation conv) {
        Group g = getGroup(conv.cid);
        if (TextUtils.isEmpty(g.topic)) {
            conv.setName(g.identifier);
            final Conversation fconv = conv;
            asyncGetGroup(conv.cid, new GetGroupCallback() {
                @Override
                public void onGroup(Group g) {
                    fconv.setName(g.topic);
                    fconv.setAvatar(g.avatarURL);
                }
            });
        } else {
            conv.setName(g.topic);
        }
        conv.setAvatar(g.avatarURL);
    }

    void loadConversations() {
        Log.i(TAG, "load conversations");

        conversations = ConversationDB.getInstance().getConversations();
        for (Conversation conv : conversations) {
            if (conv.type == Conversation.CONVERSATION_PEER) {
                IMessage msg = PeerMessageDB.getInstance().getLastMessage(conv.cid);
                conv.message = msg;

                updatePeerConversationName(conv);
                updateNotificationDesc(conv);
                updateConversationDetail(conv);
            } else if (conv.type == Conversation.CONVERSATION_PEER_SECRET) {
                IMessage msg = EPeerMessageDB.getInstance().getLastMessage(conv.cid);
                conv.message = msg;

                updatePeerConversationName(conv);
                updateNotificationDesc(conv);
                updateConversationDetail(conv);
            } else if (conv.type == Conversation.CONVERSATION_GROUP) {
                IMessage msg = GroupMessageDB.getInstance().getLastMessage(conv.cid);
                conv.message = msg;

                updateGroupConversationName(conv);
                updateNotificationDesc(conv);
                updateConversationDetail(conv);
            } else if (conv.type == Conversation.CONVERSATION_CUSTOMER_SERVICE) {
                if (conv.cid != Config.STORE_ID) {
                    continue;
                }
                IMessage msg = CustomerMessageDB.getInstance().getLastMessage(conv.cid);
                conv.message = msg;

                conv.setName("上城通讯团队");
                updateNotificationDesc(conv);
                updateConversationDetail(conv);
            }
        }

        Comparator<Conversation> cmp = new Comparator<Conversation>() {
            public int compare(Conversation c1, Conversation c2) {

                int t1 = 0;
                int t2 = 0;
                if (c1.message != null) {
                    t1 = c1.message.timestamp;
                }
                if (c2.message != null) {
                    t2 = c2.message.timestamp;
                }

                if (t1 > t2) {
                    return -1;
                } else if (t1 == t2) {
                    return 0;
                } else {
                    return 1;
                }

            }
        };
        Collections.sort(conversations, cmp);

        MessageApplication app = (MessageApplication)getApplication();
        app.setConversations(conversations);

        int count = 0;
        for (Conversation conv : conversations) {
            count += conv.getUnreadCount();
        }
        BusProvider.getInstance().post(new BadgeEvent(count));
    }

    void deleteConversation(Conversation conv) {
        if (conv.type == Conversation.CONVERSATION_PEER_SECRET) {
            TextSecureSessionStore sessionStore = new TextSecureSessionStore(this);
            SignalProtocolAddress address = new SignalProtocolAddress("" + conv.cid, 1);
            if (sessionStore.containsSession(address)) {
                sessionStore.deleteSession(address);
            }
            EPeerMessageDB.getInstance().clearCoversation(conv.cid);
        } else if (conv.type == Conversation.CONVERSATION_PEER) {
            PeerMessageDB.getInstance().clearCoversation(conv.cid);
        } else if (conv.type == Conversation.CONVERSATION_GROUP) {
            GroupMessageDB.getInstance().clearCoversation(conv.cid);
        } else if (conv.type == Conversation.CONVERSATION_CUSTOMER_SERVICE) {
            CustomerMessageDB.getInstance().clearCoversation(conv.cid);
        }

        ConversationDB.getInstance().removeConversation(conv);
        conversations.remove(conv);
        adapter.notifyDataSetChanged();
    }

    void alarmMessage() {
        int now = now();
        if (now - this.receivedTimestamp > 1) {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            if (vibrator.hasVibrator()) {
                vibrator.vibrate(new long[]{100, 200, 100, 200}, -1);
            }
            if (messageAlarmPlayer != null && !messageAlarmPlayer.isPlaying()) {
                messageAlarmPlayer.seekTo(0);
                messageAlarmPlayer.start();
            }
            this.receivedTimestamp = now;
        }
    }

    public static class User {
        public long uid;
        public String name;
        public String avatarURL;

        //name为nil时，界面显示identifier字段
        public String identifier;
    }

    public interface GetUserCallback {
        void onUser(User u);
    }

    public interface GetGroupCallback {
        void onGroup(Group g);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Conversation conv = conversations.get(position);
        Log.i(TAG, "conv:" + conv.getName());

        if (conv.type == Conversation.CONVERSATION_PEER) {
            onPeerClick(conv.cid, conv.getName(), conv.getAvatar(), false, conv.state);
        } else if (conv.type == Conversation.CONVERSATION_GROUP){
            startGroupConversation(conv.cid, conv.getName());
        } else if (conv.type == Conversation.CONVERSATION_CUSTOMER_SERVICE) {
            onStoreClick(conv.cid, conv.getName());
        } else if (conv.type == Conversation.CONVERSATION_PEER_SECRET) {
            onPeerClick(conv.cid, conv.getName(), conv.getAvatar(), true, conv.state);
        }
    }

    @Override
    public void onConnectState(IMService.ConnectState state) {
        Log.i(TAG, "content state:" + state);
    }



    public void onNewCustomerMessage(ICustomerMessage msg) {
        long cid = msg.storeID;
        int pos = findConversationPosition(cid, Conversation.CONVERSATION_CUSTOMER_SERVICE);
        Conversation conversation = null;
        if (pos == -1) {
            conversation = newCustomerConversation(cid);
        } else {
            conversation = conversations.get(pos);
        }

        conversation.message = msg;
        updateNotificationDesc(conversation);
        updateConversationDetail(conversation);

        if (msg.isSupport) {
            int unread = conversation.getUnreadCount() + 1;
            conversation.setUnreadCount(unread);
            ConversationDB.getInstance().setNewCount(conversation.rowid, unread);
        }

        if (pos == -1) {
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else if (pos > 0) {
            conversations.remove(pos);
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else {
            //pos == 0
        }

        if (msg.isSupport) {
            int count = 0;
            for (Conversation conv : conversations) {
                count += conv.getUnreadCount();
            }
            BusProvider.getInstance().post(new BadgeEvent(count));
        }
    }



    public void onNewPeerMessage(IMessage msg) {
        long cid = 0;
        if (msg.sender == this.currentUID) {
            cid = msg.receiver;
        } else {
            cid = msg.sender;
        }

        int pos = findConversationPosition(cid, Conversation.CONVERSATION_PEER);
        Conversation conversation = null;
        if (pos == -1) {
            conversation = newPeerConversation(cid);
        } else {
            conversation = conversations.get(pos);
        }

        conversation.message = msg;
        updateNotificationDesc(conversation);
        updateConversationDetail(conversation);

        if (msg.sender != Token.getInstance().uid) {
            int unread = conversation.getUnreadCount() + 1;
            conversation.setUnreadCount(unread);
            ConversationDB.getInstance().setNewCount(conversation.rowid, unread);
        }

        if (pos == -1) {
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else if (pos > 0) {
            conversations.remove(pos);
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else {
            //pos == 0
        }

        if (msg.sender != Token.getInstance().uid) {
            int count = 0;
            for (Conversation conv : conversations) {
                count += conv.getUnreadCount();
            }

            BusProvider.getInstance().post(new BadgeEvent(count));
        }

    }

    public void onNewPeerSecretMessage(IMessage msg) {
        long cid = 0;
        if (msg.sender == this.currentUID) {
            cid = msg.receiver;
        } else {
            cid = msg.sender;
        }

        int pos = findConversationPosition(cid, Conversation.CONVERSATION_PEER_SECRET);
        Conversation conversation = null;
        if (pos == -1) {
            conversation = newPeerSecretConversation(cid);
        } else {
            conversation = conversations.get(pos);
        }

        conversation.message = msg;
        updateNotificationDesc(conversation);
        updateConversationDetail(conversation);

        if (msg.sender != Token.getInstance().uid) {
            int unread = conversation.getUnreadCount() + 1;
            conversation.setUnreadCount(unread);
            ConversationDB.getInstance().setNewCount(conversation.rowid, unread);
        }

        if (pos == -1) {
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else if (pos > 0) {
            conversations.remove(pos);
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else {
            //pos == 0
        }

        if (msg.sender != Token.getInstance().uid) {
            int count = 0;
            for (Conversation conv : conversations) {
                count += conv.getUnreadCount();
            }
            BusProvider.getInstance().post(new BadgeEvent(count));
        }

    }

    @Override
    public void onRTMessage(RTMessage rt) {

        if (Token.getInstance().uid == 0) {
            return;
        }

        if (Token.getInstance().uid != rt.receiver) {
            return;
        }

        if (VOIPActivity.activityCount > 0) {
            return;
        }

        if (ConferenceActivity.activityCount > 0) {
            return;
        }


        try {
            JSONObject json = new JSONObject(rt.content);
            if (json.has("conference")) {
                JSONObject obj = json.getJSONObject("conference");
                ConferenceCommand confCommand = new ConferenceCommand(obj);

                if (ConferenceActivity.channelIDs.contains(confCommand.channelID)) {
                    return;
                }

                if (confCommand.command.equals(ConferenceCommand.COMMAND_INVITE)) {
                    if (ConferenceActivity.channelIDs.size() >= 100) {
                        ConferenceActivity.channelIDs.remove(0);
                    }
                    ConferenceActivity.channelIDs.add(confCommand.channelID);

                    WritableArray participants = Arguments.createArray();
                    List<Contact> contacts = ContactManager.getInstance().getAllContact();
                    for (int i = 0; i < confCommand.participants.length; i++) {
                        long uid = confCommand.participants[i];
                        User u = this.getUser(uid);
                        String name = TextUtils.isEmpty(u.name) ? u.identifier : u.name;
                        String avatar = u.avatarURL;
                        WritableMap map = Arguments.createMap();
                        map.putDouble("uid", uid);
                        map.putString("name", name);
                        map.putString("avatar", avatar);
                        participants.pushMap(map);
                    }

                    WritableArray users = Arguments.createArray();
                    if (confCommand.groupID > 0) {
                        ArrayList<GroupMember> members = GroupDB.getInstance().loadGroupMember(confCommand.groupID);

                        for (int i = 0; i < members.size(); i++) {
                            GroupMember m = members.get(i);

                            String name = m.nickname;
                            if (TextUtils.isEmpty(m.nickname)) {
                                name = "" + m.uid;
                                for (Contact c : contacts) {
                                    if (c.getContactId() == m.uid) {
                                        name = c.getName();
                                        break;
                                    }
                                }
                            }
                            WritableMap userMap = Arguments.createMap();
                            userMap.putDouble("uid", m.uid);
                            userMap.putString("name", name);
                            users.pushMap(userMap);
                        }
                    }

                    WritableMap map = Arguments.createMap();
                    map.putString("channelID", confCommand.channelID);
                    map.putDouble("initiator", confCommand.initiator);
                    map.putBoolean("isInitiator", false);
                    map.putDouble("uid", Token.getInstance().uid);
                    map.putArray("participants", participants);
                    map.putDouble("groupID", confCommand.groupID);
                    map.putString("token", Token.getInstance().gobelieveToken);
                    map.putArray("users", users);

                    ReactContext reactContext = NavigationApplication.instance.getReactGateway().getReactContext();
                    this.sendEvent(reactContext, "group_call", map);
                }
            } else if (json.has("voip")) {
                JSONObject obj = json.getJSONObject("voip");
                VOIPCommand command = new VOIPCommand(obj);

                if (channelIDs.contains(command.channelID)) {
                    return;
                }
                if (command.cmd == VOIPCommand.VOIP_COMMAND_DIAL) {
                    //只保留最近100次通话的ID
                    if (channelIDs.size() >= 100) {
                        channelIDs.remove(0);
                    }
                    channelIDs.add(command.channelID);

                    User u = this.getUser(rt.sender);
                    String name = TextUtils.isEmpty(u.name) ? u.identifier : u.name;
                    String avatar = u.avatarURL;

                    Intent intent = new Intent(this, VOIPVoiceActivity.class);
                    intent.putExtra("peer_uid", rt.sender);
                    intent.putExtra("peer_name", name);
                    intent.putExtra("peer_avatar", avatar);
                    intent.putExtra("current_uid", Token.getInstance().uid);
                    intent.putExtra("token", Token.getInstance().gobelieveToken);
                    intent.putExtra("is_caller", false);
                    intent.putExtra("channel_id", command.channelID);
                    startActivityForResult(intent, CALL_VOICE);
                } else if (command.cmd == VOIPCommand.VOIP_COMMAND_DIAL_VIDEO) {
                    //只保留最近100次通话的ID
                    if (channelIDs.size() >= 100) {
                        channelIDs.remove(0);
                    }
                    channelIDs.add(command.channelID);

                    User u = getUser(rt.sender);
                    String name = TextUtils.isEmpty(u.name) ? u.identifier : u.name;
                    String avatar = u.avatarURL;

                    Intent intent = new Intent(this, VOIPVideoActivity.class);
                    intent.putExtra("peer_uid", rt.sender);
                    intent.putExtra("peer_name", name);
                    intent.putExtra("peer_avatar", avatar);
                    intent.putExtra("current_uid", Token.getInstance().uid);
                    intent.putExtra("token", Token.getInstance().gobelieveToken);
                    intent.putExtra("is_caller", false);
                    intent.putExtra("channel_id", command.channelID);
                    startActivityForResult(intent, CALL_VIDEO);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPeerMessage(IMMessage msg) {
        Log.i(TAG, "on peer message");
        IMessage imsg = new IMessage();
        imsg.timestamp = now();
        imsg.msgLocalID = msg.msgLocalID;
        imsg.sender = msg.sender;
        imsg.receiver = msg.receiver;
        imsg.isOutgoing = msg.sender == currentUID;
        imsg.setContent(msg.content);

        this.onNewPeerMessage(imsg);
        if (!imsg.isOutgoing) {
            if (!isAppBackground) {
                this.alarmMessage();
            }
        }
    }

    @Override
    public void onPeerSecretMessage(IMMessage msg) {
        Log.i(TAG, "on peer secret message");
        IMessage imsg = new IMessage();
        imsg.timestamp = now();
        imsg.msgLocalID = msg.msgLocalID;
        imsg.sender = msg.sender;
        imsg.receiver = msg.receiver;
        imsg.isOutgoing = msg.sender == currentUID;
        imsg.secret = true;
        imsg.setContent(msg.content);

        if (msg.isSelf) {
            return;
        }
        if (!imsg.isOutgoing && imsg.getType() == MessageContent.MessageType.MESSAGE_P2P_SESSION) {
            int pos = findConversationPosition(msg.sender, Conversation.CONVERSATION_PEER_SECRET);
            if (pos == -1) {
                return;
            }

            Conversation conv = conversations.get(pos);
            if (conv.state == Conversation.STATE_WAIT) {
                conv.state = Conversation.STATE_CONNECTED;
                ConversationDB.getInstance().setState(conv.rowid, conv.state);
            }
        }


        this.onNewPeerSecretMessage(imsg);
        if (!imsg.isOutgoing) {
            if (!isAppBackground) {
                this.alarmMessage();
            }
        }
    }

    public Conversation findConversation(long cid, int type) {
        for (int i = 0; i < conversations.size(); i++) {
            Conversation conv = conversations.get(i);
            if (conv.cid == cid && conv.type == type) {
                return conv;
            }
        }
        return null;
    }

    public int findConversationPosition(long cid, int type) {
        for (int i = 0; i < conversations.size(); i++) {
            Conversation conv = conversations.get(i);
            if (conv.cid == cid && conv.type == type) {
                return i;
            }
        }
        return -1;
    }

    public Conversation newPeerConversation(long cid) {
        Conversation conversation = new Conversation();
        conversation.type = Conversation.CONVERSATION_PEER;
        conversation.cid = cid;

        updatePeerConversationName(conversation);

        ConversationDB.getInstance().addConversation(conversation);
        return conversation;
    }

    public Conversation newPeerSecretConversation(long cid) {
        return newPeerSecretConversation(cid, Conversation.STATE_UNINITIALIZE);
    }

    public Conversation newPeerSecretConversation(long cid, int state) {
        Conversation conversation = new Conversation();
        conversation.type = Conversation.CONVERSATION_PEER_SECRET;
        conversation.cid = cid;
        conversation.state = state;

        updatePeerConversationName(conversation);
        ConversationDB.getInstance().addConversation(conversation);
        return conversation;
    }

    public Conversation newGroupConversation(long cid) {
        Conversation conversation = new Conversation();
        conversation.type = Conversation.CONVERSATION_GROUP;
        conversation.cid = cid;
        updateGroupConversationName(conversation);
        ConversationDB.getInstance().addConversation(conversation);
        return conversation;
    }

    public Conversation newCustomerConversation(long cid) {
        Conversation conversation = new Conversation();
        conversation.type = Conversation.CONVERSATION_CUSTOMER_SERVICE;
        conversation.cid = cid;
        conversation.setName("上城通讯团队");
        ConversationDB.getInstance().addConversation(conversation);
        return conversation;
    }

    public static int now() {
        Date date = new Date();
        long t = date.getTime();
        return (int)(t/1000);
    }
    @Override
    public void onPeerMessageACK(IMMessage im) {
        Log.i(TAG, "message ack on main");

        int msgLocalID = im.msgLocalID;
        long uid = im.receiver;
        if (msgLocalID == 0) {
            MessageContent c = IMessage.fromRaw(im.plainContent);
            if (c.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                Revoke r = (Revoke)c;
                int pos = -1;
                if (!im.secret) {
                    pos = findConversationPosition(uid, Conversation.CONVERSATION_PEER);
                } else {
                    pos = findConversationPosition(uid, Conversation.CONVERSATION_PEER_SECRET);
                }
                Conversation conversation = conversations.get(pos);
                if (r.msgid.equals(conversation.message.getUUID())) {
                    conversation.message.setContent(r);
                    updateNotificationDesc(conversation);
                    updateConversationDetail(conversation);
                }
            }
        }
    }

    @Override
    public void onPeerMessageFailure(IMMessage im) {
    }

    public void onNewGroupMessage(IMessage msg) {
        int unread = 0;
        if (msg.getType() != MessageContent.MessageType.MESSAGE_GROUP_VOIP &&
                msg.sender != Token.getInstance().uid) {
            unread = 1;
        }

        onNewGroupMessage(msg, unread);
    }

    public void onNewGroupMessage(IMessage msg, int unread) {
        int pos = findConversationPosition(msg.receiver, Conversation.CONVERSATION_GROUP);
        Conversation conversation = null;
        if (pos == -1) {
            conversation = newGroupConversation(msg.receiver);
        } else {
            conversation = conversations.get(pos);
        }

        conversation.message = msg;
        updateNotificationDesc(conversation);
        updateConversationDetail(conversation);

        if (unread > 0) {
            int newUnread = conversation.getUnreadCount() + unread;
            conversation.setUnreadCount(newUnread);
            ConversationDB.getInstance().setNewCount(conversation.rowid, newUnread);
        }

        if (pos == -1) {
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else if (pos > 0) {
            conversations.remove(pos);
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else {
            //pos == 0
        }

        if (unread > 0) {
            int count = 0;
            for (Conversation conv : conversations) {
                count += conv.getUnreadCount();
            }
            BusProvider.getInstance().post(new BadgeEvent(count));
        }
    }

    @Override
    public void onGroupMessages(List<IMMessage> msgs) {
        HashMap<Long, Integer> unreadDict = new HashMap<>();
        HashMap<Long, IMMessage> msgDict = new HashMap<>();

        for (IMMessage msg : msgs) {
            int count = 0;
            if (unreadDict.containsKey(msg.receiver)) {
                count = (Integer)unreadDict.get(msg.receiver);
            }
            if (msg.sender != this.currentUID) {
                count += 1;
            }

            unreadDict.put(msg.receiver, count);
            msgDict.put(msg.receiver, msg);
        }

        Iterator iter = msgDict.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<Long, IMMessage> entry = (Map.Entry<Long, IMMessage>)iter.next();

            IMMessage im = entry.getValue();
            Long groupId = entry.getKey();
            int unread = unreadDict.get(groupId);
            onGroupMessage(im, unread);

        }

    }

    public void onGroupMessage(IMMessage msg, int unread) {
        Log.i(TAG, "on group message");
        IMessage imsg = new IMessage();
        imsg.timestamp = msg.timestamp;
        imsg.msgLocalID = msg.msgLocalID;
        imsg.sender = msg.sender;
        imsg.receiver = msg.receiver;
        imsg.isOutgoing = msg.sender == currentUID;
        imsg.setContent(msg.content);

        onNewGroupMessage(imsg, unread);
        if (!imsg.isOutgoing) {
            if (!isAppBackground) {
                this.alarmMessage();
            }
        }
    }

    @Override
    public void onGroupMessageACK(IMMessage im) {
        int msgLocalID = im.msgLocalID;
        long gid = im.receiver;
        if (msgLocalID == 0) {
            MessageContent c = IMessage.fromRaw(im.content);
            if (c.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                Revoke r = (Revoke)c;
                int pos = -1;
                pos = findConversationPosition(gid, Conversation.CONVERSATION_GROUP);
                Conversation conversation = conversations.get(pos);
                if (r.msgid.equals(conversation.message.getUUID())) {
                    conversation.message.setContent(r);
                    updateNotificationDesc(conversation);
                    updateConversationDetail(conversation);
                }
            }
        }
    }

    @Override
    public void onGroupMessageFailure(IMMessage im) {

    }

    @Override
    public void onGroupNotification(String text) {
        GroupNotification groupNotification = GroupNotification.newGroupNotification(text);

        if (groupNotification.notificationType == GroupNotification.NOTIFICATION_GROUP_CREATED) {
            onGroupCreated(groupNotification);
        } else if (groupNotification.notificationType == GroupNotification.NOTIFICATION_GROUP_DISBAND) {
            onGroupDisband(groupNotification);
        } else if (groupNotification.notificationType == GroupNotification.NOTIFICATION_GROUP_MEMBER_ADDED) {
            onGroupMemberAdd(groupNotification);
        } else if (groupNotification.notificationType == GroupNotification.NOTIFICATION_GROUP_MEMBER_LEAVED) {
            onGroupMemberLeave(groupNotification);
        } else if (groupNotification.notificationType == GroupNotification.NOTIFICATION_GROUP_NAME_UPDATED) {
            onGroupNameUpdate(groupNotification);
        } else if (groupNotification.notificationType == GroupNotification.NOTIFICATION_GROUP_NOTICE_UPDATED) {
            onGroupNoticeUpdate(groupNotification);
        } else {
            Log.i(TAG, "unknown notification");
            return;
        }


        IMessage imsg = new IMessage();
        imsg.sender = 0;
        imsg.receiver = groupNotification.groupID;
        imsg.timestamp = groupNotification.timestamp;
        imsg.setContent(groupNotification);

        onNewGroupMessage(imsg);
        if (groupNotification.notificationType == GroupNotification.NOTIFICATION_GROUP_NAME_UPDATED) {
            int pos = findConversationPosition(imsg.receiver, Conversation.CONVERSATION_GROUP);
            if (pos == -1) {
                return;
            }
            Conversation conv = conversations.get(pos);
            conv.setName(groupNotification.groupName);
        }
    }

    private void onGroupCreated(GroupNotification notification) {
        GroupDB db = GroupDB.getInstance();
        Group group = new Group();
        group.groupID = notification.groupID;
        group.topic = notification.groupName;
        group.master = notification.master;
        group.disbanded = false;
        group.setMembers(notification.members);

        db.addGroup(group);
    }

    private void onGroupDisband(GroupNotification notification) {
        GroupDB db = GroupDB.getInstance();
        db.disbandGroup(notification.groupID);
    }

    private void onGroupMemberAdd(GroupNotification notification) {
        if (notification.member == Profile.getInstance().uid) {
            GroupDB.getInstance().joinGroup(notification.groupID);
        }
        GroupDB.getInstance().addGroupMember(notification.groupID, notification.member);
    }

    private void onGroupMemberLeave(GroupNotification notification) {
        if (notification.member == Profile.getInstance().uid) {
            GroupDB.getInstance().leaveGroup(notification.groupID);
        }
        GroupDB.getInstance().removeGroupMember(notification.groupID, notification.member);
    }

    private void onGroupNameUpdate(GroupNotification notification) {
        GroupDB.getInstance().setGroupTopic(notification.groupID, notification.groupName);
    }

    private void onGroupNoticeUpdate(GroupNotification notification) {
        GroupDB.getInstance().setGroupNotice(notification.groupID, notification.notice);
    }

    private void updateNotificationDesc(Conversation conv) {
        final IMessage imsg = conv.message;
        if (imsg == null) {
            return;
        }

        if (imsg.getType() == MessageContent.MessageType.MESSAGE_GROUP_NOTIFICATION) {
            long currentUID = this.currentUID;
            GroupNotification notification = (GroupNotification) imsg.content;
            if (notification.notificationType == GroupNotification.NOTIFICATION_GROUP_CREATED) {
                if (notification.master == currentUID) {
                    notification.description = String.format("您创建了\"%s\"群组", notification.groupName);
                } else {
                    notification.description = String.format("您加入了\"%s\"群组", notification.groupName);
                }
            } else if (notification.notificationType == GroupNotification.NOTIFICATION_GROUP_DISBAND) {
                notification.description = "群组已解散";
            } else if (notification.notificationType == GroupNotification.NOTIFICATION_GROUP_MEMBER_ADDED) {
                User u = getUser(notification.member);
                if (!TextUtils.isEmpty(u.name)) {
                    notification.description = String.format("\"%s\"加入群", u.name);
                } else {
                    notification.description = String.format("\"%s\"加入群", notification.memberName);
                }
            } else if (notification.notificationType == GroupNotification.NOTIFICATION_GROUP_MEMBER_LEAVED) {
                User u = getUser(notification.member);
                if (!TextUtils.isEmpty(u.name)) {
                    notification.description = String.format("\"%s\"离开群", u.name);
                } else {
                    notification.description = String.format("\"%s\"离开群", notification.memberName);
                }
            } else if (notification.notificationType == GroupNotification.NOTIFICATION_GROUP_NAME_UPDATED) {
                notification.description = String.format("群组改名为\"%s\"", notification.groupName);
            } else if (notification.notificationType == GroupNotification.NOTIFICATION_GROUP_NOTICE_UPDATED) {
                notification.description = String.format("群公告:%s", notification.notice);
            }
        } else if (imsg.getType() == MessageContent.MessageType.MESSAGE_GROUP_VOIP) {
            GroupVOIP groupVOIP = (GroupVOIP)imsg.content;

            if (!groupVOIP.finished) {
                User u = this.getUser(groupVOIP.initiator);
                String name = !TextUtils.isEmpty(u.name) ? u.name : u.identifier;
                groupVOIP.description = String.format("%s发起了语音聊天", name);
            } else {
                groupVOIP.description = "语音聊天已结束";
            }
        } else if (imsg.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
            Revoke revoke = (Revoke)imsg.content;
            if (conv.type == Conversation.CONVERSATION_CUSTOMER_SERVICE) {
                ICustomerMessage cm = (ICustomerMessage)imsg;
                if (cm.isSupport) {
                    revoke.description = "上城通讯团队撤回了一条消息";
                } else {
                    revoke.description = "你撤回了一条消息";
                }
            } else {
                if (imsg.isOutgoing) {
                    revoke.description = "你撤回了一条消息";
                } else {
                    User u = getUser(imsg.sender);
                    String name = !TextUtils.isEmpty(u.name) ? u.name : u.identifier;
                    revoke.description = String.format("\"%s\"撤回了一条消息", name);
                }
            }
        }
    }

    @Override
    public void onNotification(com.beetle.bauhinia.tools.Notification notification) {

        if (notification.name.equals(PeerMessageActivity.CLEAR_NEW_MESSAGES)) {
            Long peerUID = (Long) notification.obj;
            Conversation conversation = findConversation(peerUID, Conversation.CONVERSATION_PEER);
            if (conversation != null && conversation.getUnreadCount() > 0) {
                conversation.setUnreadCount(0);
                ConversationDB.getInstance().setNewCount(conversation.rowid, 0);

                int count = 0;
                for (Conversation conv1 : conversations) {
                    count += conv1.getUnreadCount();
                }
                BusProvider.getInstance().post(new BadgeEvent(count));
            }
        } else if (notification.name.equals(PeerMessageActivity.SEND_MESSAGE_NAME)) {
            IMessage imsg = (IMessage) notification.obj;
            if (imsg.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                return;
            }
            int pos = findConversationPosition(imsg.receiver, Conversation.CONVERSATION_PEER);
            Conversation conversation = null;
            if (pos == -1) {
                conversation = newPeerConversation(imsg.receiver);
            } else {
                conversation = conversations.get(pos);
            }

            conversation.message = (IMessage) imsg.clone();
            updateNotificationDesc(conversation);
            updateConversationDetail(conversation);

            if (pos == -1) {
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else if (pos > 0){
                conversations.remove(pos);
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else {
                //pos == 0
            }

        } else if (notification.name.equals(PeerMessageActivity.CLEAR_MESSAGES)) {
            Long peerUID = (Long)notification.obj;
            Conversation conversation = findConversation(peerUID, Conversation.CONVERSATION_PEER);
            if (conversation != null) {
                conversations.remove(conversation);
                adapter.notifyDataSetChanged();
            }
        } else if (notification.name.equals(PeerMessageActivity.CLEAR_SECRET_NEW_MESSAGES)) {
            Long peerUID = (Long) notification.obj;
            Conversation conversation = findConversation(peerUID, Conversation.CONVERSATION_PEER_SECRET);
            if (conversation != null && conversation.getUnreadCount() > 0) {
                conversation.setUnreadCount(0);
                ConversationDB.getInstance().setNewCount(conversation.rowid, 0);

                int count = 0;
                for (Conversation conv1 : conversations) {
                    count += conv1.getUnreadCount();
                }
                BusProvider.getInstance().post(new BadgeEvent(count));
            }
        } else if (notification.name.equals(PeerMessageActivity.SEND_SECRET_MESSAGE_NAME)) {
            IMessage imsg = (IMessage) notification.obj;
            if (imsg.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                return;
            }
            int pos = findConversationPosition(imsg.receiver, Conversation.CONVERSATION_PEER_SECRET);
            Conversation conversation = null;
            if (pos == -1) {
                conversation = newPeerConversation(imsg.receiver);
            } else {
                conversation = conversations.get(pos);
            }

            conversation.message = (IMessage) imsg.clone();
            updateNotificationDesc(conversation);
            updateConversationDetail(conversation);

            if (pos == -1) {
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else if (pos > 0){
                conversations.remove(pos);
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else {
                //pos == 0
            }

        } else if (notification.name.equals(GroupMessageActivity.SEND_MESSAGE_NAME)) {
            IMessage imsg = (IMessage) notification.obj;
            if (imsg.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                return;
            }
            int pos = findConversationPosition(imsg.receiver, Conversation.CONVERSATION_GROUP);
            Conversation conversation = null;
            if (pos == -1) {
                conversation = newGroupConversation(imsg.receiver);
            } else {
                conversation = conversations.get(pos);
            }

            conversation.message = (IMessage) imsg.clone();
            updateNotificationDesc(conversation);
            updateConversationDetail(conversation);

            if (pos == -1) {
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else if (pos > 0){
                conversations.remove(pos);
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else {
                //pos == 0
            }

        } else if (notification.name.equals(GroupMessageActivity.CLEAR_MESSAGES)) {
            Long groupID = (Long)notification.obj;
            Conversation conversation = findConversation(groupID, Conversation.CONVERSATION_GROUP);
            if (conversation != null) {
                conversations.remove(conversation);
                adapter.notifyDataSetChanged();
            }
        } else if (notification.name.equals(GroupMessageActivity.CLEAR_NEW_MESSAGES)) {
            Long groupID = (Long)notification.obj;
            Conversation conversation = findConversation(groupID, Conversation.CONVERSATION_GROUP);
            if (conversation != null && conversation.getUnreadCount() > 0) {
                conversation.setUnreadCount(0);
                ConversationDB.getInstance().setNewCount(conversation.rowid, 0);


                int count = 0;
                for (Conversation conv1 : conversations) {
                    count += conv1.getUnreadCount();
                }
                BusProvider.getInstance().post(new BadgeEvent(count));
            }
        } else if (notification.name.equals(CustomerMessageActivity.SEND_MESSAGE_NAME)) {
            ICustomerMessage imsg = (ICustomerMessage) notification.obj;
            if (imsg.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                return;
            }
            int pos = findConversationPosition(imsg.storeID, Conversation.CONVERSATION_CUSTOMER_SERVICE);
            Conversation conversation = null;
            if (pos == -1) {
                conversation = newCustomerConversation(imsg.storeID);
            } else {
                conversation = conversations.get(pos);
            }

            conversation.message = (IMessage) imsg.clone();
            updateNotificationDesc(conversation);
            updateConversationDetail(conversation);

            if (pos == -1) {
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else if (pos > 0){
                conversations.remove(pos);
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else {
                //pos == 0
            }
        } else if (notification.name.equals(CustomerMessageActivity.CLEAR_NEW_MESSAGES)) {
            Long storeID = (Long)notification.obj;
            Conversation conversation = findConversation(storeID, Conversation.CONVERSATION_CUSTOMER_SERVICE);
            if (conversation != null && conversation.getUnreadCount() > 0) {
                conversation.setUnreadCount(0);
                ConversationDB.getInstance().setNewCount(conversation.rowid, 0);


                int count = 0;
                for (Conversation conv1 : conversations) {
                    count += conv1.getUnreadCount();
                }
                BusProvider.getInstance().post(new BadgeEvent(count));

            }
        }
    }

    @Override
    public void onSystemMessage(String sm) {
        Log.i(TAG, "system message:" + sm);
        try {
            JSONObject element = new JSONObject(sm);

            if (!element.has("p2p_secret")) {
                return;
            }

            JSONObject obj = element.getJSONObject("p2p_secret");

            long caller = obj.getLong("caller");
            obj.getLong("callee");

            String channelID = obj.getString("channel_id");
            int registrationId = obj.getInt("registration_id");
            IdentityKey identityKey = new IdentityKey(Base64.decode(obj.getString("identity_key"), Base64.DEFAULT), 0);

            JSONObject preKeyJson = obj.getJSONObject("pre_key");
            JSONObject signedJson = obj.getJSONObject("signed_pre_key");

            int preKeyId = preKeyJson.getInt("key_id");
            ECPublicKey preKey = Curve.decodePoint(Base64.decode(preKeyJson.getString("public_key"), Base64.DEFAULT), 0);

            int signedPreKeyId = signedJson.getInt("key_id");
            ECPublicKey signedPrePublicKey = Curve.decodePoint(Base64.decode(signedJson.getString("public_key"), Base64.DEFAULT), 0);
            byte[] signature = Base64.decode(signedJson.getString("signature"), Base64.DEFAULT);

            int deviceId = 1;

            Log.i(TAG, "registrationId:" + registrationId + " preKeyId:" + preKeyId + " preKey:" + preKey + " signedPreKeyId:" + signedPreKeyId);
            Log.i(TAG, "signedPrePublicKey:" + signedPrePublicKey + " signature:" + signature + " identityKey:" + identityKey);

            PreKeyBundle preKeyBundle = new PreKeyBundle(registrationId, deviceId, preKeyId, preKey, signedPreKeyId, signedPrePublicKey, signature, identityKey);

            final SignalProtocolAddress BOB_ADDRESS = new SignalProtocolAddress("" + caller, 1);

            TextSecureIdentityKeyStore identityStore = new TextSecureIdentityKeyStore(this);
            identityStore.saveIdentity(BOB_ADDRESS, identityKey, true);

            SignalProtocolStoreImpl store = SignalProtocolStoreImpl.getInstance();

            SessionBuilder aliceSessionBuilder = new SessionBuilder(store, BOB_ADDRESS);
            aliceSessionBuilder.process(preKeyBundle);

            joinP2PSecretSession(channelID, caller);

            long cid = caller;
            int pos = findConversationPosition(cid, Conversation.CONVERSATION_PEER_SECRET);
            Conversation conversation = null;
            if (pos == -1) {
                conversation = newPeerSecretConversation(cid, Conversation.STATE_CONNECTED);
            } else {
                conversation = conversations.get(pos);
                conversation.state = Conversation.STATE_CONNECTED;
            }
            updateConversationDetail(conversation);

            if (pos == -1) {
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else if (pos > 0) {
                conversations.remove(pos);
                conversations.add(0, conversation);
                adapter.notifyDataSetChanged();
            } else {
                //pos == 0
            }

        } catch (JSONException e) {

        } catch (InvalidKeyException e) {

        } catch (UntrustedIdentityException e) {

        }

    }


    @Override
    public void onCustomerSupportMessage(CustomerMessage msg) {
        ICustomerMessage cm = new ICustomerMessage();
        cm.storeID = msg.storeID;
        cm.sellerID = msg.sellerID;
        cm.customerAppID = msg.customerAppID;
        cm.customerID = msg.customerID;
        cm.timestamp = msg.timestamp;
        cm.setContent(msg.content);
        cm.isSupport = true;
        cm.isOutgoing = false;

        onNewCustomerMessage(cm);
        alarmMessage();
    }

    @Override
    public void onCustomerMessage(CustomerMessage msg) {
        ICustomerMessage cm = new ICustomerMessage();
        cm.storeID = msg.storeID;
        cm.sellerID = msg.sellerID;
        cm.customerAppID = msg.customerAppID;
        cm.customerID = msg.customerID;
        cm.timestamp = msg.timestamp;
        cm.setContent(msg.content);
        cm.isSupport = false;
        cm.isOutgoing = true;

        onNewCustomerMessage(cm);
    }

    @Override
    public void onCustomerMessageACK(CustomerMessage msg) {
        int msgLocalID = msg.msgLocalID;
        if (msgLocalID == 0) {
            MessageContent c = IMessage.fromRaw(msg.content);
            if (c.getType() == MessageContent.MessageType.MESSAGE_REVOKE) {
                Revoke r = (Revoke)c;
                int pos = -1;
                pos = findConversationPosition(msg.storeID, Conversation.CONVERSATION_CUSTOMER_SERVICE);
                Conversation conversation = conversations.get(pos);
                if (r.msgid.equals(conversation.message.getUUID())) {
                    conversation.message.setContent(r);
                    updateNotificationDesc(conversation);
                    updateConversationDetail(conversation);
                }
            }
        }
    }

    @Override
    public void onCustomerMessageFailure(CustomerMessage msg) {

    }

    @Override
    public boolean canBack() {
        return false;
    }

    protected User getUser(long uid) {
        for (Contact contact : contacts) {
            if (contact.getContactId() == uid) {
                User u = new User();
                u.uid = uid;
                u.name = contact.getName();
                u.avatarURL = contact.getAvatar();
                u.identifier = String.format("%d", uid);
                Log.i(TAG, "user name:" + u.name);
                return u;
            }
        }

        User u = new User();
        u.uid = uid;
        u.name = null;
        u.avatarURL = "";
        u.identifier = String.format("%d", uid);
        Log.i(TAG, "user name is null");
        return u;
    }


    protected void asyncGetUser(long uid, GetUserCallback cb) {

    }


    protected Group getGroup(long gid) {
        Group g = GroupDB.getInstance().loadGroup(gid);
        if (g == null) {
            g = new Group();
            g.groupID = gid;
            g.topic = null;
            g.avatarURL = "";
            g.identifier = String.format("%d", gid);
        }
        return g;
    }


    protected void asyncGetGroup(final long gid, final GetGroupCallback cb) {
        IMHttpAPI.Singleton().getGroup(gid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                               @Override
                               public void call(Object obj) {
                                   Gson g = new Gson();
                                   JsonObject jobj = g.toJsonTree(obj).getAsJsonObject();
                                   JsonObject data = jobj.getAsJsonObject("data");
                                   String name = data.get("name").getAsString();
                                   Long masterID = data.get("master").getAsLong();
                                   boolean doNotDisturb = data.get("do_not_disturb").getAsBoolean();
                                   JsonArray members = data.get("members").getAsJsonArray();

                                   Group group = new Group();
                                   group.groupID = gid;
                                   group.topic = name;
                                   group.master = masterID;
                                   group.doNotDisturb = doNotDisturb;
                                   group.timestamp = now();

                                   for (int i = 0; i < members.size(); i++) {
                                       JsonObject m = members.get(i).getAsJsonObject();
                                       long uid = m.get("uid").getAsLong();
                                       group.addMember(uid);
                                   }
                                   GroupDB.getInstance().addGroup(group);
                                   if (!TextUtils.isEmpty(name)) {
                                       cb.onGroup(group);
                                   }
                                   Log.i(TAG, "get group success");
                               }
                           }, new Action1<Throwable>() {
                               @Override
                               public void call(Throwable throwable) {
                                   Log.i(TAG, "get group fail:", throwable);

                                   RetrofitError error = (RetrofitError)throwable;
                                   if (error.getResponse() != null) {
                                       Log.i(TAG, "error:" + error.getBody());

                                   }
                               }
                           }
                );
    }

    protected void onPeerClick(long uid, String name, String avatar, boolean secret, int state) {
        // 会话列表进入聊天界面
        Intent intent = new Intent(this, PeerMessageActivity.class);
        intent.putExtra("peer_uid", uid);
        intent.putExtra("peer_name", name);
        intent.putExtra("peer_avatar", avatar);
        intent.putExtra("current_uid", this.currentUID);
        intent.putExtra("secret", secret);
        intent.putExtra("state", state);
        String screenInstanceID = BaseActivity.generateScreenInstanceID();
        String navigatorEventID = screenInstanceID + "_events";

        intent.putExtra("navigatorID", navigatorID);
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);

        startActivity(intent);
    }

    void onStoreClick(long storeID, String name) {
        if (storeID != Config.STORE_ID) {
            return;
        }

        Intent intent = new Intent(this, CustomerMessageActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("store_id", Config.STORE_ID);
        intent.putExtra("seller_id", Config.SELLER_ID);
        intent.putExtra("app_id", Config.APPID);
        intent.putExtra("current_uid", Profile.getInstance().uid);
        intent.putExtra("title", "上城通讯团队");

        String screenInstanceID = BaseActivity.generateScreenInstanceID();
        String navigatorEventID = screenInstanceID + "_events";

        intent.putExtra("navigatorID", navigatorID);
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);

        startActivity(intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        boolean videoEnabled;
        if (requestCode == CALL_VIDEO) {
            videoEnabled = true;
        } else if (requestCode == CALL_VOICE) {
            videoEnabled = false;
        } else {
            return;
        }


        int r = data.getIntExtra("result", -1);
        int duration = data.getIntExtra("duration", 0);
        long peerUID = data.getLongExtra("peer_uid", 0L);

        int flag = VOIP.VOIP_FLAG_UNRECEIVED;
        if (r == CallActivity.CALL_ACCEPTED) {
            flag = VOIP.VOIP_FLAG_ACCEPTED;
        } else if (r == CallActivity.CALL_CANCLED) {
            flag = VOIP.VOIP_FLAG_CANCELED;
        } else if (r == CallActivity.CALL_REFUSED) {
            flag = VOIP.VOIP_FLAG_REFUSED;
        } else if (r == CallActivity.CALL_UNRECEIVED) {
            flag = VOIP.VOIP_FLAG_UNRECEIVED;
        }

        IMessage msg = new IMessage();
        msg.sender = peerUID;
        msg.receiver = this.currentUID;
        VOIP voip = VOIP.newVOIP(flag, duration, videoEnabled);
        msg.setContent(voip);
        msg.timestamp = now();
        msg.isOutgoing = false;

        PeerMessageDB.getInstance().insertMessage(msg, msg.sender);

        this.onNewPeerMessage(msg);
    }

    public void startGroupConversation(long gid, String name) {
        Intent intent = new Intent(this, GroupMessageActivity.class);
        intent.putExtra("group_id", gid);
        intent.putExtra("group_name", name);
        intent.putExtra("current_uid", this.currentUID);
        intent.putExtra("navigatorID", this.navigatorID);


        String screenInstanceID = BaseActivity.generateScreenInstanceID();
        String navigatorEventID = screenInstanceID + "_events";

        intent.putExtra("navigatorID", navigatorID);
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);

        startActivity(intent);
    }


    private void startPeerConversation(long peer, String contactName) {
        Intent intent = new Intent(this, PeerMessageActivity.class);
        intent.putExtra("peer_uid", peer);
        intent.putExtra("peer_name", contactName);
        intent.putExtra("current_uid", Token.getInstance().uid);
        startActivity(intent);
    }

    private void startPeerSecretConversation(long peer, boolean sessionConnected) {
        int pos = findConversationPosition(peer, Conversation.CONVERSATION_PEER_SECRET);
        Conversation conversation;
        if (pos == -1) {
            conversation = newPeerSecretConversation(peer, Conversation.STATE_WAIT);
        } else {
            conversation = conversations.get(pos);
            conversation.state = Conversation.STATE_WAIT;
            ConversationDB.getInstance().setState(conversation.rowid, Conversation.STATE_WAIT);
        }

        updateConversationDetail(conversation);

        if (pos == -1) {
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else if (pos > 0) {
            conversations.remove(pos);
            conversations.add(0, conversation);
            adapter.notifyDataSetChanged();
        } else {
            //pos == 0
        }


        User u = getUser(peer);
        String name = u.name;
        if (TextUtils.isEmpty(name)) {
            name = u.identifier;
        }

        Intent intent = new Intent(this, PeerMessageActivity.class);
        intent.putExtra("peer_uid", peer);
        intent.putExtra("peer_name", name);
        intent.putExtra("current_uid", Token.getInstance().uid);
        intent.putExtra("secret", true);
        if (sessionConnected) {
            intent.putExtra("state", Conversation.STATE_CONNECTED);
        } else {
            intent.putExtra("state", Conversation.STATE_WAIT);
        }
        startActivity(intent);
    }

    private void joinP2PSecretSession(String channelID, long peerUID) {
        String androidID = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        String deviceID = "a" + androidID;

        IMessage msg = new IMessage();
        msg.sender = currentUID;
        msg.receiver = peerUID;
        msg.timestamp = now();
        msg.setContent(P2PSession.newP2PSession(channelID, deviceID));
        EPeerMessageDB.getInstance().insertMessage(msg, peerUID);

        IMMessage m = new IMMessage();
        m.sender = msg.sender;
        m.receiver = msg.receiver;
        m.msgLocalID = msg.msgLocalID;


        CiphertextMessage outgoingMessage = encrypt(peerUID, msg.content.getRaw());
        if (outgoingMessage == null) {
            Log.e(TAG, "session encrypt fail");
            return;
        }
        byte[] base64 = android.util.Base64.encode(outgoingMessage.serialize(), android.util.Base64.DEFAULT);
        Log.i(TAG, "encrypt len:" + outgoingMessage.serialize().length + " base64 length:" + base64.length);
        String ciphertext = new String(base64);
        Log.i(TAG, "ciphertext:" + ciphertext);
        try {
            JSONObject secretJson = new JSONObject();
            secretJson.put("ciphertext", ciphertext);
            switch (outgoingMessage.getType()) {
                case CiphertextMessage.PREKEY_TYPE:
                    secretJson.put("type", 1);
                    break;
                case CiphertextMessage.WHISPER_TYPE:
                    secretJson.put("type", 2);
                    break;
            }

            JSONObject json = new JSONObject();
            json.put("secret", secretJson);
            m.secret = true;
            m.content = json.toString();
            boolean r = IMService.getInstance().sendPeerMessage(m);
            Log.e(TAG, "p2p session sended:" + r);
        } catch (JSONException e) {
            Log.e(TAG, "exception:" + e);
        }
    }


    protected CiphertextMessage encrypt(long peerUID, String text) {
        SignalProtocolStoreImpl store = new SignalProtocolStoreImpl(this);
        try {
            SignalProtocolAddress BOB_ADDRESS = new SignalProtocolAddress("" + peerUID, 1);
            if (store != null && store.containsSession(BOB_ADDRESS)) {
                SessionCipher aliceSessionCipher = new SessionCipher(store, BOB_ADDRESS);
                CiphertextMessage outgoingMessage = aliceSessionCipher.encrypt(text.getBytes("UTF-8"));
                return outgoingMessage;
            } else {
                return null;
            }
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        } catch (UntrustedIdentityException e) {
            e.printStackTrace();
        }
        return null;
    }


}
