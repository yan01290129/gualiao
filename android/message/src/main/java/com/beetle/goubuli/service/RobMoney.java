package com.beetle.goubuli.service;

import android.accessibilityservice.AccessibilityService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.accessibility.AccessibilityEvent;

public class RobMoney extends AccessibilityService {

    // 无障碍服务：自定义耳机按键发送广播参数
    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        int key = event.getKeyCode();
        switch(key){
            case KeyEvent.KEYCODE_HEADSETHOOK:
                Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    intent.putExtra("key", "down");
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    intent.putExtra("key", "up");
                }
                sendBroadcast(intent);
                break;
        }
        return super.onKeyEvent(event);
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
    }

}
