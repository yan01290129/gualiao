package com.beetle.goubuli;


import android.os.Bundle;
import android.text.TextUtils;

import com.beetle.bauhinia.db.ICustomerMessage;
import com.beetle.bauhinia.db.IMessage;
import com.beetle.goubuli.model.Profile;
import java.io.File;

/**
 * Created by houxh on 2017/7/24.
 */

public class CustomerMessageActivity extends com.beetle.bauhinia.CustomerMessageActivity {
    String storeAvatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        File dir = getCacheDir();
        File iconFile = new File(dir, "goubuli.png");
        String path = iconFile.getAbsolutePath();
        String imageUri = "file:" + path;
        storeAvatar = imageUri;

        super.onCreate(savedInstanceState);
    }


    //加载消息发送者的名称和头像信息
    @Override
    protected void loadUserName(IMessage msg) {
        if (!(msg instanceof ICustomerMessage)) {
            return;
        }

        User u = new User();
        ICustomerMessage cm = (ICustomerMessage)msg;
        if (!cm.isSupport) {
            u.name = Profile.getInstance().name;
            u.avatarURL = Profile.getInstance().avatar;
        } else {
            u.name = this.title;
            u.avatarURL = storeAvatar;
        }
        msg.setSenderAvatar(u.avatarURL);
        if (TextUtils.isEmpty(u.name)) {
            msg.setSenderName(u.identifier);
        } else {
            msg.setSenderName(u.name);
        }
    }


}
