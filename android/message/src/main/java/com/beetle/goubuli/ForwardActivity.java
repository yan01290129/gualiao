package com.beetle.goubuli;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beetle.goubuli.model.Conversation;
import com.beetle.message.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ForwardActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String TAG = "goubuli";

    private String uuid;//msg uuid

    List<Conversation> conversations;
    private ListView lv;
    private BaseAdapter adapter;

    class ContactAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return conversations.size();
        }

        @Override
        public Object getItem(int position) {
            return conversations.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.at_contact, null);
            } else {
                view = convertView;
            }
            TextView tv = (TextView) view.findViewById(R.id.name);
            Conversation c = conversations.get(position);
            tv.setText(c.getName());


            if (!TextUtils.isEmpty(c.getAvatar())) {
                ImageView imageView = (ImageView) view.findViewById(R.id.header);
                Picasso.with(getBaseContext())
                        .load(c.getAvatar())
                        .placeholder(R.drawable.avatar_contact)
                        .into(imageView);
            } else if (c.type == Conversation.CONVERSATION_GROUP){
                ImageView imageView = (ImageView) view.findViewById(R.id.header);
                imageView.setImageResource(R.drawable.avatar_group);
            } else {
                ImageView imageView = (ImageView) view.findViewById(R.id.header);
                imageView.setImageResource(R.drawable.avatar_contact);
            }
            return view;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forward);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("选择");
        }


        Intent intent = getIntent();
        uuid = intent.getStringExtra("uuid");
        if (TextUtils.isEmpty(uuid)) {
            finish();
            return;
        }

        MessageApplication app = (MessageApplication)getApplication();
        List<Conversation> convs = app.getConversations();
        if (convs == null) {
            convs = new ArrayList<>();
        }

        conversations = new ArrayList<>();
        for (Conversation conv : convs) {
            if (conv.type == Conversation.CONVERSATION_GROUP || conv.type == Conversation.CONVERSATION_PEER) {
                conversations.add(conv);
            }
        }

        adapter = new ContactAdapter();
        lv = (ListView)findViewById(R.id.list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Conversation c = conversations.get(position);
        Intent intent = new Intent();
        intent.putExtra("cid", c.cid);
        intent.putExtra("type", c.type);
        intent.putExtra("uuid", uuid);
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish(); // back button
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
