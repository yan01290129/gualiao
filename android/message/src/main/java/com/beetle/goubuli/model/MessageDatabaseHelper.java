package com.beetle.goubuli.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MessageDatabaseHelper {
    private static final String TAG = "goubuli";

    private static final int DATABASE_VERSION  = 3;

    private static final Object lock = new Object();

    private static MessageDatabaseHelper instance;
    private DatabaseHelper databaseHelper;


    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createDatabase(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(TAG, "upgrade database, old version:" + oldVersion + " new version:" + newVersion);
            assert(newVersion == DATABASE_VERSION);

            if (oldVersion == 1) {
                //version 1 -> 2
                db.execSQL("ALTER TABLE peer_message ADD `secret` INTEGER DEFAULT 0");
                db.execSQL("DROP INDEX IF EXISTS peer_index");
                db.execSQL(SQLCreator.PEER_MESSAGE_IDX);
                db.execSQL(SQLCreator.CONVERSATION);
                db.execSQL(SQLCreator.CONVERSATION_IDX);
                oldVersion = 2;
                Log.i(TAG, "upgrade database 1->2 success");
            }

            if (oldVersion == 2) {
                //version 2 -> 3
                db.execSQL("ALTER TABLE peer_message ADD `uuid` TEXT");
                db.execSQL("ALTER TABLE group_message ADD `uuid` TEXT");
                db.execSQL("ALTER TABLE customer_message ADD `uuid` TEXT");
                db.execSQL(SQLCreator.PEER_MESSAGE_UUID_IDX);
                db.execSQL(SQLCreator.GROUP_MESSAGE_UUID_IDX);
                db.execSQL(SQLCreator.CUSTOMER_MESSAGE_UUID_IDX);
                oldVersion = 3;
                Log.i(TAG, "upgrade database 2->3 success");
            }
        }

        private void createDatabase(SQLiteDatabase db) {
            db.execSQL(SQLCreator.PEER_MESSAGE);
            db.execSQL(SQLCreator.GROUP_MESSAGE);
            db.execSQL(SQLCreator.CUSTOMER_MESSAGE);
            db.execSQL(SQLCreator.PEER_MESSAGE_FTS);
            db.execSQL(SQLCreator.GROUP_MESSAGE_FTS);
            db.execSQL(SQLCreator.CUSTOMER_MESSAGE_FTS);
            db.execSQL(SQLCreator.PEER_MESSAGE_IDX);
            db.execSQL(SQLCreator.PEER_MESSAGE_UUID_IDX);
            db.execSQL(SQLCreator.GROUP_MESSAGE_UUID_IDX);
            db.execSQL(SQLCreator.CUSTOMER_MESSAGE_UUID_IDX);
            db.execSQL(SQLCreator.CONVERSATION);
            db.execSQL(SQLCreator.CONVERSATION_IDX);
        }
    }

    public static MessageDatabaseHelper getInstance() {
        synchronized (lock) {
            if (instance == null)
                instance = new MessageDatabaseHelper();
            return instance;
        }
    }

    private MessageDatabaseHelper() {

    }

    public void open(Context context, String name) {
        if (this.databaseHelper != null) {
            this.databaseHelper.close();
        }
        this.databaseHelper = new DatabaseHelper(context, name, null, DATABASE_VERSION);
    }

    public SQLiteDatabase getDatabase() {
        return this.databaseHelper.getWritableDatabase();
    }

    public void close() {
        if (this.databaseHelper != null) {
            this.databaseHelper.close();
            this.databaseHelper = null;
        }
    }
}
