package com.beetle.goubuli;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.beetle.goubuli.model.*;
import com.beetle.message.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AtActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = "goubuli";



    ArrayList<Contact> atContacts = new ArrayList<>();

    private ListView lv;
    private BaseAdapter adapter;

    class ContactAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return atContacts.size();
        }

        @Override
        public Object getItem(int position) {
            return atContacts.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.at_contact, null);
            } else {
                view = convertView;
            }
            TextView tv = (TextView) view.findViewById(R.id.name);
            Contact c = atContacts.get(position);
            tv.setText(c.getName());


            if (!TextUtils.isEmpty(c.getAvatar())) {
                ImageView imageView = (ImageView) view.findViewById(R.id.header);
                Picasso.with(getBaseContext())
                        .load(c.getAvatar())
                        .placeholder(R.drawable.avatar_contact)
                        .into(imageView);
            } else {
                ImageView imageView = (ImageView) view.findViewById(R.id.header);
                imageView.setImageResource(R.drawable.avatar_contact);
            }
            return view;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_at);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("选择提醒的人");
        }

        Intent intent = getIntent();
        long groupID = intent.getLongExtra("group_id", -1);
        if (groupID == -1) {
            Log.i(TAG, "group id -1");
            finish();
            return;
        }

        Group group = GroupDB.getInstance().loadGroup(groupID);
        if (group == null) {
            Log.i(TAG, "can't load group");
            finish();
            return;
        }
        ArrayList<GroupMember> members = GroupDB.getInstance().loadGroupMember(groupID);
        group.setGroupMembers(members);

        List<Contact> contacts = ContactManager.getInstance().getAllContact();
        for (int i = 0; i < members.size(); i++) {
            GroupMember m = members.get(i);
            for (Contact c : contacts) {
                if (c.getContactId() == m.uid) {
                    atContacts.add(c);
                    break;
                }
            }
        }

        adapter = new ContactAdapter();
        lv = (ListView)findViewById(R.id.list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Contact c = atContacts.get(position);
        Intent intent = new Intent();
        intent.putExtra("uid", c.getContactId());
        intent.putExtra("name", c.getName());
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish(); // back button
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
