package com.beetle.goubuli;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Toast;

import com.beetle.goubuli.util.Log;

import org.linphone.Judge;
import org.linphone.LinphoneActivity;


public class Headset extends BroadcastReceiver {



    public static final String TAG = "Headset这里接收耳机广播";

    private Callback callback;
    private Context mContext;
    private KeyEvent event;

    public interface Callback {
        public void downEvent();
        public void upEvent();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("耳机事件按下功能");
        //获取action
//        String intentAction = intent.getAction();
        //获取keyEvent对象
//        KeyEvent keyEvent = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

//        if(Intent.ACTION_MEDIA_BUTTON.equals(intentAction)){
            // 获得事件的时间
//            long downtime = keyEvent.getEventTime();
//            if(downtime > 3000 ){
//                String userno = "8707";
//                String name = "qqq";
//                intent.putExtra("userno",userno);
//                intent.putExtra("name",name);
//                intent.setClass(context,LinphoneActivity.class);
//                context.startActivity(intent);
//            }

//        }

        Log.w(TAG, context + "" + intent);
        this.mContext = context;
        event = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
        int key = event.getKeyCode();
        switch(key){
            case KeyEvent.KEYCODE_HEADSETHOOK:
                intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    callback.downEvent();
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    callback.upEvent();
                }
        }

    }


}

