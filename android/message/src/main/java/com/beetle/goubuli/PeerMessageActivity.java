package com.beetle.goubuli;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.beetle.bauhinia.MessageActivity;
import com.beetle.bauhinia.db.GroupMessageDB;
import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.PeerMessageDB;
import com.beetle.bauhinia.db.message.P2PSession;
import com.beetle.bauhinia.db.message.Secret;
import com.beetle.bauhinia.db.message.VOIP;
import com.beetle.bauhinia.tools.Notification;
import com.beetle.bauhinia.tools.NotificationCenter;
import com.beetle.goubuli.crypto.storage.SignalProtocolStoreImpl;
import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.Conversation;
import com.beetle.goubuli.model.Group;
import com.beetle.goubuli.model.GroupDB;
import com.beetle.goubuli.model.Profile;
import com.beetle.goubuli.model.Token;
import com.beetle.goubuli.util.DateFormater;
import com.beetle.im.IMMessage;
import com.beetle.im.IMService;
import com.beetle.voip.CallActivity;
import com.beetle.voip.VOIPVideoActivity;
import com.beetle.voip.VOIPVoiceActivity;
import org.json.JSONException;
import org.json.JSONObject;
import org.linphone.CallPhoneActivity3;
import org.linphone.LinphoneActivity;
import org.linphone.LinphoneActivity2;
import org.whispersystems.libsignal.SessionCipher;
import org.whispersystems.libsignal.SignalProtocolAddress;
import org.whispersystems.libsignal.UntrustedIdentityException;
import org.whispersystems.libsignal.protocol.CiphertextMessage;
import org.whispersystems.libsignal.state.SignalProtocolStore;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import com.beetle.message.R;

/**
 * Created by houxh on 2017/7/24.
 */

public class PeerMessageActivity extends com.beetle.bauhinia.PeerMessageActivity {
    public static final int CALL_VIDEO = 200;
    public static final int CALL_VOICE = 201;
    public static final int FORWARD_REQUEST = 202;

    private int state;//加密会话状态
    public static SignalProtocolStore store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (secret) {
            state = getIntent().getIntExtra("state", Conversation.STATE_UNINITIALIZE);

            store = new SignalProtocolStoreImpl(this);
            SignalProtocolAddress address = new SignalProtocolAddress("" + peerUID, 1);
            if (!store.containsSession(address)) {
                inputMenu.setVisibility(View.GONE);
                findViewById(R.id.bottom_bar).setVisibility(View.VISIBLE);
                TextView tv = (TextView)findViewById(R.id.bottom_text);

                if (state == Conversation.STATE_WAIT) {
                    tv.setText("等待对方上线");
                } else if (state == Conversation.STATE_UNINITIALIZE) {
                    tv.setText("加密会话未初始化，可重新发起加密会话");
                } else {
                    Log.e(TAG, "conversation connected, but can't load secret session");
                    tv.setText("加密会话丢失，可重新发起加密会话");
                }
            }
        }
    }

    @Override
    protected void callVoice() {
        final long calleeUID = peerUID;
        Intent intent = new Intent(PeerMessageActivity.this, VOIPVoiceActivity.class);
        intent.putExtra("current_uid", Token.getInstance().uid);
        intent.putExtra("channel_id", UUID.randomUUID().toString());
        intent.putExtra("is_caller", true);
        intent.putExtra("token", Token.getInstance().gobelieveToken);
        intent.putExtra("peer_name", peerName);
        intent.putExtra("peer_avatar", peerAvatar);
        intent.putExtra("peer_uid", calleeUID);
        startActivityForResult(intent, CALL_VOICE);
    }

    @Override
    protected void callVideo() {
        final long calleeUID = peerUID;
        Intent intent = new Intent(PeerMessageActivity.this, VOIPVideoActivity.class);
        intent.putExtra("current_uid", Token.getInstance().uid);
        intent.putExtra("channel_id", UUID.randomUUID().toString());
        intent.putExtra("is_caller", true);
        intent.putExtra("token", Token.getInstance().gobelieveToken);
        intent.putExtra("peer_name", peerName);
        intent.putExtra("peer_avatar", peerAvatar);
        intent.putExtra("peer_uid", calleeUID);
        startActivityForResult(intent, CALL_VIDEO);
    }

    //语音对讲
    @Override
    protected void grpUserCall(){
        // 发送文本消息
        sendTextMessage(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+" "+"发起了语音对讲", null, null);


        Contact c = ContactManager.getInstance().getContactById(peerUID);
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
//        bundle.putString("userno",c.getUserno());
//        bundle.putString("name",c.getName());
        intent.putExtra("userno",c.getUserno());
        intent.putExtra("name",c.getName());
        intent.setClass(PeerMessageActivity.this,LinphoneActivity.class);
        PeerMessageActivity.this.startActivity(intent);

    }

    //数字电话
    @Override
    protected void userCall(){
        sendTextMessage(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+" "+"发起了语音对讲", null, null);

        Contact c = ContactManager.getInstance().getContactById(peerUID);
        String userno = "8707";
        String name = c.getName();
        Bundle bundle = new Bundle();
        bundle.putString("userno", userno);
        bundle.putString("name", name);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        intent.setClass(this,LinphoneActivity.class);
        PeerMessageActivity.this.startActivity(intent);
    }


    @Override
    protected  void call() {
        final CharSequence[] items = {
                "语音呼叫", "视频呼叫"
        };

        final long calleeUID = peerUID;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    callVoice();
                } else if (item == 1){
                    callVideo();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == CALL_VIDEO || requestCode == CALL_VOICE) {
            boolean videoEnabled = requestCode == CALL_VIDEO;


            int r = data.getIntExtra("result", 0);
            int duration = data.getIntExtra("duration", 0);
            long peerUID = data.getLongExtra("peer_uid", 0L);

            int flag = VOIP.VOIP_FLAG_UNRECEIVED;
            if (r == CallActivity.CALL_ACCEPTED) {
                flag = VOIP.VOIP_FLAG_ACCEPTED;
            } else if (r == CallActivity.CALL_CANCLED) {
                flag = VOIP.VOIP_FLAG_CANCELED;
            } else if (r == CallActivity.CALL_REFUSED) {
                flag = VOIP.VOIP_FLAG_REFUSED;
            } else if (r == CallActivity.CALL_UNRECEIVED) {
                flag = VOIP.VOIP_FLAG_UNRECEIVED;
            }

            IMessage msg = new IMessage();
            msg.sender = this.currentUID;
            msg.receiver = peerUID;
            VOIP voip = VOIP.newVOIP(flag, duration, videoEnabled);
            msg.setContent(voip);
            msg.timestamp = now();
            msg.isOutgoing = true;
            msg.setAck(true);

            this.messageDB.saveMessage(msg);
            this.loadUserName(msg);

            this.insertMessage(msg);

            NotificationCenter nc = NotificationCenter.defaultCenter();
            Notification notification = new Notification(msg, SEND_MESSAGE_NAME);
            nc.postNotification(notification);
        } else if (requestCode == FORWARD_REQUEST) {
            int type = data.getIntExtra("type", 0);
            long cid = data.getLongExtra("cid", 0);
            String uuid = data.getStringExtra("uuid");

            if (cid == 0) {
                return;
            }

            IMessage imsg = findMessage(uuid);
            if (imsg == null) {
                return;
            }

            imsg = (IMessage)imsg.clone();

            imsg.sender = currentUID;
            imsg.receiver = cid;
            imsg.content.generateRaw(UUID.randomUUID().toString());

            IMMessage msg = new IMMessage();
            msg.sender = imsg.sender;
            msg.receiver = imsg.receiver;
            msg.msgLocalID = imsg.msgLocalID;
            msg.content = imsg.content.getRaw();
            msg.plainContent = msg.content;

            if (type == Conversation.CONVERSATION_PEER) {
                PeerMessageDB db = PeerMessageDB.getInstance();
                db.insertMessage(imsg, cid);
                IMService.getInstance().sendPeerMessage(msg);
                NotificationCenter nc = NotificationCenter.defaultCenter();
                Notification notification = new Notification(imsg, com.beetle.bauhinia.PeerMessageActivity.SEND_MESSAGE_NAME);
                nc.postNotification(notification);
            } else if (type == Conversation.CONVERSATION_GROUP) {
                GroupMessageDB db = GroupMessageDB.getInstance();
                db.insertMessage(imsg, cid);
                IMService.getInstance().sendGroupMessage(msg);
                NotificationCenter nc = NotificationCenter.defaultCenter();
                Notification notification = new Notification(imsg, com.beetle.bauhinia.GroupMessageActivity.SEND_MESSAGE_NAME);
                nc.postNotification(notification);
            }

            if (type == Conversation.CONVERSATION_PEER && cid == peerUID && !secret) {
                insertMessage(imsg);
            }
        }

    }


    @Override
    protected User getUser(long uid) {
        User u = new User();
        if (uid == currentUID) {
            u.name = Profile.getInstance().name;
            u.avatarURL = Profile.getInstance().avatar;
        } else {
            u.name = peerName;
            u.avatarURL = peerAvatar;
        }
        return u;
    }


    protected CiphertextMessage encrypt(String text) {
        try {
            SignalProtocolAddress BOB_ADDRESS = new SignalProtocolAddress("" + peerUID, 1);
            if (store != null && store.containsSession(BOB_ADDRESS)) {
                SessionCipher aliceSessionCipher = new SessionCipher(store, BOB_ADDRESS);
                CiphertextMessage outgoingMessage = aliceSessionCipher.encrypt(text.getBytes("UTF-8"));
                return outgoingMessage;
            } else {
                return null;
            }
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        } catch (UntrustedIdentityException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected boolean encrypt(IMMessage im, String uuid) {
        CiphertextMessage outgoingMessage = encrypt(im.content);

        if (outgoingMessage == null) {
            return false;
        }
        byte[] base64 = android.util.Base64.encode(outgoingMessage.serialize(), android.util.Base64.DEFAULT);
        Log.i(TAG, "encrypt len:" + outgoingMessage.serialize().length + " base64 length:" + base64.length);
        String ciphertext = new String(base64);
        Log.i(TAG, "ciphertext:" + ciphertext);
        int type = 0;
        switch (outgoingMessage.getType()) {
            case CiphertextMessage.PREKEY_TYPE:
                type = 1;
                break;
            case CiphertextMessage.WHISPER_TYPE:
                type = 2;
                break;
        }
        im.content = Secret.newSecret(ciphertext, type, uuid).getRaw();
        im.secret = true;

        return true;
    }

    @Override
    protected void handleP2PSession(IMessage imsg) {
        if (!imsg.isOutgoing) {
            //P2PSession ps = (P2PSession)imsg.content;
            inputMenu.setVisibility(View.VISIBLE);
            findViewById(R.id.bottom_bar).setVisibility(View.GONE);
        }
    }

    @Override
    protected void forward(IMessage im) {
        if (TextUtils.isEmpty(im.getUUID())) {
            return;
        }
        Intent intent = new Intent(this, ForwardActivity.class);
        intent.putExtra("uuid", im.getUUID());
        startActivityForResult(intent, FORWARD_REQUEST);
    }

}
