package com.beetle.conference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by houxh on 2016/12/25.
 */

public class ConferenceCommand {
    public static final String COMMAND_INVITE = "invite";
    public static final String COMMAND_WAIT = "waiting";
    public static final String COMMAND_ACCEPT = "accept";
    public static final String COMMAND_REFUSE = "refuse";
    public static final String COMMAND_HANGUP = "hangup";
    public static final String COMMAND_ACCEPTED = "accepted";
    public static final String COMMAND_REFUSED = "refused";

    public String channelID;
    public String command;
    public long groupID = 0;
    public long initiator = 0;
    public long[] participants = new long[0];


    public ConferenceCommand() {

    }
    public ConferenceCommand(JSONObject obj) throws JSONException {
        try {
            this.initiator = obj.optLong("initiator");
            this.channelID = obj.getString("channel_id");
            this.command = obj.getString("command");
            this.groupID = obj.optLong("group_id");
            JSONArray array = obj.optJSONArray("participants");
            if (array != null) {
                long[] partipants = new long[array.length()];
                for (int i = 0; i < array.length(); i++) {
                    partipants[i] = array.getLong(i);
                }
                this.participants = partipants;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public JSONObject getContent() {
        try {
            JSONObject json = new JSONObject();
            json.put("initiator", this.initiator);
            json.put("channel_id", this.channelID);
            json.put("command", this.command);
            json.put("group_id", this.groupID);
            JSONArray array = new JSONArray();
            if (this.participants != null) {
                for (long p : this.participants) {
                    array.put(p);
                }
            }
            json.put("participants", array);
            return json;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
