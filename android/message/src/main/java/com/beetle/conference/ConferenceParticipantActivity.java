package com.beetle.conference;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.Group;
import com.beetle.goubuli.model.GroupDB;
import com.beetle.goubuli.model.GroupMember;
import com.beetle.message.R;

import java.util.ArrayList;
import java.util.List;

public class ConferenceParticipantActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {


    private static final String TAG = "goubuli";

    static class ParticipantItem {
        public long id;
        public String name;
        public int checkState;//0 unchecked,1 checked, 2 checked + disable
    }

    private BaseAdapter adapter;
    class ParticipantAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return items.size();
        }
        @Override
        public Object getItem(int position) {
            return items.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.participant, null);
                CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.cbx);
                checkBox.setOnCheckedChangeListener(ConferenceParticipantActivity.this);
            }

            ParticipantItem item = items.get(position);
            TextView textView = (TextView)convertView.findViewById(R.id.name);

            textView.setText(item.name);
            CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.cbx);
            checkBox.setTag(position);

            if (item.checkState == 0) {
                checkBox.setChecked(false);
                checkBox.setEnabled(true);
            } else if (item.checkState == 1) {
                checkBox.setChecked(true);
                checkBox.setEnabled(true);
            } else if (item.checkState == 2) {
                checkBox.setChecked(true);
                checkBox.setEnabled(false);
            }
            return convertView;
        }
    }


    long groupId;
    long[] participants;
    ArrayList<ParticipantItem> items;

    LayoutInflater inflater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conference_participant);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("");
        }

        Intent intent = getIntent();

        inflater = LayoutInflater.from(this);

        participants = intent.getLongArrayExtra("participants");

        if (participants == null || participants.length == 0) {
            Log.i(TAG, "participants empty");
            finish();
            return;
        }
        groupId = intent.getLongExtra("group_id", -1);
        if (groupId == -1) {
            Log.i(TAG, "can't get groupId");
            finish();
            return;
        }

        Group g = GroupDB.getInstance().loadGroup(groupId);
        if (g == null) {
            Log.i(TAG, "can't load group:" + groupId);
            finish();
            return;
        }

        items = new ArrayList<>();
        ArrayList<GroupMember> members = GroupDB.getInstance().loadGroupMember(groupId);
        for (int i = 0; i < members.size(); i++) {
            GroupMember m = members.get(i);

            String name = m.nickname;
            if (TextUtils.isEmpty(m.nickname)) {
                name = "" + m.uid;
                Contact c = ContactManager.getInstance().getContactById(m.uid);
                if (c != null) {
                    name = c.getName();
                }
            }
            ParticipantItem item = new ParticipantItem();
            item.id = m.uid;
            item.name = name;


            boolean exists = false;
            for (int j = 0; j < participants.length; j++) {
                if (participants[j] == m.uid) {
                    exists = true;
                    break;
                }
            }
            item.checkState = exists ? 2 : 0;
            items.add(item);
        }

        adapter = new ParticipantAdapter();
        ListView lv = (ListView) findViewById(R.id.list);
        lv.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_participant, menu);
        return true;
    }

    private long[] toArray(List<Long> values) {
        long[] result = new long[values.size()];
        int i = 0;
        for (Long l : values)
            result[i++] = l;
        return result;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.confirm) {
            ArrayList<Long> ids = new ArrayList<>();
            for (int i = 0; i < items.size(); i++) {
                ParticipantItem pi = items.get(i);
                if (pi.checkState == 1) {
                    ids.add(pi.id);
                }
            }

            if (ids.size() == 0) {
                return true;
            }
            long[] t = toArray(ids);

            Intent intent = new Intent();
            intent.putExtra("participants", t);
            setResult(RESULT_OK, intent);

            finish();
        } else if (id == android.R.id.home) {
            this.finish(); // back button
        }
        return true;
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int position = (Integer)buttonView.getTag();
        Log.i(TAG, "position:" + position);
        ParticipantItem item = items.get(position);
        item.checkState = isChecked ? 1 : 0;
    }
}
