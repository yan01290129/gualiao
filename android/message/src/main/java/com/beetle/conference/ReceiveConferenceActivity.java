package com.beetle.conference;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import com.beetle.im.IMService;
import com.beetle.im.RTMessage;
import com.beetle.im.RTMessageObserver;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.RCTNativeAppEventEmitter;
import com.reactnativenavigation.controllers.NavigationActivity;
import com.reactnativenavigation.react.NavigationApplication;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class ReceiveConferenceActivity extends NavigationActivity implements  RTMessageObserver {
    private final String TAG = "face";

    public static long activityCount = 0;
    public static ArrayList<String> channelIDs = new ArrayList<>();

    public static void convertBundle(Bundle bundle, Intent intent) {
        Bundle props = bundle.getBundle("passProps");
        if (props != null) {
            String channelID = props.getString("channelID");
            if (!TextUtils.isEmpty(channelID)) {
                intent.putExtra("channel_id", channelID);
            }
        }

        Bundle navigationParams = bundle.getBundle("navigationParams");
        if (navigationParams != null) {
            String navigatorID = navigationParams.getString("navigatorID", "");
            String navigatorEventID = navigationParams.getString("navigatorEventID", "");
            String screenInstanceID = navigationParams.getString("screenInstanceID", "");

            intent.putExtra("navigatorID", navigatorID);
            intent.putExtra("navigatorEventID", navigatorEventID);
            intent.putExtra("screenInstanceID", screenInstanceID);
        }
    }

    private static long getNumber(Bundle props, String key) {
        long r = props.getInt(key, -1);
        if (r == -1) {
            r = (long)props.getDouble(key, -1);
        }
        return r;
    }


    private String channelID = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getWindow().addFlags( WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        super.onCreate(savedInstanceState);

        activityCount++;

        Intent intent = getIntent();
        channelID = intent.getStringExtra("channel_id");
        if (TextUtils.isEmpty(channelID)) {
            Log.i(TAG, "channel id is empty");
            finish();
            return;
        }

        IMService.getInstance().addRTObserver(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        channelIDs.remove(channelID);
        activityCount--;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        IMService.getInstance().removeRTObserver(this);
    }


    private void sendEvent(ReactContext reactContext, String eventName, WritableMap params) {
        reactContext.getJSModule(RCTNativeAppEventEmitter.class).emit(eventName, params);
    }

    @Override
    public void onRTMessage(RTMessage rt) {
        try {
            JSONObject json = new JSONObject(rt.content);
            JSONObject obj = json.getJSONObject("conference");

            ConferenceCommand confCommand = new ConferenceCommand(obj);
            String command = confCommand.command;

            ReactContext reactContext = NavigationApplication.instance.getReactGateway().getReactContext();
            WritableMap map = Arguments.createMap();

            map.putString("command", command);
            map.putString("channelID", confCommand.channelID);
            map.putDouble("initiator", confCommand.initiator);
            map.putDouble("uid", rt.sender);
            WritableArray participants = Arguments.createArray();
            for (Long p : confCommand.participants) {
                participants.pushDouble(p);
            }
            map.putArray("participants", participants);
            sendEvent(reactContext, "onRTMessage", map);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
