package com.beetle.conference;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.Token;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.ConferenceEvent;
import com.beetle.im.IMService;
import com.beetle.im.RTMessage;
import com.beetle.im.RTMessageObserver;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ConferenceActivity extends GroupVOIPActivity implements  RTMessageObserver {
    private final String TAG = "face";

    public static long activityCount = 0;
    public static ArrayList<String> channelIDs = new ArrayList<>();

    static public ConferenceActivity instance;

    static class Invitee {
        public Invitee(long id, int ts) {
            this.id = id;
            this.timestamp = ts;
        }

        public long id;
        public int timestamp;
    }

    public static void convertBundle(Bundle bundle, Intent intent) {
        Bundle props = bundle.getBundle("passProps");
        if (props != null) {
            String channelID = props.getString("channelID");
            if (!TextUtils.isEmpty(channelID)) {
                intent.putExtra("channel_id", channelID);
            }

            long uid = getNumber(props, "uid");
            if (uid != -1) {
                intent.putExtra("current_uid", uid);
            }
            long initiator = getNumber(props, "initiator");
            if (initiator != -1) {
                intent.putExtra("initiator", initiator);
            }

            long groupID = getNumber(props, "groupID");
            if (groupID != -1) {
                intent.putExtra("group_id", groupID);
            }

            String token = props.getString("token");
            if (!TextUtils.isEmpty(token)) {
                intent.putExtra("token", token);
            }

            Parcelable[] v = props.getParcelableArray("participants");
            ArrayList<Long> participants = new ArrayList<>();
            if (v != null) {
                Bundle[] bundles = new Bundle[v.length];
                for (int i = 0; i < v.length; i++) {
                    Parcelable p = v[i];
                    if (p instanceof Bundle) {
                        long pid = getNumber((Bundle) p, "uid");
                        participants.add(pid);
                    } else {
                        break;
                    }
                }
            }

            long[] ps = new long[participants.size()];
            for (int i = 0; i < participants.size(); i++) {
                ps[i] = participants.get(i);
            }
            intent.putExtra("participants", ps);
        }

        Bundle navigationParams = bundle.getBundle("navigationParams");
        if (navigationParams != null) {
            String navigatorID = navigationParams.getString("navigatorID", "");
            String navigatorEventID = navigationParams.getString("navigatorEventID", "");
            String screenInstanceID = navigationParams.getString("screenInstanceID", "");

            intent.putExtra("navigatorID", navigatorID);
            intent.putExtra("navigatorEventID", navigatorEventID);
            intent.putExtra("screenInstanceID", screenInstanceID);
        }
    }

    private static long getNumber(Bundle props, String key) {
        long r = props.getInt(key, -1);
        if (r == -1) {
            r = (long)props.getDouble(key, -1);
        }
        return r;
    }


    private long groupID = 0;
    private long initiator = 0;
    long[] participantIds;

    ArrayList<Invitee> invitees = new ArrayList<>();
    private List<Contact> contacts = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance = this;
        super.onCreate(savedInstanceState);

        activityCount++;

        if (activityCount > 1) {
            Log.i(TAG, "conference activity count:" + activityCount);
            finish();
            return;
        }

        Intent intent = getIntent();

        currentUID = intent.getLongExtra("current_uid", 0);
        if (currentUID == 0) {
            Log.i(TAG, "current uid is 0");
            finish();
            return;
        }
        channelID = intent.getStringExtra("channel_id");
        if (TextUtils.isEmpty(channelID)) {
            Log.i(TAG, "channel id is empty");
            finish();
            return;
        }
        Log.i(TAG, "channel id:" + channelID);

        token = intent.getStringExtra("token");
        if (TextUtils.isEmpty(token)) {
            Log.i(TAG, "token is empty");
            finish();
            return;
        }

        initiator = intent.getLongExtra("initiator", 0);
        if (initiator == 0) {
            Log.i(TAG, "initiator 0");
            finish();
            return;
        }

        participantIds = intent.getLongArrayExtra("participants");
        if (participantIds == null || participantIds.length == 0) {
            Log.i(TAG, "participants empty");
            finish();
            return;
        }

        groupID = intent.getLongExtra("group_id", 0);

        Log.i(TAG, "initator:" + initiator);
        Log.i(TAG, "channel id:" + channelID);
        Log.i(TAG, "uid:" + Token.getInstance().uid);

        init();

        if (isInitiator()) {
            int n = now();
            for (int i = 0; i < participantIds.length; i++) {
                if (participantIds[i] != currentUID) {
                    Invitee invitee = new Invitee(participantIds[i], n);

                    invitees.add(invitee);
                }
            }
        }

        contacts = ContactManager.getInstance().getAllContact();

        //intent
        IMService.getInstance().addRTObserver(this);

        ConferenceEvent e = new ConferenceEvent(initiator, groupID, false);
        BusProvider.getInstance().post(e);

        //xiaomi imba
        if (Build.MANUFACTURER.equalsIgnoreCase("Xiaomi")) {
            requestRecordPermission();
        }

    }

    boolean isInitiator() {
        return currentUID == initiator;
    }

    @Override
    protected String getUserName(long uid) {
        for (Contact contact : contacts) {
            if (contact.getContactId() == uid) {
                return contact.getName();
            }
        }

        return String.format("%d", uid);
    }

    int now() {
        return (int)(new Date().getTime()/1000);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (instance == this) {
            instance = null;
        }

        channelIDs.remove(channelID);
        activityCount--;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        IMService.getInstance().removeRTObserver(this);

        ConferenceEvent e = new ConferenceEvent(initiator, groupID, true);
        BusProvider.getInstance().post(e);
    }


    public void requestRecordPermission() {
        MediaRecorder recorder;

        File f = new File(getCacheDir(), "permission.amr");
        String pathName = f.getAbsolutePath();
        try {
            Log.i(TAG, "start record");
            recorder = new MediaRecorder();

            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setOutputFile(pathName);
            recorder.prepare();
            recorder.start();

            recorder.stop();
            recorder.reset();
            recorder.release();
        } catch (Exception e) {
            Log.e(TAG,
                    "Record start error:  " + e != null ? e
                            .getMessage() : "");
        }
    }

    @Override
    protected void ping() {
        super.ping();

        //timeout invitees
        ArrayList<Invitee> t = new ArrayList<>();
        int n = now();
        for (Invitee p : invitees) {
            if (n - p.timestamp > 60) {
                //timeout;
                t.add(p);
                continue;
            }
            sendInvite(p.id);
        }

        for (Invitee p : t) {
            invitees.remove(p);
        }
    }



    public void sendInvite(long to) {
        try {
            String channelID = this.channelID;
            long initiator = this.initiator;
            long groupID = this.groupID;
            long[] participants = this.participantIds;

            ConferenceCommand conferenceCommand = new ConferenceCommand();
            conferenceCommand.command = ConferenceCommand.COMMAND_INVITE;
            conferenceCommand.channelID = channelID;
            conferenceCommand.initiator = initiator;
            conferenceCommand.groupID = groupID;
            conferenceCommand.participants = participants;

            JSONObject json = new JSONObject();
            json.put("conference", conferenceCommand.getContent());

            RTMessage rt = new RTMessage();
            rt.sender = currentUID;
            rt.receiver = to;
            rt.content = json.toString();
            IMService.getInstance().sendRTMessage(rt);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void sendRTMessage(String command, long to, String channelID) {
        try {
            ConferenceCommand conferenceCommand = new ConferenceCommand();
            conferenceCommand.command = command;
            conferenceCommand.channelID = this.channelID;
            JSONObject json = new JSONObject();
            json.put("conference", conferenceCommand.getContent());

            RTMessage rt = new RTMessage();
            rt.sender = currentUID;
            rt.receiver = to;
            rt.content = json.toString();
            IMService.getInstance().sendRTMessage(rt);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRTMessage(RTMessage rt) {
        try {
            JSONObject json = new JSONObject(rt.content);

            if (!json.has("conference")) {
                return;
            }

            Log.i(TAG, "conference rt message sender:" + rt.sender + " receiver:" + rt.receiver + " content:" + rt.content);
            JSONObject obj = json.getJSONObject("conference");

            ConferenceCommand confCommand = new ConferenceCommand(obj);
            String command = confCommand.command;

            if (!confCommand.channelID.equals(this.channelID)) {
                return;
            }

            if (command.equals(ConferenceCommand.COMMAND_ACCEPT)) {
                for (int i = 0; i < invitees.size(); i++) {
                    Invitee invitee = invitees.get(i);
                    if (invitee.id == rt.sender) {
                        invitees.remove(i);
                        break;
                    }
                }
                sendRTMessage(ConferenceCommand.COMMAND_ACCEPTED, rt.sender, channelID);
            }

            if (command.equals(ConferenceCommand.COMMAND_REFUSE)) {
                for (int i = 0; i < invitees.size(); i++) {
                    Invitee invitee = invitees.get(i);
                    if (invitee.id == rt.sender) {
                        invitees.remove(i);
                        break;
                    }
                }
                sendRTMessage(ConferenceCommand.COMMAND_REFUSED, rt.sender, channelID);
            }

            if (command.equals(ConferenceCommand.COMMAND_INVITE)) {
                sendRTMessage(ConferenceCommand.COMMAND_ACCEPT, rt.sender, channelID);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            Log.i(TAG, "take or select picture fail:" + resultCode);
            return;
        }


        if (requestCode == 1) {
            long[] ids = data.getLongArrayExtra("participants");
            if (ids == null) {
                return;
            }
            int n = now();
            for (int i = 0; i < ids.length; i++) {
                invitees.add(new Invitee(ids[i], n));
            }
        }
    }

    private long[] toArray(List<Long> values) {
        long[] result = new long[values.size()];
        int i = 0;
        for (Long l : values)
            result[i++] = l;
        return result;
    }


    public void add(View v) {
        if (this.connectState != ConnectState.CONNECTED) {
            return;
        }

        ArrayList<Long> ps = new ArrayList<>();
        for (Participant p : this.participants){
            ps.add(p.getUid());
        }
        for (int i = 0; i < invitees.size(); i++) {
            Invitee invitee = invitees.get(i);
            if (!ps.contains(invitee.id)) {
                ps.add(invitee.id);
            }
        }

        long[] ids = toArray(ps);

        Intent intent = new Intent();
        intent.putExtra("group_id", groupID);
        intent.putExtra("participants", ids);
        intent.setClass(this, ConferenceParticipantActivity.class);
        startActivityForResult(intent, 1);
    }



}
