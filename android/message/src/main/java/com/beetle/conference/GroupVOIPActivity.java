package com.beetle.conference;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beetle.im.Timer;
import com.beetle.voip.PeerConnectionClient;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.http.AsyncHttpClient;
import com.koushikdutta.async.http.WebSocket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.DefaultVideoDecoderFactory;
import org.webrtc.DefaultVideoEncoderFactory;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SessionDescription;
import org.webrtc.SoftwareVideoDecoderFactory;
import org.webrtc.SoftwareVideoEncoderFactory;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoDecoderFactory;
import org.webrtc.VideoEncoderFactory;
import org.webrtc.audio.AudioDeviceModule;
import org.webrtc.audio.JavaAudioDeviceModule;
import org.webrtc.voiceengine.WebRtcAudioManager;
import org.webrtc.voiceengine.WebRtcAudioUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static android.os.SystemClock.uptimeMillis;
import com.beetle.message.R;

public class GroupVOIPActivity extends Activity implements Participant.ParticipantObserver {
    private static final String TAG = "face";
    private static final int PERMISSIONS_REQUEST = 3;

    public static String WS_URL = "ws://120.236.26.28:8889/groupcall";


    private static final String VIDEO_FLEXFEC_FIELDTRIAL =
            "WebRTC-FlexFEC-03-Advertised/Enabled/WebRTC-FlexFEC-03/Enabled/";
    private static final String VIDEO_VP8_INTEL_HW_ENCODER_FIELDTRIAL = "WebRTC-IntelVP8/Enabled/";
    private static final String DISABLE_WEBRTC_AGC_FIELDTRIAL =
            "WebRTC-Audio-MinimizeResamplingOnMobile/Enabled/";


    private static final String VIDEO_CODEC_VP8 = "VP8";
    private static final String VIDEO_CODEC_VP9 = "VP9";
    private static final String VIDEO_CODEC_H264 = "H264";
    private static final String VIDEO_CODEC_H264_BASELINE = "H264 Baseline";
    private static final String VIDEO_CODEC_H264_HIGH = "H264 High";


    enum ConnectState {
        UNCONNECT,
        CONNECTING,
        CONNECTED,
        CONNECTFAIL,
    }

    PeerConnectionFactory factory;
    private EglBase rootEglBase;



    protected   ScheduledExecutorService executor;
    ArrayList<Participant> participants = new ArrayList<>();

    private MusicIntentReceiver headsetReceiver;

    protected long currentUID;
    protected String channelID;
    protected String token;

    TextView durationTextView;

    protected ConnectState connectState = ConnectState.UNCONNECT;
    WebSocket ws;
    Timer pingTimer;

    int duration;

    boolean finished = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getWindow().addFlags( WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_conference);

        durationTextView = (TextView) findViewById(R.id.duration);
    }

    protected void init() {
        executor = Executors.newSingleThreadScheduledExecutor();
        rootEglBase = EglBase.create();
        createPeerConnectionFactory(this);

        headsetReceiver = new MusicIntentReceiver();


        try {
            AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
            am.setSpeakerphoneOn(true);
            am.setMode(AudioManager.MODE_IN_COMMUNICATION);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int cameraPermission = (checkSelfPermission(Manifest.permission.CAMERA));
            int recordPermission = (checkSelfPermission(Manifest.permission.RECORD_AUDIO));

            if (cameraPermission != PackageManager.PERMISSION_GRANTED ||
                    recordPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                start();
            }
        } else {
            start();
        }
    }

    protected void start() {
        pingTimer = new Timer() {
            @Override
            protected void fire() {
                GroupVOIPActivity.this.ping();
            }
        };
        pingTimer.setTimer(uptimeMillis() + 1000, 1000);
        pingTimer.resume();

        connect();
    }

    protected void ping() {
        Log.i(TAG, "ping...");
        duration += 1;

        String text = String.format("%02d:%02d", duration/60, duration%60);
        durationTextView.setText(text);

        if (connectState == ConnectState.UNCONNECT || connectState == ConnectState.CONNECTFAIL) {
            connect();
            return;
        }

        if (connectState == ConnectState.CONNECTED && duration%10 == 0) {
            //10s发一次ping
            try {
                JSONObject m = new JSONObject();
                m.put("id", "ping");
                String s = m.toString();
                sendMessage(s);
            } catch(JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void connect() {
        if (connectState == ConnectState.CONNECTING || connectState == ConnectState.CONNECTED) {
            return;
        }
        if (finished) {
            return;
        }

        Log.i(TAG, "websocket connecting");
        connectState = ConnectState.CONNECTING;
        AsyncHttpClient.getDefaultInstance().websocket(WS_URL, "ws", new AsyncHttpClient.WebSocketConnectCallback() {
            @Override
            public void onCompleted(final Exception ex, final WebSocket webSocket) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onConnected(ex, webSocket);
                    }
                });
            }
        });
    }

    void onConnected(Exception ex, final WebSocket webSocket) {
        if (ex != null) {
            connectState = ConnectState.CONNECTFAIL;
            Log.i(TAG, "websocket connect fail:" + ex);
            ex.printStackTrace();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    GroupVOIPActivity.this.connect();
                }
            }, 1000*5);
            return;
        }

        Log.i("Socket", "Socket Connected Successfully");

        connectState = ConnectState.CONNECTED;
        ws = webSocket;

        ws.setStringCallback(new WebSocket.StringCallback() {
            public void onStringAvailable(final String responseJson) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "websocket received data:" + responseJson);
                        handleMessage(responseJson);
                    }
                });

            }
        });
        ws.setClosedCallback(new CompletedCallback() {
            @Override
            public void onCompleted(Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "websocket closed");
                        connectState = ConnectState.UNCONNECT;
                        ws.close();
                        ws = null;
                        removeAllParticipants();

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                GroupVOIPActivity.this.connect();
                            }
                        }, 100);
                    }
                });
            }
        });

        register();
    }

    public void removeAllParticipants() {
        Log.i(TAG, "on room closed");
        for (int i = 0; i < participants.size(); i++) {
            Participant p = participants.get(i);
            p.dispose();
        }
        participants.clear();
        RelativeLayout ll = (RelativeLayout) findViewById(R.id.relativeLayout);
        ll.removeAllViews();
    }

    void handleMessage(String resp) {
        Log.i(TAG, "resp:" + resp);
        try {
            JSONObject obj = new JSONObject(resp);

            String id = obj.getString("id");
            if (id.equals("existingParticipants")) {
                onExistingParticipants(obj);
            } else if (id.equals("newParticipantArrived")) {
                onNewParticipantArrived(obj);
            } else if (id.equals("participantLeft")) {
                onParticipantLeft(obj);
            } else if (id.equals("receiveVideoAnswer")) {
                onReceiveVideoAnswer(obj);
            } else if (id.equals("iceCandidate")) {
                onIceCandidate(obj);
            } else {
                Log.i(TAG, "unrecognized message:" + obj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void register() {
        try {
            JSONObject m = new JSONObject();
            m.put("id", "joinRoom");
            m.put("name", "" + this.currentUID);
            m.put("room", this.channelID);
            m.put("token", this.token);
            String s = m.toString();
            sendMessage(s);
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    void leaveRoom() {
        try {
            JSONObject m = new JSONObject();
            m.put("id", "leaveRoom");
            String s = m.toString();
            sendMessage(s);
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }


    void sendMessage(String m) {
        if (ws == null) {
            return;
        }
        ws.send(m);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST && permissions != null && grantResults != null) {
            for (int i = 0; i < permissions.length && i < grantResults.length; i++) {
                Log.i(TAG, "granted permission:" + permissions[i] + " " + grantResults[i]);
            }
        }

        if (requestCode == PERMISSIONS_REQUEST) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int cameraPermission = (checkSelfPermission(Manifest.permission.CAMERA));
                int recordPermission = (checkSelfPermission(Manifest.permission.RECORD_AUDIO));

                if (cameraPermission == PackageManager.PERMISSION_GRANTED &&
                        recordPermission == PackageManager.PERMISSION_GRANTED) {
                    start();
                }
            }
        }

    }




    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(headsetReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(headsetReceiver, filter);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    @Override
    public void onBackPressed() {
        hangup();
        super.onBackPressed();
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int cameraPermission = (checkSelfPermission(Manifest.permission.CAMERA));
            int recordPermission = (checkSelfPermission(Manifest.permission.RECORD_AUDIO));

            ArrayList<String> permissions = new ArrayList<String>();
            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (recordPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.RECORD_AUDIO);
            }

            if (permissions.size() > 0) {
                String[] array = new String[permissions.size()];
                permissions.toArray(array);
                this.requestPermissions(array, PERMISSIONS_REQUEST);
            }

        }
    }

    public void mute(View v) {
        Log.i(TAG, "toogle audio");

        if (participants.size() == 0) {
            return;
        }
        final Participant p = participants.get(0);
        if (p.getUid() != this.currentUID) {
            return;
        }

        final boolean isEnabled= !p.audioTrack.enabled();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                p.audioTrack.setEnabled(isEnabled);
            }
        });
        ImageButton muteButton = (ImageButton)findViewById(R.id.mute);
        if (isEnabled) {
            muteButton.setBackgroundColor(Color.TRANSPARENT);
        } else {
            muteButton.setBackgroundColor(Color.WHITE);
        }
    }

    public void toogleCamera(View v) {
        Log.i(TAG, "toogle camera");

        if (participants.size() == 0) {
            return;
        }
        Participant p = participants.get(0);
        if (p.getUid() != this.currentUID) {
            return;
        }

        ImageButton cameraButton = (ImageButton)findViewById(R.id.camera);

        boolean isEnabled = p.videoTrack.enabled();
        if (isEnabled) {
            cameraButton.setBackgroundColor(Color.TRANSPARENT);
        } else {
            cameraButton.setBackgroundColor(Color.WHITE);
        }

        p.toogleVideo();
    }

    public void switchCamera() {

        Log.i(TAG, "switch camera");
        if (participants.size() == 0) {
            return;
        }
        final Participant p = participants.get(0);
        if (p.getUid() != this.currentUID) {
            return;
        }
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (!p.videoTrack.enabled()) {
                    return;
                }
                CameraVideoCapturer cameraVideoCapturer = (CameraVideoCapturer) p.videoCapturer;
                cameraVideoCapturer.switchCamera(null);
            }
        });
    }

    public void hangup(View v) {
        Log.i(TAG, "hangup...");
        hangup();
        finish();
    }

    void hangup() {
        finished = true;
        this.leaveRoom();
        if (ws != null) {
            ws.close();
            ws = null;
        }

        if (pingTimer != null) {
            pingTimer.suspend();
            pingTimer = null;
        }

        for (int i = 0; i < participants.size(); i++) {
            Participant p = participants.get(i);
            p.dispose();
        }
        participants.clear();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (factory != null) {
                        factory.dispose();
                        factory = null;
                    }

                    if (rootEglBase != null) {
                        rootEglBase.release();
                        rootEglBase = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }
            }
        });
    }

    protected String getUserName(long uid) {
        return "";
    }

    void createTestParticipant() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int w = size.x/2;
        int h = w;
        int x = w*(participants.size()%2);
        int y = h*(participants.size()/2);

        SurfaceViewRenderer render = new SurfaceViewRenderer(this);
        render.init(rootEglBase.getEglBaseContext(), null);

        RelativeLayout ll = (RelativeLayout) findViewById(R.id.relativeLayout);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
        lp.leftMargin = x;
        lp.topMargin = y;
        render.setLayoutParams(lp);
        render.setBackgroundColor(Color.RED);
        ll.addView(render);


        Participant p = new Participant(-1, this.channelID, "test", this, rootEglBase, this.getApplicationContext(), executor);
        p.setFactory(factory);
        p.setVideoRender(render);
        this.participants.add(p);
    }

    void createLocalParticipant() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int w = size.x/2;
        int h = w;
        int x = w*(participants.size()%2);
        int y = h*(participants.size()/2);

        SurfaceViewRenderer render = new SurfaceViewRenderer(this);
        render.init(rootEglBase.getEglBaseContext(), null);
        render.setZOrderMediaOverlay(true);
        render.getHolder().setFormat(PixelFormat.TRANSPARENT);

        RelativeLayout ll = (RelativeLayout) findViewById(R.id.relativeLayout);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
        lp.leftMargin = x;
        lp.topMargin = y;
        render.setLayoutParams(lp);
        ll.addView(render);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GroupVOIPActivity.this.switchCamera();
            }
        };

        render.setOnClickListener(listener);

        VideoCapturer capturer = createVideoCapturer();

        String name = getUserName(currentUID);
        Participant p = new Participant(this.currentUID, name, this.channelID,this, rootEglBase, this.getApplicationContext(), executor);

        p.createPeerConnection(factory, render, rootEglBase.getEglBaseContext(), capturer);
        //default disable video track
        p.toogleVideo();

        this.participants.add(p);

    }

    void createRemoteParticipant(long pid) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int w = size.x/2;
        int h = w;
        int x = w*(participants.size()%2);
        int y = h*(participants.size()/2);

        SurfaceViewRenderer render = new SurfaceViewRenderer(this);
        render.init(rootEglBase.getEglBaseContext(), null);

        RelativeLayout ll = (RelativeLayout) findViewById(R.id.relativeLayout);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
        lp.leftMargin = x;
        lp.topMargin = y;
        render.setLayoutParams(lp);
        ll.addView(render);

        String name = getUserName(pid);
        Participant p = new Participant(pid, name, this.channelID, this, rootEglBase, this.getApplicationContext(), executor);
        p.createRemotePeerConnection(factory, render, rootEglBase.getEglBaseContext());

        this.participants.add(p);
    }

    void onExistingParticipants(JSONObject map) throws JSONException {
        if (this.participants.size() > 0) {
            Log.e(TAG, "participants not empty");
            return;
        }

        createLocalParticipant();

        JSONArray data = map.getJSONArray("data");
        for (int i = 0; i < data.length(); i++) {
            long pid = Long.parseLong(data.getString(i));
            createRemoteParticipant(pid);
        }


//        for (int i = 0; i < 10; i++) {
//            createTestParticipant();
//        }
    }

    void onReceiveVideoAnswer(JSONObject map) throws JSONException {
        String name = map.getString("name");
        long uid = Long.parseLong(name);
        int index = -1;
        for (int i = 0; i < participants.size(); i++) {
            Participant p = participants.get(i);
            if (p.getUid() == uid) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return;
        }

        String sdp = map.getString("sdpAnswer");
        SessionDescription description = new SessionDescription(SessionDescription.Type.ANSWER, sdp);


        Participant p = participants.get(index);
        p.setRemoteDescription(description);
    }

    void onNewParticipantArrived(JSONObject map) throws JSONException {
        String name = map.getString("name");
        long uid = Long.parseLong(name);
        int index = -1;
        for (int i = 0; i < participants.size(); i++) {
            Participant p = participants.get(i);
            if (p.getUid() == uid) {
                index = i;
                break;
            }
        }

        if (index != -1) {
            return;
        }
        createRemoteParticipant(uid);
    }

    void onParticipantLeft(JSONObject map) throws JSONException{
        String name = map.getString("name");
        long uid = Long.parseLong(name);
        int index = -1;
        for (int i = 0; i < participants.size(); i++) {
            Participant p = participants.get(i);
            if (p.getUid() == uid) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return;
        }
        Participant p = participants.get(index);
        p.dispose();
        participants.remove(index);

        RelativeLayout ll = (RelativeLayout) findViewById(R.id.relativeLayout);
        ll.removeView(p.videoRender);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int w = size.x/2;
        int h = w;

        for (int i = index; i < participants.size(); i++) {
            int x = w*(i%2);
            int y = h*(i/2);

            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(w, h);
            lp.leftMargin = x;
            lp.topMargin = y;
            p.videoRender.setLayoutParams(lp);
        }
    }

    void onIceCandidate(JSONObject map) throws JSONException {
        JSONObject mm = map.getJSONObject("candidate");
        String sdpMid = mm.getString("sdpMid");
        String sdp = mm.getString("candidate");
        int sdpMLineIndex = mm.getInt("sdpMLineIndex");
        String name = map.getString("name");
        long uid = Long.parseLong(name);

        int index = -1;
        for (int i = 0; i < participants.size(); i++) {
            Participant p = participants.get(i);
            if (p.getUid() == uid) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return;
        }

        IceCandidate candidate = new IceCandidate(sdpMid, sdpMLineIndex, sdp);
        Participant p = participants.get(index);
        p.addRemoteIceCandidate(candidate);
    }

    @Override
    public void onLocalOfferSDP(Participant p, SessionDescription sdp) {
        try {
            JSONObject map = new JSONObject();
            map.put("id", "receiveVideoFrom");
            map.put("sender", "" + p.getUid());
            map.put("sdpOffer", sdp.description);
            sendMessage(map.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocalIceCandidate(Participant p, IceCandidate candidate) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("sdpMid", candidate.sdpMid);
            obj.put("sdpMLineIndex", candidate.sdpMLineIndex);
            obj.put("candidate", candidate.sdp);

            JSONObject map = new JSONObject();

            map.put("id", "onIceCandidate");
            map.put("name", "" + p.getUid());
            map.put("candidate", obj);
            sendMessage(map.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class MusicIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        Log.d(TAG, "Headset is unplugged");
                        audioManager.setSpeakerphoneOn(true);
                        break;
                    case 1:
                        Log.d(TAG, "Headset is plugged");
                        audioManager.setSpeakerphoneOn(false);
                        break;
                    default:
                        Log.d(TAG, "I have no idea what the headset state is");
                }
            }
        }
    }


    public static class PeerConnectionParameters {
        public final boolean videoCallEnabled;
        public final boolean loopback;
        public final boolean tracing;
        public final int videoWidth;
        public final int videoHeight;
        public final int videoFps;
        public final int videoMaxBitrate;
        public final String videoCodec;
        public final boolean videoCodecHwAcceleration;
        public final int audioStartBitrate;
        public final String audioCodec;
        public final boolean noAudioProcessing;
        public final boolean aecDump;
        public final boolean useOpenSLES;
        public final boolean disableBuiltInAEC;
        public final boolean disableBuiltInAGC;
        public final boolean disableBuiltInNS;
        public final boolean enableLevelControl;

        public PeerConnectionParameters(boolean videoCallEnabled, boolean loopback, boolean tracing,
                                        int videoWidth, int videoHeight, int videoFps, int videoMaxBitrate, String videoCodec,
                                        boolean videoCodecHwAcceleration, int audioStartBitrate, String audioCodec,
                                        boolean noAudioProcessing, boolean aecDump, boolean useOpenSLES, boolean disableBuiltInAEC,
                                        boolean disableBuiltInAGC, boolean disableBuiltInNS, boolean enableLevelControl) {
            this.videoCallEnabled = videoCallEnabled;
            this.loopback = loopback;
            this.tracing = tracing;
            this.videoWidth = videoWidth;
            this.videoHeight = videoHeight;
            this.videoFps = videoFps;
            this.videoMaxBitrate = videoMaxBitrate;
            this.videoCodec = videoCodec;
            this.videoCodecHwAcceleration = videoCodecHwAcceleration;
            this.audioStartBitrate = audioStartBitrate;
            this.audioCodec = audioCodec;
            this.noAudioProcessing = noAudioProcessing;
            this.aecDump = aecDump;
            this.useOpenSLES = useOpenSLES;
            this.disableBuiltInAEC = disableBuiltInAEC;
            this.disableBuiltInAGC = disableBuiltInAGC;
            this.disableBuiltInNS = disableBuiltInNS;
            this.enableLevelControl = enableLevelControl;
        }
    }
    private void createPeerConnectionFactory(final Context context) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                createPeerConnectionFactoryInternal(context);
            }
        });
    }
    private void createPeerConnectionFactoryInternal(Context context) {

        PeerConnectionParameters peerConnectionParameters = new PeerConnectionParameters(true, false, false,
                0, 0, 0, 0, null, true, 0,
                null, false, false, false, false, false,
                false, false);

        Log.d(TAG,"Create peer connection factory.");

        // Enable/disable OpenSL ES playback.
        if (!peerConnectionParameters.useOpenSLES) {
            Log.d(TAG, "Disable OpenSL ES audio even if device supports it");
            WebRtcAudioManager.setBlacklistDeviceForOpenSLESUsage(true /* enable */);
        } else {
            Log.d(TAG, "Allow OpenSL ES audio if device supports it");
            WebRtcAudioManager.setBlacklistDeviceForOpenSLESUsage(false);
        }

        if (peerConnectionParameters.disableBuiltInAEC) {
            Log.d(TAG, "Disable built-in AEC even if device supports it");
            WebRtcAudioUtils.setWebRtcBasedAcousticEchoCanceler(true);
        } else {
            Log.d(TAG, "Enable built-in AEC if device supports it");
            WebRtcAudioUtils.setWebRtcBasedAcousticEchoCanceler(false);
        }

        if (peerConnectionParameters.disableBuiltInAGC) {
            Log.d(TAG, "Disable built-in AGC even if device supports it");
            WebRtcAudioUtils.setWebRtcBasedAutomaticGainControl(true);
        } else {
            Log.d(TAG, "Enable built-in AGC if device supports it");
            WebRtcAudioUtils.setWebRtcBasedAutomaticGainControl(false);
        }

        if (peerConnectionParameters.disableBuiltInNS) {
            Log.d(TAG, "Disable built-in NS even if device supports it");
            WebRtcAudioUtils.setWebRtcBasedNoiseSuppressor(true);
        } else {
            Log.d(TAG, "Enable built-in NS if device supports it");
            WebRtcAudioUtils.setWebRtcBasedNoiseSuppressor(false);
        }


        PeerConnectionFactory.InitializationOptions.Builder builder = PeerConnectionFactory.InitializationOptions.builder(context.getApplicationContext());
        builder.setFieldTrials("");
        builder.setEnableInternalTracer(true);
        PeerConnectionFactory.InitializationOptions initOptions = builder.createInitializationOptions();
        PeerConnectionFactory.initialize(initOptions);



        AudioDeviceModule adm = createJavaAudioDevice(peerConnectionParameters);

        final boolean enableH264HighProfile =
                VIDEO_CODEC_H264_HIGH.equals(peerConnectionParameters.videoCodec);
        final VideoEncoderFactory encoderFactory;
        final VideoDecoderFactory decoderFactory;

        if (peerConnectionParameters.videoCodecHwAcceleration) {
            encoderFactory = new DefaultVideoEncoderFactory(
                    rootEglBase.getEglBaseContext(), true /* enableIntelVp8Encoder */, enableH264HighProfile);
            decoderFactory = new DefaultVideoDecoderFactory(rootEglBase.getEglBaseContext());
        } else {
            encoderFactory = new SoftwareVideoEncoderFactory();
            decoderFactory = new SoftwareVideoDecoderFactory();
        }

        PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();

        factory = PeerConnectionFactory.builder()
                .setOptions(options)
                .setAudioDeviceModule(adm)
                .setVideoEncoderFactory(encoderFactory)
                .setVideoDecoderFactory(decoderFactory)
                .createPeerConnectionFactory();
        Log.d(TAG, "Peer connection factory created.");
        adm.release();
        Log.d(TAG, "Peer connection factory created.");
    }



    AudioDeviceModule createJavaAudioDevice(PeerConnectionParameters peerConnectionParameters) {
        // Enable/disable OpenSL ES playback.
        if (!peerConnectionParameters.useOpenSLES) {
            Log.w(TAG, "External OpenSLES ADM not implemented yet.");
            // TODO(magjed): Add support for external OpenSLES ADM.
        }
        JavaAudioDeviceModule.AudioTrackErrorCallback audioTrackErrorCallback = new JavaAudioDeviceModule.AudioTrackErrorCallback() {
            @Override
            public void onWebRtcAudioTrackInitError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackInitError: " + errorMessage);
            }

            @Override
            public void onWebRtcAudioTrackStartError(
                    JavaAudioDeviceModule.AudioTrackStartErrorCode errorCode, String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackStartError: " + errorCode + ". " + errorMessage);
            }

            @Override
            public void onWebRtcAudioTrackError(String errorMessage) {
                Log.e(TAG, "onWebRtcAudioTrackError: " + errorMessage);
            }
        };

        return JavaAudioDeviceModule.builder(this.getApplicationContext())
                .setUseHardwareAcousticEchoCanceler(!peerConnectionParameters.disableBuiltInAEC)
                .setUseHardwareNoiseSuppressor(!peerConnectionParameters.disableBuiltInNS)
                .setAudioTrackErrorCallback(audioTrackErrorCallback)
                .createAudioDeviceModule();
    }



    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(this);
    }

    private VideoCapturer createVideoCapturer() {
        VideoCapturer videoCapturer = null;
        if (useCamera2()) {
            Log.d(TAG, "Creating capturer using camera2 API.");
            videoCapturer = createCameraCapturer(new Camera2Enumerator(this));
        } else {
            Log.d(TAG, "Creating capturer using camera1 API.");
            videoCapturer = createCameraCapturer(new Camera1Enumerator(true));
        }
        if (videoCapturer == null) {
            Log.e(TAG, "Failed to open camera");
            return null;
        }
        return videoCapturer;
    }

    private VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // First, try to find front facing camera
        Log.d(TAG, "Looking for front facing cameras.");
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating front facing camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        Log.d(TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Log.d(TAG, "Creating other camera capturer.");
                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

}
