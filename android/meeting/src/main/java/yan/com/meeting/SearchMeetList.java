package yan.com.meeting;

import android.util.Log;
import android.widget.Toast;

import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.types.Meet;
import com.beetle.goubuli.api.types.PostModel;
import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.MeetDB;
import com.beetle.goubuli.util.ParseMD5;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class SearchMeetList {

    List<Meet> filteredData = new ArrayList<>();
    List<Meet> allmeetlist = new ArrayList<>();


    public SearchMeetList(){
        allmeetlist = MeetDB.getInstance().loadGroups();
    }

    public List<Meet> SearchToMeet(String newText) {
        String text = newText.replace(" ", "");
        Iterator it = allmeetlist.iterator();
        while(it.hasNext()) {
            Meet m = (Meet)it.next();
            if(m.fldroomname.indexOf(text) != -1){
                filteredData.add(m);
            }
        }
        return filteredData;
    };
}
