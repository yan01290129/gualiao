package yan.com.meeting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.beetle.goubuli.model.MeetDB;

import org.linphone.LinphoneActivity;

public class MeetChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet_chat);

        // 添加返回按钮
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // 设置标题
        Intent intent = getIntent();
        String fldroomname = intent.getStringExtra("fldroomname");
        if (fldroomname == null) {
            fldroomname = "";
        }
        getSupportActionBar().setTitle(fldroomname);

        String number = MeetDB.getInstance().getMeetfldtxnum(fldroomname);
        intent.putExtra("userno",number);
        intent.putExtra("name",fldroomname);
        intent.setClass(this,LinphoneActivity.class);
        startActivity(intent);
    }

    // 返回按钮事件
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish(); // back button
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
