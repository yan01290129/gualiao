package yan.com.meeting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beetle.contact.ContactDetailsActivity;
import com.beetle.goubuli.BaseActivity;
import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.types.CheckMeet;
import com.beetle.goubuli.api.types.Meet;
import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.util.ParseMD5;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import static com.beetle.goubuli.util.Utils.getContext;

public class SearchMeetActivity extends BaseActivity implements AdapterView.OnItemClickListener  {

    private static final String TAG = "goubuli";


    Toolbar toolbar;
    private SearchView searchView;
    private MenuItem searchItem;

    List<Meet> meetsList = new ArrayList<>();
    private BaseAdapter adapter;
    private ListView mListMeet = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_meet);

        mListMeet = (ListView)findViewById(yan.com.meeting.R.id.list);
        toolbar = (Toolbar)findViewById(yan.com.meeting.R.id.toolbar);

        setSupportActionBar(toolbar);
        initActionBar();

        adapter = new MeetAdapter();
        mListMeet.setAdapter(adapter);
        mListMeet.setOnItemClickListener(this);
    }

    class MeetAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return meetsList.size();
        }

        @Override
        public Object getItem(int position) {
            return meetsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.meet, null);
            } else {
                view = convertView;
            }
            Meet m = meetsList.get(position);
            ImageView avatarView = (ImageView) view.findViewById(R.id.header);
            avatarView.setImageResource(R.drawable.unmute);
            TextView tv = (TextView) view.findViewById(R.id.name);
            tv.setText(m.fldroomname);
            tv = (TextView) view.findViewById(R.id.deptname);
            tv.setText(m.flddepart);
            tv = (TextView) view.findViewById(R.id.fldonlinenum);
            tv.setText(m.fldonlinenum);
            return view;
        }
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            initSearchView(actionBar);
        }
    }

    private void initSearchView(ActionBar actionBar) {
        if (actionBar == null) {
            return;
        }
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = new SearchView(actionBar.getThemedContext());
        searchView.setQueryHint(getString(android.R.string.search_go));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setIconified(false);
        searchView.setMaxWidth(1000);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() < 1) {
                    meetsList = new  ArrayList<Meet>();
                    adapter.notifyDataSetChanged();
                    return true;
                }
                SearchMeetList searchMeetList = new SearchMeetList();
                List<Meet> filteredData = searchMeetList.SearchToMeet(newText);
                meetsList = filteredData;
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    // ListView列表点击事件
    Meet m;
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        if (position < mListMeet.getHeaderViewsCount()) {
            return;
        }
        position -= mListMeet.getHeaderViewsCount();
        m = meetsList.get(position);
        /*
        @setView 装入一个EditView
         */
        final View dialogView = LayoutInflater.from(SearchMeetActivity.this)
                .inflate(R.layout.dialog_edit, null);
        AlertDialog.Builder inputDialog =
                new AlertDialog.Builder(SearchMeetActivity.this);
        inputDialog.setTitle("输入会议室密码").setView(dialogView);
        inputDialog.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 获取EditView中的输入内容
                        EditText edit_text =
                                (EditText) dialogView.findViewById(R.id.edit_text);
                        if (TextUtils.isEmpty(edit_text.getText().toString())) {
                            Toast.makeText(getApplicationContext(), "不可为空", Toast.LENGTH_SHORT).show();
                        }else {
                            syncCheckMeet(m.fldroomname,edit_text.getText().toString());
                        }
                    }
                }).setNegativeButton("取消", null).show();
    }

    private boolean syncing = false;
    // 核对会议密码
    private void syncCheckMeet(String fldroomname, String edpwd) {
        if (syncing) {
            return;
        }
        syncing = true;
        showWaitingDialog();
        String apiStr = "RhtxCheckMeetingRoom";
        String username = "user101";
        String password = "123";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
        String token = ParseMD5.parseStrToMd5L32(username + password + apiStr + sdf.format(date));
        CheckMeet pm = new CheckMeet();
        pm.username = username;
        pm.method = apiStr;
        pm.token = token;
        pm.fldroomname = fldroomname;
        pm.password = edpwd;
        IMHttp imHttp = IMHttpFactory.Singleton();
        imHttp.checkMeet(pm)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject jsonObject) {
                        syncing = false;
                        disWaitingDialog();
                        Meet meet = new Meet();
                        boolean flag = jsonObject.get("return_code").getAsBoolean();
                        if(flag){
                            goActivity();
                        }else {
                            Toast.makeText(getApplicationContext(), "密码错误", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.i(TAG, "sync pwd fail:" + throwable);
                        syncing = false;
                        disWaitingDialog();
                        Toast.makeText(getApplicationContext(), "核对密码失败", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    // 跳转会议页面MeetChatActivity
    public void goActivity(){
        Intent intent = new Intent(this,MeetChatActivity.class);
        intent.putExtra("fldroomname",m.fldroomname);
        startActivity(intent);
    }

    // 加载中。。显示
    ProgressDialog waitingDialog;
    private void showWaitingDialog() {
        /* 等待Dialog具有屏蔽其他控件的交互能力
         * @setCancelable 为使屏幕不可点击，设置为不可取消(false)
         * 下载等事件完成后，主动调用函数关闭该Dialog
         */
        waitingDialog =
                new ProgressDialog(SearchMeetActivity.this);
        waitingDialog.setTitle("会议室检验");
        waitingDialog.setMessage("等待中...");
        waitingDialog.setIndeterminate(true);
        waitingDialog.setCancelable(true);
        waitingDialog.show();
    }
    // 加载中。。销毁
    private void disWaitingDialog() {
        waitingDialog.dismiss();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        initSearchItem(menu);
        return true;
    }


    private void initSearchItem(Menu menu) {
        searchItem = menu.add(android.R.string.search_go);

        searchItem.setIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case android.R.id.home:
                        finish();
                        break;
                }
                return false;
            }
        });

        MenuItemCompat.setActionView(searchItem, searchView);
        MenuItemCompat.setShowAsAction(searchItem, MenuItemCompat.SHOW_AS_ACTION_ALWAYS | MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                finish();
                return false;
            }
        });
        showSearch(true);
    }

    private void showSearch(boolean visible) {
        if (visible) {
            MenuItemCompat.expandActionView(searchItem);
        } else {
            MenuItemCompat.collapseActionView(searchItem);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


}
