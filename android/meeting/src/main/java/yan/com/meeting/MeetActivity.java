package yan.com.meeting;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beetle.contact.SearchContactActivity;
import com.beetle.goubuli.UserApplication;
import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.types.CheckMeet;
import com.beetle.goubuli.api.types.Meet;
import com.beetle.goubuli.api.types.PostModel;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.MeetDB;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.SyncCompleteEvent;
import com.beetle.goubuli.util.ParseMD5;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class MeetActivity extends AppCompatActivity implements ListView.OnItemClickListener {

    private ListAdapter adapter;
    private boolean syncing = false;
    private static final String TAG = "MeetActivity";
    private ArrayList<Meet> meetsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet);

        ListView listView = (ListView) findViewById(R.id.meetLsit);
        adapter = new MeetAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        // 上下文菜单
        registerForContextMenu(listView);
        // 注册监听
        BusProvider.getInstance().register(messageHandler);
    }

    // 接收广联系人更新广播后动态更新会议列表
    final Object messageHandler = new Object() {
        @Subscribe
        public void onSyncCompleteEvent(SyncCompleteEvent event) {
            syncMeet();
        }
    };


    // 获取会议列表
    private void syncMeet() {
        if (syncing) {
            return;
        }
        syncing = true;
        String apiStr = "RhtxMeetingRoom";
        String username = "user101";
        String password = "123";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
        String token = ParseMD5.parseStrToMd5L32(username + password + apiStr + sdf.format(date));
        PostModel pm = new PostModel();
        pm.username = username;
        pm.method = apiStr;
        pm.token = token;
        IMHttp imHttp = IMHttpFactory.Singleton();
        imHttp.getMeets(pm)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject jsonObject) {
                        syncing = false;
                        meetsList.clear();
                        Gson g = new Gson();
                        JsonArray jsonElements = jsonObject.getAsJsonArray("datas");
                        // 清空数据库
                        MeetDB.getInstance().clear();
                        for (JsonElement resp : jsonElements) {
                            JsonObject data = resp.getAsJsonObject();
                            Meet meet = new Meet();
                            meet.fldroomname = data.get("fldroomname").getAsString();
                            meet.fldtxnum = data.get("fldtxnum").getAsString();
                            meet.fldonlinenum = data.get("fldonlinenum").getAsString();
                            meet.flddepartid = data.get("flddepartid").getAsString();
                            meet.flddepart = data.get("flddepart").getAsString();
                            meetsList.add(meet);
                            // 持久化
                            MeetDB.getInstance().addMeet(meet);
                        }
                        ((MeetAdapter) adapter).notifyDataSetChanged();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.i(TAG, "sync meet fail:" + throwable);
                        syncing = false;
                        Toast.makeText(getApplicationContext(), "获取会议列表失败", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    // ListView列表点击事件
    Meet m;
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        m = meetsList.get(i);
        /*@setView 装入一个EditView
         */
        final View dialogView = LayoutInflater.from(MeetActivity.this)
                .inflate(R.layout.dialog_edit, null);
        AlertDialog.Builder inputDialog =
                new AlertDialog.Builder(MeetActivity.this);
        inputDialog.setTitle("输入会议室密码").setView(dialogView);
        inputDialog.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 获取EditView中的输入内容
                        EditText edit_text =
                                (EditText) dialogView.findViewById(R.id.edit_text);
                        if (TextUtils.isEmpty(edit_text.getText().toString())) {
                            Toast.makeText(getApplicationContext(), "不可为空", Toast.LENGTH_SHORT).show();
                        }else {
                            syncCheckMeet(m.fldroomname,edit_text.getText().toString());
                        }
                    }
                }).setNegativeButton("取消", null).show();
    }

    // 核对会议密码
    private void syncCheckMeet(String fldroomname, String edpwd) {
        if (syncing) {
            return;
        }
        syncing = true;
        showWaitingDialog();
        String apiStr = "RhtxCheckMeetingRoom";
        String username = "user101";
        String password = "123";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
        String token = ParseMD5.parseStrToMd5L32(username + password + apiStr + sdf.format(date));
        CheckMeet pm = new CheckMeet();
        pm.username = username;
        pm.method = apiStr;
        pm.token = token;
        pm.fldroomname = fldroomname;
        pm.password = edpwd;
        IMHttp imHttp = IMHttpFactory.Singleton();
        imHttp.checkMeet(pm)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject jsonObject) {
                        syncing = false;
                        disWaitingDialog();
                        Meet meet = new Meet();
                        boolean flag = jsonObject.get("return_code").getAsBoolean();
                        if(flag){
                            goActivity();
                        }else {
                            Toast.makeText(getApplicationContext(), "密码错误", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.i(TAG, "sync pwd fail:" + throwable);
                        syncing = false;
                        disWaitingDialog();
                        Toast.makeText(getApplicationContext(), "核对密码失败", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    // 跳转会议页面MeetChatActivity
    public void goActivity(){
        Intent intent = new Intent(this,MeetChatActivity.class);
        intent.putExtra("fldroomname",m.fldroomname);
        startActivity(intent);
    }

    // 加载中。。显示
    ProgressDialog waitingDialog;
    private void showWaitingDialog() {
        /* 等待Dialog具有屏蔽其他控件的交互能力
         * @setCancelable 为使屏幕不可点击，设置为不可取消(false)
         * 下载等事件完成后，主动调用函数关闭该Dialog
         */
        waitingDialog =
                new ProgressDialog(MeetActivity.this);
        waitingDialog.setTitle("会议室检验");
        waitingDialog.setMessage("等待中...");
        waitingDialog.setIndeterminate(true);
        waitingDialog.setCancelable(true);
        waitingDialog.show();
    }
    // 加载中。。销毁
    private void disWaitingDialog() {
        waitingDialog.dismiss();
    }

    // 适配器
    class MeetAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return meetsList.size();
        }

        @Override
        public Object getItem(int position) {
            return meetsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.meet, null);
            } else {
                view = convertView;
            }
            Meet m = meetsList.get(position);
            ImageView avatarView = (ImageView) view.findViewById(R.id.header);
            avatarView.setImageResource(R.drawable.tar_voice_select);
            TextView tv = (TextView) view.findViewById(R.id.name);
            tv.setText(m.fldroomname);
            tv = (TextView) view.findViewById(R.id.deptname);
            tv.setText(m.flddepart);
            tv = (TextView) view.findViewById(R.id.fldonlinenum);
            tv.setText(m.fldonlinenum);
            return view;
        }
    }

    // 菜单
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return true;
    }

    // 搜索
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            Intent intent = new Intent();
            intent.setClass(this, SearchMeetActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        // 销毁广播
        super.onDestroy();
        BusProvider.getInstance().unregister(messageHandler);
    }
}
