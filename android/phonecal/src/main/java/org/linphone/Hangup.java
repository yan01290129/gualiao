package org.linphone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.beetle.goubuli.model.Group;
import com.beetle.goubuli.model.GroupDB;

import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreListenerBase;
import org.linphone.ui.AddressText;
import org.linphone.ui.CallButton;

import java.util.List;


public class Hangup extends Activity {
    LinphoneCoreListenerBase mListener;
    Intent intent ;
    boolean mBoom = true;
    private AddressText mAddress;
    private CallButton mCall;

    private static Hangup instance;

    static final boolean isInstance(){
        return instance != null;
    }

    public static Hangup getInstance(){
        if (instance == null){
            instance = new Hangup();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newcall);

        mAddress = (AddressText) findViewById(R.id.address12);
        mCall = findViewById(R.id.call12);
        mCall.setAddressWidget(mAddress);


        if(LinphoneManager.getLc().getCallsNb() == 0){
            String userno = "";
            String name = "";
            List<Group> groups = GroupDB.getInstance().loadGroups();
            if(groups.size() > 0){
                Group group =  groups.get(0);
                long defaultgroupid = group.defaultgroupid;
                for (int i = 0;i<groups.size();i++){
                    if(groups.get(i).groupID == defaultgroupid){
                        userno = groups.get(i).fTxnum;
                        name = groups.get(i).topic;
                    }
                }
            }
            if(userno == "" || name == ""){
                Toast.makeText(this,"请设置默认群组",Toast.LENGTH_SHORT).show();
                Intent intent1 = getPackageManager().getLaunchIntentForPackage("com.beetle.goubuli");
                if (null != intent1){
                    startActivity(intent1);
                    overridePendingTransition(0,0);
                }
            }else {
                intent = new Intent();
                intent.putExtra("userno",userno);
                intent.putExtra("name",name);
                intent.setClass(this,LinphoneActivity.class);
                startActivity(intent);
                char ch = '#';
                LinphoneManager.getLc().sendDtmf(ch);
            }
        }else {
            intent = new Intent(this,LinphoneActivity.class);
            startActivity(intent);

        }

    }


    public void stopcall() {
        char c = '#';
      LinphoneCore lc = LinphoneManager.getLc();
      lc.sendDtmf(c);
    }
}
