package org.linphone;
/*
DialerFragment.java
Copyright (C) 2012  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.beetle.goubuli.model.GroupDB;

import org.linphone.core.LinphoneCore;
import org.linphone.mediastream.Log;
import org.linphone.ui.AddressAware;
import org.linphone.ui.AddressText;
import org.linphone.ui.CallButton;
import org.linphone.ui.EraseButton;

/**
 * @author Sylvain Berfini
 */
public class DFragment3 extends Fragment {
	private static DFragment3 instance;
	//呼叫转移
	private static boolean isCallTransferOngoing = false;

	private AddressAware numpad;
	private AddressText mAddress;
	private CallButton mCall;
	private ImageView mAddContact;
	private OnClickListener addContactListener, cancelListener, transferListener;
	private boolean shouldEmptyAddressField = true;
	private EditText editText;
	private Context mContext;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialer, container, false);


        editText = (EditText)view.findViewById(R.id.calledit);
		this.mContext = getActivity();



		mAddress = (AddressText) view.findViewById(R.id.address);
		mAddress.setDFragment3(this);

		EraseButton erase = (EraseButton) view.findViewById(R.id.erase);
		erase.setAddressWidget(mAddress);

		mCall = (CallButton) view.findViewById(R.id.call);
		mCall.setAddressWidget(mAddress);
		if (LinphoneActivity.isInstanciated() && LinphoneManager.getLc().getCallsNb() > 0) {
				if (isCallTransferOngoing) {
					mCall.setImageResource(R.drawable.call_transfer);
				} else {
					mCall.setImageResource(R.drawable.call_add);
				}
		} else {
			mCall.setImageResource(R.drawable.call_audio_start);
		}

		numpad = (AddressAware) view.findViewById(R.id.numpad);
		if (numpad != null) {
			numpad.setAddressWidget(mAddress);
	}

		mAddContact = (ImageView) view.findViewById(R.id.add_contact);
		mAddContact.setEnabled(!(LinphoneActivity.isInstanciated() && LinphoneManager.getLc().getCallsNb() > 0));

		addContactListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				LinphoneActivity.instance().displayContactsForEdition(mAddress.getText().toString());
			}
		};
		cancelListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				LinphoneActivity.instance().resetClassicMenuLayoutAndGoBackToCallIfStillRunning();
			}
		};
		transferListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				LinphoneCore lc = LinphoneManager.getLc();
				if (lc.getCurrentCall() == null) {
					return;
				}
				lc.transferCall(lc.getCurrentCall(), mAddress.getText().toString());
				isCallTransferOngoing = false;
				//如果进仍然行，重置菜单布局并返回调用
				LinphoneActivity.instance().resetClassicMenuLayoutAndGoBackToCallIfStillRunning();
			}
		};

		resetLayout(isCallTransferOngoing);

		if (getArguments() != null) {
			shouldEmptyAddressField = false;
			String number = getArguments().getString("SipUri");
			String displayName = getArguments().getString("DisplayName");
			String photo = getArguments().getString("PhotoUri");
			mAddress.setText(number);
			if (displayName != null) {
				mAddress.setDisplayedName(displayName);
			}
			if (photo != null) {
				mAddress.setPictureUri(Uri.parse(photo));
			}
		}

		instance = this;
		Bundle bundle = getArguments();
		String userno = (String)bundle.get("userno");
		Long groupid = Long.parseLong(userno);
		String groupname = GroupDB.getInstance().getGroupfTxnum(groupid);
		if(userno == null || groupname ==null){
			Toast.makeText(mContext,"没有该群组",Toast.LENGTH_SHORT).show();
		}else if(userno != null){
			newOutgoingCall(userno+"-"+groupname);
		}
		return view;
    }

	/**
	 * @return null if not ready yet
	 */
	public static DFragment3 instance() {
		return instance;
	}

	@Override
	public void onPause() {
		instance = null;
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		instance = this;

		if (LinphoneActivity.isInstanciated()) {
			LinphoneActivity.instance().selectMenu(FragmentsAvailable.DIALER);
			LinphoneActivity.instance().updateDFragment3(this);
			LinphoneActivity.instance().showStatusBar();
			LinphoneActivity.instance().hideTabBar(false);
		}

		boolean isOrientationLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
		if(isOrientationLandscape && !getResources().getBoolean(R.bool.isTablet)) {
			((LinearLayout) numpad).setVisibility(View.GONE);
		} else {
			((LinearLayout) numpad).setVisibility(View.VISIBLE);
		}

		if (shouldEmptyAddressField) {
			mAddress.setText("");
		} else {
			shouldEmptyAddressField = true;
		}
		resetLayout(isCallTransferOngoing);
	}

	public void resetLayout(boolean callTransfer) {
		if (!LinphoneActivity.isInstanciated()) {
			return;
		}
		isCallTransferOngoing = LinphoneActivity.instance().isCallTransfer();
            LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
            if (lc == null) {
			return;
		}

		if (lc.getCallsNb() > 0) {
			if (isCallTransferOngoing) {
				mCall.setImageResource(R.drawable.call_transfer);
				mCall.setExternalClickListener(transferListener);
			} else {
				mCall.setImageResource(R.drawable.call_add);
				mCall.resetClickListener();
			}
			mAddContact.setEnabled(true);
			mAddContact.setImageResource(R.drawable.call_alt_back);
			mAddContact.setOnClickListener(cancelListener);
		} else {
			mCall.setImageResource(R.drawable.call_audio_start);
			mAddContact.setEnabled(false);
			mAddContact.setImageResource(R.drawable.contact_add_button);
			mAddContact.setOnClickListener(addContactListener);
			enableDisableAddContact();
		}
	}

	public void enableDisableAddContact() {
		mAddContact.setEnabled(LinphoneManager.getLc().getCallsNb() > 0 || !mAddress.getText().toString().equals(""));
	}

	public void displayTextInAddressBar(String numberOrSipAddress) {
		shouldEmptyAddressField = false;
		mAddress.setText(numberOrSipAddress);
	}

	//这个通过键盘拨打sip电话
	public void newOutgoingCall(String numberOrSipAddress) {
		displayTextInAddressBar(numberOrSipAddress);
		LinphoneManager.getInstance().newOutgoingCall(mAddress);


	}

	public void newOutgoingCall(Intent intent) {
		if (intent != null && intent.getData() != null) {
			String scheme = intent.getData().getScheme();
			if (scheme.startsWith("imto")) {
				mAddress.setText("sip:" + intent.getData().getLastPathSegment());
			} else if (scheme.startsWith("call") || scheme.startsWith("sip")) {
				mAddress.setText(intent.getData().getSchemeSpecificPart());
			} else {
				Uri contactUri = intent.getData();
				String address = ContactsManager.getAddressOrNumberForAndroidContact(LinphoneService.instance().getContentResolver(), contactUri);
				if(address != null) {
					mAddress.setText(address);
				} else {
					Log.e("Unknown scheme: ", scheme);
					mAddress.setText(intent.getData().getSchemeSpecificPart());
				}
			}

			mAddress.clearDisplayedName();
			intent.setData(null);

			LinphoneManager.getInstance().newOutgoingCall(mAddress);
		}
	}

//	//接口回调
//    public void getEText(CallBack callBack){
////		editText = getView().findViewById(R.id.calledit);
////		Bundle bundle = getArguments();
////		String numberphone = (String) bundle.get("number");
////		editText.setText(numberphone);
//        String msg = "6349";
//        callBack.getResult(msg);
//    }
//
//    public interface CallBack{
//        public void getResult(String result);
//    }
}
