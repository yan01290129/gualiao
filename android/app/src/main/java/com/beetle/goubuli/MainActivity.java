package com.beetle.goubuli;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;
import com.beetle.bauhinia.api.IMHttpAPI;
import com.beetle.bauhinia.api.body.PostDeviceToken;
import com.beetle.contact.ContactListActivity;
import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.types.PostToken;
import com.beetle.goubuli.api.types.RtnGroup;
import com.beetle.goubuli.api.types.RtnVersion;
import com.beetle.goubuli.api.types.Version;
import com.beetle.goubuli.model.*;
import com.beetle.goubuli.service.RobMoney;
import com.beetle.goubuli.tools.Timer;
import com.beetle.goubuli.tools.event.*;
import com.beetle.goubuli.util.ParseMD5;
import com.beetle.goubuli.util.RomUtils;
import com.beetle.im.IMService;
import com.facebook.react.bridge.Callback;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.reactnativenavigation.layouts.Layout;
import com.reactnativenavigation.params.ContextualMenuParams;
import com.reactnativenavigation.params.FabParams;
import com.reactnativenavigation.params.ScreenParams;
import com.reactnativenavigation.params.SnackbarParams;
import com.reactnativenavigation.params.TitleBarButtonParams;
import com.reactnativenavigation.params.TitleBarLeftButtonParams;
import com.squareup.otto.Subscribe;
import com.yhao.floatwindow.FloatWindow;
import com.yhao.floatwindow.Screen;
import org.json.JSONException;
import org.json.JSONObject;
import org.linphone.Judge;
import org.linphone.LinphoneActivity;
import org.linphone.LinphoneLauncherActivity;
import org.linphone.ui.AddressText;
import org.linphone.ui.CallButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import retrofit.RetrofitError;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import util.UpdateAppUtils;
import yan.com.meeting.MeetActivity;

public class MainActivity extends ReactNativeActivity implements Headset.Callback, Suspension.Callback{
    private static final String TAG = "MainActivity";

    public static int TAB_IM_INDEX = 0;
    public static int TAB_CONTACT_INDEX = 1;
    public static int TAB_MEET_INDEX = 2;
    public static int TAB_ME_INDEX = 3;
    public static String TAB_IM = "conversation";
    public static String TAB_CONTACT = "contact";
    public static String TAB_MEET = "meeting";
    public static String TAB_ME = "me";
    private static MainActivity instance = null;
    private TabHost mTabHost;
    private Timer refreshTokenTimer;
    private int refreshErrorCount;
    private boolean refreshing = false;
    private boolean syncing = false;
    public static final String BROADCAST_ACTION = "com.beetle.goubuli";
    private Headset mBroadcastReceiver;
    private  Suspension mSuspension ;
    private CallButton mCall;
    private AddressText mAddress;

    int OVERLAY_PERMISSION_REQ_CODE = 1;

    // 线程
    final Object messageHandler = new Object() {
        @Subscribe
        public void onTabEvent(TabEvent event) {
            MainActivity.this.mTabHost.setCurrentTab(TAB_IM_INDEX);
        }

        @Subscribe
        public void onDeviceTokenEvent(final DeviceTokenEvent event) {
            PostDeviceToken t = new PostDeviceToken();
            if (!TextUtils.isEmpty(event.hwDeviceToken)) {
                t.hwDeviceToken = event.hwDeviceToken;
            } else {
                t.xmDeviceToken = event.deviceToken;
            }
            IMHttpAPI.Singleton().bindDeviceToken(t)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object obj) {
                            Log.i(TAG, "bind success:" + event.deviceToken + event.hwDeviceToken);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "bind fail");
                        }
                    });;
        }

        @Subscribe
        public void onApplicationEvent(ApplicationEvent event) {
            if (event.isBackground) {
                onApplicationEnterBackground();
            } else {
                onApplicationEnterForeground();
            }
        }

        @Subscribe
        public void onBadgeEvent(BadgeEvent event) {
            setTabBadge(TAB_IM_INDEX, event.count);
        }
    };

    //  线程挂起
    void onApplicationEnterBackground() {
        IMService.getInstance().enterBackground();
        if (refreshTokenTimer != null) {
            refreshTokenTimer.suspend();
        }
    }

    // 后台到前台
    void onApplicationEnterForeground() {
        IMService.getInstance().enterForeground();
        syncContact();
        getGroups();
    }

    // 单例模式
    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    public void onStart () {
        super.onStart();
        checkVersion();
//        UpdateAppUtils.from(this)
//                .checkBy(UpdateAppUtils.CHECK_BY_VERSION_NAME) //更新检测方式，默认为VersionCode
//                .serverVersionCode(2)     //服务器versionCode
//                .serverVersionName("2.0")     //服务器versionName
//                .apkPath(apkPath)     //最新apk下载地址
//                .showNotification(false) //是否显示下载进度到通知栏，默认为true
//                .updateInfo(info)  //更新日志信息 String
//                .downloadBy(UpdateAppUtils.DOWNLOAD_BY_BROWSER) //下载方式：app下载、手机浏览器下载。默认app下载
//                .isForce(true) //是否强制更新，默认false 强制更新情况下用户不同意更新则不能使用app
//                .update();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;

        BusProvider.getInstance().register(messageHandler);

        Intent intent = getIntent();
        //登录后，首次打开
        boolean firstRun = intent.getBooleanExtra("first_run", false);

        Log.d(TAG, "onCreate intent action:" + intent.getAction());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main_activity);

        mAddress = (AddressText)findViewById(R.id.addressEd);
        mCall = (CallButton)findViewById(R.id.call_ph);
        mCall.setAddressWidget(mAddress);

        // 加载页卡
        initActivity();

        // 添加推送
        final GBLApplication app = (GBLApplication) getApplication();
        if (!TextUtils.isEmpty(app.getDeviceToken())) {
            PostDeviceToken t = new PostDeviceToken();
            t.xmDeviceToken = app.getDeviceToken();
            IMHttpAPI.Singleton().bindDeviceToken(t)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object obj) {
                            Log.i(TAG, "bind xm success:" + app.getDeviceToken());
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "bind fail");
                        }
                    });;
        } else if (!TextUtils.isEmpty(app.getHwDeviceToken())) {
            PostDeviceToken t = new PostDeviceToken();
            t.hwDeviceToken = app.getHwDeviceToken();
            IMHttpAPI.Singleton().bindDeviceToken(t)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object obj) {
                            Log.i(TAG, "bind hw success:" + app.getHwDeviceToken());
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "bind fail");
                        }
                    });
        }

        // 获取联系人
        syncContact();

        // 获取群组
        getGroups();

//        new Handler().postDelayed(new Runnable(){
//            public void run() {
//                // 延时执行
//                checkVersion();
//            }
//        }, 5000);


        if (Profile.getInstance().keepalive) {
            if (firstRun) {
                // 首次提示开启
                openAutoRunSetting();
            }
        }

        Log.d(TAG, "On Create end");

        // 开启无障碍服务，监听耳机按钮事件，并发送广播
        intent = new Intent(this, RobMoney.class);
        intent.putExtra("key", "启动了无障碍service");
        startService(intent);// 启动服务


        // 动态耳机注册广播接收器
        // 1. 实例化BroadcastReceiver子类 &  IntentFilter
        mBroadcastReceiver = new Headset();
        IntentFilter intentFilter = new IntentFilter();
        // 2. 设置接收广播的类型
        intentFilter.addAction("android.intent.action.MEDIA_BUTTON");
        //设置优先级
        intentFilter.setPriority(100);
        // 3. 动态注册：调用Context的registerReceiver（）
        registerReceiver(mBroadcastReceiver, intentFilter);
        // 设置接口回调（传实现类的上下文），所以不能在AndroidManifest文件中静态注册广播接收器
        mBroadcastReceiver.setCallback(this);

        // 注册悬浮广播
        mSuspension = new Suspension();
        IntentFilter intentFilter2 = new IntentFilter();
        // 2. 设置接收广播的类型
        intentFilter2.addAction("suspensionAction");
        // 3. 动态注册：调用Context的registerReceiver（）
        registerReceiver(mSuspension, intentFilter2);
        // 设置接口回调（传实现类的上下文），所以不能在AndroidManifest文件中静态注册广播接收器
        mSuspension.setCallback(this);

        if(Build.VERSION.SDK_INT>=23)
        {
            if(Settings.canDrawOverlays(this))
            {
                //有悬浮窗权限开启服务绑定 绑定权限
                Intent intent1 = new Intent(MainActivity.this, FwService.class);
                startService(intent1);
            }else{
                //没有悬浮窗权限m,去开启悬浮窗权限
                try{
                    Intent intent1=new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                    startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        } else{
            //默认有悬浮窗权限  但是 华为, 小米,oppo等手机会有自己的一套Android6.0以下  会有自己的一套悬浮窗权限管理 也需要做适配
            Intent intent2 = new Intent(MainActivity.this, FwService.class);
            startService(intent2);
        }





//        imageView = new ImageView(getApplicationContext());
//        imageView.setImageResource(R.drawable.normal);

//        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
//        linearLayout.addView(imageView);
        // 显示悬浮状
//        FloatWindow
//                .with(getApplicationContext())
//                .setView(imageView)
//                .setWidth(Screen.width,0.22f)                               //设置控件宽高
//                .setHeight(Screen.width,0.22f)
//                .setX(Screen.width,0.7f)                                   //设置控件初始位置
//                .setY(Screen.height,0.2f)
//                .setDesktopShow(true)                        //桌面显示
//                .build();
        // 悬浮窗事件
//        imageView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                // 长按
//                imageView.setImageResource(R.drawable.press);
//                downEvent();
//                Log.w("touch","长按事件");
//                return false;
//            }
//        });
//        imageView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                     //按下
//                    imageView.setImageResource(R.drawable.press);
//                    downEvent();
//                    downX = motionEvent.getX();
//                    downY = motionEvent.getY();
//                } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
//                    // 移动
//                    float xDistance = motionEvent.getX() - downX;
//                    float yDistance = motionEvent.getY() - downY;
//                    int l,r,t,b;
//                    l = (int) (view.getLeft() + xDistance);
//                    t = (int) (view.getTop() + yDistance);
//                    r = l + view.getWidth();
//                    b = t + view.getHeight();
//                    if(l < 0) {
//                        l = 0;
//                        r = l + view.getWidth();
//                    }
//                    if(r > Screen.width) {
//                        r = Screen.width;
//                        l = r - view.getWidth();
//                    }
//                    if(t < 0) {
//                        t = 0;
//                        b = t + view.getHeight();
//                    }
//                    if(b > Screen.height) {
//                        b = Screen.height;
//                        t = b - view.getHeight();
//                    }
////                    imageView.layout(l, t, r, b);
//                    FloatWindow.get().updateX(l);
//                    FloatWindow.get().updateY(t);
//                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    // 松开
//                    imageView.setImageResource(R.drawable.normal);
//                    upEvent();
//                    if(FloatWindow.get().getY() < 0){
//                        FloatWindow.get().updateY(20);
//                    }
//                };
//                return false;
//            }
//        });
//        imageView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                int action = motionEvent.getAction();
//                if (action == MotionEvent.ACTION_DOWN) {
//                    // 按下
//                    downX = motionEvent.getX();
//                    downY = motionEvent.getY();
//                    Log.w("touch","按下事件");
//                } else if (action == MotionEvent.ACTION_MOVE) {
//                    // 移动
//                    //width和height为控件的宽高
//                    final float xDistance = motionEvent.getX() - downX;
//                    final float yDistance = motionEvent.getY() - downY;
//                    int l,r,t,b;
//                    l = (int) (view.getLeft() + xDistance);
//                    t = (int) (view.getTop() + yDistance);
//                    r = l + view.getWidth();
//                    b = t + view.getHeight();
//                    if(l < 0) {
//                        l = 0;
//                        r = l + view.getWidth();
//                    }
//                    if(r > screenWidth) {
//                        r = screenWidth;
//                        l = r - view.getWidth();
//                    }
//                    if(t < 0) {
//                        t = 0;
//                        b = t + view.getHeight();
//                    }
//                    if(b > screenHeight) {
//                        b = screenHeight;
//                        t = b - view.getHeight();
//                    }
//                    imageView.layout(l, t, r, b);
////                    FloatWindow.get().updateX(l);
////                    FloatWindow.get().updateY(t);
//                    Log.w("touch","移动事件");
//                } else if (action == MotionEvent.ACTION_UP) {
//                    // 松开
//                    imageView.setImageResource(R.drawable.normal);
//                    upEvent();
//                    Log.w("touch","松开事件");
//                }
//                return false;
//            }
//        });
//        imageView.setOnTouchListener(this::onTouch);



    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN://0
//                 Log.e("TAG", "LinearLayout onTouchEvent 按住");
//                 break;
//            case MotionEvent.ACTION_UP://1
//                  Log.e("TAG", "LinearLayout onTouchEvent onTouch抬起");
//                // 松开
//                imageView.setImageResource(R.drawable.normal);
//                upEvent();
//                Log.w("touch","松开事件");
//                if(FloatWindow.get().getY() < 0){
//                    FloatWindow.get().updateY(20);
//                }
//                  break;
//            case MotionEvent.ACTION_MOVE://2
//                  Log.e("TAG", "LinearLayout onTouchEvent 移动");
//                  break;
//        }
//        return super.onTouchEvent(event);
//    }

    private float downX;
    private float downY;
    ImageView imageView;

    Group c = null;
//    Intent callintent;
    @Override
    public void downEvent() {
        Log.w("耳机按钮事件", "按下");
        if(Judge.getInstance().getCallsNb() == 0){

            String userno = "";
            String name = "";
            long gid = 0;
            List<Group> groups = GroupDB.getInstance().loadGroups();
            if(groups.size() > 0){
                Group group =  groups.get(0);
                long defaultgroupid = group.defaultgroupid;
                for (int i = 0;i<groups.size();i++){
                    if(groups.get(i).groupID == defaultgroupid){
                        userno = groups.get(i).fTxnum;
                        name = groups.get(i).topic;
                        c = groups.get(i);
                    }
                }
            }
            if(userno == "" || name == ""){
                Toast.makeText(this,"请设置默认群组",Toast.LENGTH_SHORT).show();
                Intent intent1 = getPackageManager().getLaunchIntentForPackage("com.beetle.goubuli");
                if (null != intent1){
                    startActivity(intent1);
                    overridePendingTransition(0,0);
                }
            }else {

                // 跳转到拨号页面
                Intent callintent = new Intent(this, Judge.class);
                startActivity(callintent);
                // 跳转到默认群组（给单例覆盖联系人数据）
                BusProvider.getInstance().post(new GroupConversationEvent(c.groupID, c.topic != null ? c.topic : ""));
                // 跳转到会话页，下面就不用finish电话页面
                BusProvider.getInstance().post(new TabEvent());
                // 发记录消息
                new Handler().postDelayed(new Runnable(){
                    public void run() {
                        // 跳转到拨号页面
                        Intent callintent = new Intent(instance, Judge.class);
                        startActivity(callintent);
                        // 这里会附带执行打电话
                        GroupMessageActivity.getInstance().grpUserCall();
                        GroupMessageActivity.getInstance().finish();
                    }
                }, 1000);

//                ConversationActivity.getInstance().startGroupConversation(c.groupID, c.topic);
//                GroupMessageActivity.getInstance().grpUserCall();
//                GroupMessageActivity.getInstance().finish();
//
//                Intent callintent = new Intent(instance, Judge.class);
//                startActivity(callintent);

//                Intent intent = new Intent(this, GroupMessageActivity.class);
//                intent.putExtra("group_id", c.groupID);
//                intent.putExtra("group_name", "sss");
//                intent.putExtra("current_uid", 1);
//                intent.putExtra("navigatorID", "_navigatorID1");
//                String screenInstanceID = BaseActivity.generateScreenInstanceID();
//                String navigatorEventID = screenInstanceID + "_events";
//                intent.putExtra("navigatorID", "_navigatorID1");
//                intent.putExtra("screenInstanceID", screenInstanceID);
//                intent.putExtra("navigatorEventID", navigatorEventID);
//                startActivity(intent);
            }
        }else {
            Judge.getInstance().stratscall();
        }
    }

    @Override
    public void upEvent() {
        Log.w("耳机按钮事件","松开");
        Judge.getInstance().stopcall();
    }

    boolean longkey = false;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int j = keyCode;
        String userno = "";
        String name = "";
        List<Group> groups = GroupDB.getInstance().loadGroups();
        if(groups.size() > 0){
            Group group =  groups.get(0);
            long defaultgroupid = group.defaultgroupid;
            for (int i = 0;i<groups.size();i++){
                if(groups.get(i).groupID == defaultgroupid){
                    userno = groups.get(i).fTxnum;
                    name = groups.get(i).topic;
                }
            }
        }
        if (KeyEvent.KEYCODE_HEADSETHOOK == keyCode){
            if (event.getRepeatCount() == 0){

            }else {
                if(userno == "" || name == ""){
                    Toast.makeText(this,"请设置默认群组",Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent();
                    intent.putExtra("userno",userno);
                    intent.putExtra("name",name);
                    intent.setClass(this,LinphoneActivity.class);
                    startActivity(intent);
                }
            }
        }

        return super.onKeyDown (keyCode, event);
    }
//    @Override
//    public boolean onKeyUp(int keyCode, KeyEvent event) {
//        //松开挂断电话
////       CallActivity.getInstance().hangup();
//
//        return super.onKeyUp (keyCode, event);
//    }

    // 加载四个model
    private void initActivity() {
        this.setVisible(true);
        Log.d(TAG, "initActivity called");

        mTabHost = (TabHost) findViewById(R.id.tabhost);
        mTabHost.setup(this.getLocalActivityManager());

        TabSpec mTab = mTabHost.newTabSpec(TAB_IM);
        mTab.setIndicator(inflaterTab(getResources().getDrawable(R.drawable.tar_message),
                getResources().getString(R.string.tab_txt_im)));

        Intent intent = new Intent(this, ConversationActivity.class);
        intent.putExtra("current_uid", Token.getInstance().uid);
        intent.putExtra("navigatorID", BaseActivity.generateNavigatorID());
        String screenInstanceID = BaseActivity.generateScreenInstanceID();
        String navigatorEventID = screenInstanceID + "_events";
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);
        mTab.setContent(intent);
        mTabHost.addTab(mTab);

        mTab = mTabHost.newTabSpec(TAB_CONTACT);
        mTab.setIndicator(inflaterTab(getResources().getDrawable(R.drawable.tar_communicate),
                getResources().getString(R.string.tab_txt_contact)));
        intent = new Intent(this, ContactListActivity.class);
        intent.putExtra("navigatorID", BaseActivity.generateNavigatorID());
        screenInstanceID = BaseActivity.generateScreenInstanceID();
        navigatorEventID = screenInstanceID + "_events";
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);
        mTab.setContent(intent);
        mTabHost.addTab(mTab);

        mTab = mTabHost.newTabSpec(TAB_MEET);
        mTab.setIndicator(inflaterTab(getResources().getDrawable(R.drawable.tar_voice),
                getResources().getString(R.string.tab_txt_meet)));
        intent = new Intent(this, MeetActivity.class);
        intent.putExtra("navigatorID", BaseActivity.generateNavigatorID());
        screenInstanceID = BaseActivity.generateScreenInstanceID();
        navigatorEventID = screenInstanceID + "_events";
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);
        mTab.setContent(intent);
        mTabHost.addTab(mTab);

        mTab = mTabHost.newTabSpec(TAB_ME);
        mTab.setIndicator(inflaterTab(getResources().getDrawable(R.drawable.tar_me),
                getResources().getString(R.string.tab_txt_me)));

        intent = new Intent(this, MeActivity.class);
        intent.putExtra("navigatorID", BaseActivity.generateNavigatorID());
        screenInstanceID = BaseActivity.generateScreenInstanceID();
        navigatorEventID = screenInstanceID + "_events";
        intent.putExtra("screenInstanceID", screenInstanceID);
        intent.putExtra("navigatorEventID", navigatorEventID);
        mTab.setContent(intent);
        mTabHost.addTab(mTab);

        mTabHost.setOnTabChangedListener(new OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                //icon和文字的颜色全部重置
                ((ImageView) mTabHost.getTabWidget().getChildTabViewAt(0).findViewById(R.id.tab_imageView)).setImageDrawable(getResources().getDrawable(R.drawable.tar_message));
                ((TextView) mTabHost.getTabWidget().getChildTabViewAt(0).findViewById(R.id.tab_textView)).setTextColor(getResources().getColor(R.color.colorNavigation));
                ((ImageView) mTabHost.getTabWidget().getChildTabViewAt(1).findViewById(R.id.tab_imageView)).setImageDrawable(getResources().getDrawable(R.drawable.tar_communicate));
                ((TextView) mTabHost.getTabWidget().getChildTabViewAt(1).findViewById(R.id.tab_textView)).setTextColor(getResources().getColor(R.color.colorNavigation));
                ((ImageView) mTabHost.getTabWidget().getChildTabViewAt(2).findViewById(R.id.tab_imageView)).setImageDrawable(getResources().getDrawable(R.drawable.tar_voice));
                ((TextView) mTabHost.getTabWidget().getChildTabViewAt(2).findViewById(R.id.tab_textView)).setTextColor(getResources().getColor(R.color.colorNavigation));
                ((ImageView) mTabHost.getTabWidget().getChildTabViewAt(3).findViewById(R.id.tab_imageView)).setImageDrawable(getResources().getDrawable(R.drawable.tar_me));
                ((TextView) mTabHost.getTabWidget().getChildTabViewAt(3).findViewById(R.id.tab_textView)).setTextColor(getResources().getColor(R.color.colorNavigation));
                if (mTabHost.getCurrentTabTag() == tabId) {
                    if(tabId.equals("conversation")){
                        ((ImageView) mTabHost.getCurrentTabView().findViewById(R.id.tab_imageView)).setImageDrawable(getResources().getDrawable(R.drawable.tar_message_select));
                    }
                    if(tabId.equals("contact")){
                        ((ImageView) mTabHost.getCurrentTabView().findViewById(R.id.tab_imageView)).setImageDrawable(getResources().getDrawable(R.drawable.tar_communicate_select));
                    }
                    if(tabId.equals("meeting")){
                        ((ImageView) mTabHost.getCurrentTabView().findViewById(R.id.tab_imageView)).setImageDrawable(getResources().getDrawable(R.drawable.tar_voice_select));
                    }
                    if(tabId.equals("me")){
                        ((ImageView) mTabHost.getCurrentTabView().findViewById(R.id.tab_imageView)).setImageDrawable(getResources().getDrawable(R.drawable.tar_me_select));
                    }
                    ((TextView) mTabHost.getCurrentTabView().findViewById(R.id.tab_textView)).setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        });

        // tab栏每个activity选中一次，激活其中注册的广播
        // 在第一次发送联系人广播之前执行注册广播代码
        mTabHost.setCurrentTab(0);
//        mTabHost.setCurrentTab(1);
        mTabHost.setCurrentTab(2);
        mTabHost.setCurrentTab(3);
        mTabHost.setCurrentTab(TAB_CONTACT_INDEX);// 默认选中联系人
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK) {
//            Log.w("sssss","处理按下去的事件");
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK) {
            Log.w("sssss","处理按键弹起的事件");
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HEADSETHOOK) {
            Log.w("sssss","处理长按事件");
        }
        return super.onKeyDown(keyCode, event);
    }

    // 获取联系人
    private void syncContact() {
        if (syncing){
            return;
        }
        syncing = true;
        IMHttp imHttp = IMHttpFactory.Singleton();
        final long syncKey = Profile.getInstance().contactSyncKey;
        String username = "user101";
        String password = "123";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd",Locale.CHINA);
        String token = ParseMD5.parseStrToMd5L32(username+password+"RhtxContacts"+sdf.format(date));
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            json.put("method", "RhtxContacts");
            json.put("token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PostToken p = new PostToken();
        p.username = username;
        p.token = token;
        imHttp.getContacts(p)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject resp1) {
                        Log.i(TAG, "resp:" + resp1);
                        syncing = false;
                        try {
                            // 联系人入库
                            syncContact(resp1);
//                            Toast.makeText(MainActivity.this, "获取部门联系人列表成功", Toast.LENGTH_SHORT).show();
                            // 查询所有联系人并广播更新联系人完毕
                            List<Contact> contacts = ContactManager.getInstance().getAllContact();
                            BusProvider.getInstance().post(new SyncCompleteEvent(contacts));
                            long uid = Token.getInstance().uid;
                            int index = -1;
                            String userno = "";
                            for (int i = 0; i < contacts.size(); i++) {
                                Contact c = contacts.get(i);
                                if (c.getContactId() == uid) {
                                    index = i;
                                    userno = c.getUserno();
                                }
                            }
                            Contact c = contacts.get(index);
                            //初始化linphone
                            Intent intent = new Intent();
                            Profile profile = Profile.getInstance();
                            intent.putExtra("userno",userno);
                            intent.setClass(MainActivity.this,LinphoneLauncherActivity.class);
                            MainActivity.this.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.i(TAG, "sync contact fail:" + throwable);
                        syncing = false;
                        Toast.makeText(getApplicationContext(), "同步联系人失败", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    // 联系人部门入库
    private void syncContact(JsonObject resp1) throws JSONException {
        // 删除部门
        DepartmentManager departmentManager = DepartmentManager.getInstance();
        departmentManager.deleteAllDepartment();
        // 删除联系人
        ContactManager contactManager = ContactManager.getInstance();
        contactManager.deleteAllContacts();
        // 添加部门
        ArrayList<Department> d = new ArrayList<Department>();
        JsonArray departments = resp1.getAsJsonArray("departments");
        for (JsonElement resp : departments) {
            JsonObject json = resp.getAsJsonObject();
            Department dept = new Department();
            dept.id = json.get("dept_id").getAsString();
            dept.name = json.get("name").getAsString();
            dept.parentID = json.get("parent_id").getAsString();
            d.add(dept);
        }
        // 入库
        departmentManager.addDepartments(d);
//        // 添加联系人
        ArrayList<Contact> l = new ArrayList<Contact>();
        JsonArray contacts = resp1.getAsJsonArray("contacts");
        for (JsonElement resp : contacts) {
            JsonObject json = resp.getAsJsonObject();
            Contact contact = new Contact();
            contact.setContactId(json.get("user_id").getAsLong());
            contact.setName(json.get("showname").getAsString());
            contact.setMobile(json.get("mobile").getAsString());
            contact.setAvatar(json.get("avatar").getAsString());
            contact.setDeptID(json.get("departinfoid").getAsString());
            contact.setUserno(json.get("f_mPhone").getAsString());
            l.add(contact);
        }
        // 入库
        contactManager.batchAddContacts(l);
    }

    // 获取群主
    private void getGroups() {
        String username = "user101";
        String method = "RtnGroup";
        String password = "123";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd",Locale.CHINA);
        String token = ParseMD5.parseStrToMd5L32(username+password+method+sdf.format(date));
        RtnGroup rtnGroup = new RtnGroup();
        rtnGroup.username  = username;
        rtnGroup.method  = method;
        rtnGroup.token = token;
        rtnGroup.userId =  String.valueOf(Token.getInstance().uid);
        IMHttp imHttp = IMHttpFactory.Singleton();
        imHttp.getRtnGroup(rtnGroup)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                               @Override
                               public void call(Object obj) {
                                   Gson g = new Gson();
                                   JsonArray data = g.toJsonTree(obj).getAsJsonArray();
                                   // 群主操作
                                   addGroups(data);
//                                   Toast.makeText(MainActivity.this, "获取群主列表成功", Toast.LENGTH_SHORT).show();
                                   Log.i(TAG, "get groups success");
                               }
                           }, new Action1<Throwable>() {
                               @Override
                               public void call(Throwable throwable) {
                                   Log.i(TAG, "get groups fail:", throwable);
                                   RetrofitError error = (RetrofitError)throwable;
                                   if (error.getResponse() != null) {
                                       Log.i(TAG, "error:" + error.getBody());

                                   }
                                   Toast.makeText(MainActivity.this, "获取群主列表失败", Toast.LENGTH_SHORT).show();
                               }
                           }
                );
    }

    // 群主入库
    void addGroups(JsonArray arr) {
        // 清空群主
        GroupDB.getInstance().clear();

        for (int i = 0;i<arr.size();i++){
            JsonObject data = arr.get(i).getAsJsonObject();
            long gid = data.get("id").getAsLong();
            String fTxnum = data.get("f_Txnum").getAsString();
            String name = data.get("name").getAsString();
            Long masterID = data.get("master").getAsLong();
            boolean doNotDisturb = data.get("do_not_disturb").getAsBoolean();
            Long defaultgroupid = data.get("Defaultgroupid").getAsLong();

            JsonArray members = data.get("members").getAsJsonArray();

            Group group = new Group();
            group.groupID = gid;
            group.fTxnum = fTxnum;
            group.topic = name;
            group.master = masterID;
            group.doNotDisturb = doNotDisturb;
            group.readonly = true;
            group.timestamp = now();
            group.defaultgroupid = defaultgroupid;

            for (int j = 0; j < members.size(); j++) {
                JsonObject m = members.get(j).getAsJsonObject();
                long uid = m.get("uid").getAsLong();
                group.addMember(uid);
            }
            // 群主入库
            GroupDB.getInstance().addGroup(group);
        }
    }

    // 检验版本
    void checkVersion() {
        String username = "user101";
        String method = "RhtxAppVersion";
        String password = "123";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd",Locale.CHINA);
        String token = ParseMD5.parseStrToMd5L32(username+password+method+sdf.format(date));
        RtnVersion rtnVersion = new RtnVersion();
        rtnVersion.username  = username;
        rtnVersion.method  = method;
        rtnVersion.token = token;

        IMHttp imHttp = IMHttpFactory.Singleton();
        imHttp.getLatestVersion(rtnVersion)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Version>() {
                    @Override
                    public void call(Version obj) {
                        MainActivity.this.checkVersion(obj);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.i(TAG, "get latest version fail:" + throwable.getMessage());
                    }
                });
    }

    // 检验版本
    private void checkVersion(final Version version) {
        // 检验版本
        PackageManager pm = this.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
            if (version.version_num > info.versionCode){
                UpdateAppUtils.from(instance)
                        .serverVersionCode(version.version_num)  //服务器versionCode
                        .serverVersionName(version.f_version) //服务器versionName
                        .apkPath(version.f_adress) //最新apk下载地址
                        .updateInfo(version.f_isupdate> 0 ? "本次更新为强制更新，否则无法使用":"")  //更新日志信息 String
                        .downloadBy(UpdateAppUtils.DOWNLOAD_BY_APP) //下载方式：app下载、手机浏览器下载。默认app下载
                        .isForce(version.f_isupdate> 0) //是否强制更新，默认false 强制更新情况下用户不同意更新则不能使用app
                        .update();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

//        Log.i(TAG, "latest version:" + version.major + ":" + version.minor + " url:" + version.url);
//        int versionCode = version.major*10+version.minor;
//        PackageManager pm = this.getPackageManager();
//        try {
//            PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
//            String a = info.versionName;
//            if (versionCode > info.versionCode) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(this);
//                builder.setMessage("是否更新上城通讯?");
//                builder.setTitle("提示");
//                builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        Uri uri = Uri.parse("https://www.baidu.com");
//                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                        startActivity(intent);
////                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(version.url));
////                        startActivity(browserIntent);
//                    }
//                });
//                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                builder.create().show();
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
    }

    // 提示
    private void openAutoRunSetting() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("为了保证在后台接受新消息通知，需要在系统设置中开启上城通讯“自启动权限”。");
        builder.setTitle("提示");
        builder.setPositiveButton("设置", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                RomUtils.openAutoRunSetting(MainActivity.this);
            }
        });
        builder.setNegativeButton("稍后设置", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static int now() {
        Date date = new Date();
        long t = date.getTime();
        return (int)(t/1000);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "MainActivity onResume");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "MainActivity onStop");
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        if (instance == this) {
            instance = null;
        }

        //销毁
        FloatWindow.destroy();

        super.onDestroy();
        BusProvider.getInstance().unregister(messageHandler);
        unregisterReceiver(mBroadcastReceiver);
        //结束服务
        stopService(new Intent(MainActivity.this, RobMoney.class));
        super.onDestroy();
    }

    public static void setTabBadge(int tab, int nCount) {
        if (instance == null) {
            return;
        }

        View v = instance.mTabHost.getTabWidget().getChildTabViewAt(tab);
        if (v != null) {
            TextView txt = (TextView)v.findViewById(R.id.txt_badge);
            if (nCount > 0) {
                txt.setVisibility(View.VISIBLE);
                txt.setText(String.valueOf(nCount));
            } else {
                txt.setVisibility(View.GONE);
            }
        }
    }
    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent called");
        super.onNewIntent(intent);
    }

    private View inflaterTab(Drawable icon, String strText) {
        View view = LayoutInflater.from(this).inflate(R.layout.tab_item_layout, null, false);
        TextView textView = (TextView) view.findViewById(R.id.tab_textView);
        ImageView imageView = (ImageView) view.findViewById(R.id.tab_imageView);
        imageView.setImageDrawable(icon);
        textView.setText(strText);

        return view;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return super.dispatchKeyEvent(event);
    }



    public static int getNow() {
        Date date = new Date();
        long t = date.getTime();
        return (int)(t/1000);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if(Build.VERSION.SDK_INT>=23) {
                if (!Settings.canDrawOverlays(this)) {
                    Toast.makeText(this, "权限授予失败，无法开启悬浮窗", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "权限授予成功！", Toast.LENGTH_SHORT).show();
                    //有悬浮窗权限开启服务绑定 绑定权限
                    Intent intent = new Intent(MainActivity.this, FwService.class);
                    startService(intent);
                }
            }
        }
    }
}
