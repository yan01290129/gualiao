package com.beetle.goubuli;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceScreen;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beetle.bauhinia.api.IMHttpAPI;
import com.beetle.bauhinia.api.body.PostDeviceToken;
import com.beetle.bauhinia.db.CustomerMessageDB;
import com.beetle.bauhinia.db.EPeerMessageDB;
import com.beetle.bauhinia.db.GroupMessageDB;
import com.beetle.bauhinia.db.PeerMessageDB;
import com.beetle.bauhinia.tools.ImageMIME;
import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.api.body.PostQRCode;
import com.beetle.goubuli.crypto.storage.TextSecureSessionStore;
import com.beetle.goubuli.model.*;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.tools.event.SyncCompleteEvent;
import com.beetle.goubuli.util.ParseMD5;
import com.beetle.goubuli.util.RomUtils;
import com.beetle.im.IMService;
import com.beetle.im.SystemMessageObserver;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.yhao.floatwindow.FloatWindow;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.linphone.CallPhoneActivity;
import org.linphone.LinphoneActivity;
import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.LinphoneService;

import retrofit.mime.TypedFile;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import yan.com.meeting.MeetActivity;


public class MeActivity extends BaseActivity implements SystemMessageObserver {
    private static final String TAG = "goubuli";
    private static final int PERMISSIONS_REQUEST = 2;

    private static final int QRCODE_SCAN_REQUEST = 100;
    public static final int SELECT_PICTURE = 101;
    public static final int SELECT_PICTURE_KITKAT = 102;
    public static final int TAKE_PICTURE = 103;

    private File captureFile;

    ImageView avatarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        captureFile = new File(getExternalFilesDir(null), "pic.jpg");

        TextView companyTextView = (TextView) findViewById(R.id.company);
        TextView nameTextView = (TextView) findViewById(R.id.name);
        companyTextView.setText(Profile.getInstance().organizationName);
        nameTextView.setText(Profile.getInstance().name);
        avatarView = (ImageView) findViewById(R.id.avatar);
        TextView versionTextView = (TextView) findViewById(R.id.version);
        PackageManager pm = this.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
            versionTextView.setText(info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            versionTextView.setText("");
            e.printStackTrace();
        }



        String avatar = Profile.getInstance().avatar;
        if (!TextUtils.isEmpty(avatar)) {
            Picasso.with(this)
                    .load(avatar)
                    .placeholder(R.drawable.avatar_contact)
                    .into(avatarView);
        }
        Profile profile = Profile.getInstance();

        requestPermission();

        IMService.getInstance().addSystemObserver(this);
        BusProvider.getInstance().register(this);
    }

    private void openAutoRunSetting() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("为了保证在后台接受新消息通知，需要在系统设置中开启上城通讯“自启动权限”。");
        builder.setTitle("提示");
        builder.setPositiveButton("设置", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                RomUtils.openAutoRunSetting(MeActivity.this);
            }
        });
        builder.setNegativeButton("稍后设置", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        IMService.getInstance().removeSystemObserver(this);
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public boolean canBack() {
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST) {
            Log.i(TAG, "granted permission:" + grantResults);
        }
    }


    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int readExternalPermission = (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE));
            int cameraPermission = (checkSelfPermission(Manifest.permission.CAMERA));

            ArrayList<String> permissions = new ArrayList<String>();
            if (readExternalPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA);
            }

            if (permissions.size() > 0) {
                String[] array = new String[permissions.size()];
                permissions.toArray(array);
                this.requestPermissions(array, PERMISSIONS_REQUEST);
            }
        }
    }

    @Override
    public void onSystemMessage(String sm) {
        Log.i(TAG, "meactivity system message:" + sm);
        try {
            JSONObject obj = new JSONObject(sm);
            int platform = 0;
            int timestamp = 0;
            String deviceID = "";
            String deviceName = "";
            if (obj.has("platform")) {
                platform = obj.getInt("platform");
            }
            if (obj.has("timestamp")) {
                timestamp = obj.getInt("timestamp");
            }

            if (obj.has("device_id")) {
                deviceID = obj.getString("device_id");
            }

            if (obj.has("device_name")) {
                deviceName = obj.getString("device_name");
            }

            int PLATFORM_IOS = 1;
            int PLATFORM_ANDROID = 2;
            Token t = Token.getInstance();
            Profile profile = Profile.getInstance();
            String androidID = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            // 如果是当前平台当前设备登陆
            if (platform == PLATFORM_ANDROID && deviceID.equals(androidID)) {
                return;
            }
            // 如果不是移动端登陆
            if (platform != PLATFORM_ANDROID && platform != PLATFORM_IOS) {
                return;
            }
            // 如果设备已登陆的设备时间戳大于新推送的时间戳
            if (profile.loginTimestamp > timestamp) {
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                doLogout();

                AlertDialog.Builder builder = new AlertDialog.Builder(MeActivity.this);
                builder.setMessage("你的账号在其它设备上登录");
                builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        Intent intent = new Intent(MeActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                builder.show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    private void unbindDeviceToken() {
        GBLApplication app = (GBLApplication) getApplication();
        if (!TextUtils.isEmpty(app.getDeviceToken())) {
            final String deviceToken = app.getDeviceToken();
            PostDeviceToken t = new PostDeviceToken();
            t.xmDeviceToken = app.getDeviceToken();
            IMHttpAPI.Singleton().unBindDeviceToken(t)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object obj) {
                            Log.i(TAG, "unbind xm success:" + deviceToken);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "unbind fail");
                        }
                    });
            ;
        } else if (!TextUtils.isEmpty(app.getHwDeviceToken())) {
            final String deviceToken = app.getHwDeviceToken();
            PostDeviceToken t = new PostDeviceToken();
            t.xmDeviceToken = app.getHwDeviceToken();
            IMHttpAPI.Singleton().unBindDeviceToken(t)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object obj) {
                            Log.i(TAG, "unbind hw success:" + deviceToken);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "unbind fail");
                        }
                    });
        }
    }

    private void doLogout() {

        //销毁
//        FloatWindow.destroy();

        unbindDeviceToken();

        ConversationDB.getInstance().resetState(Conversation.STATE_UNINITIALIZE);
        new TextSecureSessionStore(this).archiveAllSessions();

        Intent service = new Intent(MeActivity.this, ForegroundService.class);
        stopService(service);

        IMService.getInstance().stop();
        Token t = Token.getInstance();
        t.clear(this);

        Profile p = Profile.getInstance();
        p.clear(this);

        ContactManager.getInstance().deleteAllContacts();
        GroupDB.getInstance().clear();

        MessageDatabaseHelper.getInstance().close();
        PeerMessageDB.getInstance().setDb(null);
        EPeerMessageDB.getInstance().setDb(null);
        GroupMessageDB.getInstance().setDb(null);
        CustomerMessageDB.getInstance().setDb(null);
        ConversationDB.getInstance().setDb(null);

        //-----------------------销毁对讲服务-------------------------
        stopService(new Intent(Intent.ACTION_MAIN).setClass(this, LinphoneService.class));
        LinphonePreferences mPrefs = LinphonePreferences.instance();
        mPrefs.deleteAccount(0);
    }

    private void kickout() {
        doLogout();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("你的账号已被管理员删除");
        builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                Intent intent = new Intent(MeActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        builder.show();

    }

    private void logout() {
        doLogout();

        //同时会导致MainActivity destroy
        finish();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    public void onLogout(View view) {


        Log.i(TAG, "logout");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("确认退出吗？");
        builder.setTitle("提示");
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                logout();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();


//        Log.i(TAG, "logout");
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage("确认退出吗？");
//        builder.setTitle("提示");
//        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                logout();
//            }
//        });
//        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        builder.create().show();
    }

    public void onScan(View view) {
        Intent intent = new Intent(this, ZBarActivity.class);
        startActivityForResult(intent, QRCODE_SCAN_REQUEST);
    }

    public void onAbout(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void onContact(View view) {
        Intent intent = new Intent(this, CustomerMessageActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("store_id", Config.STORE_ID);
        intent.putExtra("seller_id", Config.SELLER_ID);
        intent.putExtra("app_id", Config.APPID);
        intent.putExtra("current_uid", Profile.getInstance().uid);
        intent.putExtra("title", "上城通讯团队");
        startActivity(intent);
    }

    public void onAvatar(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("选择操作");
        final CharSequence[] items = {"拍摄", "从手机相册选择"};
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Log.i(TAG, "item:" + items[item]);
                if (item == 0) {
                    capturePicture();
                } else if (item == 1) {
                    getPicture();
                }
            }
        }).show();
    }

    void getPicture() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent
                    , getResources().getString(com.beetle.imkit.R.string.product_fotos_get_from))
                    , SELECT_PICTURE);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, SELECT_PICTURE_KITKAT);
        }
    }

    void capturePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(captureFile));
        startActivityForResult(takePictureIntent, TAKE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "result:" + resultCode + " request:" + requestCode);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == QRCODE_SCAN_REQUEST) {
            String symbol = data.getStringExtra("symbol");
            Log.i(TAG, "symbol:" + symbol);
            if (TextUtils.isEmpty(symbol)) {
                return;
            }

            PostQRCode qrcode = new PostQRCode();
            qrcode.sid = symbol;
            IMHttp imHttp = IMHttpFactory.Singleton();
            imHttp.postQRCode(qrcode)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object obj) {
                            Log.i(TAG, "sweep success");
                            Toast.makeText(MeActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "sweep fail");
                            Toast.makeText(MeActivity.this, "登录失败", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else if (requestCode == TAKE_PICTURE) {
            if (captureFile.exists()) {
                Log.i(TAG, "take picture success:" + captureFile.getAbsolutePath());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bmp = BitmapFactory.decodeFile(captureFile.getAbsolutePath(), options);
                setAvatar(bmp);
            }
        } else if (requestCode == SELECT_PICTURE || requestCode == SELECT_PICTURE_KITKAT) {
            try {
                Uri selectedImageUri = data.getData();
                Log.i(TAG, "image type:" + data.getType());
                Log.i(TAG, "selected image uri:" + selectedImageUri);
                InputStream is = getContentResolver().openInputStream(selectedImageUri);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bmp = BitmapFactory.decodeStream(is, null, options);
                setAvatar(bmp);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    String multipart_form_data = "multipart/form-data";
    String lineStart = "--";
    String boundary = "****************ef5fH38L0hL9DIO";    // 数据分隔符
    String lineEnd = "\r\n";

    private void addText(String usernName, String token, String UID, DataOutputStream output) {
        StringBuilder sb = new StringBuilder();
        sb.append(lineStart + boundary + lineEnd);
        sb.append("Content-Disposition: form-data; name=\"userName\"" + lineEnd);
        sb.append(lineEnd);
        sb.append(usernName + lineEnd);
        sb.append(lineStart + boundary + lineEnd);
        sb.append("Content-Disposition: form-data; name=\"token\"" + lineEnd);
        sb.append(lineEnd);
        sb.append(token + lineEnd);
        sb.append(lineStart + boundary + lineEnd);
        sb.append("Content-Disposition: form-data; name=\"UID\"" + lineEnd);
        sb.append(lineEnd);
        sb.append(UID + lineEnd);
        try {
            output.writeBytes(sb.toString());// 发送表单字段数据
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("**************result" + sb);
    }

    private void addImage(ArrayList<File> files, DataOutputStream output) {
        for (File file : files) {
            StringBuilder split = new StringBuilder();
            split.append(lineStart + boundary + lineEnd);
            split.append("Content-Disposition: form-data; name=\"" + "files" + "\"; filename=\"" + file.getName() + "\"" + lineEnd);
            split.append("Content-Type: " + "image/jpeg" + lineEnd);
            split.append(lineEnd);
            System.out.println("**************result" + split);
            try {
                // 发送图片数据
                output.writeBytes(split.toString());
                FileInputStream fis = new FileInputStream(file);
                byte[] b = new byte[1024];
                int n;
                while ((n = fis.read(b)) != -1) {
                    output.write(b, 0, n);
                }
                fis.close();
                output.writeBytes(lineEnd);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static final int SHOW_RESPONSE = 0;

    //子线程：使用POST方法向服务器发送用户名、密码等数据
    class PostThread extends Thread {

        File file;

        public PostThread(File file) {
            this.file = file;
        }

        @Override
        public void run() {
            String baseurl = "http://120.236.26.28:8888/bpdm/RhtxUploadImage";
            String apiStr = "RhtxUploadImage";
            String username = "user101";
            String password = "123";
            String UID = String.valueOf(Token.getInstance().uid);
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
            String token = ParseMD5.parseStrToMd5L32(username + password + apiStr + sdf.format(date));
            String returncode = "";

            HttpURLConnection conn = null;
            DataOutputStream output = null;
            BufferedReader input = null;
            try {
                URL url = new URL(baseurl);
                conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(120000);
                conn.setDoInput(true);        // 允许输入
                conn.setDoOutput(true);        // 允许输出
                conn.setUseCaches(false);    // 不使用Cache
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
                conn.setRequestProperty("Accept-Encoding", "gzip,deflate,sdch");
                conn.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8");
                conn.setRequestProperty("Connection", "keep-alive");
                conn.setRequestProperty("Content-Type", multipart_form_data + "; boundary=" + boundary);
                conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
                conn.connect();
                output = new DataOutputStream(conn.getOutputStream());

                addText(username, token, UID, output);    // 添加文本字段
                ArrayList<File> files = new ArrayList<>();
                files.add(this.file);
                addImage(files, output);    // 添加图片

                output.writeBytes(lineStart + boundary + lineStart + lineEnd);// 数据结束标志
                output.flush();

                int code = conn.getResponseCode();
                System.out.println("**************result" + code);
                if (code == 200) {
                    System.out.println("**************result");
                    input = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String oneLine;
                    while ((oneLine = input.readLine()) != null) {
                        response.append(oneLine + lineEnd);
                    }
                    returncode = response.toString();
                } else {
                    returncode = "";
                }

                //在子线程中将Message对象发出去
                Message message = new Message();
                message.what = SHOW_RESPONSE;
                message.obj = returncode;
                handler.sendMessage(message);

            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                // 统一释放资源
                try {
                    if (output != null) {
                        output.close();
                    }
                    if (input != null) {
                        input.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                if (conn != null) {
                    conn.disconnect();
                }
            }
        }

    }

    //新建Handler的对象，在这里接收Message
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SHOW_RESPONSE:
                    String response = (String) msg.obj;
                    Gson g = new Gson();
                    JsonObject jobj = new JsonParser().parse(response).getAsJsonObject();
                    boolean flag = jobj.get("return_code").getAsBoolean();
                    if (flag) {
                        disWaitingDialog();
                        Contact contact =  new Contact();
                        contact.setContactId(Token.getInstance().uid);
                        contact.setAvatar(jobj.get("images_url").getAsString());
                        ContactManager.getInstance().upContact(contact);
                        Toast.makeText(getApplicationContext(), "更新头像成功", Toast.LENGTH_SHORT).show();
                        // 查询所有联系人并广播更新联系人完毕
                        List<Contact> contacts = ContactManager.getInstance().getAllContact();
                        BusProvider.getInstance().post(new SyncCompleteEvent(contacts));
                    } else {
                        Toast.makeText(getApplicationContext(), "上传失败", Toast.LENGTH_SHORT).show();
                        disWaitingDialog();
                    }
                    break;
                default:
                    break;
            }
        }

    };

    ProgressDialog dialog = null;

    // 销毁
    private void disWaitingDialog() {
        dialog.dismiss();
    }


    private void setAvatar(Bitmap bmp) {
        try {
            dialog = ProgressDialog.show(this, null, "");

            File outputDir = getCacheDir();
            File outputFile = File.createTempFile("goubuli", ".jpg", outputDir);
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            fileOutputStream.close();
            Log.i(TAG, "file size:" + outputFile.length());
            String type = ImageMIME.getMimeType(outputFile);
            TypedFile typedFile = new TypedFile(type, outputFile);

            //使用POST方法向服务器发送数据
            PostThread postThread = new PostThread(outputFile);
            postThread.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 动态更新“我”的页卡信息
    @Subscribe
    public void onSyncCompleteEvent(SyncCompleteEvent event) {
        List<Contact> contacts = event.contacts;
        if (contacts == null) {
            return;
        }

        long uid = Token.getInstance().uid;
        for (int i = 0; i < contacts.size(); i++) {
            Contact c = contacts.get(i);
            if (c.getContactId() == uid) {
                Profile.getInstance().name = c.getName();
                Department department = DepartmentManager.getInstance().getDepartment(c.getDeptID());
                Profile.getInstance().deptName = department.name;
                Profile.getInstance().avatar = c.getAvatar();
                Profile.getInstance().originAvatar = c.getOriginAvatar();
                Profile.getInstance().save(MeActivity.this);
                // 更改展示
                TextView nameTextView = (TextView) findViewById(R.id.name);
                nameTextView.setText(Profile.getInstance().name);
                TextView company = (TextView) findViewById(R.id.company);
                company.setText(Profile.getInstance().deptName);

                avatarView = (ImageView) findViewById(R.id.avatar);
                String avatar = Profile.getInstance().avatar;
                if (TextUtils.isEmpty(avatar)) {
                    avatar = null;
                }
                Picasso.with(this)
                        .load(avatar)
                        .placeholder(R.drawable.avatar_contact)
                        .into(avatarView);
            }
        }
    }
}
