package com.beetle.goubuli.react;

import com.beetle.goubuli.model.GroupDB;
import com.beetle.goubuli.model.Profile;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by houxh on 2017/7/30.
 */

public class GroupModule  extends ReactContextBaseJavaModule {
    public GroupModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "GroupModule";
    }

    @ReactMethod
    public void updateGroupNotice(final double groupID, final String notice) {
        getReactApplicationContext().runOnUiQueueThread(new Runnable() {
            @Override
            public void run() {
                GroupDB.getInstance().setGroupNotice((long)groupID, notice);
            }
        });
    }

    @ReactMethod
    public void updateGroupDoNotDisturb(final double groupID, final boolean doNotDisturb) {
        getReactApplicationContext().runOnUiQueueThread(new Runnable() {
            @Override
            public void run() {
                GroupDB.getInstance().setGroupDoNotDisturb((long)groupID, doNotDisturb);
            }
        });
    }

    @ReactMethod
    public void updateNicknameInGroup(final double groupID, final String nickname) {
        getReactApplicationContext().runOnUiQueueThread(new Runnable() {
            @Override
            public void run() {
                GroupDB.getInstance().updateMemberNickname((long)groupID, Profile.getInstance().uid, nickname);
            }
        });
    }
}
