package com.beetle.goubuli;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class FwService extends Service {

    private WindowManager wManager;// 窗口管理者
    private WindowManager.LayoutParams mParams;// 窗口的属性
    ImageView imageView; // 悬浮视图
    private int mTouchStartX, mTouchStartY;//手指按下时坐标

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public interface Callback {
        public void downEvent();
        public void upEvent();
    }
    private FwService.Callback callback;
    public void setCallback(FwService.Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        wManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        mParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.TYPE_SYSTEM_ERROR, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSPARENT);
        mParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;// 系统提示window
        mParams.format = PixelFormat.TRANSLUCENT;// 支持透明
        // mParams.format = PixelFormat.RGBA_8888;
        mParams.flags |= WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;// 焦点
        mParams.width = 150;// 窗口的宽和高
        mParams.height = 150;
        mParams.gravity = Gravity.LEFT | Gravity.TOP;
        mParams.y = 0;
        mParams.x = 0;
        mParams.windowAnimations = android.R.style.Animation_Toast;
        // mParams.alpha = 0.8f;//窗口的透明度
        imageView = new ImageView(getApplicationContext());
        imageView.setImageResource(R.drawable.normal);
        wManager.addView(imageView, mParams);// 添加窗口
        // 长按
        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                // 长按
                imageView.setImageResource(R.drawable.press);
                Intent intent = new Intent();
                //设置广播的名字（设置Action）
                intent.setAction("suspensionAction");
                //携带数据
                intent.putExtra("data","downEvent");
                //发送广播（无序广播）
                sendBroadcast(intent);
//                MainActivity.getInstance().downEvent();
//                    callback.downEvent();
//                Toast.makeText(getApplicationContext(), "长按事件！", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        // 滑动
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        imageView.setImageResource(R.drawable.press);
                        mTouchStartX = (int) event.getRawX();
                        mTouchStartY = (int) event.getRawY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mParams.x += (int) event.getRawX() - mTouchStartX;
                        mParams.y += (int) event.getRawY() - mTouchStartY;//相对于屏幕左上角的位置
                        wManager.updateViewLayout(imageView, mParams);
                        mTouchStartX = (int) event.getRawX();
                        mTouchStartY = (int) event.getRawY();
                        break;
                    case MotionEvent.ACTION_UP:
                        imageView.setImageResource(R.drawable.normal);
                        Intent intent = new Intent();
                        //设置广播的名字（设置Action）
                        intent.setAction("suspensionAction");
                        //携带数据
                        intent.putExtra("data","upEvent");
                        //发送广播（无序广播）
                        sendBroadcast(intent);
//                        MainActivity.getInstance().upEvent();
//                        callback.upEvent();
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        if ( wManager != null && imageView != null) {
            wManager.removeView(imageView);
        }
        super.onDestroy();
    }
}