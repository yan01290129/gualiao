package com.beetle.goubuli;

import android.app.ActivityGroup;
import android.view.KeyEvent;

import com.reactnativenavigation.react.NavigationApplication;
import com.reactnativenavigation.react.JsDevReloadHandler;
import com.reactnativenavigation.react.ReactDevPermission;

/**
 * Created by houxh on 2017/7/23.
 */

public class ReactNativeActivity  extends ActivityGroup {



    @Override
    protected void onResume() {
        super.onResume();

        if (ReactDevPermission.shouldAskPermission()) {
            ReactDevPermission.askPermission(this);
            return;
        }

        if (!NavigationApplication.instance.isReactContextInitialized() &&
                !NavigationApplication.instance.hasStartedCreatingContext()) {
            NavigationApplication.instance.startReactContextOnceInBackgroundAndExecuteJS();
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
//        return super.onKeyUp(keyCode, event);
        return JsDevReloadHandler.onKeyUp(getCurrentFocus(), keyCode) || super.onKeyUp(keyCode, event);
    }

}
