
package com.beetle.goubuli;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.beetle.goubuli.crypto.IdentityKeyUtil;
import com.beetle.goubuli.model.Token;
import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.util.KeyHelper;


/**
 * 启动初始activity
 *
 */
public class LauncherActivity extends Activity {
    private String TAG = "goubuli";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.welcome);
        startApp();
    }

    private void startApp() {
        Token t = Token.getInstance();
//        Log.i(TAG, "access token:" + t.accessToken);


        int registrationId = IdentityKeyUtil.getLocalRegistrationId(this);
        boolean hasIdentity = IdentityKeyUtil.hasIdentityKey(this);

        if (t.uid != 0 && registrationId != 0 && hasIdentity) {
            Intent intent = new Intent(LauncherActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(LauncherActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
