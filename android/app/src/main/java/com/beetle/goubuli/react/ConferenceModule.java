package com.beetle.goubuli.react;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;

import com.beetle.conference.ConferenceActivity;
import com.beetle.conference.ConferenceCommand;
import com.beetle.im.IMService;
import com.beetle.im.RTMessage;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by houxh on 2017/7/30.
 */

public class ConferenceModule extends ReactContextBaseJavaModule {
    public ConferenceModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "ConferenceModule";
    }


    @ReactMethod
    public void startConference(final ReadableMap map) {
        final String token = map.getString("token");
        final String channelId = map.getString("channelID");
        final long uid = (long)map.getDouble("uid");
        final long initiator = (long)map.getDouble("initiator");
        final long groupId = (long)map.getDouble("groupID");
        ReadableArray p = map.getArray("participants");
        final long[] participants = new long[p.size()];
        for (int i = 0; i < p.size(); i++) {
            ReadableMap m = p.getMap(i);
            long pid = (long)m.getDouble("uid");
            participants[i] = pid;
        }

        getReactApplicationContext().runOnUiQueueThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getReactApplicationContext(), ConferenceActivity.class);
                intent.putExtra("token", token);
                intent.putExtra("initiator", initiator);
                intent.putExtra("channel_id", channelId);
                intent.putExtra("group_id", groupId);
                intent.putExtra("current_uid", uid);
                intent.putExtra("participants", participants);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                if (ConferenceActivity.activityCount > 0) {
                    return;
                }
                getReactApplicationContext().startActivity(intent);
            }
        });
    }

    @ReactMethod
    public void sendInvite(final ReadableMap map, final double src, final double to) {
        getReactApplicationContext().runOnUiQueueThread(new Runnable() {
            @Override
            public void run() {
                sendInvite(map, (long)src, (long)to);
            }
        });
    }


    @ReactMethod
    public void sendRTMessage(final String command, final double src, final double to,  final String channelID) {
        getReactApplicationContext().runOnUiQueueThread(new Runnable() {
            @Override
            public void run() {
                sendRTMessage(command, (long)src, (long)to, channelID);
            }
        });
    }


    public void sendInvite(ReadableMap map,  long sender, long to) {
        try {
            String channelID = map.getString("channelID");
            long initiator = (long)map.getDouble("initiator");
            ReadableArray array = map.getArray("participants");
            long groupID = 0;
            if (map.hasKey("groupID")) {
                groupID = (long) map.getDouble("groupID");
            }

            long[] participants = new long[array.size()];
            for (int i = 0; i < array.size(); i++) {
                participants[i] = (long)array.getDouble(i);
            }

            ConferenceCommand conferenceCommand = new ConferenceCommand();
            conferenceCommand.command = ConferenceCommand.COMMAND_INVITE;
            conferenceCommand.channelID = channelID;
            conferenceCommand.initiator = initiator;
            conferenceCommand.groupID = groupID;
            conferenceCommand.participants = participants;

            JSONObject json = new JSONObject();
            json.put("conference", conferenceCommand.getContent());

            RTMessage rt = new RTMessage();
            rt.sender = sender;
            rt.receiver = to;
            rt.content = json.toString();
            IMService.getInstance().sendRTMessage(rt);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void sendRTMessage(String command, long sender, long to, String channelID) {
        try {
            ConferenceCommand conferenceCommand = new ConferenceCommand();
            conferenceCommand.command = command;
            conferenceCommand.channelID = channelID;
            JSONObject json = new JSONObject();
            json.put("conference", conferenceCommand.getContent());

            RTMessage rt = new RTMessage();
            rt.sender = sender;
            rt.receiver = to;
            rt.content = json.toString();
            IMService.getInstance().sendRTMessage(rt);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    @ReactMethod
    public void enableSpeaker() {
        final  Context context = getReactApplicationContext();
        getReactApplicationContext().runOnUiQueueThread(new Runnable() {
            @Override
            public void run() {
                AudioManager audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
                if (!audioManager.isWiredHeadsetOn()) {
                    audioManager.setSpeakerphoneOn(true);
                } else {
                    audioManager.setSpeakerphoneOn(false);
                }
            }
        });
    }
}