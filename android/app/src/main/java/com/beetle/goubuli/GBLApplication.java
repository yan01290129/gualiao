package com.beetle.goubuli;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.beetle.bauhinia.db.CustomerMessageHandler;
import com.beetle.bauhinia.db.GroupMessageHandler;
import com.beetle.bauhinia.db.PeerMessageHandler;
import com.beetle.bauhinia.db.SyncKeyHandler;
import com.beetle.bauhinia.tools.FileDownloader;
import com.beetle.bauhinia.tools.PeerOutbox;
import com.beetle.conference.DialConferenceActivity;
import com.beetle.conference.ReceiveConferenceActivity;
import com.beetle.goubuli.crypto.IdentityKeyUtil;
import com.beetle.goubuli.crypto.PreKeyUtil;
import com.beetle.goubuli.crypto.storage.TextSecureSessionStore;
import com.beetle.goubuli.model.*;
import com.beetle.goubuli.tools.event.LoginSuccessEvent;
import com.beetle.im.IMService;

import com.beetle.bauhinia.MessageActivity;
import com.beetle.bauhinia.api.IMHttpAPI;

import com.beetle.bauhinia.db.PeerMessageDB;
import com.beetle.bauhinia.db.EPeerMessageDB;
import com.beetle.bauhinia.db.GroupMessageDB;
import com.beetle.bauhinia.db.CustomerMessageDB;

import com.beetle.bauhinia.tools.FileCache;
import com.beetle.bauhinia.tools.ImageMIME;

import com.beetle.goubuli.crypto.storage.SignalProtocolStoreImpl;

import com.beetle.conference.ConferenceActivity;
import com.beetle.goubuli.api.IMHttp;
import com.beetle.goubuli.api.IMHttpFactory;
import com.beetle.goubuli.react.GoubuliPackage;
import com.beetle.goubuli.tools.CrashHandler;
import com.beetle.goubuli.tools.event.ApplicationEvent;
import com.beetle.goubuli.tools.event.BusProvider;
import com.beetle.goubuli.util.Utils;

import com.beetle.im.RTMessage;
import com.beetle.im.RTMessageObserver;
import com.facebook.react.ReactPackage;
import com.google.code.p.leveldb.LevelDB;
import com.reactnativenavigation.controllers.NavigationCommandsHandler;
import com.reactnativenavigation.react.NavigationApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;



import com.remobile.toast.RCTToastPackage;
import com.squareup.otto.Subscribe;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.yhao.floatwindow.FloatWindow;
import com.yhao.floatwindow.PermissionListener;
import com.yhao.floatwindow.Screen;
import com.yhao.floatwindow.ViewStateListener;
import com.zmxv.RNSound.RNSoundPackage;

import org.json.JSONObject;
import org.linphone.Judge;
import org.linphone.LinphoneManager;
import org.whispersystems.libsignal.IdentityKeyPair;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.libsignal.util.KeyHelper;

import retrofit.mime.TypedFile;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import static android.database.sqlite.SQLiteDatabase.OPEN_READWRITE;

import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by houxh on 15-1-2.
 */
public class GBLApplication extends NavigationApplication implements
        Application.ActivityLifecycleCallbacks, RTMessageObserver, UserApplication, MessageApplication {
    private static final String TAG = "goubuli";

    private static GBLApplication instance;
    private PowerManager.WakeLock wakeLock;
    private boolean isBackground = true;

    private SignalProtocolStoreImpl store;

    List<Contact> contacts;
    List<Conversation> conversations;

    //小米推送
    private String deviceToken;
    //华为推送
    private String hwDeviceToken;

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
    public String getDeviceToken() {
        return deviceToken;
    }

    public void setHWDeviceToken(String deviceToken) {
        this.hwDeviceToken = deviceToken;
    }

    public String getHwDeviceToken() {
        return this.hwDeviceToken;
    }

    public SignalProtocolStoreImpl getStore() {
        return store;
    }

    @Override
    public boolean isDebug() {
        // Make sure you are using BuildConfig from your own application
        return BuildConfig.DEBUG;
    }

    public static GBLApplication getApplication() {
        return instance;
    }


    public PowerManager.WakeLock getWakeLock() {
        return wakeLock;
    }

    public boolean isBackground() {
        return this.isBackground;
    }


    final Object messageHandler = new Object() {
        @Subscribe
        public void onLoginSuccess(LoginSuccessEvent event) {

            Log.i(TAG, "user login success");
        }
    };


    @Override
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public List<Contact> getContacts() {
        return contacts;
    }

    @Override
    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }

    @Override
    public List<Conversation> getConversations() {
        return conversations;
    }


    @NonNull
    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        // Add the packages you require here.
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
                new RCTToastPackage(),
                new RNSoundPackage(),
                new GoubuliPackage()
        );
    }

    void checkCrash() {
        String crashPath = CrashHandler.instance().getCrashLogFilePath();
        final File f = new File(crashPath);
        if (f.exists()) {
            Log.i(TAG, "upload crash log:" + crashPath + "...");
            String type = ImageMIME.getMimeType(f);
            TypedFile typedFile = new TypedFile(type, f);
            IMHttp imHttp = IMHttpFactory.Singleton();
            imHttp.postCrash(typedFile)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Object>() {
                        @Override
                        public void call(Object r) {
                            Log.i(TAG, "upload crash log success");
                            f.delete();
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.i(TAG, "upload crash log err:" + throwable);
                        }
                    });
        }
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        if (!BuildConfig.DEBUG) {
            CrashHandler.instance().init(this.getApplicationContext());
            checkCrash();
        }

        PowerManager powerManager =
                ((PowerManager) this.getSystemService(Context.POWER_SERVICE));
        this.wakeLock =
                powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "goubuli");
        wakeLock.setReferenceCounted(false);


        NavigationCommandsHandler.registerActivityClass(GroupMessageActivity.class, "chat.GroupChat");
        NavigationCommandsHandler.registerActivityClass(ConferenceActivity.class, "chat.Conference");
        NavigationCommandsHandler.registerActivityClass(DialConferenceActivity.class, "chat.DialConference");
        NavigationCommandsHandler.registerActivityClass(ReceiveConferenceActivity.class, "chat.ReceiveConference");

        //init MessageActivity.uptime
        int uptime = MessageActivity.uptime;

        Profile.getInstance().load(this);
        Token.getInstance().load(this);

        if (!isAppProcess()) {
            Log.i(TAG, "service application create");
            return;
        }
        Log.i(TAG, "app application create");

        SignalProtocolStoreImpl.init(this.getApplicationContext());
        store = SignalProtocolStoreImpl.getInstance();

        LevelDB ldb = LevelDB.getDefaultDB();
        String dir = getFilesDir().getAbsoluteFile() + File.separator + "db";
        Log.i(TAG, "dir:" + dir);
        ldb.open(dir);

        Utils.saveGlobleContext(getApplicationContext());

        ContactDatabaseHelper.initDatabase(this.getApplicationContext());
        FileCache fc = FileCache.getInstance();
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            //can write external storage
            String path = getExternalFilesDir(null).getAbsolutePath();
            File dir1 = new File(path, "download");
            if (!dir1.exists()) {
                dir1.mkdirs();
            }
            fc.setDir(dir1);
            Log.i(TAG, "file cache:" + dir1.getAbsolutePath());
        } else {
            File f = new File(getFilesDir(), "cache");
            if (!f.exists()) {
                f.mkdir();
            }
            fc.setDir(f);
            Log.i(TAG, "file cache:" + this.getDir("cache", MODE_PRIVATE).getAbsolutePath());
        }

        IMService im =  IMService.getInstance();
        String androidID = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        im.setDeviceID(androidID);
        im.setWakeLock(wakeLock);
        im.setHost("120.236.26.28");
        IMHttpAPI.setAPIURL("http://120.236.26.28:8080");
        PeerMessageHandler.getInstance().setStore(store);
        PeerOutbox.getInstance().setStore(store);
        FileDownloader.getInstance().setStore(store);

        im.setPeerMessageHandler(PeerMessageHandler.getInstance());
        im.setGroupMessageHandler(GroupMessageHandler.getInstance());
        im.setCustomerMessageHandler(CustomerMessageHandler.getInstance());
        im.registerConnectivityChangeReceiver(getApplicationContext());

        registerActivityLifecycleCallbacks(this);

        Log.i(TAG, "register push...");
        this.registerPush();

        generateIcon();

        Token t = Token.getInstance();
        t.load(this);

        if (t.uid != 0) {
            login();
        }

        //不需要removeobserver
        IMService.getInstance().addRTObserver(this);
        BusProvider.getInstance().register(this);

    }

    public void downEvent() {
        Log.w("耳机按钮事件", "按下");
        if(Judge.getInstance().getCallsNb() == 0){
            Intent callintent = new Intent(this, Judge.class);
            startActivity(callintent);
        }else {
            Judge.getInstance().stratscall();
        }
    }

    public void upEvent() {
        Log.w("耳机按钮事件","松开");
        Judge.getInstance().stopcall();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Subscribe
    public void onLoginSuccess(LoginSuccessEvent event) {

        // 悬浮窗
//        final ImageView imageView = new ImageView(getApplicationContext());
//        imageView.setImageResource(com.beetle.user.R.drawable.chatting_setmode_voice_btn_normal);
//        // 显示悬浮状
//        FloatWindow
//                .with(getApplicationContext())
//                .setView(imageView)
//                .setWidth(Screen.width,0.15f)                               //设置控件宽高
//                .setHeight(Screen.width,0.15f)
//                .setX(Screen.width,0.6f)                                   //设置控件初始位置
//                .setY(Screen.height,0.2f)
//                .setDesktopShow(true)                        //桌面显示
//                .build();
//        // 悬浮窗事件
//        imageView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                // 长按
//                imageView.setImageResource(com.beetle.user.R.drawable.chatting_setmode_voice_btn_focused);
//                downEvent();
//                Log.w("touch","长按事件");
//                return false;
//            }
//        });
//        imageView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                int action = motionEvent.getAction();
//                if (action == MotionEvent.ACTION_DOWN) {
//                    // 按下
//                    Log.w("touch","按下事件");
//                } else if (action == MotionEvent.ACTION_UP) {
//                    // 松开
//                    imageView.setImageResource(com.beetle.user.R.drawable.chatting_setmode_voice_btn_normal);
//                    upEvent();
//                    Log.w("touch","松开事件");
//                }
//                return false;
//            }
//        });

        // 加密
        this.generateIdentity();
        new TextSecureSessionStore(this.getApplicationContext()).archiveAllSessions();

        login();


        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("first_run", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        Log.i(TAG, "user login success");
    }

    //初始化db，start im
    public void login() {
        Profile.getInstance().load(this);
        long currentUID = Token.getInstance().uid;

        if (PeerMessageDB.SQL_ENGINE_DB && GroupMessageDB.SQL_ENGINE_DB && EPeerMessageDB.SQL_ENGINE_DB) {
            File p = this.getDir("db", MODE_PRIVATE);
            File f = new File(p, String.format("gobelieve_%d.db", currentUID));
            String path = f.getPath();
            if (f.exists()) {
                SQLiteDatabase db = SQLiteDatabase.openDatabase(path, null, OPEN_READWRITE, null);
                Log.i(TAG, "db version:" + db.getVersion());
                if (db.getVersion() == 0) {
                    //兼容初始版本更新
                    db.setVersion(1);
                }
                db.close();
            }

            MessageDatabaseHelper dh = MessageDatabaseHelper.getInstance();
            dh.open(this.getApplicationContext(), path);
            SQLiteDatabase db = dh.getDatabase();
            Log.i(TAG, "db version:" + db.getVersion());
            PeerMessageDB.getInstance().setDb(db);
            EPeerMessageDB.getInstance().setDb(db);
            GroupMessageDB.getInstance().setDb(db);
            CustomerMessageDB.getInstance().setDb(db);
            ConversationDB.getInstance().setDb(db);

        } else {
        }

        // 默认
        Profile.getInstance().keepalive = true;

        IMService im = IMService.getInstance();

        Profile profile = Profile.getInstance();
        im.setKeepAlive(profile.keepalive);
        if (profile.keepalive) {
            im.startAlarm(this.getApplicationContext());
        }

        im.setToken(Token.getInstance().gobelieveToken);
        IMHttpAPI.setToken(Token.getInstance().gobelieveToken);

        PeerMessageHandler.getInstance().setUID(Token.getInstance().uid);
        GroupMessageHandler.getInstance().setUID(Token.getInstance().uid);

        SyncKeyHandler handler = new SyncKeyHandler(this.getApplicationContext(), "sync_key_"+currentUID);
        handler.load();

        HashMap<Long, Long> groupSyncKeys = handler.getSuperGroupSyncKeys();
        IMService.getInstance().clearSuperGroupSyncKeys();
        for (Map.Entry<Long, Long> e : groupSyncKeys.entrySet()) {
            IMService.getInstance().addSuperGroupSyncKey(e.getKey(), e.getValue());
            Log.i(TAG, "group id:" + e.getKey() + "sync key:" + e.getValue());
        }
        IMService.getInstance().setSyncKey(handler.getSyncKey());
        Log.i(TAG, "sync key:" + handler.getSyncKey());
        IMService.getInstance().setSyncKeyHandler(handler);

        im.start();


        if (Profile.getInstance().keepalive) {
            Intent service = new Intent(this, ForegroundService.class);
            startService(service);
        }
    }



    void generateIdentity() {
        try {
            int registrationId = KeyHelper.generateRegistrationId(false);
            IdentityKeyUtil.setLocalRegistrationId(this.getApplicationContext(), registrationId);
            IdentityKeyUtil.generateIdentityKeys(this.getApplicationContext());
            IdentityKeyPair identityKeyPair = IdentityKeyUtil.getIdentityKeyPair(this);

            SignedPreKeyRecord signedPreKey = PreKeyUtil.generateSignedPreKey(this, identityKeyPair, true);

            //debug log
            JSONObject signedPreKeyJson = new JSONObject();
            signedPreKeyJson.put("key_id", signedPreKey.getId());
            String publicKey = new String(Base64.encode(signedPreKey.getKeyPair().getPublicKey().serialize(), Base64.DEFAULT));
            signedPreKeyJson.put("public_key", publicKey);
            String signature = new String(Base64.encode(signedPreKey.getSignature(), Base64.DEFAULT));
            signedPreKeyJson.put("signature", signature);

            JSONObject json = new JSONObject();
            json.put("registration_id", registrationId);
            String identityKey = new String(Base64.encode(identityKeyPair.getPublicKey().serialize(), Base64.DEFAULT));
            json.put("identity_key", identityKey);

            json.put("signed_pre_key", signedPreKeyJson);
            Log.i(TAG, "identity:" + json.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void generateIcon() {
        try
        {
            File dir = getCacheDir();
            File iconFile = new File(dir, "goubuli.png");
            if (iconFile.length() == 0) {
                InputStream inputStream = getResources().openRawResource(+R.drawable.app_icon);
                OutputStream out = new FileOutputStream(iconFile);
                byte buf[] = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                out.close();
                inputStream.close();
            }
        }
        catch (IOException e) {

        }
    }

    private void registerMiPush() {
        // 注册push服务，注册成功后会向XiaomiPushReceiver发送广播
        // 可以从onCommandResult方法中MiPushCommandMessage对象参数中获取注册信息
        String appId = "2882303761517554371";
        String appKey = "5191755442371";
        MiPushClient.registerPush(this.getApplicationContext(), appId, appKey);
    }

    private void registerHWPush() {

    }

    private void registerPush() {
        registerMiPush();
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private int started = 0;
    private int stopped = 0;

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.i(TAG,"onActivityCreated:" + activity.getLocalClassName());
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.i(TAG,"onActivityDestroyed:" + activity.getLocalClassName());
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.i(TAG,"onActivityPaused:" + activity.getLocalClassName());
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.i(TAG,"onActivityResumed:" + activity.getLocalClassName());
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity,
                                            Bundle outState) {
        Log.i(TAG,"onActivitySaveInstanceState:" + activity.getLocalClassName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.i(TAG,"onActivityStarted:" + activity.getLocalClassName());
        ++started;

        if (started - stopped == 1 ) {
            if (stopped == 0) {
                Log.i(TAG, "app startup");
            } else {
                Log.i(TAG, "app enter foreground");
            }
            isBackground = false;
            ConversationActivity.isAppBackground = isBackground;
            ForegroundService.isAppBackground = isBackground;
            BusProvider.getInstance().post(new ApplicationEvent(false));
        }
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i(TAG,"onActivityStopped:" + activity.getLocalClassName());
        ++stopped;
        if (stopped == started) {
            isBackground = true;
            ConversationActivity.isAppBackground = isBackground;
            ForegroundService.isAppBackground = isBackground;
            BusProvider.getInstance().post(new ApplicationEvent(true));
        }
    }

    @Override
    public void onRTMessage(RTMessage rt) {
        if (!isBackground) {
            return;
        }

        if (MainActivity.getInstance() == null) {
            ComponentName componentName = new ComponentName(this, LauncherActivity.class);
            Intent intent = Intent.makeMainActivity(componentName);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Log.i(TAG, "start launcher activity");
        }
    }

    private boolean isAppProcess() {
        Context context = getApplicationContext();
        int pid = android.os.Process.myPid();
        Log.i(TAG, "pid:" + pid + "package name:" + context.getPackageName());
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            Log.i(TAG, "package name:" + appProcess.processName + " importance:" + appProcess.importance + " pid:" + appProcess.pid);
            if (pid == appProcess.pid) {
                if (appProcess.processName.equals(context.getPackageName())) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}
