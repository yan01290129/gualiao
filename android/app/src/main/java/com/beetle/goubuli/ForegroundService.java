package com.beetle.goubuli;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.message.Audio;
import com.beetle.bauhinia.db.message.File;
import com.beetle.bauhinia.db.message.GroupVOIP;
import com.beetle.bauhinia.db.message.Image;
import com.beetle.bauhinia.db.message.Location;
import com.beetle.bauhinia.db.message.MessageContent;
import com.beetle.bauhinia.db.message.P2PSession;
import com.beetle.bauhinia.db.message.Secret;
import com.beetle.bauhinia.db.message.Text;
import com.beetle.bauhinia.db.message.VOIP;
import com.beetle.bauhinia.db.message.Video;
import com.beetle.goubuli.model.Contact;
import com.beetle.goubuli.model.ContactManager;
import com.beetle.goubuli.model.Group;
import com.beetle.goubuli.model.GroupDB;
import com.beetle.goubuli.model.Profile;
import com.beetle.im.GroupMessageObserver;
import com.beetle.im.IMMessage;
import com.beetle.im.IMService;
import com.beetle.im.PeerMessageObserver;

import java.util.Date;
import java.util.List;


/**
 * Created by houxh on 16/7/19.
 */
public class ForegroundService extends Service implements
        PeerMessageObserver, GroupMessageObserver {
    private final String TAG = "goubuli";

    private final int PID = android.os.Process.myPid();

    public static boolean isAppBackground = true;

    long currentUID;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "start foreground service");
        currentUID = Profile.getInstance().uid;


        startForeground(PID, getNotification());

        IMService.getInstance().addPeerObserver(this);
        IMService.getInstance().addGroupObserver(this);
    }


    @Override
    public void onDestroy() {
        stopForeground(true);
        IMService.getInstance().removePeerObserver(this);
        IMService.getInstance().removeGroupObserver(this);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    private Notification getNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        String appName = getResources().getString(R.string.app_name);
        String running = getResources().getString(R.string.running);

        Notification.Builder mBuilder =
                new Notification.Builder(this)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentTitle(appName)
                        .setContentText(running)
                        .setOngoing(true)
                        .setContentIntent(pendingIntent);
        return mBuilder.build();
    }

    public  String messageContentToString(MessageContent content) {
        if (content instanceof Text) {
            return ((Text) content).text;
        } else if (content instanceof Image) {
            return "一张图片";
        } else if (content instanceof Audio) {
            return "一段语音";
        } else if (content instanceof File) {
            return "一个文件";
        } else if (content instanceof Video) {
            return "一个视频";
        } else if (content instanceof com.beetle.bauhinia.db.message.Notification) {
            return ((com.beetle.bauhinia.db.message.Notification) content).description;
        } else if (content instanceof Location) {
            return "一个地理位置";
        } else if (content instanceof GroupVOIP) {
            return ((GroupVOIP) content).description;
        } else if (content instanceof VOIP) {
            VOIP voip = (VOIP) content;
            if (voip.videoEnabled) {
                return "视频聊天";
            } else {
                return "语音聊天";
            }
        } else if (content instanceof Secret) {
            return "消息未能解密";
        } else if (content instanceof P2PSession) {
            return "";
        } else {
            return "未知的消息类型";
        }
    }


    @Override
    public void onPeerMessage(IMMessage msg) {
        if (!isAppBackground) {
            return;
        }

        Log.i(TAG, "on peer message");
        IMessage imsg = new IMessage();
        imsg.timestamp = now();
        imsg.msgLocalID = msg.msgLocalID;
        imsg.sender = msg.sender;
        imsg.receiver = msg.receiver;
        imsg.isOutgoing = msg.sender == currentUID;
        imsg.setContent(msg.content);

        if (!imsg.isOutgoing) {
            String content = messageContentToString(imsg.content);
            String title = getResources().getString(com.beetle.message.R.string.app_name);
            String name = this.getUser(msg.sender);
            if (!TextUtils.isEmpty(name)) {
                title = name;
            }
            this.showNotification(title, content);
        }
    }

    @Override
    public void onPeerSecretMessage(IMMessage msg) {
        if (!isAppBackground) {
            return;
        }

        Log.i(TAG, "on peer secret message");
        IMessage imsg = new IMessage();
        imsg.timestamp = now();
        imsg.msgLocalID = msg.msgLocalID;
        imsg.sender = msg.sender;
        imsg.receiver = msg.receiver;
        imsg.isOutgoing = msg.sender == currentUID;
        imsg.secret = true;
        imsg.setContent(msg.content);


        if (!imsg.isOutgoing) {
            String content = messageContentToString(imsg.content);
            String title = getResources().getString(com.beetle.message.R.string.app_name);
            String name = this.getUser(msg.sender);
            if (!TextUtils.isEmpty(name)) {
                title = name;
            }
            this.showNotification(title, content);
        }
    }
    @Override
    public void onPeerMessageACK(IMMessage msg) {

    }
    @Override
    public void onPeerMessageFailure(IMMessage msg) {

    }


    @Override
    public void onGroupMessages(List<IMMessage> msgs) {
        if (!isAppBackground) {
            return;
        }

        if (msgs.size() == 0) {
            return;
        }
        IMMessage msg = msgs.get(msgs.size() - 1);
        Log.i(TAG, "on group message");
        IMessage imsg = new IMessage();
        imsg.timestamp = msg.timestamp;
        imsg.msgLocalID = msg.msgLocalID;
        imsg.sender = msg.sender;
        imsg.receiver = msg.receiver;
        imsg.isOutgoing = msg.sender == currentUID;
        imsg.setContent(msg.content);

        if (!imsg.isOutgoing) {
            String content = messageContentToString(imsg.content);
            String name = this.getUser(msg.sender);
            if (!TextUtils.isEmpty(name)) {
                content = name + ":" + content;
            }
            String title = getResources().getString(com.beetle.message.R.string.app_name);
            String topic = getGroup(msg.receiver);
            if (!TextUtils.isEmpty(topic)) {
                title = topic;
            }
            this.showNotification(title, content);
        }
    }

    @Override
    public void onGroupMessageACK(IMMessage msg) {

    }
    @Override
    public void onGroupMessageFailure(IMMessage msg) {

    }

    @Override
    public void onGroupNotification(String notification) {

    }


    public void showNotification(String title, String content) {

        PackageManager pm = getPackageManager();
        Intent intent= pm.getLaunchIntentForPackage(this.getPackageName());
        PendingIntent pendingIntent = null;
        if (intent != null) {
            pendingIntent = PendingIntent.getActivity(this, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            Log.d(TAG, "intent is null ");
        }
        android.app.Notification notification = null;
        android.app.Notification.Builder ntBuilder = new android.app.Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                .setDefaults(android.app.Notification.DEFAULT_SOUND|android.app.Notification.DEFAULT_VIBRATE)
                .setSmallIcon(com.beetle.message.R.drawable.app_icon)
                .setWhen(System.currentTimeMillis() )
                .setPriority(android.app.Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent);
        notification = ntBuilder.build();
        NotificationManager manager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, notification);
    }



    protected String getGroup(long gid) {
        Group g = GroupDB.getInstance().loadGroup(gid);
        if (g != null && !TextUtils.isEmpty(g.topic)) {
            return g.topic;
        }
        return "";
    }

    protected String getUser(long uid) {
        Contact c = ContactManager.getInstance().getContactById(uid);
        if (c != null && !TextUtils.isEmpty(c.getName())) {
            return c.getName();
        }
        return "";
    }

    public static int now() {
        Date date = new Date();
        long t = date.getTime();
        return (int)(t/1000);
    }

}
