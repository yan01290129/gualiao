package com.beetle.goubuli;

import android.util.Log;

import com.beetle.bauhinia.db.IMessage;
import com.beetle.bauhinia.db.message.Text;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class ApplicationTest  {

    private static final String TAG = "unittest";
    @Before
    public void setup() {
        Log.i(TAG, "setup unittest");
    }

    @After
    public void teardown() {
        Log.i(TAG, "teardown unittest");
    }
    @Test
    public void testAt() {

        ArrayList<Long> at = new ArrayList<>();
        at.add(100L);
        ArrayList<String> atNames = new ArrayList<>();
        atNames.add("hhhh");
        Text t = Text.newText("haha", at, atNames);

        IMessage m = new IMessage();
        m.setContent(t.getRaw());
        Text t2 = (Text)m.content;

        assertEquals((long)t2.at.get(0),  100L);
        assertEquals(t2.atNames.get(0),  "hhhh");
    }
}