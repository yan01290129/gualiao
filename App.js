import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TextInput,
    Image,
    ScrollView,
    Navigator,
    TouchableHighlight,
    ActionSheetIOS,
    NetInfo,
    AppState,
    View,
    Platform,
    AsyncStorage,
    NativeModules,
    NativeAppEventEmitter,
} from 'react-native';

import { createStore } from 'redux'
import { Provider } from 'react-redux'
import RCTDeviceEventEmitter from 'RCTDeviceEventEmitter'
import {Navigation} from 'react-native-navigation';

import {GroupCreator, GroupSelectMember} from "./group/group_creator";
import GroupSetting from './group/group_setting';
import GroupName from './group/group_name';
import GroupMemberAdd from './group/group_member_add';
import GroupMemberRemove from './group/group_member_remove';
import GroupNickname from './group/group_nickname';
import GroupNotice from './group/group_notice';
import {setGroup, groupReducer} from './group/actions';

import {DialConference, ReceiveConference, ConferenceCreator, ParticipantAdd} from './group_call';

import PeerSetting from './peer_setting';

var ProfileManager = NativeModules.ProfileManager;


function profileReducer(state={}, action) {
    switch(action.type) {
        case "set_profile":
            return action.profile;
        default:
            return state;
    }
}

//do not use combineReducers which ignore init state of createStore
function appReducer(state={}, action) {
    return {
        group:groupReducer(state.group, action),
        profile:profileReducer(state.profile, action),
    };
}

//仅注册用，不会真正创建
class AppView extends Component {
    static navigatorStyle = {
        navBarBackgroundColor: '#212121',
        navBarTextColor: '#ffffff',
        navBarSubtitleTextColor: '#ff0000',
        navBarButtonColor: '#ffffff',
        statusBarTextColorScheme: 'light',
    };
    
    
    constructor(props) {
        super(props);
    }

 
    
    render() {
        return (
            <Text>test2</Text>
        );
    }
}


var app = {
    registerScreens: function() {
        Navigation.registerComponent('app.PeerSetting', () => PeerSetting);
        
        Navigation.registerComponent('chat.GroupChat', () => AppView, this.store, Provider);

        Navigation.registerComponent('chat.DialConference', () => DialConference);                
        Navigation.registerComponent('chat.ReceiveConference', () => ReceiveConference);        
        Navigation.registerComponent('chat.Conference', () => AppView);
        Navigation.registerComponent('chat.ConferenceCreator', () => ConferenceCreator);
        Navigation.registerComponent('chat.ParticipantAdd', () => ParticipantAdd);
        
        Navigation.registerComponent('group.GroupSelectMember', () => GroupSelectMember, this.store, Provider);
        Navigation.registerComponent('group.GroupCreator', () => GroupCreator, this.store, Provider);
        Navigation.registerComponent('group.GroupSetting', () => GroupSetting, this.store, Provider);
        Navigation.registerComponent('group.GroupName', () => GroupName, this.store, Provider);
        Navigation.registerComponent('group.GroupNickname', () => GroupNickname, this.store, Provider);
        Navigation.registerComponent('group.GroupNotice', () => GroupNotice, this.store, Provider);
        
        Navigation.registerComponent('group.GroupMemberAdd', () => GroupMemberAdd, this.store, Provider);
        Navigation.registerComponent('group.GroupMemberRemove', () => GroupMemberRemove, this.store, Provider); 
    },
    
    startApp: function() {

        console.log("start app...");
        
        this.store = createStore(appReducer);
        this.registerScreens();
         
        var self = this;
           
        RCTDeviceEventEmitter.addListener('create_group', function(event) {
            console.log("create group:", event);
            
            console.log("init profile:", event.profile);
            self.store.dispatch({type:"set_profile", profile:event.profile});

            var params;
            params = {
                title:"选择联系人",                    
                screen:"group.GroupSelectMember",
                navigatorStyle:{
                    tabBarHidden:true
                },
                leftButton: {
                    id:"back",
                },                    
                
                passProps:{
                    users:event.users                         
                },
            }
            Navigation.push(event.navigatorID, params);
        });

        RCTDeviceEventEmitter.addListener('group_setting', function(event) {
            console.log("group setting event:", event);
            
            console.log("init profile:", event.profile);
            self.store.dispatch({type:"set_profile", profile:event.profile});
            
            self.store.dispatch(setGroup(event.group));

            var params = {
                title:"聊天信息",
                screen:"group.GroupSetting",
                navigatorStyle:{
                    tabBarHidden:true
                },
                passProps:{
                    contacts:event.contacts
                },
            }
            Navigation.push(event.navigatorID, params);
        });


        //发起群视频
        RCTDeviceEventEmitter.addListener('create_group_call', function(event) {
            console.log("create group call event:", event);
            var params = {
                title:"添加成员",
                screen:"chat.ConferenceCreator",
                navigatorStyle:{
                    tabBarHidden:true
                },
                passProps:{
                    users:event.users,
                    groupID:event.groupID,
                    channelID:event.channelID,
                    uid:event.uid,
                    token:event.token,
                    url:event.url,
                },
            }
            Navigation.push(event.navigatorID, params);
        });

        RCTDeviceEventEmitter.addListener('group_call', function(event) {
            console.log("group call event:", event);
            var params = {
                title:"群视频",
                screen:"chat.ReceiveConference",
                navigatorStyle:{
                    tabBarHidden:true
                },
                passProps:{
                    initiator:event.initiator,
                    uid:event.uid,
                    isInitiator:event.isInitiator,
                    channelID:event.channelID,
                    participants:event.participants,
                    users:event.users,
                    groupID:event.groupID,
                    token:event.token,
                },
            }
            Navigation.showModal(params);
        });

        RCTDeviceEventEmitter.addListener('peer_setting', function(event) {
            console.log("peer setting event:", event);
            var params = {
                title:"聊天详情",
                screen:"app.PeerSetting",
                navigatorStyle:{
                    tabBarHidden:true
                },
                passProps:{
                 
                },
            }
            Navigation.push(event.navigatorID, params);            
        });
        
    },
}

app.startApp();
