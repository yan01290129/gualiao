import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableHighlight,
    Image,
    Platform,
    NativeModules,
    NativeAppEventEmitter,
    BackHandler
} from 'react-native';

import Permissions from 'react-native-permissions';
import GroupCall from './GroupCall.js';
var Sound = require('react-native-sound');

var {API_URL} = require('../config.js');

var ConferenceModule = NativeModules.ConferenceModule

const SESSION_DIAL = "dial"; //呼叫方 dial -> connected
const SESSION_ACCEPT = "accept"; //被叫方 accept -> connected
const SESSION_CONNECTED = "connected";

const CONFERENCE_COMMAND_INVITE = "invite";//呼叫方 -> 被叫方
const CONFERENCE_COMMAND_WAIT = "waiting"; //被叫方 -> 呼叫方
const CONFERENCE_COMMAND_ACCEPT = "accept";//被叫方 -> 呼叫方
const CONFERENCE_COMMAND_REFUSE = "refuse";//被叫方 -> 呼叫方
const CONFERENCE_COMMAND_HANGUP = "hangup";//被叫方 <-> 呼叫方


const CONFERENCE_STATE_WAITING = 1  //等待被叫方接听
const CONFERENCE_STATE_ACCEPTED = 2 //被叫方接听了通话
const CONFERENCE_STATE_REFUSED = 3  //被叫方拒绝了通话
const CONFERENCE_STATE_CONNECTED = 4 //视频流已经建立
const CONFERENCE_STATE_DISCONNECTED = 5 //视频流断开

export default class Conference extends GroupCall {

    static navigatorStyle = {
        navBarHidden:true     
    };
    
    constructor(props) {
        super(props);

        var sessionState = this.props.isInitiator ? SESSION_DIAL : SESSION_ACCEPT;        
        //var sessionState = SESSION_CONNECTED;
        this.state.sessionState = sessionState;


        //任何成员都可以邀请其它人加入会议
        var inviter = this.props.initiator;//邀请人
        this.participantStates = this.props.participants.map(function(p) {
            return {...p, state:CONFERENCE_STATE_WAITING, inviter:inviter};
        });
        
        this.canceled = false;
        this._onCancel = this._onCancel.bind(this);
        this._onRefuse = this._onRefuse.bind(this);
        this._onAccept = this._onAccept.bind(this);

        this._handleBack = this._handleBack.bind(this);
        this._onAddParticipant = this._onAddParticipant.bind(this);

        this.name = "" + this.props.uid;
        this.room = this.props.channelID;
        this.token = this.props.token;
        
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        
        console.log("uid:", this.props.uid, "channel id:", this.props.channelID);
        console.log("name:", this.name, " room:", this.room);        
    }
    
    onNavigatorEvent(event) {
        if (event.type == 'NavBarButtonPress') { 
            if (event.id == 'backPress') {
                console.log("handle back");
                this._handleBack();
            }     
        }
    }
    
    /**
     * Inits new connection and conference when conference screen is entered.
     *
     * @inheritdoc
     * @returns {void}
     */
    componentWillMount() {

        this.subscription = NativeAppEventEmitter.addListener(
            'onRTMessage',
            (event) => {
                this._onRTMessage(event);
            }
        );

        if (this.state.sessionState != SESSION_CONNECTED) {
            if (!this.props.isInitiator) {
                this.play("start.mp3");                            
            } else {
                this.play("call.mp3");
            }


            //60s接听/呼叫超时
            this.timer = setTimeout(
                () => {
                    if (this.whoosh) {
                        this.whoosh.stop();
                        this.whoosh.release();
                        this.whoosh = null;
                    }

                    //todo dismiss
                    this.props.navigator.dismissModal();

                },
                60*1000
            );

            if (this.props.isInitiator) {
                this.inviteTimer = setInterval(() => {
                    this.invite();
                }, 1000);
            }
        } else {
            //test
            this.start();
        }

        if (this.props.isInitiator) {
            this.createConference();
        }
        
        Promise.resolve()
               .then(() => this.requestPermission('camera'))
               .then(() => this.requestPermission('microphone'));
    }

    //发送呼叫通知
    createConference() {
        var participants = this.props.participants.map(function(p) {
            return p.uid
        })
        var url = API_URL + "/conferences";
        var obj = {
            "channel_id":this.props.channelID,
            "participants":participants
        };

        fetch(url, {
            method:"POST",  
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + this.props.token,
            },
            body:JSON.stringify(obj),
        }).then((response) => {
            console.log("create conference response status:", response.status);
            return response.json().then((responseJson)=>{
                if (response.status == 200) {
                    console.log("create conference response json:", responseJson);
                } else {
                    console.log("create conference response error:", responseJson);
                }
            });
        }).catch((error) => {
            console.log("error:", error);
        });
    }

    hangupConference() {
        var participants = this.props.participants.map(function(p) {
            return p.uid
        })        
        var url = API_URL + "/conferences/hangup";
        var obj = {
            "channel_id":this.props.channelID,
            "participants":participants,
        };

        fetch(url, {
            method:"POST",  
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + this.props.token,
            },
            body:JSON.stringify(obj),
        }).then((response) => {
            console.log("hangup conference response status:", response.status);
            return response.json().then((responseJson)=>{
                if (response.status == 200) {
                    console.log("hangup conference response json:", responseJson);
                } else {
                    console.log("hangup conference response error:", responseJson);
                }
            });
        }).catch((error) => {
            console.log("error:", error);
        });        
    }
    
    requestPermission(permission) {
        return Permissions.check(permission)
                          .then(response => {
                              console.log("permission:" + permission + " " + response);
                              if (response == 'authorized') {
                                  
                              } else if (response == 'undetermined') {
                                  return response;
                              } else if (response == 'denied' || 
                                         response == 'restricted') {
                                  throw response;
                              }
                          })
                          .then(() => Permissions.request(permission))
                          .then((response) => {
                              console.log("permission:" + permission + " " + response);
                              if (response == 'authorized') {
                                  return response;
                              } else if (response == 'undetermined') {
                                  throw response;
                              } else if (response == 'denied' || 
                                         response == 'restricted') {
                                  throw response;
                              }                               
                          })
                          .catch((e) => {
                              console.log("err:", e);
                          });        
    }

    componentWillUnmount() {
        console.log("conference component will unmount");

        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        if (this.inviteTimer) {
            clearInterval(this.inviteTimer);
            this.inviteTimer = 0;
        }

        if (this.whoosh) {
            this.whoosh.stop();
            this.whoosh.release();
            this.whoosh = null;
        }
        if (this.props.isInitiator) {
            this.hangupConference();
        }
        
        this.subscription.remove();
    }

    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        console.log("session state:", this.state.sessionState);
        
        if (this.state.sessionState == SESSION_DIAL) {
            return this.renderDial();
        } else if (this.state.sessionState == SESSION_ACCEPT) {
            return this.renderAccept();
        } else if (this.state.sessionState == SESSION_CONNECTED) {
            return this.renderConference();
        }
        return null;
    }


    
    //呼叫界面
    renderDial() {
        console.log("render dial");
        return (
            <View style={{flex:1, backgroundColor:"white"}}>
                <View style={{flex:1, justifyContent:"center",  alignItems:"center"}} >
                    <ScrollView
                        contentContainerStyle = {{ justifyContent:"center",
                                                  alignItems:"center"}}

                        horizontal = { true }
                        showsHorizontalScrollIndicator = { false }
                        showsVerticalScrollIndicator = { false } >
                        {this.props.participants.map(p => {
                             return <Image
                                        style = {{resizeMode:"stretch", width:120, height:120}}
                                        key = { p.uid }
                                        source = {p.avatar ? {uri:p.avatar} : require('../img/avatar_contact.png') } />})
                        }
                    </ScrollView>
                </View>
                <View style={{
                    flex:1,
                    flexDirection:"row",
                    justifyContent:'center',
                    alignItems: 'center'}}>
                    
                    <TouchableHighlight onPress={this._onCancel}
                                        style = {{
                                            backgroundColor:"blue",
                                            borderRadius: 35,
                                            height:60,
                                            width: 60,
                                            justifyContent: 'center'
                                        }}
                                        underlayColor="red" >
                        
                        <Image source={{uri: 'Call_hangup'}}
                               style={{width: 60, height: 60}} />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }

    //接听界面
    renderAccept() {
        console.log("render accept");

        var p = this.props.participants.find(p => p.uid == this.props.initiator);

        if (!p) {
            return;
        }
        
        return (
            <View style={{flex:1, backgroundColor:"white"}}>
                <View style={{
                    flex:1,
                    justifyContent:"center",
                    alignItems:"center"}}>
                    <Image
                        style = {{resizeMode:"stretch", width:120, height:120}}
                        key = { p.uid }
                        source = {p.avatar ? {uri:p.avatar} : require('../img/avatar_contact.png') } />
                    <Text style={{fontSize:32}}>{p.name}</Text>
                    <Text style={{fontSize:12}}>邀请你进行语音通话</Text>
                </View>
                <View style={{flex:1}}>
                    <View style={{alignItems: 'center'}}>
                        <Text>
                            通话成员
                        </Text>
                        <View style={{
                            flex:1,
                            flexDirection:"row",
                            justifyContent:"center"}}>
                            {this.props.participants.map(p => {
                                 return <Image
                                            style = {{resizeMode:"stretch", width:32, height:32}}
                                            key = { p.uid }
                                            source = {p.avatar ? {uri:p.avatar} : require('../img/avatar_contact.png') } />})
                            }
                        </View>
                    </View>
                    <View style={{
                        flex:1,
                        flexDirection:"row",
                        justifyContent:'space-around',
                        alignItems: 'center' }}>
                        <TouchableHighlight onPress={this._onRefuse}
                                            style = {{
                                                backgroundColor:"blue",
                                                borderRadius: 35,
                                                height:60,
                                                width: 60,
                                                justifyContent: 'center'
                                            }}
                                            underlayColor="red">

                            <Image source={{uri: 'Call_hangup'}}
                                   style={{alignSelf: 'center', width: 60, height: 60}} />
                            
                        </TouchableHighlight>
                        

                        <TouchableHighlight onPress={this._onAccept}
                                            style = {{
                                                backgroundColor:"blue",
                                                borderRadius: 35,
                                                height:60,
                                                width: 60,
                                                justifyContent: 'center'
                                            }} >
                            <Image source={{uri: 'Call_Ans'}}
                                   style={{alignSelf: 'center', width: 60, height: 60}} />
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );        
    }

    _handleBack() {
        console.log("session state:", this.state.sessionState);
        
        if (this.state.sessionState == SESSION_DIAL) {
            this._onCancel();
        } else if (this.state.sessionState == SESSION_ACCEPT) {
            this._onRefuse();
        } else if (this.state.sessionState == SESSION_CONNECTED) {
            this._onHangUp();
        }

        //不立刻退出界面
        return true;
    }

    _onRTMessage(event) {
        console.log("on rt message:", event);
        if (this.props.channelID != event.channelID) {
            console.log("channel id do not match")
            return;
        }
        
        if (this.props.isInitiator) {
            var participant = this.participantStates.find((p) => {
                return p.uid == event.uid
            });
            if (!participant) {
                return;
            }
            if (event.command == CONFERENCE_COMMAND_ACCEPT) {
                console.log("on remote accept");
                participant.state = CONFERENCE_STATE_ACCEPTED;

                
                if (this.whoosh) {
                    this.whoosh.stop();
                    this.whoosh.release();
                    this.whoosh = null;
                }

                if (this.timer) {
                    clearTimeout(this.timer);
                    this.timer = null;
                }
                
                if (this.state.sessionState == SESSION_DIAL) {
                    this.setState({sessionState:SESSION_CONNECTED});
                    this.start();
                }
            } else if (event.command == CONFERENCE_COMMAND_REFUSE) {
                console.log("on remote refuse");
                participant.state = CONFERENCE_STATE_REFUSED;

                //check all refused
                var c = 0;
                this.participantStates.forEach((p) => {
                    if (p.state == CONFERENCE_STATE_REFUSED) {
                        c = c + 1;
                    }
                });

                if (c == this.participantStates.length) {
                    //on remote all refused
                    this._onCancel();
                }
            } else if (event.command == CONFERENCE_COMMAND_HANGUP) {
                console.log("on remote hangup");
                var index = this.participantStates.findIndex((p) => {
                    return p.uid == event.uid
                });
                this.participantStates.splice(index, 1);
            }
        } else {
            if (event.command == CONFERENCE_COMMAND_INVITE) {
                console.log("on invite");
                if (this.state.sessionState == SESSION_ACCEPT) {
                    ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_WAIT, this.props.initiator, this.props.channelID);
                } else if (this.state.sessionState == SESSION_CONNECTED) {
                    ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_ACCEPT, this.props.initiator, this.props.channelID);
                }
            } else if (event.command == CONFERENCE_COMMAND_HANGUP) {
                console.log("on remote hangup");
                var index = this.participantStates.findIndex((p) => {
                    return p.uid == event.uid
                });
                this.participantStates.splice(index, 1);
            }
        }
    }

    _onHangUp() {
        super._onHangUp();
        this.participantStates.forEach((p) => {
            if (p.uid != this.props.uid) {
                ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_HANGUP, p.uid, this.props.channelID);
            }
        });
        
        this.dismiss();
    }
    
    _onCancel() {
        if (this.canceled) {
            return;
        }
         
        this.canceled = true;
        this.dismiss();
    }
    
    _onAdd() {
        var users = this.props.users.map((u) => {
            var p = this.participantStates.find((p) => {
                return p.uid == u.uid
            });
            var checked = (p && p.state != CONFERENCE_STATE_REFUSED) ? true : false;
            return {...u, checked:checked};
        });

        
        var params = {
            title:"群视频",
            screen:"chat.ParticipantAdd",
            navigatorStyle:{
                tabBarHidden:true
            },
            overrideBackPress:true,
            passProps:{
                users:users,
                onAdd:this._onAddParticipant
            },
        }
        this.props.navigator.showModal(params);        
    }

    _onAddParticipant(participants) {
        var inviter = this.props.uid;
        var ps = participants.map(function(p) {
            var t = {...p, state:CONFERENCE_STATE_WAITING, inviter:inviter}
            delete(t.checked);
            delete(t.selected);
            return t;
        });

        this.participantStates = this.participantStates.concat(ps);
        if (!this.inviteTimer) {
            this.inviteTimer = setInterval(() => {
                this.invite();
            }, 1000);
        }
    }

    _onParticipantConnected(uid) {
        console.log("participant connected:", uid)
        var participant = this.participantStates.find((p) => {
            return p.uid == uid
        });
        if (!participant) {
            return;
        }
        //assert state == CONFERENCE_STATE_ACCEPTED ||  CONFERENCE_STATE_DISCONNECTED
        participant.state = CONFERENCE_STATE_CONNECTED;
    }

    _onParticipantDisconnected(uid) {
        console.log("participant disconnected:", uid)
        var participant = this.participantStates.find((p) => {
            return p.uid == uid
        });
        if (!participant) {
            return;
        }
        participant.state = CONFERENCE_STATE_DISCONNECTED;
        participant.disconnectTimestamp = (new Date().getTime())/1000;
    }
    
    _onAccept() {
        console.log("accept...");
        //todo 对所有成员广播
        ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_ACCEPT, this.props.initiator, this.props.channelID);
        if (this.whoosh) {
            this.whoosh.stop();
            this.whoosh.release();
            this.whoosh = null;
        }

        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        this.setState({sessionState:SESSION_CONNECTED});
        this.start();
    }

    _onRefuse() {
        console.log("refuse...");
        //todo 对所有成员广播
        ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_REFUSE, this.props.initiator, this.props.channelID);
        this.dismiss();
    }
    
    dismiss() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        if (this.whoosh) {
            this.whoosh.stop();
            this.whoosh.release();
            this.whoosh = null;
        }
        
        this.props.navigator.dismissModal();
    }

    invite() {
        this.participantStates.forEach((p) => {
            if (p.uid != this.props.uid &&
                p.state == CONFERENCE_STATE_WAITING &&
                p.inviter == this.props.uid) {
                var participants = this.props.participants.map((p1)=>{
                    return p1.uid;
                });
                var obj = {
                    channelID:this.props.channelID,
                    initiator:this.props.initiator,
                    participants:participants,
                    groupID:this.props.groupID
                };
                ConferenceModule.sendInvite(obj, p.uid);
            }
        });
    }

    play(name) {
        console.log("play:" + name);
        // Load the sound file 'whoosh.mp3' from the app bundle
        // See notes below about preloading sounds within initialization code below.
        var whoosh = new Sound(name, Sound.MAIN_BUNDLE);
        
        // Loop indefinitely until stop() is called        
        whoosh.setNumberOfLoops(-1);
        whoosh.prepare((error) => {
            if (error) {
                console.log('failed to load the sound', error);
            } else { // loaded successfully
                console.log('duration in seconds: ' + whoosh.getDuration() +
                            'number of channels: ' + whoosh.getNumberOfChannels());
                // Get properties of the player instance
                console.log('volume: ' + whoosh.getVolume());
                console.log('pan: ' + whoosh.getPan());
                console.log('loops: ' + whoosh.getNumberOfLoops());
                
                whoosh.play((success) => {
                    if (success) {
                        console.log('successfully finished playing');
                    } else {
                        console.log('playback failed due to audio decoding errors');
                    }
                });
            }
        });

    
        this.whoosh = whoosh;
    }

}
