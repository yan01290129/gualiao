'use strict';

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    ListView,
    ScrollView,
    TouchableHighlight,
    TextInput,
    Alert,
    View,
    Platform
} from 'react-native';

var Toast = require('@remobile/react-native-toast');

import { NativeModules } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

var {NAVIGATOR_STYLE} = require('../config.js');

class ParticipantAdd extends Component {
    static navigatorButtons = {
        rightButtons: [
            {
                title: '确定', 
                id: 'ok', 
            },
        ],
        leftButtons: [
            {
                title:"取消",
                id:"cancel",
            }
        ]
    };

    static navigatorStyle = NAVIGATOR_STYLE;
    
    constructor(props) {
        super(props);

        console.log("uid:", this.props.uid);
        var rowHasChanged = function (r1, r2) {
            return r1 !== r2;
        }
        var ds = new ListView.DataSource({rowHasChanged: rowHasChanged});
        var data = [];
        for (var i = 0; i < this.props.users.length; i++) {
            var t = {...this.props.users[i], id:i}
            data.push(t);
        }
        this.state = {
            data:data,
            dataSource: ds.cloneWithRows(data),
            visible:false,
        };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) {
        if (event.type == 'NavBarButtonPress') { 
            if (event.id == 'ok') {
                this.handleOK();
            } else if (event.id == 'cancel') {
                this.handleCancel();
            }
        }
    }

    
    handleOK() {
        var participants = this.state.data.filter((p) => {
            return p.selected
        });

        if (participants.length > 0) {
            this.props.onAdd(participants);
            this.props.navigator.dismissModal();
        }
    }

    handleCancel() {
        this.props.navigator.dismissModal();        
    }

    render() {
        var renderRow = (rowData) => {
            var selectImage = () => {
                if (rowData.checked) {
                    return  require('../img/CellGraySelected.png');
                } else if (rowData.selected) {
                    return  require('../img/CellBlueSelected.png');
                } else {
                    return require('../img/CellNotSelected.png');
                }
            }

            return (
                <TouchableHighlight style={styles.row} onPress={() => this.rowPressed(rowData)}
                                    underlayColor='#eeeeee' >
                    <View style={{flexDirection:"row", flex:1, alignItems:"center" }}>
                        <Image style={{marginLeft:10}} source={selectImage()}></Image>
                        <Text style={{marginLeft:10}}>{rowData.name}</Text>
                    </View>
                </TouchableHighlight>
            );
        }
        

        return (
            <View style={{ flex:1, backgroundColor:"#F5FCFF" }}>

                <View style={{height:1, backgroundColor:"lightgrey"}}></View>

                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={renderRow}
                />

                <Spinner visible={this.state.visible} />
            </View>
        );
    }

    rowPressed(rowData) {
        if (rowData.checked) {
            return;
        }
        
        var data = this.state.data;
        var ds = this.state.dataSource;
        var newData = data.slice();
        var newRow = {...rowData, selected:!rowData.selected};
        newData[rowData.id] = newRow;
        this.setState({data:newData, dataSource:ds.cloneWithRows(newData)});
    }

}


const styles = StyleSheet.create({
    row: {
        height:50,
    },
});

export default ParticipantAdd;

