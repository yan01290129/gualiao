'use strict';

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    ListView,
    ScrollView,
    TouchableHighlight,
    TextInput,
    Alert,
    View,
    Platform
} from 'react-native';

var Toast = require('@remobile/react-native-toast');

import { NativeModules } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import {NAVIGATOR_STYLE} from '../config';

class ConferenceCreator extends Component {
    static navigatorButtons = {
        rightButtons: [
            {
                title: '完成', 
                id: 'ok', 
            },
        ]
    };

    static navigatorStyle = NAVIGATOR_STYLE;
    
    constructor(props) {
        super(props);

        console.log("uid:", this.props.uid, this.props.users);
        var rowHasChanged = function (r1, r2) {
            return r1 !== r2;
        }
        var ds = new ListView.DataSource({rowHasChanged: rowHasChanged});
        var data = this.props.users.map((user) => {
            var selected = this.props.uid == user.uid;
            return {...user, id:user.uid, selected:selected};
        });

        for (var i = 0; i < data.length; i++) {
            data[i].id = i;
            data[i].selected = this.props.uid == data[i].uid;
        }
        this.state = {
            data:data,
            dataSource: ds.cloneWithRows(data),
            visible:false,
        };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) {
        if (event.type == 'NavBarButtonPress') { 
            if (event.id == 'ok') {
                this.handleCreate();
            }     
        }
    }

    createConference(participants) {
        var params = {
            title:"群视频",
            screen:"chat.DialConference",
            navigatorStyle:{
                tabBarHidden:true
            },
            overrideBackPress:true,
            passProps:{
                initiator:this.props.uid,
                uid:this.props.uid,
                isInitiator:true,
                channelID:this.props.channelID,
                participants:participants,
                users:this.props.users,
                groupID:this.props.groupID,
                token:this.props.token,
            },
        }
        this.props.navigator.pop({ animated: false});
        this.props.navigator.showModal(params);
    }
   
    handleCreate() {
        var participants = [];
        var data = this.state.data;
        for (var i = 0; i < data.length; i++) {
            let u = data[i];
            if (u.selected) {
                participants.push(u);
            }
        }

        if (participants.length <= 1) {
            return;
        }
        
        this.createConference(participants);
    }

    handleCancel() {
        native.onCancel();
    }

    showSpinner() {
        this.setState({visible:true});
    }

    hideSpinner() {
        this.setState({visible:false});
    }

    render() {
        var renderRow = (rowData) => {
            var selectImage = () => {
                if (rowData.uid == this.props.uid) {
                    return  require('../img/CellGraySelected.png');
                } else if (rowData.selected) {
                    return  require('../img/CellBlueSelected.png');
                } else {
                    return require('../img/CellNotSelected.png');
                }
            }

            return (
                <TouchableHighlight style={styles.row} onPress={() => this.rowPressed(rowData)}
                                    underlayColor='#eeeeee' >
                    <View style={{flexDirection:"row", flex:1, alignItems:"center" }}>
                        <Image style={{marginLeft:10}} source={selectImage()}></Image>
                        <Text style={{marginLeft:10}}>{rowData.name}</Text>
                    </View>
                </TouchableHighlight>
            );
        }

  

        return (
            <View style={{ flex:1, backgroundColor:"#F5FCFF" }}>

                <View style={{height:1, backgroundColor:"lightgrey"}}></View>

                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={renderRow}
                />

                <Spinner visible={this.state.visible} />
            </View>
        );
    }

    rowPressed(rowData) {
        if (rowData.uid == this.props.uid) {
            return;
        }
        
        var data = this.state.data;
        var ds = this.state.dataSource;
        var newData = data.slice();
        var newRow = {...rowData, selected:!rowData.selected};
        newData[rowData.id] = newRow;
        this.setState({data:newData, dataSource:ds.cloneWithRows(newData)});
    }

}


const styles = StyleSheet.create({
    row: {
        height:50,
    },
});

export default ConferenceCreator;
