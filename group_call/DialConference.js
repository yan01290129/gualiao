import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableHighlight,
    Image,
    Platform,
    NativeModules,
    NativeAppEventEmitter,
    BackHandler
} from 'react-native';

import Permissions from 'react-native-permissions';
import GroupCall from './GroupCall.js';
var Sound = require('react-native-sound');

var {API_URL} = require('../config.js');

var ConferenceModule = NativeModules.ConferenceModule



const CONFERENCE_COMMAND_INVITE = "invite";//呼叫方 -> 被叫方
const CONFERENCE_COMMAND_WAIT = "waiting"; //被叫方 -> 呼叫方
const CONFERENCE_COMMAND_ACCEPT = "accept";//被叫方 -> 呼叫方
const CONFERENCE_COMMAND_REFUSE = "refuse";//被叫方 -> 呼叫方
const CONFERENCE_COMMAND_HANGUP = "hangup";//被叫方 <-> 呼叫方
const CONFERENCE_COMMAND_ACCEPTED = "accepted";//呼叫方 -> 被叫方
const CONFERENCE_COMMAND_REFUSED = "refused";//呼叫方 -> 被叫方


const CONFERENCE_STATE_WAITING = 1  //等待被叫方接听
const CONFERENCE_STATE_ACCEPTED = 2 //被叫方接听了通话
const CONFERENCE_STATE_REFUSED = 3  //被叫方拒绝了通话
const CONFERENCE_STATE_CONNECTED = 4 //视频流已经建立
const CONFERENCE_STATE_DISCONNECTED = 5 //视频流断开

export default class DialConference extends Component {

    static navigatorStyle = {
        navBarHidden:true     
    };
    
    constructor(props) {
        super(props);

        this.state = {};        

        //任何成员都可以邀请其它人加入会议
        var inviter = this.props.initiator;//邀请人
        this.participantStates = this.props.participants.map(function(p) {
            return {...p, state:CONFERENCE_STATE_WAITING, inviter:inviter};
        });

        this.canceled = false;
        this._onRemoteRefused = this._onRemoteRefused.bind(this);
        this._onHangUp = this._onHangUp.bind(this);
        this._handleBack = this._handleBack.bind(this);

        this.name = "" + this.props.uid;
        this.room = this.props.channelID;
        this.token = this.props.token;
        
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        
        console.log("uid:", this.props.uid, "channel id:", this.props.channelID);
        console.log("name:", this.name, " room:", this.room);        
    }
    
    onNavigatorEvent(event) {
        if (event.type == 'NavBarButtonPress') { 
            if (event.id == 'backPress') {
                console.log("handle back");
                this._handleBack();
            }     
        }
    }

    componentWillMount() {

        this.subscription = NativeAppEventEmitter.addListener(
            'onRTMessage',
            (event) => {
                this._onRTMessage(event);
            }
        );

        
        this.play("call.mp3");
        
        //60s接听/呼叫超时
        this.timer = setTimeout(
            () => {
                this.dismiss();
            },
            60*1000
        );

        this.inviteTimer = setInterval(() => {
            this.invite();
        }, 1000);
        
        
        this.createConference();
    }

    //发送呼叫通知
    createConference() {
        var participants = this.props.participants.map(function(p) {
            return p.uid
        })
        var url = API_URL + "/conferences";
        var obj = {
            "channel_id":this.props.channelID,
            "participants":participants
        };

        fetch(url, {
            method:"POST",  
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + this.props.token,
            },
            body:JSON.stringify(obj),
        }).then((response) => {
            console.log("create conference response status:", response.status);
            return response.json().then((responseJson)=>{
                if (response.status == 200) {
                    console.log("create conference response json:", responseJson);
                } else {
                    console.log("create conference response error:", responseJson);
                }
            });
        }).catch((error) => {
            console.log("error:", error);
        });
    }

    hangupConference() {
        var participants = this.props.participants.map(function(p) {
            return p.uid
        })        
        var url = API_URL + "/conferences/hangup";
        var obj = {
            "channel_id":this.props.channelID,
            "participants":participants,
        };

        fetch(url, {
            method:"POST",  
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + this.props.token,
            },
            body:JSON.stringify(obj),
        }).then((response) => {
            console.log("hangup conference response status:", response.status);
            return response.json().then((responseJson)=>{
                if (response.status == 200) {
                    console.log("hangup conference response json:", responseJson);
                } else {
                    console.log("hangup conference response error:", responseJson);
                }
            });
        }).catch((error) => {
            console.log("error:", error);
        });        
    }

    componentWillUnmount() {
        console.log("conference component will unmount");

        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        if (this.inviteTimer) {
            clearInterval(this.inviteTimer);
            this.inviteTimer = 0;
        }

        if (this.whoosh) {
            this.whoosh.stop();
            this.whoosh.release();
            this.whoosh = null;
        }
        if (this.props.isInitiator) {
            this.hangupConference();
        }
        
        this.subscription.remove();
    }

    render() {
        console.log("render dial");
        return (
            <View style={{flex:1, backgroundColor:"white"}}>
                <View style={{flex:1, justifyContent:"center",  alignItems:"center"}} >
                    <ScrollView
                        contentContainerStyle = {{ justifyContent:"center",
                                                   alignItems:"center"}}

                        horizontal = { true }
                        showsHorizontalScrollIndicator = { false }
                        showsVerticalScrollIndicator = { false } >
                        {this.props.participants.map(p => {
                             return <Image
                                        style = {{resizeMode:"stretch", width:120, height:120}}
                                        key = { p.uid }
                                        source = {p.avatar ? {uri:p.avatar} : require('../img/avatar_contact.png') } />})
                        }
                    </ScrollView>
                </View>
                <View style={{
                    flex:1,
                    flexDirection:"row",
                    justifyContent:'center',
                    alignItems: 'center'}}>
                    
                    <TouchableHighlight onPress={this._onHangUp}
                                        style = {{
                                            backgroundColor:"blue",
                                            borderRadius: 35,
                                            height:60,
                                            width: 60,
                                            justifyContent: 'center'
                                        }}
                                        underlayColor="red" >
                        
                        <Image source={{uri: 'Call_hangup'}}
                               style={{width: 60, height: 60}} />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }

    

    _handleBack() {
        this._onCancel();

        //不立刻退出界面
        return true;
    }

    _onRTMessage(event) {
        console.log("on rt message:", event);
        if (this.props.channelID != event.channelID) {
            console.log("channel id do not match")
            return;
        }
        
        var participant = this.participantStates.find((p) => {
            return p.uid == event.uid
        });
        if (!participant) {
            return;
        }
        if (event.command == CONFERENCE_COMMAND_ACCEPT) {
            console.log("on remote accept");
            participant.state = CONFERENCE_STATE_ACCEPTED;
            
            this.dismiss();
            var passProps = {
                initiator:this.props.uid,
                uid:this.props.uid,
                isInitiator:true,
                channelID:this.props.channelID,
                participants:this.props.participants,
                users:this.props.users,
                groupID:this.props.groupID,
                token:this.props.token,
            };


            console.log("token tttttt:", this.props.token);
            ConferenceModule.startConference(passProps);
        } else if (event.command == CONFERENCE_COMMAND_REFUSE) {
            console.log("on remote refuse");
            participant.state = CONFERENCE_STATE_REFUSED;

            ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_REFUSED, this.props.uid, participant.uid, this.props.channelID);
            //check all refused
            var c = 0;
            this.participantStates.forEach((p) => {
                if (p.state == CONFERENCE_STATE_REFUSED) {
                    c = c + 1;
                }
            });

            if (c == this.participantStates.length - 1) {
                //on remote all refused
                this._onRemoteRefused();
            }
        }
        
    }

    _onHangUp() {
        this.participantStates.forEach((p) => {
            if (p.uid != this.props.uid) {
                ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_HANGUP, this.props.uid, p.uid, this.props.channelID);
            }
        });
        
        this.dismiss();
    }
    
    _onRemoteRefused() {
        if (this.canceled) {
            return;
        }
        this.canceled = true;
      
        this.dismiss();
    }
    
 
    dismiss() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }
        if (this.inviteTimer) {
            clearTimeout(this.inviteTimer);
            this.inviteTimer = null;
        }
        
        if (this.whoosh) {
            this.whoosh.stop();
            this.whoosh.release();
            this.whoosh = null;
        }
        
        this.props.navigator.dismissModal({animationType:"none"});
    }

    invite() {
        this.participantStates.forEach((p) => {
            if (p.uid != this.props.uid &&
                p.state == CONFERENCE_STATE_WAITING &&
                p.inviter == this.props.uid) {
                var participants = this.props.participants.map((p1)=>{
                    return p1.uid;
                });
                var obj = {
                    channelID:this.props.channelID,
                    initiator:this.props.initiator,
                    participants:participants,
                    groupID:this.props.groupID
                };
                ConferenceModule.sendInvite(obj, this.props.uid, p.uid);
            }
        });
    }

    play(name) {
        console.log("play:" + name);
        // Load the sound file 'whoosh.mp3' from the app bundle
        // See notes below about preloading sounds within initialization code below.
        var whoosh = new Sound(name, Sound.MAIN_BUNDLE);
        
        // Loop indefinitely until stop() is called        
        whoosh.setNumberOfLoops(-1);
        whoosh.prepare((error) => {
            if (error) {
                console.log('failed to load the sound', error);
            } else { // loaded successfully
                console.log('duration in seconds: ' + whoosh.getDuration() +
                            'number of channels: ' + whoosh.getNumberOfChannels());
                // Get properties of the player instance
                console.log('volume: ' + whoosh.getVolume());
                console.log('pan: ' + whoosh.getPan());
                console.log('loops: ' + whoosh.getNumberOfLoops());
                
                whoosh.play((success) => {
                    if (success) {
                        console.log('successfully finished playing');
                    } else {
                        console.log('playback failed due to audio decoding errors');
                    }
                });
            }
        });

        
        this.whoosh = whoosh;
    }

}
