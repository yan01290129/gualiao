import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableHighlight,
    Image,
    Platform,
    NativeModules,
    NativeAppEventEmitter,
    BackHandler
} from 'react-native';

import Permissions from 'react-native-permissions';
import GroupCall from './GroupCall.js';
var Sound = require('react-native-sound');

var ConferenceModule = NativeModules.ConferenceModule

const SESSION_DIAL = "dial"; //呼叫方 dial -> connected
const SESSION_ACCEPT = "accept"; //被叫方 accept -> connected
const SESSION_CONNECTED = "connected";

const CONFERENCE_COMMAND_INVITE = "invite";//呼叫方 -> 被叫方
const CONFERENCE_COMMAND_WAIT = "waiting"; //被叫方 -> 呼叫方
const CONFERENCE_COMMAND_ACCEPT = "accept";//被叫方 -> 呼叫方
const CONFERENCE_COMMAND_REFUSE = "refuse";//被叫方 -> 呼叫方
const CONFERENCE_COMMAND_HANGUP = "hangup";//被叫方 <-> 呼叫方
const CONFERENCE_COMMAND_ACCEPTED = "accepted";//呼叫方 -> 被叫方
const CONFERENCE_COMMAND_REFUSED = "refused";//呼叫方 -> 被叫方


export default class ReceiveConference extends Component {

    static navigatorStyle = {
        navBarHidden:true     
    };
    
    constructor(props) {
        super(props);

        this.state = {};
        this.state.sessionState = SESSION_ACCEPT;


        //任何成员都可以邀请其它人加入会议
        var inviter = this.props.initiator;//邀请人
        this.participantStates = this.props.participants.map(function(p) {
            return {...p, inviter:inviter};
        });
        
        this._onRefuse = this._onRefuse.bind(this);
        this._onAccept = this._onAccept.bind(this);

        this._handleBack = this._handleBack.bind(this);

        this.name = "" + this.props.uid;
        this.room = this.props.channelID;
        this.token = this.props.token;
        
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        
        console.log("uid:", this.props.uid, "channel id:", this.props.channelID);
        console.log("name:", this.name, " room:", this.room);        
    }
    
    onNavigatorEvent(event) {
        if (event.type == 'NavBarButtonPress') { 
            if (event.id == 'backPress') {
                console.log("handle back");
                this._handleBack();
            }     
        }
    }
 
    componentWillMount() {

        this.subscription = NativeAppEventEmitter.addListener(
            'onRTMessage',
            (event) => {
                this._onRTMessage(event);
            }
        );

        this.play("start.mp3");                            

        //60s接听超时
        this.timer = setTimeout(
            () => {
                this.dismiss();
            },
            60*1000
        );
    }


    componentWillUnmount() {
        console.log("conference component will unmount");

        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        if (this.inviteTimer) {
            clearInterval(this.inviteTimer);
            this.inviteTimer = 0;
        }

        if (this.whoosh) {
            this.whoosh.stop();
            this.whoosh.release();
            this.whoosh = null;
        }
        if (this.props.isInitiator) {
            this.hangupConference();
        }
        
        this.subscription.remove();
    }

    render() {
        console.log("render accept");

        var p = this.props.participants.find(p => p.uid == this.props.initiator);

        if (!p) {
            return;
        }
        
        return (
            <View style={{flex:1, backgroundColor:"white"}}>
                <View style={{
                    flex:1,
                    justifyContent:"center",
                    alignItems:"center"}}>
                    <Image
                        style = {{resizeMode:"stretch", width:120, height:120}}
                        key = { p.uid }
                        source = {p.avatar ? {uri:p.avatar} : require('../img/avatar_contact.png') } />
                    <Text style={{fontSize:32}}>{p.name}</Text>
                    <Text style={{fontSize:12}}>邀请你进行语音通话</Text>
                </View>
                <View style={{flex:1}}>
                    <View style={{alignItems: 'center'}}>
                        <Text>
                            通话成员
                        </Text>
                        <View style={{
                            flex:1,
                            flexDirection:"row",
                            justifyContent:"center"}}>
                            {this.props.participants.map(p => {
                                 return <Image
                                            style = {{resizeMode:"stretch", width:32, height:32}}
                                            key = { p.uid }
                                            source = {p.avatar ? {uri:p.avatar} : require('../img/avatar_contact.png') } />})
                            }
                        </View>
                    </View>
                    <View style={{
                        flex:1,
                        flexDirection:"row",
                        justifyContent:'space-around',
                        alignItems: 'center' }}>
                        <TouchableHighlight onPress={this._onRefuse}
                                            style = {{
                                                backgroundColor:"blue",
                                                borderRadius: 35,
                                                height:60,
                                                width: 60,
                                                justifyContent: 'center'
                                            }}
                                            underlayColor="red">

                            <Image source={{uri: 'Call_hangup'}}
                                   style={{alignSelf: 'center', width: 60, height: 60}} />
                            
                        </TouchableHighlight>
                        

                        <TouchableHighlight onPress={this._onAccept}
                                            style = {{
                                                backgroundColor:"blue",
                                                borderRadius: 35,
                                                height:60,
                                                width: 60,
                                                justifyContent: 'center'
                                            }} >
                            <Image source={{uri: 'Call_Ans'}}
                                   style={{alignSelf: 'center', width: 60, height: 60}} />
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );        
    }

    _handleBack() {
        console.log("session state:", this.state.sessionState);
        this._onRefuse();
        //不立刻退出界面
        return true;
    }

    _onRTMessage(event) {
        console.log("on rt message:", event);
        if (this.props.channelID != event.channelID) {
            console.log("channel id do not match")
            return;
        }

        if (event.command == CONFERENCE_COMMAND_INVITE) {
            console.log("on invite");
            ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_WAIT,
                                           this.props.uid,
                                           this.props.initiator,
                                           this.props.channelID);
        } else if (event.command == CONFERENCE_COMMAND_HANGUP) {
            console.log("on remote hangup");
            this.dismiss();
        } else if (event.command == CONFERENCE_COMMAND_ACCEPTED) {
            console.log("on accepted on another device");
            this.dismiss();            
        } else if (event.command == CONFERENCE_COMMAND_REFUSED) {
            console.log("on refused on another device");
            this.dismiss();            
        }
    }
    
    _onAccept() {
        console.log("accept...");
        //todo 对所有成员广播
        ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_ACCEPT, this.props.uid, this.props.initiator, this.props.channelID);

        
        this.dismiss();

        var passProps = {
            initiator:this.props.initiator,
            uid:this.props.uid,
            isInitiator:false,
            channelID:this.props.channelID,
            participants:this.props.participants,
            users:this.props.users,
            groupID:this.props.groupID,
            token:this.props.token,
        };
        ConferenceModule.startConference(passProps);
    }

    _onRefuse() {
        console.log("refuse...");
        //todo 对所有成员广播
        ConferenceModule.sendRTMessage(CONFERENCE_COMMAND_REFUSE, this.props.uid, this.props.initiator, this.props.channelID);
        this.dismiss();
    }
    
    dismiss() {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }

        if (this.whoosh) {
            this.whoosh.stop();
            this.whoosh.release();
            this.whoosh = null;
        }
        
        this.props.navigator.dismissModal({animationType:"none"});
    }



    play(name) {
        console.log("play:" + name);
        // Load the sound file 'whoosh.mp3' from the app bundle
        // See notes below about preloading sounds within initialization code below.
        var whoosh = new Sound(name, Sound.MAIN_BUNDLE);
        
        // Loop indefinitely until stop() is called        
        whoosh.setNumberOfLoops(-1);
        whoosh.prepare((error) => {
            if (error) {
                console.log('failed to load the sound', error);
            } else { // loaded successfully
                console.log('duration in seconds: ' + whoosh.getDuration() +
                            'number of channels: ' + whoosh.getNumberOfChannels());
                // Get properties of the player instance
                console.log('volume: ' + whoosh.getVolume());
                console.log('pan: ' + whoosh.getPan());
                console.log('loops: ' + whoosh.getNumberOfLoops());
                
                whoosh.play((success) => {
                    if (success) {
                        console.log('successfully finished playing');
                    } else {
                        console.log('playback failed due to audio decoding errors');
                    }
                });
            }
        });

        
        this.whoosh = whoosh;
    }

}
