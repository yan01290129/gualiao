import {Platform} from 'react-native';

export const API_URL = "http://120.236.26.29:8080";

export const NAVIGATOR_STYLE = Platform.select({
    ios: {

    },
    android: {
        navBarBackgroundColor: '#212121',
        navBarTextColor: '#ffffff',
        navBarSubtitleTextColor: '#ff0000',
        navBarButtonColor: '#ffffff',
        statusBarTextColorScheme: 'light',
    }
});


