import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    TouchableHighlight,
    TextInput,
    Platform,
    NativeModules,    
    View
} from 'react-native';

import { connect } from 'react-redux';
import {updateGroupNickname} from "./actions";
import Spinner from 'react-native-loading-spinner-overlay';
import {API_URL, NAVIGATOR_STYLE} from './config';

var GroupManager = NativeModules.GroupModule;


class GroupNickname extends Component {
    static navigatorStyle = NAVIGATOR_STYLE;
    
    static navigatorButtons = {
        rightButtons: [
            {
                title: '确定', 
                id: 'setting', 
                showAsAction: 'ifRoom' 
            },
        ]
    };

 
    constructor(props) {
        super(props);
        this.state = {
            nickname:this.props.nickname,
            visible:false
        };

        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) {
        if (event.type == 'NavBarButtonPress') { 
            if (event.id == 'setting') {
                this.updateName();
            }     
        }
    }

    updateName() {
        if (this.state.nickname == this.props.nickname) {
            return;
        }
        console.log("update nickname in group...");

        GroupManager.updateNicknameInGroup(this.props.groupID, this.state.nickname);
        this.props.dispatch(updateGroupNickname(this.state.nickname, this.props.uid));
        this.props.navigator.pop();
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ScrollView style={{flex:1, backgroundColor:"#F5FCFF"}}>
                    <View style={{marginTop:12}}>
                        <Text style={{marginLeft:12, marginBottom:4}}>群内昵称</Text>
                        <TextInput
                            style={{paddingLeft:12, height: 40, backgroundColor:"white"}}
                            placeholder=""
                            onChangeText={(text) => this.setState({nickname:text})}
                            value={this.state.nickname}/>
                    </View>
                </ScrollView>

                <Spinner visible={this.state.visible} />
            </View>
        );
    }
    
}



export default connect((state) => {
    return {
        nickname:state.group.nickname,
        groupID:state.group.id,
        uid:state.profile.uid,        
        token:state.profile.gobelieveToken
    };
})(GroupNickname);
