
export const UPDATE_NAME = "update_name";
export const ADD_MEMBERS = "add_members";
export const REMOVE_MEMBERS = "remove_members"
export const SET_GROUP = "set_group";
export const UPDATE_NOTICE = "update_notice"
export const UPDATE_NICKNAME = "update_nickname"
export const UPDATE_DONOTDISTURB = "update_donotdisturb"

export function setGroup(group) {
    return {
        type:SET_GROUP,
        group:group
    };
}

export function updateGroupName(name) {
    return {
        type:"update_name",
        name
    };
}

export function updateGroupNotice(notice) {
    return {
        type:"update_notice",
        notice
    };
}

export function updateGroupNickname(nickname, uid) {
    return {
        type:"update_nickname",
        nickname:nickname,
        uid:uid
    };
}

export function updateGroupDoNotDisturb(doNotDisturb) {
    return {
        type:"update_donotdisturb",
        doNotDisturb
    };    
}

export function addGroupMembers(members) {
    return {
        type:"add_members",
        members
    };
}

export function removeGroupMembers(members) {
    return {
        type:"remove_members",
        members
    };
}


function arrayObjectIndexOf(myArray, searchTerm, property) {
    for(var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) return i;
    }
    return -1;
}

export function groupReducer(state = {name:"", members:[]}, action) {
    switch(action.type) {
        case UPDATE_NAME:
            console.log("update name:" + action.name);
            return {...state, name:action.name};
        case UPDATE_NOTICE:
            return {...state, notice:action.notice};
        case UPDATE_NICKNAME:
            var members = state.members.map((member) => {
                if (member.uid == action.uid) {
                    member.name = action.nickname
                }
                return member;
            });
            return {...state, nickname:action.nickname, members:members};
        case UPDATE_DONOTDISTURB:
            return {...state, doNotDisturb:action.doNotDisturb}
        case ADD_MEMBERS:
            return {...state, members:[...state.members, ...action.members]};
        case REMOVE_MEMBERS:
            var members = state.members.slice();
            action.members.forEach((member) => {
                var index = members.findIndex((m) => {
                    return m.uid == member.uid
                });
                if (index != -1) {
                    members.splice(index, 1);
                }
            });
            return {
                ...state,
                members:members
            };
        case SET_GROUP:
            console.log("set group....:");
            return action.group;
        default:
            return state;
    }
}

