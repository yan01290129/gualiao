
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    TouchableHighlight,
    ActionSheetIOS,
    Platform,
    Switch,
    NativeModules,    
    View
} from 'react-native';

import { connect } from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import Spinner from 'react-native-loading-spinner-overlay';
import {updateGroupDoNotDisturb} from './actions';
import {API_URL, NAVIGATOR_STYLE} from './config';

var GroupManager = NativeModules.GroupModule;

class GroupSetting extends Component {
    static navigatorStyle = NAVIGATOR_STYLE;
    
    constructor(props) {
        super(props);
        
        this.handleName = this.handleName.bind(this);
        this.handleNotice = this.handleNotice.bind(this);
        this.handleDoNotDisturb = this.handleDoNotDisturb.bind(this);
        this.handleActionSheetPress = this.handleActionSheetPress.bind(this);
        this.handleQuit = this.handleQuit.bind(this);
        this.state = {
            spinnerVisible:false,
        };
        console.log("group name:", this.props.topic);
    }
    
    componentDidMount() {

    }


    componentWillUnmount() {

    }

   

    render() {
        var self = this;
        var rows = this.props.members.map(function(i) {
            var avatar = i.avatar ? i.avatar : "";
            return (
                <View key={i.uid} style={{alignItems:'center', height:78}}>
                    <TouchableHighlight underlayColor='gray' style={styles.headButton} onPress={self.handleClickMember.bind(self, i)} >
                        <Image
                            defaultSource={require('./img/PersonalChat.png')}
                            source={avatar ? {uri:avatar} : require('./img/PersonalChat.png')}
                            style={styles.head}
                        />
                    </TouchableHighlight>
                    <Text numberOfLines={1} style={{textAlign:"center", width:50, height:20}}>{i.name}</Text>

                </View>
            );
            
        });

        var BUTTONS = [
            '确定',
            '取消',
        ];
        
        return (
            <View style={{flex:1, backgroundColor: 'rgb(235, 235, 237)'}}>
                <ScrollView style={styles.container}>
                    <View style={styles.block}>
                        <View style={{flex: 1, flexDirection:'row', flexWrap: 'wrap', marginTop:10, marginLeft:10, marginBottom:10}}>
                            {rows}
                            <TouchableHighlight  underlayColor='gray' style={styles.headButton} onPress={this.handleAdd.bind(this)}>
                                <Image
                                    source={require('./img/AddGroupMemberBtn.png')}
                                    style={styles.head}
                                />
                            </TouchableHighlight>
                            {
                                this.props.is_master ?
                                (
                                    <TouchableHighlight  underlayColor='gray' style={styles.headButton} onPress={this.handleRemove.bind(this)}>
                                        <Image
                                            source={require('./img/RemoveGroupMemberBtn.png')}
                                            style={{width:50, height:50}}
                                        />
                                    </TouchableHighlight>
                                ) : null
                            }
                        </View>
                    </View>
                    
                    <View style={{marginTop:20, backgroundColor:'#FFFFFF'}}>
                        <TouchableHighlight underlayColor='ghostwhite' style={styles.item} onPress={this.handleName} >
                            <View style={styles.itemInternal}>
                                <Text>群聊名称</Text>
                                <View style={{flexDirection:'row', alignItems:"center", marginRight:8}}>
                                    <Text>{this.props.topic}</Text>
                                    <Image source={require('./img/TableViewArrow.png')}
                                           style={{marginLeft:4, width:20, height:20}} />
                                </View>
                            </View>
                        </TouchableHighlight>


                        <View style={styles.line}/>

                        <TouchableHighlight underlayColor='ghostwhite' style={styles.item} onPress={this.handleNotice} >
                            <View  style={{flexDirection:'column'}}>
                                <Text>群公告</Text>
                                <Text style={styles.notice}>{this.props.notice}</Text>
                            </View>
                        </TouchableHighlight>

                        <View style={styles.line}/>
          
                        
                        <View style={{flexDirection:'row',
                                      padding:8,
                                      paddingLeft:16,
                                      alignItems:"center",
                                     justifyContent: 'space-between'}}>
                            <Text>消息免打扰</Text>
                            <Switch
                                onValueChange={this.handleDoNotDisturb}
                                value={this.props.doNotDisturb} />
                        </View>
                    </View>

                    <TouchableHighlight underlayColor="lightcoral" style={styles.quit} onPress={this.handleQuit}>
                        <Text>退出</Text>
                    </TouchableHighlight>
                    
                </ScrollView>

                <Spinner visible={this.state.spinnerVisible} />

                <ActionSheet
                    ref={o => this.actionSheet = o}
                    title={"退出后不会在接受此群聊消息"}
                    options={BUTTONS}
                    cancelButtonIndex={1}
                    destructiveButtonIndex={0}
                    onPress={this.handleActionSheetPress}
                />                
            </View>
        );
    }

    handleDoNotDisturb(value) {
        console.log("do not disturb:", value);
        this.setState({spinnerVisible:true});
        var url = API_URL + "/client/groups/" + this.props.groupID + "/members/" + this.props.uid;

        var oldValue = this.props.doNotDisturb;
        this.props.dispatch(updateGroupDoNotDisturb(value));        
        fetch(url, {
            method:"PATCH",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + this.props.token,
            },
            body:JSON.stringify({do_not_disturb:value}),
        }).then((response) => {
            console.log("update group name status:", response.status);
            if (response.status == 200) {
                this.setState({spinnerVisible:false});
                GroupManager.updateGroupDoNotDisturb(this.props.groupID, value);
            } else {
                return response.json().then((responseJson)=>{
                    console.log(responseJson.meta.message);
                    this.setState({spinnerVisible:false});
                    this.props.dispatch(updateGroupDoNotDisturb(oldValue));
                });
            }
        }).catch((error) => {
            console.log("error:", error);
            this.setState({spinnerVisible:false});
            this.props.dispatch(updateGroupDoNotDisturb(oldValue));
        });
    }

    handleNotice(event) {
        this.props.navigator.push({
            title:"群公告",
            screen:"group.GroupNotice",
            navigatorStyle:{
                tabBarHidden:true
            },
            passProps:{
            },
        });
    }
    
    handleName(event) {
        this.props.navigator.push({
            title:"群聊名称",
            screen:"group.GroupName",
            navigatorStyle:{
                tabBarHidden:true
            },
            passProps:{
            },
        });
    }

    handleRemove(event) {
        this.props.navigator.push({
            title:"名称",
            screen:"group.GroupMemberRemove",
            navigatorStyle:{
                tabBarHidden:true
            },
            passProps:{
            },
        });
    }

    handleAdd(event) {
        var self = this;
        var users = this.props.contacts;

        var users = users.map((u) => {
            var index = this.props.members.findIndex((e) => {
                return e.uid == u.id;
            });

            var is_member = (index != -1);
            return Object.assign({}, u, {selected:false, is_member:is_member});
        });
   
        console.log("users:", users, "length:", users.length);

        self.props.navigator.push({
            title:"添加",
            screen:"group.GroupMemberAdd",
            navigatorStyle:{
                tabBarHidden:true
            },
            passProps:{
                users:users
            },
        });

    }

    handleClickMember(i, event) {
    
    }
    
    quitGroup() {
        console.log("remove member:", this.props.uid);
        let url = API_URL + "/client/groups/" + this.props.groupID + "/members/" + this.props.uid;
        
        this.setState({visible:true});
        fetch(url, {
            method:"DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + this.props.token,
            },
        }).then((response) => {
            console.log("status:", response.status);
            if (response.status == 200) {
                this.setState({visible:false});
                this.props.navigator.popToRoot();
            } else {
                return response.json().then((responseJson)=>{
                    console.log(responseJson);
                    this.setState({visible:false});
                });
            }
        }).catch((error) => {
            console.log("error:", error);
            this.setState({visible:false});
        });
    }

    handleQuit(event) {
        this.actionSheet.show();
    }

    handleActionSheetPress(buttonIndex) {
        console.log('button index:', buttonIndex);  
        if (buttonIndex == 0) {
            this.quitGroup();
        }
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgb(235, 235, 237)',
        marginTop: 10,
    },

    block: {
        backgroundColor: '#FFFFFF',
    },

    line: {
        alignSelf: 'stretch',
        height:1,
        backgroundColor: 'lightgray',
        marginLeft:10,
    },

    item: {
        paddingTop:12,
        paddingBottom:12,
        paddingLeft:16,
        alignSelf:'stretch',
    },
    itemInternal: {
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },

    headButton: {
        width:50,
        height:50,
        margin:4,
        borderRadius:4,
    },
    head: {
        width: 50,
        height: 50,
        borderRadius:4,
    },

    quit: {
        margin: 16,
        padding: 16,
        backgroundColor: 'orangered',
        alignSelf: 'stretch',
        alignItems: 'center',
    },

    notice: {
        marginTop:8,
        marginLeft:8,
        marginRight:10,
        color:'gray',
    }
});

export default connect(function(state) {
    return {
        topic:state.group.name,
        notice:state.group.notice,
        nickname:state.group.nickname,
        doNotDisturb:state.group.doNotDisturb,
        groupID:state.group.id,
        members:state.group.members,
        is_master:(state.group.master == state.profile.uid),
        token:state.profile.gobelieveToken,        
        uid:state.profile.uid,
        name:state.profile.name,
    };
})(GroupSetting);
