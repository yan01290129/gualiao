import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    TouchableHighlight,
    TextInput,
    Platform,
    NativeModules,    
    View
} from 'react-native';

import { connect } from 'react-redux';
import {updateGroupNotice} from "./actions";
import Spinner from 'react-native-loading-spinner-overlay';
import {API_URL, NAVIGATOR_STYLE} from './config';

var GroupManager = NativeModules.GroupModule;

class GroupNotice extends Component {
    static navigatorStyle = NAVIGATOR_STYLE;
    
    static navigatorButtons = {
        rightButtons: [
            {
                title: '确定', 
                id: 'setting', 
                showAsAction: 'ifRoom' 
            },
        ]
    };

    
    constructor(props) {
        super(props);
        this.state = {
            notice:this.props.notice,
            visible:false
        };

        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) {
        if (event.type == 'NavBarButtonPress') { 
            if (event.id == 'setting') {
                this.updateNotice();
            }     
        }
    }
    
    updateNotice() {
        if (this.state.notice == this.props.notice) {
            return;
        }
        console.log("update group notice...");
        this.setState({visible:true});
 
        var notice = this.state.notice;
        var url = API_URL + "/client/groups/" + this.props.groupID;
        fetch(url, {
            method:"PATCH",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "Bearer " + this.props.token,
            },
            body:JSON.stringify({notice:notice}),
        }).then((response) => {
            console.log("update group name status:", response.status);
            if (response.status == 200) {
                this.setState({visible:false});
                GroupManager.updateGroupNotice(this.props.groupID, this.state.notice);
                this.props.dispatch(updateGroupNotice(this.state.notice));
                this.props.navigator.pop();
            } else {
                return response.json().then((responseJson)=>{
                    console.log(responseJson.meta.message);
                    this.setState({visible:false});
                });
            }
        }).catch((error) => {
            console.log("error:", error);
            this.setState({visible:false});
        });
    }

    render() {
        return (
            <View style={{flex:1}}>
                <ScrollView style={{flex:1, backgroundColor:"#F5FCFF"}}>
                    <View style={{marginTop:12}}>
                        <Text style={{marginLeft:12, marginBottom:4}}>群公告</Text>
                        <TextInput
                            style={{paddingLeft:12, textAlignVertical:'top', fontSize:14, height: 140, backgroundColor:"white"}}
                            multiline={true}
                            editable={true}
                            placeholder=""
                            onChangeText={(text) => this.setState({notice:text})}
                            value={this.state.notice}/>
                    </View>
                </ScrollView>

                <Spinner visible={this.state.visible} />
            </View>
        );
    }
    
}



export default connect((state) => {
    return {
        notice:state.group.notice,
        groupID:state.group.id,
        token:state.profile.gobelieveToken
    };
})(GroupNotice);
