import {Platform} from 'react-native';

var API_URL = "http://120.236.26.29:8080";
var DEBUG = true;
var NAVIGATOR_STYLE = Platform.select({
    ios: {

    },
    android: {
        navBarBackgroundColor: '#212121',
        navBarTextColor: '#ffffff',
        navBarSubtitleTextColor: '#ff0000',
        navBarButtonColor: '#ffffff',
        statusBarTextColorScheme: 'light',
    }
});




module.exports.API_URL = API_URL;
module.exports.DEBUG = DEBUG;
module.exports.NAVIGATOR_STYLE = NAVIGATOR_STYLE;
