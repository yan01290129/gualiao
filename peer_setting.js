
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    ScrollView,
    TouchableHighlight,
    ActionSheetIOS,
    Platform,
    Switch,
    NativeModules,    
    View
} from 'react-native';

import { connect } from 'react-redux';
import ActionSheet from 'react-native-actionsheet';
import Spinner from 'react-native-loading-spinner-overlay';
import {NAVIGATOR_STYLE} from './config';

export default class PeerSetting extends Component {
    static navigatorStyle = NAVIGATOR_STYLE;
    
    constructor(props) {
        super(props);
        
        this.handleDoNotDisturb = this.handleDoNotDisturb.bind(this);
        this.handleActionSheetPress = this.handleActionSheetPress.bind(this);
        this.handleClear = this.handleClear.bind(this);
        
        this.state = {
            doNotDisturb:this.props.doNotDisturb
        };
    }
    
    componentDidMount() {

    }


    componentWillUnmount() {

    }

    

    render() {
        var BUTTONS = [
            '确定',
            '取消',
        ];

        return (
            <View style={{flex:1, backgroundColor:'rgb(235, 235, 237)'}}>
                <ScrollView style={styles.container}>
                    <View style={{marginTop:40, backgroundColor:'#FFFFFF'}}>
                        <View style={{flexDirection:'row',
                                      padding:8,
                                      paddingLeft:16,
                                      alignItems:"center",
                                      justifyContent: 'space-between'}}>
                            <Text>消息免打扰</Text>
                            <Switch
                                onValueChange={this.handleDoNotDisturb}
                                value={this.state.doNotDisturb} />
                        </View>

                        <View style={styles.line}/>
                        

                        <TouchableHighlight underlayColor="lightcoral" style={styles.item} onPress={this.handleClear}>
                            <Text>清空聊天记录</Text>
                        </TouchableHighlight>
                    </View>
                </ScrollView>

                <ActionSheet
                    ref={o => this.actionSheet = o}
                    options={BUTTONS}
                    cancelButtonIndex={1}
                    destructiveButtonIndex={0}
                    onPress={this.handleActionSheetPress}
                />                
            </View>
        );
    }

    handleDoNotDisturb(value) {
        this.setState({doNotDisturb:value});
    }


    handleClear(event) {
        this.actionSheet.show();
    }

    handleActionSheetPress(buttonIndex) {
        console.log('button index:', buttonIndex);  
        if (buttonIndex == 0) {
            console.log("clear...");
        }
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgb(235, 235, 237)',
    },

    block: {
        backgroundColor: '#FFFFFF',
    },

    line: {
        alignSelf: 'stretch',
        height:1,
        backgroundColor: 'gray',
        marginLeft:10,
    },

    item: {
        paddingVertical:16,
        paddingLeft:16,
        alignSelf:'stretch',
    },
    itemInternal: {
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },

    headButton: {
        width:50,
        height:50,
        margin:4,
        borderRadius:4,
    },
    head: {
        width: 50,
        height: 50,
        borderRadius:4,
    },


});
